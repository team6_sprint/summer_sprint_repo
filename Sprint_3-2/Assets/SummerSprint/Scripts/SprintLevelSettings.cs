﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using HappyFunTimes;

public class SprintLevelSettings : Singleton<SprintLevelSettings> {
	//Mix of default LevelSettings code and my own
    public Transform[] spawnPoints;
    public bool pressRed, punish;
    public int team1Score, team2Score;
    public int team1RoundsScore , team2RoundsScore;
    public float timeframe;

    Vector3 placetobe;
    Vector3 placetogo;

    float test;


    public Text timerText;
    public Text scoreT1Text;
    public Text scoreT2Text;
    public Text roundscoreT1Text;
    public Text roundscoreT2Text;

    //public Text redorblueText;

    public Image redButtonOn;
    public Image blueButtonOn;

    public int pullpower=1;

    public SpawnerPlacement Spawner;
    public PlaceInWorld placer;
    public MyHFTScript nicscript;
    public GameObject MasterObject;
    
    //[HideInInspector]
    public float timerCount;
    int timerdelta;
    int timerdelta2=30;
    //public int playerTotal;

    private IEnumerator coroutine;
    static private SprintLevelSettings s_settings;
    private GameServer server;

	//Checks if there's more than one object with this script
    void Awake()
    {
        if (s_settings != null)
        {
            throw new System.InvalidProgramException("there is more than one level settings object!");
        }
        s_settings = this;
        Spawner = GetComponent<SpawnerPlacement>();
        placer = GetComponent<PlaceInWorld>();
        nicscript = GetComponent<MyHFTScript>();
        
    }

    // Use this for initialization
    void Start () {
		//Scene defaults
        timerCount = 30f;
        team1Score = 1;
        team2Score = 1;
        scoreT1Text.text = "" + team1Score;
        scoreT2Text.text = "" + team2Score;
        roundscoreT1Text.text = "" + team1RoundsScore;
        roundscoreT2Text.text = "" + team2RoundsScore;
       



    }
	
	// Update is called once per frame
	void Update () {
        if (nicscript.Lindex > 0 && nicscript.Rindex > 0)
        {
            //MasterObject.transform.position = new Vector3(0, 0, 0);
            timeframe += Time.deltaTime;
            // placetobe = MasterObject.transform.position;
            if (MasterObject.transform.position != placetobe)
            {
                MasterObject.transform.position = Vector3.Lerp(placetogo, placetobe, timeframe * 2);
            }
            // playerTotal = server.GetNumPlayers();
            //Sets the timer to countdown
            if (timerCount > 0)
            {
                timerCount = timerCount - Time.deltaTime;
                timerText.text = "" + (int)timerCount;

            }
            else
            {
                //display winner
                coroutine = EndMatch(5.0f);
                StartCoroutine(coroutine);
            }
            timerdelta = (int)timerCount;

            if (timerdelta == timerdelta2 - 2)
            {
                timeframe = 0;
                RedOrBlue();
                if (team1Score / team2Score > .5f)
                {
                   pullpower = 2;
                    //pullpower += team1Score / team2Score;

                }
                if (team2Score / team1Score > .5f)
                {
                    pullpower = 2;
                   // pullpower += team2Score / team1Score;
                }


                if (team1Score / nicscript.Rindex > team2Score / nicscript.Lindex)
                {
                    placetogo += new Vector3(-pullpower, 0, 0);
                    team1RoundsScore+=pullpower;
                    team2RoundsScore-=pullpower;
                    //placetogo = MasterObject.transform.position;
                    placetobe = placetogo + new Vector3(-pullpower, 0, 0);
                    pullpower = 1;
                    timeframe = 0;
                    

                }
                if (team2Score / nicscript.Lindex > team1Score / nicscript.Rindex)
                {
                    placetogo += new Vector3(pullpower, 0, 0);
                    team2RoundsScore +=pullpower;
                    team1RoundsScore-=pullpower;
                    //placetogo = MasterObject.transform.position;
                    placetobe = placetogo + new Vector3(pullpower, 0, 0);
                    pullpower = 1;
                    timeframe = 0;
                }

                timerdelta2 = timerdelta;
                team2Score = 1;
                team1Score = 1;
            }
            //Updates the Team scores
            if (team1Score == 1)
            {
                scoreT1Text.text = "RED TEAM" + "0";
            }
            else
            {
                scoreT1Text.text = "RED TEAM" + (team1Score - 1) / nicscript.Rindex;
            }
            if (team2Score == 1)
            {
                scoreT2Text.text = "BLUE TEAM" + "0";
            }
            else
            {
                scoreT2Text.text = "BLUE TEAM " + (team2Score - 1) / nicscript.Lindex;
            }
            roundscoreT1Text.text = "" + team1RoundsScore;
            roundscoreT2Text.text = "" + team2RoundsScore;
        }
    }

	//Plays at the end of a match, prints winner to console, waits 5 seconds, wipes the scene and reloads it
    private IEnumerator EndMatch(float waitTime)
    {
        while (true)
        {
            if (team1RoundsScore > team2RoundsScore)
            {
                print("Team 1 Wins!");
            }else if (team1Score == team2Score)
            {
                print("Draw!");
            }else
            {
                print("Team 2 Wins!");
            }
            yield return new WaitForSeconds(waitTime);
            Cleanup();
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }

	//Returns itself for some reason, more HFT code
    public static SprintLevelSettings settings
    {
        get
        {
            return s_settings;
        }
    }

	//HFT Stuff that wipes the slate clean
    void Cleanup()
    {
        s_settings = null;
    }

    void OnDestroy()
    {
        Cleanup();
    }

    void OnApplicationExit()
    {
        Cleanup();
    }
void RedOrBlue()
    {
        float randV;
        randV = Random.Range(0, 100);
        if (randV > 50)
        {
            pressRed = true;
            //redorblueText.text = "RED";
            //redorblueText.color = Color.red;
            redButtonOn.enabled=true;
            blueButtonOn.enabled = false;
        }
        else
        {
            pressRed = false;
            //redorblueText.text = "BLUE";
            //redorblueText.color = Color.blue;
            redButtonOn.enabled = false;
            blueButtonOn.enabled = true;
        }
        print(randV);
            }

}
