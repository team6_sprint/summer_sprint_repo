//Maya ASCII 2017 scene
//Name: NewTugAnim.ma
//Last modified: Fri, Jun 23, 2017 12:39:28 AM
//Codeset: 1252
requires maya "2017";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" "mtoa" "1.3.0.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201606150345-997974";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "74EF73B5-4CFD-6702-88AB-059A59D01FFA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 129.4944511110667 42.025297610373499 10.1217910501936 ;
	setAttr ".r" -type "double3" 344.20000000847784 3323.6000000021759 0 ;
	setAttr ".rp" -type "double3" -1.4210854715202004e-014 3.5527136788005009e-015 -7.1054273576010019e-015 ;
	setAttr ".rpt" -type "double3" -2.4970936981127035e-014 -4.1466812520224172e-015 
		2.2565134851202188e-014 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "18BD744D-457F-354E-D144-AE88ED85BF5E";
	setAttr -k off ".v" no;
	setAttr ".ovr" 1.3;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 138.95978087620503;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -14.278892472723633 28.725627747045028 52.897188818153651 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".dr" yes;
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "E92B3FCD-4956-6DE2-F86A-42999B233448";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -15.45092703121527 1001.3484027935623 47.65460349705041 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "616E1DD4-49C0-87BF-37B2-049242CCD14E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 969.67526515033239;
	setAttr ".ow" 28.674303583974453;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -15.45092703121527 31.673137643229683 47.654603497050196 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "39FC9EAE-4CBC-1F80-A622-B1B15DB79D47";
	setAttr ".t" -type "double3" -15.45092703121527 31.673137643229683 1002.5300884103677 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "9BF16889-4F6A-3508-892A-5689A41CFF8A";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 954.87548491331756;
	setAttr ".ow" 10.324574101920037;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" -15.45092703121527 31.673137643229683 47.654603497050196 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "85FF8658-4D5D-6E70-4754-608BC93B7E67";
	setAttr ".t" -type "double3" 1007.6049593284464 -3.6878780840037209 -3.4219880130951026 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "36D20B4A-49AB-5B75-1341-9DA49FCCAC48";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1014.7909671372538;
	setAttr ".ow" 117.43471562944865;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" -7.1860078088075845 -24.852709184507901 -34.922800829854154 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode joint -n "Corgi_Root";
	rename -uid "F44A96B1-422C-8C24-5C3C-80B267F1D2D3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -13.559594067844019 -31.239012503314999 -0.062433451202958734 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 90.175215904905343 7.0166764886325894e-015 -180 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 -3.745079733147397e-019 0.0030580896759415666 0.99999532403283464 0
		 -1.2246410727391313e-016 0.99999532403283464 -0.0030580896759415666 0 -13.559594067844019 -31.239012503314999 -0.062433451202958734 1;
	setAttr ".radi" 2;
	setAttr ".liw" yes;
createNode joint -n "Corgi_center" -p "Corgi_Root";
	rename -uid "2B69E7D9-42E8-807F-1906-B2ADCA97FC8F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -5.7438341183480568e-015 -0.0062770152073854946 29.309480043945232 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 90.241473326888226 0 0 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 -1.2246144130787183e-016 0.99997355476807082 -0.0072725349437677026 0
		 8.9062866405723834e-019 -0.0072725349437677026 -0.99997355476807082 0 -13.559594067844019 0.12290486546375945 -0.1583414553888316 1;
	setAttr ".radi" 1.8125206200680142;
createNode joint -n "Corgi_Hips" -p "Corgi_center";
	rename -uid "6EF721F8-486F-9F2D-05AC-AF9BC1692853";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.1688295699958685e-014 -1.6424575253205046e-015 26.375398654648283 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 16.65689440674123 91.349706947087896 17.979340187674723 ;
	setAttr ".bps" -type "matrix" 0.022404427953783834 -1.420218109782212e-014 0.99974898930084655 0
		 0.023154628266791731 0.99973176099206829 -0.0005188964493463245 0 -0.9994808176237755 0.023160441785476691 0.022398418212256611 0
		 -13.559594067844031 -0.068911142907975481 -26.53304260650247 1;
	setAttr ".radi" 2;
createNode joint -n "Corgi_Leg_R" -p "Corgi_Hips";
	rename -uid "2555570D-4510-A318-0E36-A5A2773529B1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -1.5543122344752199e-015 1.6682326168187062e-014 6.5453076270635853 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 55.170507394530183 -81.666634903860825 -9.0999290129541173 ;
	setAttr ".bps" -type "matrix" -0.98625234986514732 -1.4536982728685643e-015 0.16524618720404996 0
		 -0.11881830684130343 0.6949706398071912 -0.70915302986404138 0 -0.11484124844689633 -0.7190381142929656 -0.6854164265971272 0
		 -20.101503486540679 0.082681073356683954 -26.38643806894363 1;
	setAttr ".radi" 1.4832214194213145;
createNode joint -n "joint2" -p "Corgi_Leg_R";
	rename -uid "EBC63E2E-406B-F716-58F8-21B476310060";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.5771785916527938 -7.0432105892672938 9.1846773058381679 ;
	setAttr ".bps" -type "matrix" -0.98625234986514732 -1.4536982728685643e-015 0.16524618720404996 0
		 -0.11881830684130343 0.6949706398071912 -0.70915302986404138 0 -0.11484124844689633 -0.7190381142929656 -0.6854164265971272 0
		 -21.874917030156102 -11.416276546542468 -27.426429889199078 1;
	setAttr ".radi" 1.4832214194213145;
createNode joint -n "Corgi_Shin_R" -p "joint2";
	rename -uid "113CDAF2-4952-B428-6A4C-A581824FCF37";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.5771785916527892 7.0432105892672814 11.529797540308335 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 37.760722378564758 -6.594468590250731 6.8695755759605577 ;
	setAttr ".bps" -type "matrix" -1.0000000000000004 -1.4849232954361469e-015 -4.3298697960381105e-015 0
		 4.2188474935755949e-015 0.10223554176714028 -0.99476021934915559 0 1.9984014443252818e-015 -0.99476021934915548 -0.10223554176714039 0
		 -22.480379638793977 -14.811815858585577 -40.584479396825429 1;
	setAttr ".radi" 0.95129778661351927;
createNode joint -n "Corgi_Foot_R" -p "Corgi_Shin_R";
	rename -uid "FE4313F2-4FBC-F62F-D260-9C897E7F47F1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.7639915219954499e-016 2.125621499848794e-015 11.203647653956066 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" -76.881675871062129 0 179.99999980622516 ;
	setAttr ".bps" -type "matrix" 1.0000000000000004 3.4576300910136589e-010 -3.3642837023419653e-009 0
		 7.6758729634275761e-010 0.9455967265907077 0.32534109894223184 0 3.2937465296284877e-009 -0.32534109894223173 0.94559672659070781 0
		 -22.480379638793956 -25.956758856345566 -41.729890384495782 1;
	setAttr ".radi" 0.92577305907807284;
createNode joint -n "Corgi_Toes_R" -p "Corgi_Foot_R";
	rename -uid "81CC3D52-4B64-D3F7-B777-B39C9AC3C750";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -3.1200668628256211e-008 3.2257113264326829e-015 9.4726876600419665 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -18.986241639901326 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000004 3.4576300910136589e-010 -3.3642837023419653e-009 0
		 -3.4576308079217353e-010 1.0000000000000002 -6.6613381477509392e-016 0 3.364283631162449e-009 7.2164496600635175e-016 1.0000000000000004 0
		 -22.480379638793991 -29.038613469600133 -32.772547941143905 1;
	setAttr ".radi" 0.92577305907807284;
createNode joint -n "Corgi_Leg_L" -p "Corgi_Hips";
	rename -uid "BAAC6A23-49D6-2527-C462-4EB4F7940D4F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0.21933664359010335 0.27145783837047865 -6.1770854123746943 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 39.537433761278471 258.08052406546807 6.3008327792122056 ;
	setAttr ".bps" -type "matrix" -0.98305508367291661 -8.7083118494035716e-016 -0.18331039922720022 0
		 0.13180902621271628 0.6949601278684745 -0.70686406138796565 0 0.12739341848655683 -0.71904827422992534 -0.68318408665108399 0
		 -7.3745160719853606 0.059409852784018918 -26.452258819664298 1;
	setAttr ".radi" 1.4707820054797873;
createNode joint -n "joint1" -p "Corgi_Leg_L";
	rename -uid "E593FC82-468E-894A-0269-73832973D5EB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.6662293892799274 -7.1527090687181216 9.1484934221919083 ;
	setAttr ".bps" -type "matrix" -0.98305508367291661 -8.7083118494035716e-016 -0.18331039922720022 0
		 0.13180902621271628 0.6949601278684745 -0.70686406138796565 0 0.12739341848655683 -0.71904827422992534 -0.68318408665108399 0
		 -5.5138545663643033 -11.489646163249239 -27.340933785744305 1;
	setAttr ".radi" 1.4707820054797873;
createNode joint -n "Corgi_Shin_L" -p "joint1";
	rename -uid "A9F889E9-48CF-67C9-3FD2-7BAB78753410";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 1.6662293892799327 7.1527090687181181 11.392314718181581 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 37.666719916069326 7.3189937741879509 -7.6367301229394444 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 -1.3045120539345589e-015 -1.1976530878143876e-014 0
		 1.1837753000065732e-014 0.10224000072939654 -0.99475976107342334 0 2.581268532253489e-015 -0.9947597610734229 -0.1022400007293966 0
		 -4.7577523045066936 -14.710422791839544 -40.48541206812051 1;
	setAttr ".radi" 0.98021171958694708;
createNode joint -n "Corgi_Foot_L" -p "Corgi_Shin_L";
	rename -uid "243138CF-4B6D-B837-E55F-2883297DC127";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -2.5061417847857109e-015 7.2867459224140048e-016 11.16739310594264 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" -75.65543950203211 -4.5198400492033624e-029 179.99999999999943 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 2.3159138279560917e-015 2.1359422641258192e-015 0
		 -2.9827526407559474e-015 0.93841625194685641 0.34550678442255622 0 -1.2451610386481181e-015 -0.345506784422556 0.93841625194685685 0
		 -4.7577523045066625 -25.819296089720034 -41.627166347417543 1;
	setAttr ".radi" 0.96962777912718567;
createNode joint -n "Corgi_Toes_L" -p "Corgi_Foot_L";
	rename -uid "A646240B-4CB7-9682-2B08-D79CE01F91BF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 3.1194547900250197e-014 -1.1102230246251565e-015 9.5067329929166426 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -20.212734834417386 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 2.3159138279560917e-015 2.1359422641258192e-015 0
		 -2.3688519670712327e-015 1.0000000000000002 4.6074255521943996e-015 0 -2.1990406285938848e-015 -4.5519144009631418e-015 1.0000000000000007 0
		 -4.757752304506643 -29.103936836466485 -32.705893603945185 1;
	setAttr ".radi" 0.96962777912718567;
createNode joint -n "Corgi_Shoulders" -p "Corgi_center";
	rename -uid "E711B30E-4AC0-3FE4-A0BF-24B88F87A24A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -1.0451493343957269e-014 9.0549710440854536 -19.234855950469605 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 76.760542466541096 90.428205521617713 76.681547633405884 ;
	setAttr ".bps" -type "matrix" 0.0017216250816360912 6.695165255532487e-015 0.99999851800244111 0
		 -0.0013724570887204738 0.99999905817753487 2.3628600424683427e-006 0 -0.99999757618137153 -0.001372459122701532 0.0017216234601709229 0
		 -13.55959406784401 9.3175226107779672 19.010153231521119 1;
	setAttr ".radi" 2;
createNode joint -n "Corgi_Arm_R" -p "Corgi_Shoulders";
	rename -uid "C91DE243-4BC1-2FA4-E2D3-259D16B20A82";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 11.154262074143805 -7.5497436675905973 6.6904345992779337 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 68.869416703155323 65.13819352531732 -0.16970354174686375 ;
	setAttr ".bps" -type "matrix" 0.90804780778936878 4.8312048805954078e-016 0.41886654052206396 0
		 -0.39119331644330446 0.35744791278676696 0.84805588189368342 0 -0.14972297064582504 -0.93393307557039862 0.32457979360490957 0
		 -20.220447294153342 -3.7283327069414991 33.518446204521524 1;
	setAttr ".radi" 1.2977107565852963;
createNode joint -n "Corgi_Forearm_R" -p "Corgi_Arm_R";
	rename -uid "75EC380B-4F8E-3A59-EF68-EBA4DFA9E8F0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -4.1042472084696673 -8.9661510474011994 14.423450367210014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" -9.3111033113503296 8.6108726490537126 -23.306713934769363 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 3.0531133177191805e-016 -3.8233305410528828e-015 0
		 2.9802549317281546e-015 0.47678331984112371 0.87902085636307692 0 2.0539125955565396e-015 -0.8790208563630767 0.47678331984112399 0
		 -22.599323446406657 -14.916865633704843 25.534530925157831 1;
	setAttr ".radi" 1.0144329916162156;
createNode joint -n "Corgi_Hand_R" -p "Corgi_Forearm_R";
	rename -uid "DC6AAE24-47CD-13F3-3547-4C9DFCE2D4FD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -3.1519695106906887e-015 1.2239713727797744e-015 13.006138509181579 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" -57.784283854116417 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 3.0531133177191805e-016 -3.8233305410528828e-015 0
		 -1.4890768671904103e-016 0.99787010592873049 0.065232290269345905 0 3.6163936330426993e-015 -0.065232290269346072 0.99787010592873082 0
		 -22.599323446406633 -26.349532644022425 31.735640821878913 1;
	setAttr ".radi" 0.93695467320086812;
createNode joint -n "Corgi_Fingers_R" -p "Corgi_Hand_R";
	rename -uid "7B2370C1-4F3C-F6F1-60CA-9EA67E120CB2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.2738905830479637e-015 -8.1350188384908484e-016 9.6547471238717115 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.740190700978828 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 3.0531133177191805e-016 -3.8233305410528828e-015 0
		 -3.8449616831878756e-016 1 -4.163336342344337e-017 0 3.5989775082409116e-015 -1.3877787807814457e-016 1.0000000000000004 0
		 -22.599323446406601 -26.979333910883959 41.369824357091886 1;
	setAttr ".radi" 0.93695467320086812;
createNode joint -n "Corgi_Arm_L" -p "Corgi_Shoulders";
	rename -uid "F18E8816-434A-1EC1-3000-5FA2AFCBCC95";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 11.184014153513541 -7.5837415288733485 -8.7558177556030756 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 32.418656857965701 89.873849848202553 -38.561396154527493 ;
	setAttr ".bps" -type "matrix" 1 1.708702623837155e-016 -1.3333518317226734e-015 0
		 1.1403638450202536e-015 0.32589619120077779 0.94540555983177199 0 6.6439909129911712e-016 -0.94540555983177199 0.32589619120077773 0
		 -4.7741344956547369 -3.7411311862482943 33.521605529040684 1;
	setAttr ".radi" 1.3221019494898871;
createNode joint -n "Corgi_Forearm_L" -p "Corgi_Arm_L";
	rename -uid "4CD3D328-4AB9-82CF-6C8F-C8B0A46DC44D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -3.5899609040157606e-015 -9.8620783329198627 14.20066312694167 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" -9.1101749725956438 0 0 ;
	setAttr ".bps" -type "matrix" 1 1.708702623837155e-016 -1.3333518317226734e-015 0
		 1.0207823804151753e-015 0.47147453247360493 0.88187967729662287 0 8.3657586827639122e-016 -0.88187967729662287 0.47147453247360488 0
		 -4.774134495654736 -14.893594413132773 25.483337007928867 1;
	setAttr ".radi" 1.0821086622079408;
createNode joint -n "Corgi_Hand_L" -p "Corgi_Forearm_L";
	rename -uid "E5A501CD-4E92-B5B6-BDFE-BD849C579140";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 9.0111742976083552e-016 5.6621374255882984e-015 12.923075113121305 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" -57.994737217542976 0 0 ;
	setAttr ".bps" -type "matrix" 1 1.708702623837155e-016 -1.3333518317226734e-015 0
		 -1.6840408917554814e-016 0.99771361518459678 0.06758359324038915 0 1.3090056968162605e-015 -0.067583593240389095 0.99771361518459678 0
		 -4.7741344956547245 -26.290191723572203 31.576237805009018 1;
	setAttr ".radi" 0.97495801814695249;
createNode joint -n "Corgi_Fingers_L" -p "Corgi_Hand_L";
	rename -uid "CDF2887E-4163-6EF2-84BB-BA8FD0FB23E4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 2.6758688460888269e-015 -4.1217029789208937e-015 9.9066599305408491 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.8752085133779479 0 0 ;
	setAttr ".bps" -type "matrix" 1 1.708702623837155e-016 -1.3333518317226734e-015 0
		 -2.5648636118590561e-016 1 2.1788126858268697e-013 0 1.2946314526049809e-015 -2.1782575743145571e-013 1 0
		 -4.7741344956547085 -26.95971939868874 41.460247298713313 1;
	setAttr ".radi" 0.97495801814695249;
createNode joint -n "Corgi_head" -p "Corgi_Shoulders";
	rename -uid "F76158FC-4A19-0AE4-1513-828F4B4105F9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 11.426141714842744 16.604646740479271 -0.0031176405186609021 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" -93.1603669141018 89.873849848202553 -38.561396154518029 ;
	setAttr ".bps" -type "matrix" 1 5.5966516143701739e-016 -1.111307226797642e-015 0
		 -1.2290515827295678e-015 0.57929677791156131 -0.81511670520317736 0 2.3460779759870709e-016 0.81511670520317736 0.57929677791156131 0
		 -13.559594067844012 25.922157991462164 30.436311879902981 1;
	setAttr ".radi" 1.7794610882368855;
createNode joint -n "Corgi_Hat" -p "Corgi_head";
	rename -uid "5FC403F5-4D12-E61A-C2EA-0F8A211C2025";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -3.0429151716230374e-015 -1.0470273515473241e-012 18.708207755051525 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000028 2.157771069498188 90.000000000000028 ;
	setAttr ".bps" -type "matrix" -1.6811026094186355e-015 0.54819581043568211 -0.83635001848554169 0
		 6.3225525618141841e-016 0.83635001848554169 0.54819581043568211 0 1 4.5494007625918435e-016 -1.7305512087183399e-015 0
		 -13.55959406784401 41.171530657015687 41.273916352905268 1;
	setAttr ".radi" 1.1541170632156401;
createNode joint -n "Corgi_Ear_R" -p "Corgi_head";
	rename -uid "72029AC0-4CDC-6F80-E77C-0C95F750E467";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -7.146691005129262 8.7916972783649356 13.109675265037701 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 32.491009581091355 -35.064317925866732 -99.030988805128302 ;
	setAttr ".bps" -type "matrix" -0.12848003334551047 -4.0634162701280729e-014 0.99171209583806985 0
		 0.88146023125097794 0.45824340968204541 0.11419648946419389 0 -0.45444553221976897 0.88882674210611667 -0.058875128556273804 0
		 -20.70628507297328 41.701075205513931 30.864445201721654 1;
	setAttr ".radi" 0.58584191753767545;
createNode joint -n "Corgi_Eartip_R" -p "Corgi_Ear_R";
	rename -uid "F8656EEB-411C-35D0-D9C2-AEB74D1E386F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -9.3476941858463187e-015 -2.3906918663683836e-015 9.7740427788075124 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 93.789687670090998 27.029264471155425 98.292928262257618 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 2.9087843245179101e-014 4.3533232574333169e-013 0
		 -2.9087843245179101e-014 1.0000000000000002 3.6345926268666062e-014 0 -4.351900784183016e-013 -3.6463887465032485e-014 1.0000000000000004 0
		 -25.148055145527252 50.388505805807227 30.288997176604834 1;
	setAttr ".radi" 0.52764800346416674;
createNode joint -n "Corgi_Ear_L" -p "Corgi_head";
	rename -uid "16371368-4704-E327-67D1-17BA4CACA62A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 7.2306454077744071 8.7843195399833167 13.092672073809931 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 29.926422504701048 35.032232311295772 99.450472079997994 ;
	setAttr ".bps" -type "matrix" -0.13444764643663201 4.0245584642661925e-014 -0.9909206983243668 0
		 -0.90192643795570204 0.41419024876872279 0.12237294775171005 0 0.41042969054905082 0.91019033054900333 -0.055686904123921521 0
		 -6.3289486600696128 41.682941720229678 30.860609025630652 1;
	setAttr ".radi" 0.58584191753767545;
createNode joint -n "Corgi_Eartip_L" -p "Corgi_Ear_L";
	rename -uid "8EE0E859-49E1-5583-1CE7-2A8BD389ECC2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 2.3094521886690241e-015 2.2923222153842519e-015 9.6033920652487019 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 7.7266811809399991 65.531667061271847 -90.000000000000099 ;
	setAttr ".bps" -type "matrix" -2.9143354396410359e-014 -1.0000000000000002 -3.602673714908633e-014 0
		 -4.9619336417450199e-013 3.6012859361278515e-014 -1 0 1 -2.8976820942716586e-014 -4.9599213625128868e-013 0
		 -2.38743142650838 50.423856318490074 30.325825852428718 1;
	setAttr ".radi" 0.52764800346416674;
createNode joint -n "Corgi_lowerJaw" -p "Corgi_head";
	rename -uid "DC12EF99-4F98-69B1-307A-8390553433CE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 9.2038368447298817e-016 -6.2112297915028938 8.3169022721700081 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 45.526430612847257 0 0 ;
	setAttr ".bps" -type "matrix" 1 5.5966516143701739e-016 -1.111307226797642e-015 0
		 -6.9363920737112075e-016 0.98748967924184261 -0.15768364972578136 0 1.0413804596312218e-015 0.15768364972578136 0.98748967924184261 0
		 -13.559594067844001 29.103258563964275 40.317143731286066 1;
	setAttr ".radi" 0.5;
	setAttr ".liw" yes;
createNode joint -n "Corgi_Jawtip" -p "Corgi_lowerJaw";
	rename -uid "670014D5-41A9-6D2E-924C-A9935C1DAF89";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.9627649019491387e-015 -6.6917625036924008e-015 16.091346078720633 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000142 46.79605288447874 90.000000000000057 ;
	setAttr ".bps" -type "matrix" -2.1221265367191636e-015 0.56109379366851597 -0.82775223026378675 0
		 1.7616300007407779e-015 0.82775223026378675 0.56109379366851586 0 1 -3.2342084637567006e-016 -2.7691560621631794e-015 0
		 -13.559594067843987 31.640600742657579 56.207181909131386 1;
	setAttr ".radi" 1.3412400121110009;
	setAttr ".liw" yes;
createNode joint -n "Corgi_Tongue_root" -p "Corgi_head";
	rename -uid "3411A70C-4B44-468C-D964-EEB18D1FCEE2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -0.16200916777881708 -2.7549576593474003 9.7149617094170182 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 52.989064350514738 0 0 ;
	setAttr ".bps" -type "matrix" 1 5.5966516143701739e-016 -1.111307226797642e-015 0
		 -5.5250987099540747e-016 0.99960530565396222 -0.028093289384640896 0 1.1226493014992751e-015 0.028093289384640896 0.99960530565396222 0
		 -13.721603235622823 32.245047475874465 38.309769905963961 1;
	setAttr ".radi" 0.79707570938518835;
	setAttr ".liw" yes;
createNode joint -n "Corgi_tongue1" -p "Corgi_Tongue_root";
	rename -uid "EE7FDBAE-4964-6868-877B-DF89ED99BDE2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 5.6181278345831304e-015 -6.2966483662331905e-015 4.6152666471701664 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" -0.016767757843148118 1.2873141624809881 0.036185260444047587 ;
	setAttr ".bps" -type "matrix" 0.99974740863347267 -1.8998789087093515e-013 -0.022474851070835203 0
		 -0.00063812659218357077 0.99959683984874614 -0.028385745698375191 0 0.022465790106483485 0.028392917504203006 0.9993443503169922 0
		 -13.721603235622812 32.374705497380688 42.923214933483031 1;
	setAttr ".radi" 0.80378094240462472;
	setAttr ".liw" yes;
createNode joint -n "Corgi_tongue2" -p "Corgi_tongue1";
	rename -uid "A1EE4E4A-4EB0-95BB-1A8F-97A8ED9583AC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 1.6244874364002338e-014 -2.4031876522595784e-015 4.5665621184235077 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 2.49870289649055 -24.391005819272067 -0.73795665350700679 ;
	setAttr ".bps" -type "matrix" 0.91972792027426242 -1.6299461780278079e-014 0.39255643246287536 0
		 -0.0048794193930970543 0.99992274637276568 0.011432084356431494 0 -0.39252610605457344 -0.012429854638985029 0.91965686795635238 0
		 -13.619011809562075 32.504363518886905 47.486782986901162 1;
	setAttr ".radi" 0.59250350962979403;
	setAttr ".liw" yes;
createNode joint -n "Corgi_tongue3" -p "Corgi_tongue2";
	rename -uid "9FDFC043-4014-D8BE-2581-8EACEC7D5953";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 4.4090076216436444e-015 6.6196266887466204e-016 3.1526075975036725 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" -12.146637578361963 -8.2456493073427115 0.10321374086373358 ;
	setAttr ".bps" -type "matrix" 0.85391475219052426 -2.593086335933581e-014 0.52041290913215787 0
		 0.10310383169003855 0.98017791765591689 -0.16917697724732902 0 -0.51009724159441183 0.1981192815949803 0.83698838365777894 0
		 -14.856492593728252 32.465177064716173 50.386100215916791 1;
	setAttr ".radi" 0.5;
	setAttr ".liw" yes;
createNode joint -n "Corgi_tongue4" -p "Corgi_tongue3";
	rename -uid "E827758C-4C10-64B2-F067-52A984CFD3AB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 5.1188681142563042e-016 -1.8331261410719564e-015 4.6106665063842707 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "zyx";
	setAttr ".jo" -type "double3" 65.780334216926946 -22.162630572062401 -4.7226380137971891 ;
	setAttr ".bps" -type "matrix" 0.58785084996131087 -3.334832410217814e-014 0.80896933081530631 0
		 -0.64968936864240701 0.59583410469289988 0.47210744958943529 0 -0.48201151695036804 -0.80310753930268319 0.35026158487963621 0
		 -17.208380860546612 33.378639000635062 54.245174522680422 1;
	setAttr ".radi" 0.58183984105061348;
	setAttr ".liw" yes;
createNode joint -n "Corgi_Tongue5" -p "Corgi_tongue4";
	rename -uid "C0E31EDF-43FD-17EC-D2EC-D68D5E693DE7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0.091108133005783284 -0.033998000844557022 3.9308344835626743 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -66.436402422215423 28.816859967634162 -47.860627548972552 ;
	setAttr ".bps" -type "matrix" 1 -1.6264767310758543e-013 -1.2548295735825832e-013 0
		 1.6286971771251046e-013 1.0000000000000002 -7.7993167479917247e-014 0 1.2551071293387395e-013 7.8104189782379763e-014 1.0000000000000002 0
		 -19.027442219719831 30.201499022740339 55.679647814712268 1;
	setAttr ".radi" 0.63353204307400768;
	setAttr ".liw" yes;
createNode transform -n "group2";
	rename -uid "80F3BE54-4D35-56B3-1926-20B6D46A3A62";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -13.49674672861601 9.8675239266974266 5.3748475342354602 ;
	setAttr ".sp" -type "double3" -13.49674672861601 9.8675239266974266 5.3748475342354602 ;
createNode mesh -n "group2Shape" -p "group2";
	rename -uid "072DEEC5-46AD-E031-BA74-7FAFDD507307";
	setAttr -k off ".v";
	setAttr -s 11 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".clst[0].clsn" -type "string" "Colors";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "group2ShapeOrig" -p "group2";
	rename -uid "E3DE0982-4167-BA24-92BC-A09A6B9E33DA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 1734 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.25 0.5 0.33333334 0.66666669
		 0.375 0.5 0.2992844 0.51866782 0.625 0.5 0.66666663 0.66666669 0.75 0.5 0.41666672
		 0.66666663 0.58333331 0.66666669 0.5 0.5 0.41666666 0.83333331 0.58333337 0.83333331
		 0.5 0.66666669 0.45833334 0.83333343 0.54166669 0.83333343 0.5 0.83333343 0.46470964
		 0.92941928 0.53529042 0.92941928 0.5 0.92941934 0.4819352 0.92774075 0.51360053 0.94559783
		 0.5 1 0.25 0.5 0.29701912 0.5202499 0.375 0.5 0.33333334 0.66666669 0.75 0.5 0.66666663
		 0.66666669 0.625 0.5 0.41666672 0.66666663 0.58333331 0.66666669 0.41666666 0.83333331
		 0.5 0.5 0.58333337 0.83333331 0.45833334 0.83333343 0.5 0.66666669 0.54166669 0.83333343
		 0.46470964 0.92941928 0.5 0.83333343 0.53529042 0.92941928 0.4819352 0.92774075 0.5
		 0.92941934 0.51360053 0.94559783 0.5 1 0 1 0.5 1 0 0 0 1 0 0 0 1 0.5 0 0.75 0.75
		 0 1 0 0 0 0 1 0.5 1 0.5 0.75 1 0.625 0.625 0 1 0 1 1 0 1 1 0.75 0.25 1 1 0.5625 0.69312614
		 0.75 0.25 0.25 0 0 0 0.16499999 0.14812499 1 0 1 1 0 0.5 1 1 0.25 0.25 0 1 0.375
		 0.38625216 0.42161328 0.46999514 0.35009158 1 0.5 0 1 1 0 1 0 1 0.24731848 0.76493841
		 0.35009158 1 1 1 0 1 0 1 1 0 0.85163218 0.55489641 0.5 0.76125216 0 0 1 0 0 1 0 0
		 0 0 1 0 0.26716805 0.81033808 1 1 1 0 0.42581609 0.60613322 1 0 1 1 0 0 0 0.52250433
		 0 1 1 0 1 1 1 1 0.1496558 0.54156536 0 0 0 1 0.5 1 1 0 0 0 0 1 0.125 0.075000003
		 0.35009158 1 0 1 1 0 0 0 0.91968429 0.75905287 0 0 1 1 0 1 0 1 0 0.20845805 0 0 1
		 0 0 1 0 0 0 0 1 0 0 1 0.32120678 0.33322319 1 1 1 0 0 0 1 1 1 0 0 1 0.28701758 0.85573775
		 0.17049323 0.5892244 0.61423075 1 1 1 0 1 1 0 0 1 0 0 0 1 0 0.65737009 0.45984215
		 0.77564436 1 0 1 0 1 1 0 0 0 0 1 0 1 1 0 0.4169161 0 1 1 0 1 1 1 1 1 0 1 1 0 0 1
		 1 0.33843052 0.049818516 0 0 0 1 0.5 0 1 1 1 0 1 1 0 0 0 0 0 1 0.35009158 1 1 0 0
		 1 0 0.19927406 1 1 0 0 0 1 0 0 0.5 1 0 0 1 0 0 1 0 0 1 0 0 1 0 1 0 1 0 0.15779243
		 0 0 0 1 0.5 1 1 1 1 0 1 0 1 1 0.32515714 0.94297022 0.35009158 1 1 0 0.19133066 0.63688368
		 0.043382406 0.29849789 1 1 0 1 0 1 0 0 0 0.54901361 0 0.79223585 1 1 1 1 1 1 0 0.31558487
		 1 0 1 1 0 0 1 0 1 1 0 0 0.45124069 0 0 0.25 0 0 0 0 1 0.5 0.5 0 0.5 1 1 0 1 1 1 0
		 1 1 1 1 0 0 0 0 0.64093828 1 0 1 0.32515708 0.9429701 0.32515714 0.94297022 0.35009158
		 1 0.16499999 0.14812499 0.22562037 0.099637032 1 0 0 0 0 1 0 1 0 0;
	setAttr ".uvst[0].uvsp[250:499]" 0 0.25 0 1 0.5 1 0 0 1 1 0 1 0 0 1 1 0 1 0
		 0.1577924 0 1 0 1 0 0.25 1 1 1 0 1 0 1 0 0 1 0 1 1 1 0 1 0.23137283 0.7284677 0.28701758
		 0.85573775 1 0 0.086764812 0.39772168 0.19813687 0.19069549 1 1 0 0 0 0 0 0.25 0
		 0 0 0.6811111 0 0.44501767 0 1 1 1 1 0 1 0 0 0.31558481 0 1 1 0 1 1 1 0 1 1 0 0 0.45124072
		 0 0 0.5 0 0.5 0.75 0.25 0 1 1 1 1 1 1 1 0 0.15779243 0 0 0 0 0.23137283 0.7284677
		 0.23137285 0.7284677 0.19133066 0.63688368 0.35009158 1 1 0 0.16921526 0.12454629
		 1 0 0 0 0 1 0 0.5 0 0 0 0.57445049 0 0.84055555 0 1 0.5 1 0 0 1 1 0 1 0 0 1 1 0 1
		 0 0.31558487 0 0 0 1 0.22562036 0.099637032 0 0.5 0 1 1 0 1 0 1 1 0 0 0 1 0 1 0 1
		 0.17352962 0.59616929 0.17049323 0.5892244 0.26716805 0.81033808 0.35009158 1 0.17065339
		 0.28175399 1 1 0 1 0 0 0 0.25 0 0.44501764 0 0.57445049 0 0.78722525 0 0.89361268
		 1 1 1 0 1 0 0 0.44501767 1 0 1 1 0 0 1 0 1 1 0 0 0.22562037 0.099637032 0 0.75 0
		 0.75 0 0.75 1 0.5 0.5 0 0 0 1 1 1 0 1 0 0 1 0 0.20845805 0 0.4169161 1 0 0 0 0 0
		 1 1 0 1 0.086764812 0.39772168 0.17352962 0.59616929 0.17352964 0.59616929 0.043382406
		 0.29849789 0.24731848 0.76493841 0.16921526 0.12454629 0.11281019 0.14945555 1 0
		 0 1 0 1 0 0.5 0 0 0 0.89361268 0 1 0.5 1 0 0 1 0 0 1 0 0 1 1 0 1 0 0.54901361 0.45124072
		 0 0 1 0 1 0 0.75 0 0 1 1 1 1 1 1 0 0 0.35009158 1 0 1 0 1 1 1 0.11568642 0.46387091
		 0.11568642 0.46387094 0 0.19927406 0.1496558 0.54156536 0 0.5 0.25 0.25 0 0 0 0 0
		 0.25 0 0.78722525 0 0.94680631 1 1 1 1 1 0 0 0.57445049 0.45124072 0 0 1 1 0 1 1
		 0 1 1 0 1 1 0.22562036 0.099637032 0 0.75 0 1 0 1 0 1 0.75 0.25 1 1 0 1 1 1 0 1 0
		 1 1 0 1 0 1 0 0 0 0.5 0.76125216 0 0.52250433 0 0.65737009 0.42161328 0.46999514
		 1 0.5 0.5 1 0.5 0 1 0.5 0.19813687 0.19069549 0.17065339 0.28175399 0.22562037 0.099637032
		 0.11281019 0.14945555 0 0.19927406 0.5 0 0 1 0 0.5 0 0 0 0.78722525 0 0.89361268
		 0 1 0.5 1 1 1 0 0 1 1 0 1 0 0 1 0 0 1 0 0.6811111 0 0 0 0 0.16921526 0.12454629 0
		 1 0.5 0 0 1 0 1 1 1 1 1 1 1 0 1 1 1 1 1 0 0.79223585 0.75 0.25 0.75 0.75 0.11568642
		 0.46387091 0.45124069 0 0.33843052 0.049818516;
	setAttr ".uvst[0].uvsp[500:749]" 0.32120678 0.33322319 0.11281019 0.14945555
		 0.75 0.25 0.25 0 0.5 1 0.5 0 0 0.25 1 0 1 1 0 0.84055555 1 0 1 1 0 0 0 1 1 0 1 1
		 0 1 0 0.75 0 0 0 0 0 0.5 0 0 0 0.5 0 0 0.5 0.5 0 0.5 0 0 0 0 0 0 0 0 1 0 1 0 1 0
		 0 1 0.375 0.38625216 0.5625 0.69312614 0.42581609 0.60613322 0.45984215 0.77564436
		 1 0 1 1 0 0 0.625 0.625 0.75 1 0 0.19927406 0 0.19927406 1 0 1 1 0 0 0 0 0.5 1 0
		 0.5 1 0.5 0 1 0.5 1 1 1 0 0 1 0 0 1 0 0 1 0 0 1 0 1 0 1 0 0 0 0 0 0.5 0 0.5 1 0.5
		 0.5 0.5 0.25 0.25 1 1 0 0 0 1 1 1 1 1 0 0 0.5 1 0 1 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0.5
		 0 0.75 0.25 1 1 1 1 1 1 0 1 1 0 1 1 0 0.5 0 1 0 0.75 0 1 0 1 0 1 0.5 1 1 1 1 0 1
		 1 0 1 0.75 0.75 1 1 0 1 1 1 1 1 0 1 1 0 0.85163218 0.55489641 0.91968429 0.75905287
		 1 0 1 1 0 0 0 1 1 0 1 1 0 0 1 0 1 1 1 1 0.5 1 1 0.5 1 1 0 0 1 0 0 1 0 0 1 0 0 1 0
		 0 0 0 0 1 0 1 0 1 0 1 1 0 0.5 1 0 0.5 0 1 0.75 0.5 0 1 0.5 0 0.5 0.5 0 0 1 1 0 1
		 0 1 0 0 1 0 0 1 0 0 0 1 1 0 1 1 1 1 0 0.5 0 1 0 0 0.75 0.25 0.5 0 0 0 0 0 0 0.25
		 0 0 0 0.25 0 0 0 0.25 0.5 0 1 0.5 1 1 1 0 1 0.75 0 1 0.5 0.5 0.5 0 0.5 0 1 0.5 0.25
		 0 0 0 0.75 0.5 0 0 1 0 0 1 1 0 1 1 0 0 0 1 1 0 1 1 1 1 1 1 0 1 0 0 1 0 0 1 0 0.25
		 0 0 0 0 0 0.25 0 0 0 0.25 0.5 0.5 0 0.25 0.5 1 1 1 1 0.25 1 0 0 0 1 0 1 0.5 0 0 0.25
		 0.25 0.5 0.5 0 0 1 1 0.5 1 1 0.5 1 0.5 0.75 0.25 0 0.5 0 0.5 0 0.5 0 1 0 1 1 1 0.5
		 0 0.25 0 0 0 1 0 1 0.5 0 1 0.5 1 1 0 1 1 0.75 0 0.75 0.75 0 1 1 0 1 1 0 1 1 0 0 1
		 0.5 0 0.75 0.25 0 0.5 0 0.5 0 0.5;
	setAttr ".uvst[0].uvsp[750:999]" 1 1 0 0.5 0 1 0 0 1 1 0.5 1 0 0.5 0 0 0.25
		 0 0.5 1 1 1 0 1 1 0 1 0 0 0 0.5 0 0 1 0 1 0 1 0 0 0 0 1 1 1 1 0.5 1 0 1 0 1 1 1 1
		 0 0 0 0 1 0.5 0 1 0.5 1 0 0 1 0 0 0 1 0.25 0 0.5 0 1 0.5 0 0 0.5 0 0 0 0.5 0 0 1
		 0 1 1 1 1 0.75 0 1 0 0 0 1 0 0 0 1 1 0.25 0 0 0.5 1 0 0 0 0 0 0 0 1 0 1 0.5 1 0 1
		 0 0 0 1 1 0 0 0.5 1 0 1 0.5 1 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 1 0 0 1 1 1 0.5 0.5 0
		 0 0 1 0 1 1 1 0.5 0 0.5 0 0.75 0 1 1 1 0 0.5 1 0 1 1 1 1 0 1 0 0 0.5 1 0.5 0 0 1
		 0 1 0 0 0 1 0 0 0 0.5 1 0 0 0 0 0.5 0 1 1 1 0 1 1 0 0 0 1 0 0 0.5 0.5 1 1 1 0 0.5
		 1 1 0.5 0.875 0.5 0.5 1 0 1 0 1 0 1 1 1 0.5 1 1 0 1 1 0 1 0 1 0 0 0 0 0 1 0 0 0.5
		 0 0 1 1 0.5 1 0 0 1 0.875 0.5 1 0.5 0.5 1 0 1 0 1 0 1 0.75 0.5 0 1 0.5 0 1 0 0.5
		 1 0 0 1 1 1 1 0.5 0 0.75 0.5 1 0.75 0.8125 0.75 0.25 0.75 1 0 1 1 1 0 1 1 1 1 1 1
		 1 0 0.5 0 0.25 1 0.5 0 0.5 0 1 1 1 0 0.8125 0.75 1 0.75 0.25 0.75 1 0 1 1 1 0 1 1
		 1 1 1 0 0.5 0.75 0.625 0.75 1 0 0.25 0 1 0 0.625 0.75 0.5 0.75 0 0.5 1 1 0.75 1 0
		 0 0 0 0 0 0 0 0.25 0 0 1 1 0 0.75 1 1 1 0 0.5 0 0 0 0 0 0 0 1 0.5 1 0 0 0.5 1 0 1
		 0.5 1 1 1 1 0 0 0.5 0 0 0.25 1 0 0 1 0 1 1 1 1 1 0 1 1 1 0 0 0 0 0 1 1 1 0 0.25 1
		 0 0 0 0 0 0.5 0.5 1 1 0 1 1 1 0 1 1 1 0 1 1 0 0 0.5 1 0.5 0.75 1 0 0 0 0 1 0 0 0.5
		 0.75 0.5 1 0 0 0.25 0.25 0.5 0 1 0 1 0.25 1 0 1 0.25;
	setAttr ".uvst[0].uvsp[1000:1249]" 1 0 0.5 0 0.5 0.5 0 1 1 1 0 0 0.5 0 1 1 1
		 0 1 0 1 0.25 1 0 1 0 1 0.25 0.5 0 0.5 0 0 0 0.25 0.25 0.5 0.5 0 0 1 1 1 0.25 0.25
		 0.25 0 0 0.5 0.625 1 0 0.5 0 1 0 0.5 1 1 0.25 0.5 0.625 0 0 0.25 0.25 1 1 1 0 1 0
		 0.75 0.75 1 0 1 0 0.5 0.25 1 0.5 1 0.5 0.125 0.25 0 1 0 0 1 0.5 1 0.5 0.5 0.25 1
		 0 0.125 0.25 1 0 1 0 0.75 0.75 0 0 0 0 0 0.25 0.5 0.5 1 0.5 0 0.25 0.25 0.5 0 0 1
		 0.5 0.5 0.5 0 0.25 0 0.25 0 0 0.25 0.5 0 0 0 0 0 0.25 0 0.5 0.5 1 1 1 1 1 0.5 0.5
		 0 0.5 0.5 0.5 1 1 1 1 0.5 1 0 0.5 0 0.25 0 0.5 0 0 0 0.5 0 1 1 1 0.5 1 0 1 0.5 1
		 1 1 0 1 0 0.5 0 1 0 1 1 1 0.5 0.5 1 1 1 0 1 1 1 0 1 1 1 0 0.5 0.5 0.5 0.5 1 0 1 0
		 1 1 1 0 1 1 0.5 0.5 1 1 1 1 0 1 1 1 1 1 0 0 0 0 1 1 1 1 0 0 0 0.5 0 0 0 1 0 1 0 1
		 0 0.5 1 1 1 1 0 0.5 0 1 0 1 0 1 0 0 0 0.5 0 0 0 0.5 0 1 0 1 0 0.5 0.49886286 0.25
		 0.5 0.25 0.50000006 0 0.75 0.25 0.43612182 1 0.50000006 0 0.5 0.25 0.49726743 0.25
		 0.75 0.25 0.25436744 1 0.026041668 0.38515624 0.098958328 0.087239578 0.098958328
		 0.089583337 0.421875 0.026041668 0.026041668 0.38515624 0.098958328 0.089583337 0.421875
		 0.026041668 0.098958328 0.087239586 0 0.47812501 0 0.45625001 0 0.47812501 0.105564
		 1 0.105564 1 0.105564 0 0.71533424 0.27322358 0.105564 0 0.33924246 0.97518599 0.34133786
		 0.97997856 0.34343323 0.98477113 0.34745941 0.99397969 0.34745941 0.99397969 0.34745941
		 0.99397969 0.34343323 0.98477113 0.34133786 0.97997856 0.33924246 0.97518599 0.71533424
		 0.27322358 0.894436 0 0.894436 1 0.894436 0 0.894436 1 0.19791666 0.19791667 0.19791666
		 0.19791667 0.083333336 0.25 0.14583334 0.14583333 0.10416667 0.3125 0.020833334 0.265625
		 0.083333336 0.25 0.14583334 0.14583333 0.10416667 0.3125 0.020833334 0.27500001 0.5
		 0 0.125 0.087499999 0.3125 0.020833334 0.625 0 1 0 0.35009158 1 0.5 0 0.5 0 0.3125
		 0.020833334 0.125 0.078125 0.5 0 1 0 0.35009158 1 0.5 0 0.3125 0.020833334 0.625
		 0 1 0 0.35009158 1 0.5 0 0.125 0.078125 0.5 0 1 0 0.35009158 1 0.3125 0.020833334
		 0.125 0.087499999 0 0.3125 0 0.375 0 0.47812501 0 0.34999999 0 0.625 0.020833334
		 0.27500001 0 0.30000001 0 0.34999999 0 0.625 0 0.47812501 0 0.3125 0 0.375 0.020833334
		 0.265625 0.25 0.083333336 0.14583333 0.14583334 0.3125 0.10416667 0.083333336 0.25
		 0.25 0.083333336 0.25 0.083333336 0.14583333 0.14583334 0.3125 0.10416667 0.083333336
		 0.25 0.25 0.083333336 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1;
	setAttr ".uvst[0].uvsp[1250:1499]" 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1
		 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5
		 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5
		 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0.5
		 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0.1 0.125 0.1 0 0.2 0.125 0.2 0.25 0.1 0.25 0.2 0.375
		 0.1 0.375 0.2 0.5 0.1 0.5 0.2 0.625 0.1 0.625 0.2 0.75 0.1 0.75 0.2 0.875 0.1 0.875
		 0.2 1 0.1 1 0.2 0 0.30000001 0.125 0.30000001 0.25 0.30000001 0.375 0.30000001 0.5
		 0.30000001 0.625 0.30000001 0.75 0.30000001 0.875 0.30000001 1 0.30000001 0 0.40000001
		 0.125 0.40000001 0.25 0.40000001 0.375 0.40000001 0.5 0.40000001 0.625 0.40000001
		 0.75 0.40000001 0.875 0.40000001 1 0.40000001 0 0.5 0.125 0.5 0.25 0.5 0.375 0.5
		 0.5 0.5 0.625 0.5 0.75 0.5 0.875 0.5 1 0.5 0 0.60000002 0.125 0.60000002 0.25 0.60000002
		 0.375 0.60000002 0.5 0.60000002 0.625 0.60000002 0.75 0.60000002 0.875 0.60000002
		 1 0.60000002 0 0.70000005 0.125 0.70000005 0.25 0.70000005 0.375 0.70000005 0.5 0.70000005
		 0.625 0.70000005 0.75 0.70000005 0.875 0.70000005 1 0.70000005 0 0.80000007 0.125
		 0.80000007 0.25 0.80000007 0.375 0.80000007 0.5 0.80000007 0.625 0.80000007 0.75
		 0.80000007 0.875 0.80000007 1 0.80000007 0 0.9000001 0.125 0.9000001 0.25 0.9000001
		 0.375 0.9000001 0.5 0.9000001 0.625 0.9000001 0.75 0.9000001 0.875 0.9000001;
	setAttr ".uvst[0].uvsp[1500:1733]" 1 0.9000001 0.0625 0 0.1875 0 0.3125 0 0.4375
		 0 0.5625 0 0.6875 0 0.8125 0 0.9375 0 0.0625 1 0.1875 1 0.3125 1 0.4375 1 0.5625
		 1 0.6875 1 0.8125 1 0.9375 1 0 0.1 0.125 0.1 0 0.2 0.125 0.2 0.25 0.1 0.25 0.2 0.375
		 0.1 0.375 0.2 0.5 0.1 0.5 0.2 0.625 0.1 0.625 0.2 0.75 0.1 0.75 0.2 0.875 0.1 0.875
		 0.2 1 0.1 1 0.2 0 0.30000001 0.125 0.30000001 0.25 0.30000001 0.375 0.30000001 0.5
		 0.30000001 0.625 0.30000001 0.75 0.30000001 0.875 0.30000001 1 0.30000001 0 0.40000001
		 0.125 0.40000001 0.25 0.40000001 0.375 0.40000001 0.5 0.40000001 0.625 0.40000001
		 0.75 0.40000001 0.875 0.40000001 1 0.40000001 0 0.5 0.125 0.5 0.25 0.5 0.375 0.5
		 0.5 0.5 0.625 0.5 0.75 0.5 0.875 0.5 1 0.5 0 0.60000002 0.125 0.60000002 0.25 0.60000002
		 0.375 0.60000002 0.5 0.60000002 0.625 0.60000002 0.75 0.60000002 0.875 0.60000002
		 1 0.60000002 0 0.70000005 0.125 0.70000005 0.25 0.70000005 0.375 0.70000005 0.5 0.70000005
		 0.625 0.70000005 0.75 0.70000005 0.875 0.70000005 1 0.70000005 0 0.80000007 0.125
		 0.80000007 0.25 0.80000007 0.375 0.80000007 0.5 0.80000007 0.625 0.80000007 0.75
		 0.80000007 0.875 0.80000007 1 0.80000007 0 0.9000001 0.125 0.9000001 0.25 0.9000001
		 0.375 0.9000001 0.5 0.9000001 0.625 0.9000001 0.75 0.9000001 0.875 0.9000001 1 0.9000001
		 0.0625 0 0.1875 0 0.3125 0 0.4375 0 0.5625 0 0.6875 0 0.8125 0 0.9375 0 0.0625 1
		 0.1875 1 0.3125 1 0.4375 1 0.5625 1 0.6875 1 0.8125 1 0.9375 1 0.375 0.42528197 0.40000001
		 0.42528197 0.375 0.46287596 0.40000001 0.46287596 0.42500001 0.42528197 0.42500001
		 0.46287596 0.45000002 0.42528197 0.45000002 0.46287596 0.47500002 0.42528197 0.47500002
		 0.46287596 0.5 0.42528197 0.5 0.46287596 0.52499998 0.42528197 0.52499998 0.46287596
		 0.54999995 0.42528197 0.54999995 0.46287596 0.57499993 0.42528197 0.57499993 0.46287596
		 0.5999999 0.42528197 0.5999999 0.46287596 0.62499988 0.42528197 0.62499988 0.46287596
		 0.375 0.50046992 0.40000001 0.50046992 0.42500001 0.50046992 0.45000002 0.50046992
		 0.47500002 0.50046992 0.5 0.50046992 0.52499998 0.50046992 0.54999995 0.50046992
		 0.57499993 0.50046992 0.5999999 0.50046992 0.62499988 0.50046992 0.375 0.53806388
		 0.40000001 0.53806388 0.42500001 0.53806388 0.45000002 0.53806388 0.47500002 0.53806388
		 0.5 0.53806388 0.52499998 0.53806388 0.54999995 0.53806388 0.57499993 0.53806388
		 0.5999999 0.53806388 0.62499988 0.53806388 0.375 0.57565784 0.40000001 0.57565784
		 0.42500001 0.57565784 0.45000002 0.57565784 0.47500002 0.57565784 0.5 0.57565784
		 0.52499998 0.57565784 0.54999995 0.57565784 0.57499993 0.57565784 0.5999999 0.57565784
		 0.62499988 0.57565784 0.375 0.61325181 0.40000001 0.61325181 0.42500001 0.61325181
		 0.45000002 0.61325181 0.47500002 0.61325181 0.5 0.61325181 0.52499998 0.61325181
		 0.54999995 0.61325181 0.57499993 0.61325181 0.5999999 0.61325181 0.62499988 0.61325181
		 0.375 0.65084577 0.40000001 0.65084577 0.42500001 0.65084577 0.45000002 0.65084577
		 0.47500002 0.65084577 0.5 0.65084577 0.52499998 0.65084577 0.54999995 0.65084577
		 0.57499993 0.65084577 0.5999999 0.65084577 0.62499988 0.65084577 0.375 0.68843973
		 0.40000001 0.68843973 0.42500001 0.68843973 0.45000002 0.68843973 0.47500002 0.68843973
		 0.5 0.68843973 0.52499998 0.68843973 0.54999995 0.68843973 0.57499993 0.68843973
		 0.5999999 0.68843973 0.62499988 0.68843973 0.54828393 0.9923526 0.53218925 0.9428184
		 0.62640893 0.93559146 0.58427262 0.90497762 0.4517161 0.9923526 0.46781072 0.9428184
		 0.37359107 0.93559146 0.41572738 0.90497768 0.34375 0.84375 0.39583331 0.84375 0.37359107
		 0.75190854 0.41572738 0.78252238 0.45171607 0.6951474 0.46781072 0.7446816 0.54828387
		 0.6951474 0.53218925 0.7446816 0.62640893 0.75190854 0.58427262 0.78252232 0.65625
		 0.84375 0.60416669 0.84375 0.51609462 0.8932842 0.54213631 0.87436378 0.48390538
		 0.8932842 0.45786369 0.87436384 0.44791666 0.84375 0.45786369 0.81313622 0.48390535
		 0.7942158 0.51609462 0.7942158 0.54213631 0.81313616 0.55208331 0.84375 0.5 0.83749998
		 0.46249998 0.42528197;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".clst[0].clsn" -type "string" "Colors";
	setAttr -s 22 ".clst[0].clsp[0:21]"  0 0 0.5 1 0 0 0.5 1 0 0 1 1 0 0
		 0.5 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 0.5 1 0 0 1 1 0 0 0.5 1 0 0 0.5
		 1 0 0 0.5 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 1087 ".vt";
	setAttr ".vt[0:165]"  -20.39625931 38.52291107 26.6912365 -22.99122047 45.12733078 27.82906914
		 -17.36268234 43.38259506 33.53764343 -19.71736908 40.80699158 29.0068225861 -21.9462738 38.29590988 32.18283844
		 -20.91575241 47.59477234 30.95804214 -26.14040565 43.81145859 30.95454407 -20.89141464 41.70706558 32.60831451
		 -23.78079414 47.11862183 28.64606667 -23.47484207 45.79359818 30.6686573 -21.70000839 49.015922546 30.66054535
		 -26.42333031 47.27592087 30.62548065 -23.95449257 47.55313492 30.51783752 -24.42851448 49.50048447 29.78461266
		 -24.42851448 49.59447098 30.19134331 -23.13970375 50.323452 30.29447556 -25.62962341 49.68535995 30.44192505
		 -24.61759377 50.5629921 30.40439034 -20.65620422 40.34176254 30.028478622 -20.6712532 40.3279953 30.012443542
		 -6.59723186 38.52291107 26.6912365 -7.32716846 40.7678566 28.85022354 -10.028039932 43.38259506 33.53764343
		 -4.27065372 45.12733078 27.82906914 -5.047214985 38.29590988 32.18283844 -6.34770727 47.59477234 30.95804214
		 -1.11906195 43.81145859 30.95454407 -3.48047614 47.11862183 28.64606667 -6.10207701 41.70706558 32.60831451
		 -5.56285334 49.015922546 30.66054535 -3.7866621 45.79359818 30.6686573 -0.83592099 47.27592087 30.62548065
		 -2.83226037 49.50048447 29.78461266 -3.30664515 47.55313492 30.51783752 -4.12205744 50.323452 30.29447556
		 -2.83226037 49.59447098 30.19134331 -1.63023543 49.68535995 30.44192505 -2.6430366 50.5629921 30.40439034
		 -6.20992947 40.89834595 31.077205658 -6.25166225 40.92580414 31.10409737 -22.6172123 34.88476563 37.34000015
		 -24.30004501 31.90816307 36.76398087 -22.43687057 35.012882233 39.78742981 -21.88730812 37.75127792 38.022716522
		 -22.6453495 34.88476181 31.75736618 -24.13547325 31.89101219 41.076728821 -21.7402916 37.16446304 40.82120895
		 -24.25860977 32.82263184 31.88421059 -21.13431931 32.44865417 43.65289307 -22.76420784 28.4581604 37.46208191
		 -20.80132294 40.61779022 38.70543289 -20.51177216 35.10300064 43.3795433 -21.54347229 36.533535 27.1333046
		 -23.62347603 29.18408585 31.69061661 -21.84366226 34.54415894 27.57537079 -22.96735191 29.23776817 42.33659363
		 -20.695467 39.31307602 41.8549881 -19.87153435 36.25078201 43.94293213 -21.98698235 32.7830162 25.80256653
		 -20.54459381 30.061668396 44.4086113 -19.054876328 33.43997192 47.75035095 -23.23911095 26.49063873 31.43476677
		 -22.56336975 26.82439232 37.60256958 -17.2580452 42.31267166 41.029506683 -18.76304245 35.27270508 47.41041946
		 -20.75878906 34.80061722 24.28984833 -22.78596306 28.24432182 24.91250992 -18.88046265 33.0025253296 47.82479858
		 -21.033481598 27.92846107 42.53134537 -18.89232445 33.48749161 47.66667175 -18.56026649 37.3985672 44.50632095
		 -18.59337616 36.33010101 47.7626152 -17.85671043 35.8684082 23.93572807 -20.39545822 29.89737511 21.55751419
		 -18.45889473 30.32944489 48.41230774 -18.71920204 33.054569244 47.72916031 -18.50189972 28.66761971 44.9190712
		 -17.85541725 34.016941071 51.46520615 -20.99075699 32.55021667 43.56051254 -18.16396141 35.57989502 51.47912216
		 -20.66564178 22.5704937 24.53367233 -22.64775467 20.62901878 31.52057648 -20.40555954 25.42392731 38.90866089
		 -13.49674511 43.60400772 33.59565735 -13.51644897 39.85888672 44.34541321 -18.98000908 40.24169922 26.74489403
		 -17.87570572 30.83222008 20.96955872 -17.9648819 33.12714005 51.70218658 -17.76200867 33.14385605 51.61681366
		 -19.25287628 27.14616394 42.88805389 -17.84589958 34.25360107 51.44400024 -18.064422607 33.92235947 51.53680038
		 -18.87303543 33.71981812 47.61808014 -21.40909386 19.9726944 38.30122375 -13.49674511 42.51697922 42.28839111
		 -13.49674511 40.52230835 26.5513916 -16.79210091 38.45340347 45.57899857 -17.88550186 37.38749695 48.11480331
		 -18.10817337 36.33426666 51.47177887 -13.49674511 31.36279297 20.71429825 -18.75278091 23.58862305 19.15208626
		 -16.8269043 24.66971397 18.51472282 -17.38887787 29.28356171 48.89644241 -17.85658455 30.91843987 51.59054947
		 -17.74423027 32.90736389 51.59030533 -18.69417191 32.81813812 47.71206665 -17.40047073 28.18520355 45.28934479
		 -16.48304749 34.3689003 53.92240524 -16.61708069 34.0031967163 54.027534485 -20.97314644 32.54958725 43.53917313
		 -24.023870468 18.71216774 23.31253624 -24.44360352 16.30979156 39.0073699951 -19.2489624 19.34177589 40.61949539
		 -17.602211 24.68896484 40.092025757 -10.058197975 42.31267166 41.029506683 -13.49674511 38.64181137 45.88480759
		 -16.53060913 35.90546036 54.52336884 -13.49674511 36.42379761 23.48048782 -21.022136688 21.15340614 18.10562515
		 -16.49002266 31.048181534 53.6038475 -16.45515633 32.88970947 54.19996262 -16.30399704 32.90284348 54.08291626
		 -17.14297104 26.80402565 43.23125458 -16.46316147 34.13173294 53.91917419 -25.80859184 17.38920975 30.78832817
		 -21.50111198 15.57037067 41.62009811 -8.013480186 40.24169922 26.74489403 -16.31653786 38.16347122 48.6868248
		 -10.20711231 38.45444107 45.57899857 -17.56697083 37.088634491 51.46443176 -16.50581169 36.54565811 54.62071991
		 -9.13677883 35.8684082 23.93572807 -13.49674511 24.89536858 18.31301117 -17.82902527 22.35773849 17.08523941
		 -16.55270004 28.75256348 49.19699097 -17.31420898 29.97722816 51.63255692 -16.30043221 32.66533661 54.057079315
		 -16.080982208 27.60341454 45.60023117 -15.30187416 34.24375153 55.37688446 -15.42502785 34.094276428 55.53167343
		 -21.90854454 19.37532616 17.37745094 -26.44832039 13.61397648 30.1243763 -25.49027443 11.76015854 39.97787476
		 -16.66788673 19.077079773 41.71256638 -17.61468506 15.22374153 42.9600563 -13.49674511 24.6006813 40.5845871
		 -6.19961643 40.62018585 38.70543289 -13.48039532 38.60009003 49.12891388 -10.64425564 38.16347122 48.6868248
		 -6.31291914 39.3130722 41.8549881 -15.64992237 35.96985626 55.90527344 -9.11778641 30.83221626 20.96955872
		 -13.49674511 22.71026993 16.87393761 -10.16658592 24.66971397 18.51472092 -18.15348816 20.7494812 16.54581261
		 -16.37609291 30.17745399 53.4162941 -15.53227329 31.036968231 54.85095978 -15.1980648 32.78866959 55.52446365
		 -15.19284344 32.55108643 55.49974823 -13.49674511 26.44763756 43.54216766 -15.33477592 34.47841644 55.39925385
		 -24.97945976 16.24014664 21.92678452 -22.047073364 10.53827858 42.80228043 -16.091396332 38.15325546 51.83837128
		 -9.075287819 37.38749695 48.11480331 -8.4285202 37.3985672 44.50632095;
	setAttr ".vt[166:331]" -16.16427231 37.18585205 54.7180748 -15.71581078 36.51014709 55.88559341
		 -6.23470068 34.80061722 24.28984833 -13.49674511 21.15365219 16.27968979 -16.93071747 29.49376869 51.77950668
		 -15.42297935 28.38630867 49.37086105 -15.45932007 30.35702515 54.47226715 -15.32129955 32.7679863 55.6837616
		 -13.48244286 27.42551804 45.89340591 -14.19956017 34.33171082 56.44910049 -14.26951599 34.18591309 56.63689041
		 -18.39947891 19.81468582 15.99955177 -22.30550385 18.27490997 16.8410511 -26.93750191 9.99712181 29.60078812
		 -26.18777275 7.0077681541 40.020046234 -13.49674511 15.18684578 43.45170975 -13.49674511 18.86873055 41.99875641
		 -17.92533112 10.10687256 44.17613602 -9.85051823 26.80402565 43.23125458 -5.25749493 37.16217422 40.82120895
		 -5.11382437 37.75247574 38.022716522 -10.90209389 38.15325546 51.83837128 -13.49674606 38.5147171 52.13829041
		 -9.42651844 37.088634491 51.46443176 -7.10954523 36.25078583 43.94292831 -14.5059185 36.4442482 56.90756226
		 -6.59803247 29.89737511 21.55751419 -9.164464 22.35773849 17.08523941 -13.49674511 20.19659042 15.76770496
		 -15.75167847 29.20069885 51.89297104 -15.9576931 29.85726929 53.41105652 -14.65399837 30.42753029 55.42293167
		 -14.47351646 31.23188591 56.057296753 -14.13795948 32.74987793 56.4501152 -14.1264925 32.51296997 56.42249298
		 -10.94957733 27.60341454 45.60023117 -14.23036194 34.5655632 56.48209 -14.42417526 35.98962402 56.86951447
		 -25.40148163 14.25807285 20.77273941 -22.71710587 5.71708536 42.48764801 -10.32560444 19.077079773 41.71256638
		 -9.39127827 24.68896484 40.092025757 -5.45001698 36.5306282 27.1333046 -14.92656898 38.08083725 54.86182022
		 -8.88531685 36.33426666 51.47177887 -8.24208546 36.33010101 47.7626152 -15.54811096 37.050437927 55.86590958
		 -5.0065078735 32.78302002 25.80256271 -8.2407074 23.58862305 19.15208626 -8.84000111 20.7494812 16.54581261
		 -13.49506664 28.18588638 49.49611282 -13.49674511 29.040096283 51.91502762 -15.06264782 29.62879372 53.4601593
		 -14.85564423 30.0090808868 54.53902054 -14.22624683 32.72592545 56.63495636 -11.53781319 28.38630867 49.37086105
		 -13.49674606 34.28133774 56.7831459 -13.49674606 34.42055893 56.57203293 -13.49674511 35.93643951 57.033519745
		 -13.49674511 18.74769211 14.73287773 -18.38424492 18.34355736 15.048742294 -22.35374451 16.69769859 15.77184677
		 -27.048873901 6.48628569 28.89190483 -27.19069672 0.86650592 38.39906311 -13.49674511 9.90822697 44.62420273
		 -18.081075668 5.04010582 43.49134064 -7.7629962 27.14616394 42.88805389 -9.59302044 28.18520355 45.28934479
		 -6.46290636 35.10300446 43.37953949 -4.55093908 35.014247894 39.78742599 -4.38384819 34.88476181 37.33999634
		 -5.14982653 34.53834915 27.57537079 -10.829216 37.18585205 54.7180748 -12.066926956 38.08083725 54.86182022
		 -13.49674511 38.49477005 54.96957779 -10.48767471 36.54565811 54.62071991 -7.9602828 35.27270508 47.41041946
		 -13.49674511 36.43547821 57.044986725 -14.48143482 36.89887619 56.94561005 -4.20752764 28.24432182 24.91250992
		 -6.32784796 22.5704937 24.53367233 -5.97135353 21.15340614 18.10562515 -8.59401131 19.81468582 15.99955177
		 -13.49674511 29.90842056 54.56814575 -13.49674511 30.56908798 55.88415527 -13.49674511 31.26498413 56.40444565
		 -13.49674606 32.78242874 56.80324554 -13.49674606 32.58001709 56.57226563 -10.40423775 28.7525692 49.19699478
		 -13.49674606 34.65470505 56.6083374 -25.40221977 12.64569569 19.16067314 -23.000034332275 -1.20825708 41.0054626465
		 -9.068160057 10.10687256 44.17613602 -9.37880421 15.22374153 42.9600563 -7.74452639 19.34177589 40.61949539
		 -6.58793163 25.42392731 38.90866089 -4.34814072 34.88476181 31.75736618 -15.024769783 37.88935471 55.96612167
		 -13.49674606 38.41516113 55.96759796 -10.46287918 35.90546036 54.52336502 -8.8295269 35.57989502 51.47912216
		 -2.73487926 32.82263184 31.88421059 -3.37001395 29.18408585 31.69061661 -5.084945202 19.37532616 17.37745094
		 -13.49674511 29.5234108 53.44009781 -13.49674511 30.25004959 55.40530777 -13.49674606 32.81631088 56.60251236
		 -11.28521156 29.20069885 51.89297104 -12.72397614 34.18591309 56.63689041 -12.79393005 34.33171082 56.44910049
		 -12.76312923 34.5655632 56.48209 -12.56931973 35.98962402 56.86951447 -8.60924339 18.34355736 15.048742294
		 -13.49674511 16.98757362 12.73182011 -18.39735031 16.5730114 12.93035698 -25.49033356 11.2041254 16.0074195862
		 -27.29165268 3.85950875 25.90416336 -26.34453011 -4.3435297 35.11819839 -13.49674511 4.79322052 44.17370987
		 -8.91241646 5.04010582 43.49133682 -18.28181839 -1.89812636 42.27739334 -5.9600091 27.92846107 42.53134537
		 -8.52865887 28.66761971 44.9190712 -9.57191277 29.28356171 48.89644241 -7.7871809 33.43997192 47.75035095
		 -5.85917044 32.44865417 43.65289307 -2.69344497 31.90816498 36.76398087 -11.2776804 36.51013947 55.88559341
		 -11.44537926 37.050430298 55.86590958 -11.34356689 35.96985245 55.90527344 -13.49674606 36.93451691 57.056453705
		 -14.49422073 37.82816696 56.81058884 -3.75437808 26.49063492 31.43476677 -2.96961999 18.71216774 23.31253624
		 -4.68798637 18.27490997 16.8410511 -11.93084145 29.62879372 53.4601593 -12.13784409 30.0090808868 54.53902054
		 -12.33949089 30.42753029 55.42293167 -12.51997852 31.23188591 56.057296753 -12.76724625 32.72237778 56.66328049
		 -12.85553455 32.74631882 56.4784317 -10.062773705 29.49376869 51.77950668 -12.48757362 36.4442482 56.90756226
		 -22.38840103 14.81402779 13.59517765 -27.66818047 1.32832158 22.45034218 -23.14412308 -6.80792141 37.46782684
		 -4.27638435 5.71708536 42.48764801 -4.94641638 10.53827858 42.80228043 -5.49237728 15.57037067 41.62009811
		 -5.58439732 19.9726944 38.30122757 -18.26574135 -7.26045275 38.50022125 -4.43011951 26.82439232 37.60256958
		 -9.72268105 29.97722816 51.63255692 -2.85801673 31.89101219 41.076728821 -11.96872139 37.88935089 55.96612167
		 -11.56846237 34.094276428 55.53167725 -10.37641048 34.0031967163 54.027534485 -8.92906857 33.92235947 51.53680038
		 -12.51205444 36.89887619 56.94561005 -4.22927952 28.4581604 37.46208191 -4.34573555 20.62901878 31.52057648
		 -2.014032841 16.24014664 21.92678452 -12.86699963 32.5094223 56.45080948 -11.053881645 29.8572731 53.4110527
		 -11.65871334 34.47841644 55.39925766 -8.596138 16.5730114 12.93035698;
	setAttr ".vt[332:497]" -4.63974524 16.69769859 15.77184677 -18.40455627 15.19872665 6.89828014
		 -22.47480011 13.39955425 7.55169725 -29.60205269 -0.32921007 14.88449669 -25.60558701 9.86351109 9.20098782
		 -25.5139389 -10.92437649 30.15594864 -13.49674511 -2.28411746 42.68586731 -8.71167088 -1.89812636 42.27739334
		 -3.99345469 -1.20825708 41.0054626465 -13.49674511 -7.37888861 39.018764496 -6.44889832 30.061668396 44.4086113
		 -4.026138306 29.23776817 42.33659363 -8.5202322 30.32944107 48.41230774 -9.13690472 30.91843987 51.59054947
		 -7.95076895 33.49333954 47.66516876 -13.49674511 38.0099754333 56.89407349 -12.49927616 37.82816696 56.81058884
		 -9.13750172 34.020610809 51.46253204 -1.18489861 17.38920975 30.78832817 -1.59201002 14.25807285 20.77273941
		 -0.54517174 13.61397648 30.1243763 -11.57248783 30.29802513 54.46486282 -11.46121788 31.036968231 54.85095978
		 -11.67219067 32.7679863 55.6837616 -11.79542446 32.78866959 55.52446365 -10.61739826 30.17745399 53.4162941
		 -11.69161797 34.24375153 55.37689209 -8.58893299 15.19872665 6.89828062 -13.49674511 15.64348793 6.68038416
		 -26.81443024 -13.38168621 22.83397293 -19.20365906 -12.51588726 32.79550552 -22.45353699 -11.93913555 32.20100784
		 0.19720535 0.86650592 38.39906311 -0.8057186 7.0077681541 40.020042419 -1.50321758 11.76015854 39.97787476
		 -2.54988575 16.30979156 39.0073699951 -13.49674511 -11.93913937 32.50461197 -10.5034647 31.048181534 53.6038475
		 -6.0041108131 32.55249023 43.56198883 -10.53032875 34.13172913 53.91917801 -0.055990823 9.99712181 29.60078812
		 -11.80064583 32.55108643 55.49975967 -10.50963783 34.3688736 53.9229393 -4.51868963 13.39955425 7.55169725
		 -4.60508966 14.81402683 13.59517765 -1.59127188 12.64569569 19.16067314 -22.39139938 12.36278439 0.55183798
		 -18.40131187 14.20491791 -0.11742891 -25.51175308 8.74485397 1.92580068 -27.95772362 -2.057038784 7.14213181
		 -25.46974945 -13.79187584 14.40576553 -25.27262115 -16.76439285 32.077140808 -8.72774982 -7.26045322 38.50022125
		 -0.64896488 -4.3435297 35.11819839 -7.93409109 33.025413513 47.81983566 -9.014216423 33.053646088 51.66356659
		 -9.14324665 34.25764847 51.44256973 -8.095219612 33.078273773 47.72429657 -7.96597958 33.7265358 47.61860275
		 0.055377427 6.48628569 28.89190483 -10.53833294 32.88970947 54.19996262 -10.68949127 32.90284348 54.08291626
		 -4.60209084 12.36278439 0.55183798 -8.59217834 14.20491791 -0.11742819 -1.50315499 11.2041254 16.0074195862
		 -13.49674511 14.64723778 -0.29870245 -25.24253273 -17.80990601 20.74344635 -26.6532402 -17.65864754 26.87714577
		 -22.48353577 -16.32106018 33.4053154 -18.89904022 -14.97797775 23.77543068 -3.84936857 -6.80792141 37.46782684
		 -7.78983116 -12.51588726 32.79550552 -8.12117386 32.84200668 47.70658112 -6.020688534 32.55226135 43.54154205
		 -10.69317055 32.66535187 54.056991577 -1.38790357 9.86351109 9.20098782 -1.48173833 8.74485397 1.92580032
		 0.2981562 3.85950875 25.90416718 0.67469007 1.32832193 22.45034218 -25.39411736 8.35981274 -3.12925196
		 -22.39608574 11.99183273 -4.35115194 -18.41098595 13.83577824 -4.97089291 -27.92342186 -2.44760537 0.25308415
		 -25.31780243 -13.08986187 7.05274725 -22.36917686 -16.51742935 14.93810654 -19.66965485 -16.76439667 32.077144623
		 -19.45475197 -17.65865135 26.87714767 -13.49674511 -14.7647028 23.69883728 -26.57974625 -21.33929634 28.51452065
		 -22.46088982 -19.90289307 33.19591904 -1.47955263 -10.92437649 30.15594864 -8.094449043 -14.97797775 23.77543068
		 -9.21701336 33.070915222 51.57825089 -9.23528862 32.83450317 51.55150604 -1.59937394 8.35981274 -3.12925196
		 -4.59740353 11.99183273 -4.35115194 -8.58250237 13.83577824 -4.97089291 2.60856318 -0.32920972 14.88449669
		 -13.49674511 14.29402256 -5.10280752 -22.30576324 -16.22755814 7.99086714 -22.45883179 -18.9128418 20.50334167
		 -25.18741989 -22.11121941 25.54782104 -19.68709755 -19.40897751 20.88084984 -19.0105896 -17.40538597 16.12378883
		 -25.22053146 -20.41705132 32.13723373 -4.53995228 -11.93913555 32.20100784 -7.53873873 -17.65865135 26.87714767
		 0.96423465 -2.057039261 7.14213181 0.92993224 -2.44760537 0.25308415 -0.17905913 -13.38168621 22.83396912
		 -27.92734528 -2.35362983 -6.70663261 -25.43484306 8.25376129 -8.22085381 -22.50543976 11.87314796 -9.25836086
		 -18.48763275 13.69258881 -9.84050846 -25.32738495 -13.67086601 -2.060936213 -22.38162613 -16.7239418 -1.42755568
		 -18.87548828 -17.47899246 8.61678505 -19.77852821 -20.41705132 32.13722992 -19.56234932 -21.33929443 28.51452065
		 -19.78557396 -22.11121941 25.54782486 -13.49674511 -17.1901722 15.84916306 -7.98290014 -17.40538597 16.12378883
		 -25.37577438 -25.99578094 27.60694122 -27.26824379 -25.16411591 30.6962719 -22.23163223 -24.0012016296 34.36613083
		 -4.50995255 -16.32106018 33.4053154 -1.7208693 -16.76439285 32.077140808 -7.30639267 -19.40897751 20.88084984
		 -7.32383537 -16.76439667 32.077144623 0.93385535 -2.35362959 -6.70663261 -1.55864823 8.25376129 -8.22085381
		 -4.48804951 11.87314796 -9.25836086 -8.50585747 13.69258881 -9.84051037 -1.52374053 -13.79187584 14.40576458
		 -13.49674511 14.13892555 -10.044708252 -18.89058685 -18.14845848 -1.35621858 -22.45265579 -22.41888618 24.84008217
		 -25.63187981 -24.0012016296 34.36613083 -0.34025067 -17.65864754 26.87714577 -7.20791769 -22.11121941 25.54782486
		 -7.4311409 -21.33929443 28.51452065 -1.67568886 -13.08986187 7.05274725 -1.66610515 -13.67086315 -2.060936213
		 -1.7509594 -17.80990601 20.74344635 -25.50728798 -11.68662643 -11.84772778 -27.49912643 -1.97720504 -13.30433846
		 -25.98975754 8.46341419 -14.63502598 -23.11015701 12.010229111 -15.68099022 -13.49674511 14.14506054 -16.56502533
		 -22.58130646 -14.56508923 -11.82397938 -19.041244507 -15.84527969 -12.19038582 -13.49674511 -17.57542992 8.536376
		 -18.84211159 -24.44509697 32.92520523 -18.63898468 -25.16413307 30.69629097 -18.86590767 -26.036190033 27.6069603
		 -22.10605431 -26.33560753 27.0072383881 -8.11800385 -17.47899246 8.61678505 -4.53465843 -18.9128418 20.50334167
		 -25.3506546 -29.26012611 30.66640472 -27.16937447 -27.382967 35.5093956 -25.40452576 -24.21528435 38.79929733
		 -4.53259993 -19.90289307 33.19591904 -0.41374409 -21.33929634 28.51452065 -4.540833 -22.41888618 24.84008217
		 -7.21496201 -20.41705132 32.13722992 -1.48620546 -11.68662834 -11.84772682;
	setAttr ".vt[498:663]" 0.50563651 -1.97720504 -13.30433846 -1.0037345886 8.46341419 -14.63502598
		 -3.88333297 12.010229111 -15.68099022 -4.6877265 -16.22755814 7.99086714 -4.62431335 -16.51742935 14.93810654
		 -18.96067238 13.73546028 -16.39893913 -13.49674511 -16.0069313049 -12.24798679 -13.49674511 -18.31049538 -1.36685967
		 -19.068407059 -24.21530724 38.79932785 -22.19203758 -23.6986084 39.92637253 -1.77295828 -20.41705132 32.13723373
		 -1.80606985 -22.11121941 25.54782104 -4.61107826 -26.33560753 27.0072364807 -7.85122347 -26.036190033 27.6069603
		 -8.18189812 -25.16413307 30.69629097 -4.61186218 -16.7239418 -1.42755568 -4.41218281 -14.56508923 -11.82397842
		 -8.032819748 13.73546028 -16.39893913 -24.14891434 -12.67304707 -19.69452477 -27.17244339 -9.90292645 -19.43579865
		 -28.55112076 -1.83002472 -20.16965675 -26.78191566 8.65990353 -20.9767437 -23.66346169 12.60047722 -22.0051803589
		 -19.55368805 14.41157532 -22.96910667 -19.77452469 -13.80328274 -20.28005028 -13.49674511 -13.9764719 -20.32008362
		 -8.10290432 -18.14845848 -1.35621858 -18.17933273 -27.38298225 35.50941849 -19.079866409 -29.26014137 30.66642189
		 -22.16187668 -29.74025345 30.54954338 -25.45615387 -29.89838982 35.18230057 -26.65732002 -27.25116348 39.84295654
		 -22.20986938 -26.91324806 41.63259125 -4.48550177 -24.0011997223 34.36613083 -0.28520527 -25.16411591 30.6962719
		 -1.34136045 -25.99578094 27.60694122 -7.87502193 -24.44509697 32.92520523 -2.84457779 -12.6730442 -19.69452477
		 0.17895369 -9.90292645 -19.43579865 1.5576303 -1.83002436 -20.16965866 -0.2115752 8.65990353 -20.9767437
		 -3.33002973 12.60047722 -22.0051803589 -7.43980169 14.41157532 -22.96910667 -13.49674511 14.82104206 -23.23227501
		 -7.95224428 -15.84527969 -12.19038582 -18.065885544 -27.251194 39.84298325 -22.20735741 -30.3721447 35.14441681
		 -25.35735321 -29.52961731 40.1720314 -0.29961485 -24.44509125 32.92520905 -7.63726425 -29.26014137 30.66642189
		 -8.60703659 -27.38298225 35.50941849 -7.21896553 -13.80328274 -20.28005028 -27.4371624 -11.27551079 -24.097017288
		 -24.42282104 -13.96875381 -23.95549583 -29.60688972 -1.99181116 -26.42352486 -27.10899734 9.57648373 -27.43148232
		 -24.042909622 13.4743185 -28.82309151 -19.68847084 15.8487606 -29.97036171 -13.49674511 -15.44206905 -24.12467003
		 -19.086566925 -29.52963448 40.17205811 -18.93905258 -29.89840889 35.18231964 -1.31260753 -24.21528625 38.79930115
		 -7.64872503 -24.21530724 38.79932785 -1.36647904 -29.26012611 30.66640472 -0.13214508 -27.382967 35.5093956
		 -4.55525446 -29.74025345 30.5495472 -2.57066894 -13.96875095 -23.95549583 0.44367102 -11.27551079 -24.097017288
		 2.61339617 -1.99181116 -26.42352486 0.11550438 9.57648277 -27.43148041 -2.95058084 13.47431755 -28.8230896
		 -7.30501842 15.8487606 -29.97036171 -19.92517853 -15.28440857 -24.073366165 -13.49674511 16.12748909 -30.57733917
		 -22.19203758 -30.37196732 41.31223297 -4.52509642 -23.6986084 39.92637253 -4.50977612 -30.3721447 35.14441681
		 -7.77807999 -29.89840889 35.18231964 -8.65124702 -27.251194 39.84298325 -7.068312168 -15.28440857 -24.073366165
		 -27.58104706 -12.47041225 -27.25947952 -24.48828125 -15.20966434 -26.50914574 -29.17988396 -3.81582999 -30.079597473
		 -27.23858452 7.31476116 -33.15358353 -24.10433197 11.20049 -34.83803558 -19.7795372 13.41911602 -36.36456299
		 -13.49674511 -16.57818031 -26.41332054 -0.059813466 -27.25116348 39.84296036 -4.50726318 -26.91324806 41.63259125
		 -1.26097822 -29.89838982 35.18230057 -7.63056707 -29.52963448 40.17205811 -2.50521016 -15.20966434 -26.50914574
		 0.58755505 -12.47041225 -27.25947571 2.18639493 -3.81582999 -30.079597473 0.2450919 7.31476164 -33.15358353
		 -2.88915706 11.20049 -34.83803558 -7.21395254 13.41911602 -36.36456299 -19.94687653 -16.43697739 -26.58430099
		 -12.83004284 12.74501419 -44.34559631 -1.35978091 -29.52961731 40.1720314 -7.046612263 -16.4369812 -26.58430099
		 -28.75878716 -6.45813751 -33.90430069 -27.69352722 -14.33670616 -30.69884109 -24.53437614 -16.4333725 -29.73455429
		 -27.16637611 2.95957875 -37.15982056 -24.12955475 5.99033308 -39.089214325 -19.79533958 7.89887285 -40.74615097
		 -13.49674511 8.13285637 -41.34108734 -13.49674511 -17.23182106 -29.57719421 -4.52509642 -30.37196541 41.31223297
		 -2.4591167 -16.4333725 -29.73455429 0.70003462 -14.33670616 -30.69884109 1.76529598 -6.45813751 -33.90430069
		 0.17288508 2.95957875 -37.15982056 -2.86393714 5.99033308 -39.089214325 -7.19814968 7.89887285 -40.74615097
		 -19.97975731 -17.43201637 -29.76529121 -7.013733864 -17.43201637 -29.76529121 -27.18767929 -4.035991192 -39.93335342
		 -28.82892799 -13.91318703 -37.56356049 -24.81193161 -17.77070618 -36.04088974 -20.63431358 -18.047456741 -36.17671204
		 -24.2039299 -2.53878927 -42.33399963 -19.87215424 -1.77650952 -44.064273834 -13.49674511 -1.30685365 -45.021575928
		 -18.65770912 -14.82952023 -37.63169479 -2.18156004 -17.77070618 -36.040885925 -6.35917759 -18.047456741 -36.17671204
		 1.83543599 -13.91318703 -37.56356049 0.19419014 -4.035991192 -39.93335342 -2.78956032 -2.53878927 -42.33399963
		 -7.12133598 -1.77650988 -44.064273834 -27.79170609 -16.63995361 -36.87662888 -8.33578205 -14.82952023 -37.63169479
		 -13.49674511 -14.61288929 -37.47760391 0.79821259 -16.63995361 -36.87662506 -27.40106773 -12.24553108 -42.68235779
		 -24.50995445 -11.22942448 -44.9319191 -29.166502 -18.91379356 -40.4170723 -27.70804024 -22.20811653 -37.98330307
		 -24.81454086 -22.23692894 -37.68758392 -19.25953102 -18.93207359 -40.82847214 -20.25298882 -10.95733738 -45.74888229
		 -13.49674511 -9.020227432 -46.31172943 -18.44428062 -13.65114212 -40.99758911 -2.17895174 -22.23692894 -37.68758392
		 0.71454567 -22.20811653 -37.98330307 -7.73395967 -18.93207359 -40.82847214 2.17300773 -18.91379738 -40.4170723
		 0.40757674 -12.24553108 -42.68235779 -2.48353839 -11.22942448 -44.9319191 -6.74050283 -10.95733929 -45.74887466
		 -20.89967537 -22.24771881 -37.97572327 -8.54920959 -13.65114212 -40.99758911 -13.49674511 -12.5999012 -41.17708588
		 -6.093815327 -22.24771881 -37.97572708 -24.81757545 -17.51497459 -45.60309219 -27.37453651 -17.55867004 -44.22200394
		 -21.11039925 -17.511549 -44.52482986 -29.14769554 -22.008644104 -41.36335754 -27.67021751 -25.0074539185 -38.20047379
		 -24.784832 -25.088756561 -37.75974274 -19.42147827 -22.048772812 -41.3910141 -2.20865893 -25.088754654 -37.75974274
		 0.67672503 -25.0074539185 -38.20047379 2.15420246 -22.008644104 -41.36335754;
	setAttr ".vt[664:829]" -5.88309145 -17.511549 -44.52482605 -7.57201147 -22.048772812 -41.3910141
		 0.38104558 -17.55867004 -44.22200394 -2.17591524 -17.51497459 -45.60309219 -27.37472916 -21.29505539 -44.28273773
		 -29.14858627 -24.70503616 -40.89174652 -20.87861633 -25.036184311 -38.18482971 -21.2367363 -21.26111221 -44.4188385
		 -19.42686272 -24.70503616 -40.89641571 -6.11487532 -25.036182404 -38.18482971 2.15509391 -24.70503616 -40.89175415
		 0.38123673 -21.29505539 -44.28273773 -5.75675392 -21.26111221 -44.4188385 -7.56662655 -24.70503616 -40.89640808
		 -24.81795883 -21.18277168 -45.3800621 -27.68199539 -23.73854828 -44.009765625 -29.147192 -26.18614388 -40.59943008
		 -27.68600082 -26.70378876 -34.56124878 -20.92702103 -26.70379639 -34.56127167 -20.93841553 -23.738554 -44.012111664
		 -6.066468239 -26.70379639 -34.56127167 0.69250697 -26.70378876 -34.56124878 2.15370011 -26.18614388 -40.59943008
		 0.68850446 -23.73855209 -44.009765625 -2.17553306 -21.18277168 -45.3800621 -6.055075169 -23.738554 -44.012111664
		 -24.82063484 -23.63207436 -44.74581909 -27.6664505 -25.52299118 -43.77231979 -24.81454086 -26.4729557 -34.24905777
		 -19.40670776 -26.18614388 -40.59944534 -20.93803215 -25.5229969 -43.77466583 -7.58678389 -26.18614388 -40.59944534
		 -2.17895174 -26.4729557 -34.2490654 0.67296064 -25.52299118 -43.77231979 -2.17285919 -23.63207436 -44.74581909
		 -6.055459499 -25.5229969 -43.77466583 -24.81733322 -25.4186058 -44.62229156 -27.60774231 -28.68617821 -43.22827148
		 -29.060258865 -28.41737175 -40.020664215 -28.23246574 -28.83807182 -33.74945068 -24.81692696 -28.95212936 -32.40330887
		 -19.52256775 -28.41737366 -40.020675659 -7.4709239 -28.41737366 -40.020675659 -2.17656493 -28.95212936 -32.40330887
		 2.066765785 -28.41737175 -40.020664215 1.23897243 -28.83807182 -33.74945068 0.61424786 -28.68617821 -43.22827148
		 -2.17615867 -25.4186058 -44.62229156 -24.73908806 -28.86919594 -43.44155884 -20.35223007 -28.83808517 -33.74947357
		 -20.89341736 -28.68618584 -43.22829437 -6.10007334 -28.68618584 -43.22829437 -6.6412611 -28.83808517 -33.74947357
		 -2.25440407 -28.86919594 -43.44155884 -27.009437561 -30.65101242 -39.87854385 -27.52404022 -30.22419167 -34.068996429
		 -21.12298012 -30.2241993 -34.069019318 -20.96089554 -30.65101242 -39.87854767 -6.032593727 -30.65101242 -39.87854767
		 -5.87051249 -30.2241993 -34.069019318 0.53054684 -30.22419167 -34.068996429 0.015944501 -30.65101433 -39.87854385
		 -24.81309509 -30.82794571 -39.91513443 -24.78949738 -30.80801582 -34.0089836121 -2.20399594 -30.80801582 -34.0089836121
		 -2.1803956 -30.82794571 -39.91513443 -17.10356522 40.6196785 43.20486069 -18.53447151 39.19673538 43.16175461
		 -15.66415119 39.57885361 43.75917435 -17.058570862 38.5813942 43.90472794 -9.99125195 40.58695984 43.21797943
		 -11.41345787 39.57956696 43.75499725 -10.010135651 38.6141243 43.89542007 -8.54996395 39.20939636 43.17398453
		 -17.23267746 41.55800629 43.12346649 -13.49674511 41.47986984 43.58088684 -9.98888111 41.55800247 43.12346649
		 -6.71322346 32.95914459 44.97107697 -6.69713211 32.87791061 45.00070571899 -6.54555368 32.79985046 45.097446442
		 -6.99598169 35.16341782 44.81455612 -7.5127368 36.27902222 45.30276108 -8.65877342 37.39462662 45.79096222
		 -10.36273766 38.35085678 46.68540192 -13.49092388 38.62696075 47.032035828 -16.62279701 38.35018539 46.68540192
		 -18.32004547 37.39462662 45.79096222 -19.41650391 36.2790184 45.30276108 -19.88921356 35.16341782 44.81455994
		 -20.3940239 32.79985046 45.097446442 -20.24370193 32.87425995 45.00047683716 -20.22549248 32.95485687 44.96959305
		 -17.12296295 39.42911148 41.87966156 -9.96038055 39.42907333 41.87923813 -17.58863831 39.87475204 42.58470154
		 -17.70638466 39.32627869 42.36850739 -17.12190247 40.04365921 42.34433746 -18.096567154 40.15426636 43.15585327
		 -10.44327927 40.0070343018 42.72813416 -10.5634737 39.49163437 42.54779816 -9.96080494 40.043624878 42.34400558
		 -11.042509079 40.43453217 43.54619598 -15.02253437 38.88032532 44.97932434 -14.63029099 39.77669525 44.31502914
		 -16.011165619 38.92938995 44.092193604 -16.8765564 38.43155289 44.75868607 -15.14442253 38.54760742 45.73190689
		 -13.50971603 39.14474487 44.96334839 -18.58901024 38.34909439 43.9623642 -18.07964325 38.62805939 43.65080261
		 -19.36058044 39.20932007 42.9957428 -19.62786674 38.3558197 43.18065643 -17.6761837 37.92598724 45.04265976
		 -8.46109295 38.37849426 43.96115875 -8.99442291 38.65909958 43.64825058 -10.15458775 38.45945358 44.74119186
		 -9.31781673 37.92650223 45.04265976 -7.37071991 38.3558197 43.18065643 -7.72331572 39.22010422 43.014400482
		 -11.97930717 38.89849472 44.95735168 -11.85192871 38.54812622 45.73190689 -11.055538177 38.95096207 44.077335358
		 -12.39443398 39.77555847 44.3072319 -18.6540947 40.42085648 42.9123497 -18.96407318 40.43553925 42.48922729
		 -17.049568176 41.092720032 43.2939949 -15.12702847 40.97730637 43.72249222 -15.36471176 41.51893997 43.35217667
		 -16.03834343 40.46215057 43.54346085 -13.52083302 40.64792633 43.85746002 -11.92865181 40.95049286 43.7273407
		 -11.74281311 41.51893616 43.35217667 -10.081172943 41.063415527 43.30768967 -8.47136784 40.40579224 42.93331146
		 -8.15090084 40.43553925 42.48922729 -8.99706459 40.13631439 43.16910934 -16.66366005 39.16134262 42.90801239
		 -16.55016327 39.49163437 42.54779816 -17.12190056 39.03175354 42.56918716 -16.66366386 40.0070343018 42.72813416
		 -17.58863831 39.029056549 42.76457977 -9.47493267 39.028915405 42.763237 -9.35303879 39.3260994 42.3668251
		 -9.96080685 39.031719208 42.56885147 -9.47493172 39.87460709 42.58335495 -10.44328022 39.16134262 42.90801239
		 -17.84589958 33.80478668 40.46598816 -16.48304749 33.80478668 42.94439316 -18.87303543 33.80477905 36.64006805
		 -20.97314644 29.31665802 32.99053955 -20.22549248 33.80477524 33.99158096 -17.74423027 30.88006401 40.80032349
		 -18.69417191 30.13017464 37.054336548 -16.30043221 31.34552002 43.12546539 -15.19284344 31.61879158 44.49056244
		 -15.33477592 33.8047905 44.42124176 -14.1264925 31.79492378 45.37041473 -14.23036194 33.8047905 45.50407791
		 -13.49674606 31.82633209 45.52731705 -13.49674606 33.8047905 45.63032532 -12.76312923 33.8047905 45.50407791
		 -12.86699963 31.80023384 45.3969574 -11.65871334 33.8047905 44.42124557 -11.80064583 31.61878967 44.49057007
		 -10.50963783 33.8047905 42.94492722 -9.14324665 33.80478668 40.46455765;
	setAttr ".vt[830:995]" -7.96597958 33.80477905 36.64059067 -6.71322346 33.80477142 33.99306488
		 -10.69317055 31.34550285 43.12538147 -8.12117386 30.1300354 37.053657532 -9.23528862 30.86978531 40.74898911
		 -6.020688534 29.31722069 32.99332809 -13.48113155 32.58150482 39.34194183 -9.78258228 39.6866684 41.34528351
		 -10.047991753 39.79660416 41.34528351 -10.31340122 39.6866684 41.34528351 -10.42333794 39.42125702 41.34528351
		 -10.31340122 39.15584564 41.34528351 -10.047991753 39.045909882 41.34528351 -9.78258228 39.15584564 41.34528351
		 -9.67264557 39.42125702 41.34528351 -9.54315281 39.92609787 41.51781082 -10.047991753 40.13520813 41.51781082
		 -10.5528307 39.92609787 41.51781082 -10.76194191 39.42125702 41.51781082 -10.5528307 38.91641617 41.51781082
		 -10.047991753 38.70730591 41.51781082 -9.54315281 38.91641617 41.51781082 -9.3340416 39.42125702 41.51781082
		 -9.35313988 40.11610794 41.78652954 -10.047991753 40.40392685 41.78652954 -10.74284363 40.11610794 41.78652954
		 -11.030660629 39.42125702 41.78652954 -10.74284363 38.7264061 41.78652954 -10.047991753 38.43858719 41.78652954
		 -9.35313988 38.7264061 41.78652954 -9.065322876 39.42125702 41.78652954 -9.23114491 40.23810577 42.12513351
		 -10.047991753 40.57645416 42.12513351 -10.8648386 40.23810577 42.12513351 -11.20318794 39.42125702 42.12513351
		 -10.8648386 38.60440826 42.12513351 -10.047991753 38.26605988 42.12513351 -9.23114491 38.60440826 42.12513351
		 -8.89279556 39.42125702 42.12513351 -9.18910789 40.28013992 42.50048065 -10.047991753 40.6359024 42.50048065
		 -10.90687561 40.28013992 42.50048065 -11.26263714 39.42125702 42.50048065 -10.90687561 38.56237411 42.50048065
		 -10.047991753 38.20661163 42.50048065 -9.18910789 38.56237411 42.50048065 -8.83334637 39.42125702 42.50048065
		 -9.23114491 40.23810577 42.87582779 -10.047991753 40.57645416 42.87582779 -10.8648386 40.23810577 42.87582779
		 -11.20318794 39.42125702 42.87582779 -10.8648386 38.60440826 42.87582779 -10.047991753 38.26605988 42.87582779
		 -9.23114491 38.60440826 42.87582779 -8.89279556 39.42125702 42.87582779 -9.35313988 40.11610794 43.21443176
		 -10.047991753 40.40392685 43.21443176 -10.74284363 40.11610794 43.21443176 -11.030660629 39.42125702 43.21443176
		 -10.74284363 38.7264061 43.21443176 -10.047991753 38.43858719 43.21443176 -9.35313988 38.7264061 43.21443176
		 -9.065322876 39.42125702 43.21443176 -9.54315281 39.92609787 43.48315048 -10.047991753 40.13520813 43.48315048
		 -10.5528307 39.92609787 43.48315048 -10.76194191 39.42125702 43.48315048 -10.5528307 38.91641617 43.48315048
		 -10.047991753 38.70730591 43.48315048 -9.54315281 38.91641617 43.48315048 -9.3340416 39.42125702 43.48315048
		 -9.78258228 39.6866684 43.6556778 -10.047991753 39.79660416 43.6556778 -10.31340122 39.6866684 43.6556778
		 -10.42333794 39.42125702 43.6556778 -10.31340122 39.15584564 43.6556778 -10.047991753 39.045909882 43.6556778
		 -9.78258228 39.15584564 43.6556778 -9.67264557 39.42125702 43.6556778 -10.047991753 39.42125702 41.28583527
		 -10.047991753 39.42125702 43.71512604 -16.73149872 39.6866684 41.3662796 -16.99690819 39.79660416 41.3662796
		 -17.26231766 39.6866684 41.3662796 -17.37225342 39.42125702 41.3662796 -17.26231766 39.15584564 41.3662796
		 -16.99690819 39.045909882 41.3662796 -16.73149872 39.15584564 41.3662796 -16.62156296 39.42125702 41.3662796
		 -16.49206924 39.92609787 41.53880692 -16.99690819 40.13520813 41.53880692 -17.50174713 39.92609787 41.53880692
		 -17.7108593 39.42125702 41.53880692 -17.50174713 38.91641617 41.53880692 -16.99690819 38.70730591 41.53880692
		 -16.49206924 38.91641617 41.53880692 -16.28295708 39.42125702 41.53880692 -16.30205727 40.11610794 41.80752563
		 -16.99690819 40.40392685 41.80752563 -17.69175911 40.11610794 41.80752563 -17.97957611 39.42125702 41.80752563
		 -17.69175911 38.7264061 41.80752563 -16.99690819 38.43858719 41.80752563 -16.30205727 38.7264061 41.80752563
		 -16.014240265 39.42125702 41.80752563 -16.18006134 40.23810577 42.14612961 -16.99690819 40.57645416 42.14612961
		 -17.81375504 40.23810577 42.14612961 -18.15210342 39.42125702 42.14612961 -17.81375504 38.60440826 42.14612961
		 -16.99690819 38.26605988 42.14612961 -16.18006134 38.60440826 42.14612961 -15.841712 39.42125702 42.14612961
		 -16.13802528 40.28013992 42.52147675 -16.99690819 40.6359024 42.52147675 -17.85579109 40.28013992 42.52147675
		 -18.21155357 39.42125702 42.52147675 -17.85579109 38.56237411 42.52147675 -16.99690819 38.20661163 42.52147675
		 -16.13802338 38.56237411 42.52147675 -15.7822628 39.42125702 42.52147675 -16.18006134 40.23810577 42.89682388
		 -16.99690819 40.57645416 42.89682388 -17.81375504 40.23810577 42.89682388 -18.15210342 39.42125702 42.89682388
		 -17.81375504 38.60440826 42.89682388 -16.99690819 38.26605988 42.89682388 -16.18006134 38.60440826 42.89682388
		 -15.841712 39.42125702 42.89682388 -16.30205727 40.11610794 43.23542786 -16.99690819 40.40392685 43.23542786
		 -17.69175911 40.11610794 43.23542786 -17.97957611 39.42125702 43.23542786 -17.69175911 38.7264061 43.23542786
		 -16.99690819 38.43858719 43.23542786 -16.30205727 38.7264061 43.23542786 -16.014240265 39.42125702 43.23542786
		 -16.49206924 39.92609787 43.50414658 -16.99690819 40.13520813 43.50414658 -17.50174713 39.92609787 43.50414658
		 -17.7108593 39.42125702 43.50414658 -17.50174713 38.91641617 43.50414658 -16.99690819 38.70730591 43.50414658
		 -16.49206924 38.91641617 43.50414658 -16.28295708 39.42125702 43.50414658 -16.73149872 39.6866684 43.67667389
		 -16.99690819 39.79660416 43.67667389 -17.26231766 39.6866684 43.67667389 -17.37225342 39.42125702 43.67667389
		 -17.26231766 39.15584564 43.67667389 -16.99690819 39.045909882 43.67667389 -16.73149872 39.15584564 43.67667389
		 -16.62156296 39.42125702 43.67667389 -16.99690819 39.42125702 41.30683136 -16.99690819 39.42125702 43.73612213
		 -11.12669563 32.64822006 38.24778366 -12.64500141 32.8560257 38.24778366 -14.52173328 32.8560257 38.24778366
		 -16.040039063 32.64822006 38.24778366 -16.61997986 32.31198883 38.24778366 -16.040039063 31.97575378 38.24778366
		 -14.52173328 31.76795197 38.24778366 -12.64500141 31.76795197 38.24778366 -11.12669563 31.97575378 38.24778366
		 -10.54675484 32.31198883 38.24778366 -11.12669563 32.64822006 40.67707443;
	setAttr ".vt[996:1086]" -12.64500141 32.8560257 40.67707443 -14.52173328 32.8560257 40.67707443
		 -16.040039063 32.64822006 40.67707443 -16.61997986 32.31198883 40.67707443 -16.040039063 31.97575378 40.67707443
		 -14.52173328 31.76795197 40.67707443 -12.64500141 31.76795197 40.67707443 -11.12669563 31.97575378 40.67707443
		 -10.54675484 32.31198883 40.67707443 -11.12669563 32.64822006 43.1063652 -12.64500141 32.8560257 43.1063652
		 -14.52173328 32.8560257 43.1063652 -16.040039063 32.64822006 43.1063652 -16.61997986 32.31198883 43.1063652
		 -16.040039063 31.97575378 43.1063652 -14.52173328 31.76795197 43.1063652 -12.64500141 31.76795197 43.1063652
		 -11.12669563 31.97575378 43.1063652 -10.54675484 32.31198883 43.1063652 -11.12669563 32.64822006 45.53565598
		 -12.64500141 32.8560257 45.53565598 -14.52173328 32.8560257 45.53565598 -16.040039063 32.64822006 45.53565598
		 -16.61997986 32.31198883 45.53565598 -16.040039063 31.97575378 45.53565598 -14.52173328 31.76795197 45.53565598
		 -12.64500141 31.76795197 45.53565598 -11.12669563 31.97575378 45.53565598 -10.54675484 32.31198883 45.53565598
		 -11.12669563 32.64822006 47.96494293 -12.64500141 32.8560257 47.96494293 -14.52173328 32.8560257 47.96494293
		 -16.040039063 32.64822006 47.96494293 -16.61997986 32.31198883 47.96494293 -16.040039063 31.97575378 47.96494293
		 -14.52173328 31.76795197 47.96494293 -12.64500141 31.76795197 47.96494293 -11.12669563 31.97575378 47.96494293
		 -10.54675484 32.31198883 47.96494293 -12.78096581 32.64822006 52.64883423 -13.99748421 32.8560257 51.74035645
		 -15.50118542 32.8560257 50.61740875 -16.71770477 32.64822006 49.70893097 -17.18237305 32.31198883 49.36192322
		 -16.71770477 31.97575378 49.70893097 -15.50118542 31.76795197 50.61740875 -13.99748421 31.76795197 51.74035645
		 -12.78096676 31.97575378 52.64883423 -12.31629658 32.31198883 52.99584198 -14.23453331 33.11551666 54.59526825
		 -15.45105267 33.32331848 53.68678665 -16.95475388 33.32331848 52.56384277 -18.17127228 33.11551666 51.65536118
		 -18.63594055 32.77928543 51.30835342 -18.17127228 32.44305038 51.65536118 -16.95475388 32.23524857 52.56384277
		 -15.45105267 32.23524857 53.68678665 -14.23453331 32.44305038 54.59526443 -13.76986408 32.77928543 54.942276
		 -15.03211689 33.50925446 56.007484436 -16.24863625 33.71705627 55.099002838 -17.7523365 33.71705627 53.97605896
		 -18.96885681 33.50925446 53.067577362 -19.43352509 33.17302322 52.72056961 -18.96885872 32.83678818 53.067577362
		 -17.7523365 32.62898636 53.97605896 -16.24863625 32.62898636 55.099002838 -15.032117844 32.83678818 56.007484436
		 -14.56744862 33.17302322 56.35449219 -16.68506813 31.58147621 56.8852272 -17.78864098 31.7321949 56.16360855
		 -19.076545715 31.73467255 55.17220306 -20.056846619 31.5879631 54.28970337 -20.35509872 31.34810066 53.85317993
		 -19.85738373 31.10670662 54.029380798 -18.75380898 30.95598984 54.75099564 -17.46590424 30.95351028 55.74240112
		 -16.48560333 31.10021973 56.62490845 -16.18735123 31.34008408 57.06142807 -17.92557907 30.16470718 56.55070496
		 -18.59963036 30.16838074 56.18635559 -19.38289452 30.06908226 55.67923737 -19.97619247 29.90473557 55.22306061
		 -20.1529007 29.73812866 54.99206161 -19.84552765 29.63288689 55.074478149 -19.17147636 29.62921906 55.43881989
		 -18.3882122 29.72851753 55.94593811 -17.79491425 29.89286041 56.40211868 -17.61820412 30.05947113 56.63311768
		 -19.044290543 29.66329193 56.10387802 -13.58336926 32.31199265 38.24778748;
	setAttr -s 3247 ".ed";
	setAttr ".ed[0:165]"  0 1 1 2 3 1 3 0 1 0 4 0 1 5 1 5 2 0 6 1 1 4 6 0 7 2 1
		 1 8 1 4 7 0 5 9 1 9 7 1 8 10 1 10 5 0 9 6 1 11 8 1 6 11 0 12 9 1 10 12 1 8 13 1 12 11 1
		 14 12 1 13 15 1 15 10 0 16 13 1 11 16 0 15 14 1 14 16 1 13 17 1 17 15 0 16 17 0 17 14 1
		 20 21 1 21 22 1 23 20 1 24 20 0 22 25 0 25 23 1 23 26 1 26 24 0 27 23 1 22 28 1 28 24 0
		 25 29 0 29 27 1 28 30 1 30 25 1 26 30 1 27 31 1 31 26 0 32 27 1 33 29 1 30 33 1 29 34 0
		 34 32 1 33 35 1 31 33 1 32 36 1 36 31 0 34 37 0 37 32 1 35 34 1 36 35 1 37 36 0 35 37 1
		 40 41 1 42 40 1 43 40 1 40 44 1 41 45 1 45 42 1 43 4 1 42 46 1 46 43 1 4 44 1 44 47 1
		 47 41 1 45 48 1 41 49 1 7 4 0 50 43 1 42 51 1 4 52 1 47 53 1 44 54 1 54 47 1 45 55 1
		 48 51 1 53 49 1 49 55 1 50 7 1 46 56 1 56 50 1 51 57 1 57 46 1 4 0 0 0 52 1 52 54 1
		 58 53 1 54 58 1 55 59 1 59 48 1 48 753 1 53 61 1 49 62 1 63 50 1 51 752 1 65 52 1
		 0 65 1 65 54 1 58 66 1 65 58 1 67 48 1 55 68 1 69 60 1 60 64 1 66 61 1 61 62 1 62 68 1
		 63 2 1 56 774 1 57 70 1 70 775 1 71 751 1 64 71 1 72 65 1 73 66 1 65 73 1 74 67 1
		 59 74 1 67 75 1 68 76 1 76 59 1 69 77 1 48 78 1 78 754 1 79 64 1 66 80 1 61 81 1
		 62 82 1 83 2 1 85 72 1 0 85 1 72 86 1 86 73 1 87 67 1 88 75 1 75 78 1 68 89 1 90 77 1
		 91 60 1 77 91 1 92 69 1 91 79 1 80 81 1 93 62 1 81 93 1 82 89 1 94 83 1 63 94 1 83 95 1
		 96 769 1 70 776 1 84 793 1 97 750 1;
	setAttr ".ed[166:331]" 71 97 1 98 71 1 79 98 1 3 85 1 85 95 1 99 86 1 73 100 1
		 100 80 1 86 101 1 102 74 1 76 102 1 103 87 1 74 103 1 87 88 1 88 104 1 75 105 1 89 106 1
		 106 76 1 90 107 0 92 90 0 108 91 1 109 755 0 78 109 1 80 110 1 111 93 1 93 112 1
		 112 82 1 82 113 1 94 114 1 83 22 1 85 2 1 115 771 1 96 770 1 79 116 1 95 117 1 117 72 1
		 117 99 1 100 118 1 101 100 1 103 119 1 87 120 1 88 121 1 104 105 0 105 109 0 89 122 1
		 123 77 1 107 123 1 123 108 1 108 116 1 118 110 1 110 124 1 124 81 1 124 111 1 111 125 1
		 125 112 1 113 122 1 114 22 1 126 95 1 22 126 1 127 749 1 97 127 1 115 784 1 84 786 1
		 129 97 1 98 129 1 130 98 1 116 130 1 131 117 1 99 132 1 132 101 1 133 118 1 101 133 1
		 106 134 1 134 102 1 135 103 1 102 135 1 119 120 1 120 121 1 121 136 1 136 104 0 137 106 1
		 122 137 1 138 123 1 139 108 1 118 140 1 141 124 1 110 141 1 111 142 1 143 113 1 112 143 1
		 125 144 1 113 145 1 114 146 1 21 126 1 126 131 1 127 147 1 147 748 1 148 747 1 128 779 1
		 116 150 1 151 99 1 131 151 1 132 152 1 153 132 1 133 154 1 152 133 1 155 119 1 135 155 1
		 119 156 1 157 121 1 158 136 0 122 159 1 138 139 1 107 160 0 160 138 1 139 150 1 154 140 1
		 161 110 1 140 161 1 141 142 1 142 162 1 162 125 1 144 143 1 145 159 1 149 146 1 146 28 1
		 20 126 1 163 127 1 129 163 1 147 148 1 148 164 1 128 780 1 165 781 1 130 166 1 166 129 1
		 150 167 1 167 130 1 131 168 1 151 153 1 169 154 1 152 169 1 134 170 1 170 135 1 171 134 1
		 137 171 1 155 172 1 172 156 1 156 173 1 173 120 1 173 157 1 157 158 1 174 137 1 159 174 1
		 138 175 1 139 176 1 154 177 1 140 178 1 179 141 1 161 179 1 142 180 1 144 181 1 182 145 1
		 143 182 1 183 144 1 162 183 1 184 159 1;
	setAttr ".ed[332:497]" 149 185 1 146 186 1 20 168 1 187 148 1 163 188 1 188 147 1
		 189 164 1 164 746 1 165 190 1 191 167 1 192 151 1 168 192 1 193 152 1 153 193 1 169 194 1
		 195 170 1 171 195 1 170 196 1 196 155 1 196 172 1 172 197 1 156 198 1 199 157 1 200 158 0
		 201 174 1 175 176 1 160 202 0 202 175 1 176 203 1 203 150 1 194 177 1 177 178 1 204 161 1
		 178 204 1 179 180 1 180 205 1 205 162 1 181 182 1 182 206 1 207 184 1 145 207 1 184 201 1
		 190 185 1 185 186 1 24 28 0 186 24 1 208 168 1 20 208 1 166 209 1 209 163 1 187 189 1
		 188 187 1 189 210 1 164 211 1 211 745 1 167 212 1 212 166 1 203 191 1 168 213 1 214 153 1
		 192 214 1 169 215 1 193 215 1 216 171 1 174 216 1 217 195 1 196 218 1 219 197 1 172 219 1
		 197 198 1 220 173 1 198 220 1 220 199 1 199 200 1 201 221 1 222 176 1 223 175 1 224 203 1
		 194 225 1 177 226 1 178 227 1 228 179 1 204 228 1 180 229 1 230 181 1 183 230 1 206 207 1
		 231 183 1 205 231 1 232 184 1 233 201 1 190 234 1 185 235 1 186 236 1 20 24 0 24 208 1
		 237 168 1 208 237 1 238 189 1 239 187 1 209 240 1 240 188 1 241 210 1 210 211 1 211 242 1
		 243 191 1 191 244 1 244 212 1 237 213 1 245 192 1 213 245 1 246 214 1 247 193 1 214 247 1
		 215 248 1 248 194 1 195 218 1 221 216 1 216 217 1 218 219 1 219 249 1 197 250 1 198 251 1
		 220 252 1 253 200 0 254 221 1 222 224 1 223 222 1 202 255 0 255 223 1 224 243 1 225 226 1
		 226 227 1 256 204 1 227 256 1 228 229 1 229 257 1 257 205 1 230 258 1 259 206 1 181 259 1
		 206 260 1 261 232 1 207 261 1 232 233 1 233 254 1 242 744 1 234 235 1 235 236 1 262 24 1
		 236 262 1 262 237 1 263 209 1 212 263 1 238 241 1 239 238 1 240 239 1 264 240 1 241 265 1
		 210 266 1 266 242 1 237 267 1 213 268 1 245 246 1 215 269 1 247 269 1;
	setAttr ".ed[498:663]" 270 218 1 217 270 1 270 249 1 271 197 1 249 271 1 271 250 1
		 250 251 1 251 252 1 252 272 1 272 199 1 272 253 1 273 221 1 222 274 1 223 275 1 255 276 0
		 224 277 1 248 278 1 278 225 1 225 279 1 226 280 1 281 256 1 282 228 1 256 282 1 283 229 1
		 284 230 1 231 284 1 285 258 1 258 259 1 260 261 1 286 231 1 257 286 1 287 232 1 288 233 1
		 289 254 1 242 290 1 234 291 1 292 236 1 262 267 1 263 264 1 293 241 1 294 238 1 295 265 1
		 265 266 1 243 296 1 296 244 1 297 263 1 244 297 1 267 268 1 268 298 1 298 245 1 299 247 1
		 246 299 1 269 300 1 300 248 1 273 217 1 301 270 1 302 249 1 303 271 1 303 250 1 304 251 1
		 305 252 1 306 272 1 254 307 1 307 273 1 275 274 1 274 277 1 276 275 1 277 308 1 308 243 1
		 279 280 1 280 309 1 309 227 1 281 310 1 309 281 1 310 282 1 282 283 1 283 311 1 311 257 1
		 284 285 1 285 312 1 258 313 1 314 260 1 259 314 1 260 315 1 316 286 1 287 288 1 261 317 1
		 317 287 1 288 289 1 289 318 1 290 743 1 291 319 1 319 235 1 319 292 1 292 267 1 293 295 1
		 294 293 1 239 320 1 320 294 1 264 320 1 295 321 1 265 322 1 323 290 1 266 323 1 324 296 1
		 268 325 1 298 326 1 326 246 1 269 327 1 299 327 1 273 301 1 301 302 1 302 303 1 303 304 1
		 304 305 1 305 306 1 306 328 1 328 253 0 307 329 1 318 307 1 321 274 1 295 277 1 276 330 0
		 308 324 1 278 331 1 331 279 1 300 332 1 332 278 1 333 280 1 334 309 1 335 310 1 336 281 1
		 311 316 1 283 337 1 338 284 1 286 338 1 339 285 1 340 312 1 312 313 1 313 314 1 315 317 1
		 316 341 1 342 288 1 343 287 1 344 289 1 345 318 1 290 346 1 343 319 1 325 292 1 297 347 1
		 347 264 1 293 308 1 324 294 1 348 320 1 321 322 1 322 323 1 323 349 1 296 347 1 324 348 1
		 325 317 1 317 298 1 326 350 1 350 299 1 351 300 1 327 351 1 299 352 1;
	setAttr ".ed[664:829]" 329 301 1 353 302 1 353 303 1 354 304 1 355 305 1 306 356 1
		 357 329 1 318 357 1 275 358 1 358 321 1 330 358 1 331 359 1 333 334 1 279 360 1 360 333 1
		 334 336 1 336 335 1 335 361 1 310 337 1 362 316 1 337 363 1 363 311 1 341 338 1 338 339 1
		 339 340 1 340 364 1 312 365 1 313 366 1 314 367 1 367 315 1 315 326 1 368 341 1 343 342 1
		 342 344 1 325 343 1 344 345 1 345 369 1 349 346 1 346 742 1 370 291 1 291 342 1 347 348 1
		 371 322 1 350 352 1 367 350 1 327 372 1 352 372 1 329 353 1 353 354 1 354 355 1 355 356 1
		 356 373 1 373 328 0 357 353 1 369 357 1 358 371 1 330 374 0 375 359 1 359 360 1 332 376 1
		 376 331 1 377 332 1 351 377 1 378 334 1 379 333 1 380 336 1 381 335 1 382 361 1 361 337 1
		 363 362 1 362 368 1 337 383 1 339 384 1 385 364 1 364 365 1 365 366 1 366 367 1 384 341 1
		 386 344 1 387 345 1 349 388 1 370 389 1 346 390 1 291 386 1 371 349 1 372 391 1 391 351 1
		 352 366 1 369 354 1 392 355 1 356 393 1 374 371 1 376 375 1 375 394 1 359 395 1 396 376 1
		 377 396 1 378 380 1 379 378 1 397 379 1 360 397 1 380 381 1 381 382 1 382 398 1 361 399 1
		 363 400 1 362 401 1 399 383 1 383 400 1 402 340 1 384 402 1 391 364 1 402 385 1 365 372 1
		 403 384 1 368 403 1 386 387 1 387 392 1 392 369 1 374 388 0 388 390 0 404 389 1 389 386 1
		 405 370 1 390 741 0 392 393 1 393 406 1 406 373 0 407 375 1 408 394 1 394 395 1 395 397 1
		 396 407 1 409 377 1 391 409 1 410 396 1 411 380 1 412 378 1 413 379 1 414 381 1 415 382 1
		 382 416 1 398 399 1 417 362 1 400 417 1 418 401 1 419 368 1 401 419 1 399 420 1 421 400 1
		 385 409 1 422 385 1 423 403 1 424 387 1 404 425 0 405 404 0 389 424 1 424 393 1 407 408 1
		 408 426 1 394 427 1 395 428 1 429 407 1 409 410 1 410 429 1 411 414 1;
	setAttr ".ed[830:995]" 412 411 1 413 412 1 397 430 1 430 413 1 414 415 1 415 431 1
		 431 416 1 416 432 1 432 398 1 398 433 1 417 418 1 418 434 1 401 435 1 419 423 1 433 420 1
		 436 383 1 420 436 1 436 421 1 437 402 1 403 437 1 422 410 1 437 422 1 423 438 1 425 424 1
		 425 406 0 439 408 1 440 426 1 426 427 1 427 428 1 428 430 1 429 439 1 441 429 1 442 414 1
		 443 411 1 444 412 1 445 413 1 446 415 1 447 431 1 431 448 1 416 435 1 449 417 1 421 449 1
		 450 418 1 451 434 1 434 435 1 452 419 1 435 452 1 453 423 1 433 454 1 420 455 1 456 421 1
		 457 437 1 422 441 1 458 422 1 459 438 1 438 460 1 460 403 1 439 440 1 440 461 1 426 462 1
		 427 463 1 428 464 1 465 439 1 441 465 1 442 446 1 443 442 1 444 443 1 445 444 1 466 445 1
		 430 466 1 446 447 1 447 467 1 467 448 1 448 435 1 432 468 1 468 433 1 434 432 1 449 450 1
		 450 451 1 451 468 1 452 453 1 453 459 1 454 455 1 469 436 1 455 469 1 469 456 1 457 458 1
		 460 457 1 470 441 1 458 470 1 459 471 1 438 472 1 473 440 1 474 461 1 461 462 1 462 463 1
		 463 464 1 464 466 1 465 473 1 475 465 1 476 446 1 477 442 1 478 443 1 479 444 1 466 480 1
		 481 447 1 482 467 1 483 448 1 484 449 1 456 484 1 485 450 1 486 451 1 487 468 1 483 452 1
		 488 453 1 489 459 1 487 454 1 454 490 1 491 469 1 455 491 1 469 492 1 457 493 1 470 475 1
		 494 470 1 495 471 1 471 472 1 472 496 1 496 460 1 473 474 1 474 497 1 461 498 1 462 499 1
		 463 500 1 501 473 1 502 465 1 476 481 1 477 476 1 478 477 1 479 478 1 503 479 1 445 503 1
		 480 503 1 481 482 1 482 504 1 505 483 1 467 505 1 506 484 1 484 485 1 485 486 1 486 487 1
		 483 488 1 502 453 1 502 489 1 489 495 1 492 507 1 507 456 1 508 458 1 493 508 1 496 493 1
		 509 475 1 508 494 1 494 509 1 495 510 1 471 511 1 472 512 1 513 474 1;
	setAttr ".ed[996:1161]" 514 497 1 497 498 1 498 499 1 499 500 1 500 515 1 515 464 1
		 515 480 1 502 501 1 501 513 1 475 489 1 516 481 1 517 476 1 518 477 1 519 478 1 520 479 1
		 521 503 1 522 482 1 523 504 1 504 505 1 505 524 1 507 506 1 525 484 1 525 485 1 486 526 1
		 524 488 1 488 501 1 509 495 1 487 527 1 527 490 1 490 528 1 528 491 1 491 529 1 529 492 1
		 530 507 1 493 531 1 532 494 1 533 509 1 510 511 1 511 512 1 512 534 1 534 496 1 513 514 1
		 514 535 1 497 536 1 498 537 1 499 538 1 500 539 1 515 540 1 524 513 1 517 516 1 516 522 1
		 518 517 1 519 518 1 520 519 1 521 520 1 480 541 1 541 521 1 522 523 1 542 504 1 506 543 1
		 543 525 1 526 527 1 527 544 1 544 528 1 528 545 1 529 530 1 545 529 1 531 546 1 546 508 1
		 534 531 1 532 533 1 546 532 1 533 510 1 547 511 1 512 548 1 548 534 1 542 514 1 549 535 1
		 535 536 1 536 537 1 537 538 1 538 539 1 539 540 1 540 541 1 524 542 1 517 550 1 516 551 1
		 552 518 1 553 519 1 554 520 1 555 521 1 556 523 1 549 542 1 523 549 1 530 543 1 543 557 1
		 558 526 1 525 558 1 559 546 1 534 560 1 561 533 1 562 532 1 546 562 1 563 547 1 510 563 1
		 564 535 1 565 536 1 537 566 1 538 567 1 539 568 1 540 569 1 550 551 1 570 522 1 551 570 1
		 552 550 1 553 552 1 554 553 1 555 554 1 541 571 1 571 555 1 570 556 1 557 558 1 558 544 1
		 544 572 1 572 545 1 572 530 1 531 573 1 573 559 1 560 573 1 561 563 1 563 574 1 547 575 1
		 575 548 1 548 576 1 576 560 1 577 564 1 549 577 1 564 565 1 565 566 1 566 567 1 567 568 1
		 568 569 1 569 571 1 550 578 1 551 579 1 552 580 1 553 581 1 554 582 1 571 583 1 583 555 1
		 556 577 1 584 556 1 572 557 1 585 562 1 559 585 1 573 586 1 562 587 1 587 561 1 574 575 1
		 575 588 1 576 586 1 588 576 1 589 564 1 590 565 1 591 566 1 592 567 1;
	setAttr ".ed[1162:1327]" 593 568 1 569 594 1 594 571 1 580 578 1 578 579 1 595 570 1
		 579 595 1 581 580 1 582 581 1 583 582 1 571 596 1 596 583 1 595 584 1 586 585 1 585 597 1
		 597 587 1 587 574 1 598 589 1 577 598 1 589 590 1 590 591 1 591 592 1 592 593 1 593 594 1
		 594 596 1 580 599 1 578 600 1 579 601 1 581 602 1 582 603 1 583 604 1 596 605 1 605 583 1
		 584 598 1 606 584 1 607 588 1 574 607 1 607 586 1 608 589 1 609 590 1 610 591 1 611 592 1
		 612 593 1 613 594 1 594 605 1 602 599 1 599 600 1 600 601 1 614 595 1 601 614 1 603 602 1
		 604 603 1 605 604 1 614 606 1 607 597 1 608 609 1 598 615 1 615 608 1 609 610 1 610 611 1
		 611 612 1 612 613 1 613 605 1 602 616 1 616 599 1 617 600 1 599 617 1 618 601 1 619 614 1
		 603 620 1 604 621 1 605 622 1 606 615 1 614 623 1 608 624 1 615 625 1 626 610 1 609 626 1
		 610 627 1 627 611 1 628 612 1 629 613 1 620 616 1 630 600 1 617 630 1 630 618 1 618 619 1
		 619 623 1 621 620 1 622 621 1 631 615 1 632 606 1 623 632 1 625 624 1 624 633 1 633 609 1
		 631 625 1 633 626 1 627 628 1 628 629 1 629 622 1 634 617 1 616 634 1 620 635 1 617 636 1
		 636 630 1 630 637 1 618 638 1 619 639 1 621 640 1 622 641 1 632 631 1 623 642 1 643 624 1
		 644 633 1 645 625 1 633 646 1 646 626 1 647 627 1 626 647 1 648 628 1 649 629 1 635 634 1
		 640 635 1 637 638 1 650 619 1 638 650 1 639 642 1 641 640 1 651 631 1 652 632 1 642 652 1
		 653 643 1 625 653 1 643 644 1 651 645 1 647 648 1 648 649 1 649 641 1 635 654 1 634 655 1
		 655 636 1 640 656 1 636 657 1 657 637 1 637 658 1 638 659 1 650 660 1 660 639 1 639 656 1
		 641 652 1 652 651 1 642 640 1 661 643 1 662 644 1 663 646 1 644 663 1 664 645 1 665 653 1
		 645 665 1 646 666 1 666 647 1 667 648 1 664 649 1 656 654 1 654 655 1;
	setAttr ".ed[1328:1493]" 655 668 1 657 669 1 668 657 1 669 658 1 658 659 1 670 650 1
		 659 670 1 660 671 1 672 660 1 671 656 1 649 651 1 673 661 1 653 673 1 661 662 1 662 674 1
		 674 663 1 663 675 1 664 676 1 676 665 1 665 677 1 666 667 1 675 666 1 667 664 1 678 654 1
		 678 668 1 679 669 1 668 679 1 669 680 1 658 681 1 682 670 1 670 672 1 683 671 1 672 683 1
		 671 678 1 677 673 1 673 684 1 685 662 1 686 674 1 674 687 1 687 675 1 688 676 1 676 689 1
		 689 677 1 688 667 1 675 688 1 678 690 1 679 691 1 690 679 1 691 680 1 680 681 1 681 692 1
		 692 659 1 692 682 1 682 693 1 693 672 1 683 690 1 694 683 1 677 695 1 695 684 1 696 661 1
		 684 696 1 685 686 1 696 685 1 686 697 1 697 687 1 687 698 1 688 698 1 698 689 1 689 699 1
		 700 691 1 690 700 1 691 701 1 680 702 1 681 703 1 692 704 1 705 693 1 693 694 1 694 700 1
		 699 695 1 695 706 1 707 696 1 708 686 1 709 685 1 710 697 1 697 711 1 711 698 1 711 699 1
		 700 712 1 712 701 1 701 702 1 702 703 1 703 704 1 713 682 1 704 713 1 705 714 1 713 705 1
		 714 694 1 699 715 1 715 706 1 706 716 1 716 684 1 707 709 1 716 707 1 709 708 1 708 710 1
		 710 717 1 717 711 1 718 701 1 718 702 1 719 703 1 720 713 1 721 714 1 705 721 1 714 712 1
		 717 715 1 722 706 1 715 722 1 723 716 1 724 709 1 708 725 1 725 710 1 726 718 1 712 726 1
		 718 719 1 719 727 1 727 704 1 727 720 1 720 721 1 722 723 1 707 728 1 728 724 1 723 728 1
		 724 725 1 725 729 1 729 717 1 721 726 1 726 727 1 729 722 1 729 728 1 18 19 1 19 0 1
		 0 18 1 7 19 1 18 7 1 19 4 1 4 0 1 7 4 1 0 7 1 20 38 1 38 39 1 39 20 1 20 24 1 24 38 1
		 38 28 1 28 39 1 24 28 1 28 20 1 3 5 1 21 25 1 730 789 1 732 767 1 734 796 1 737 782 1
		 733 768 1 732 792 1 730 761 1;
	setAttr ".ed[1494:1659]" 731 773 1 736 778 1 737 799 1 734 765 1 735 785 1 738 63 1
		 739 94 1 740 114 1 56 788 1 738 791 1 739 795 1 740 798 1 741 405 0 742 370 1 743 291 1
		 744 234 1 745 190 1 746 165 1 747 128 1 748 115 1 749 96 1 750 70 1 751 57 1 752 64 1
		 753 60 1 754 69 1 755 92 0 741 742 1 742 743 1 743 744 1 744 745 1 745 746 1 746 747 1
		 747 748 1 748 749 1 749 750 1 750 751 1 751 752 1 752 753 1 753 754 1 754 755 1 733 802 1
		 756 760 1 732 801 1 731 759 1 736 807 1 757 764 1 737 806 1 735 763 1 759 756 1 760 730 1
		 761 731 1 759 758 1 760 758 1 761 758 1 763 757 1 764 734 1 765 735 1 763 762 1 764 762 1
		 765 762 1 767 84 1 768 732 1 769 733 1 770 115 1 771 84 1 767 766 1 768 766 1 769 766 1
		 770 766 1 771 766 1 773 733 1 774 731 1 775 56 1 776 96 1 773 772 1 774 772 1 775 772 1
		 776 772 1 769 772 1 778 737 1 779 736 1 780 165 1 781 149 1 782 149 1 778 777 1 779 777 1
		 780 777 1 781 777 1 782 777 1 784 128 1 785 736 1 786 735 1 771 783 1 784 783 1 779 783 1
		 785 783 1 786 783 1 788 738 1 789 738 1 788 787 1 774 787 1 761 787 1 789 787 1 791 739 1
		 792 730 1 793 739 1 791 790 1 789 790 1 792 790 1 767 790 1 793 790 1 795 740 1 796 740 1
		 795 794 1 793 794 1 786 794 1 765 794 1 796 794 1 798 149 1 799 734 1 798 797 1 796 797 1
		 799 797 1 782 797 1 801 756 1 802 756 1 768 800 1 801 800 1 802 800 1 801 803 1 792 803 1
		 760 803 1 802 804 1 759 804 1 773 804 1 806 757 1 807 757 1 778 805 1 806 805 1 807 805 1
		 806 808 1 799 808 1 764 808 1 807 809 1 763 809 1 785 809 1 90 810 1 107 811 1 810 811 1
		 92 812 1 812 810 1 109 813 0 755 814 1 813 814 1 104 815 1 105 816 1 815 816 1 816 813 1
		 136 817 1 817 815 1 158 818 1 818 817 1 160 819 1 811 819 1 200 820 1;
	setAttr ".ed[1660:1825]" 820 818 1 202 821 1 819 821 1 253 822 1 822 820 1 255 823 1
		 821 823 1 276 824 1 823 824 1 328 825 1 825 822 1 330 826 1 824 826 1 373 827 1 827 825 1
		 374 828 1 826 828 1 388 829 1 828 829 1 390 830 1 829 830 1 741 831 1 830 831 1 406 832 1
		 832 827 1 404 833 1 425 834 1 833 834 1 405 835 0 835 833 1 834 832 1 831 835 1 814 812 1
		 810 836 1 811 836 1 812 836 1 813 836 0 814 836 1 815 836 1 816 836 1 817 836 1 818 836 1
		 819 836 1 820 836 1 821 836 1 822 836 1 823 836 1 824 836 1 825 836 1 826 836 1 827 836 1
		 828 836 1 829 836 1 830 836 1 831 836 1 832 836 1 833 836 1 834 836 1 835 836 0 4 1 1
		 5 7 1 8 5 1 7 6 1 6 8 1 5 12 1 13 10 1 9 11 1 11 13 1 10 14 1 12 16 1 21 23 1 23 24 1
		 25 27 1 28 25 1 26 28 1 27 26 1 29 32 1 25 33 1 31 30 1 32 31 1 35 29 1 33 36 1 41 42 1
		 42 43 1 44 41 1 4 40 1 48 42 1 49 45 1 43 7 1 46 50 1 51 46 1 52 44 1 53 41 1 55 48 1
		 47 58 1 60 752 1 61 49 1 62 55 1 50 2 1 759 761 1 775 57 1 57 56 1 64 751 1 66 53 1
		 59 67 1 68 59 1 60 754 1 0 72 1 58 73 1 75 48 1 77 60 1 60 79 1 80 61 1 81 62 1 82 68 1
		 63 83 1 71 750 1 79 71 1 86 65 1 73 80 1 76 74 1 74 87 1 67 88 1 89 76 1 77 92 1
		 69 755 1 93 82 1 95 2 1 95 72 1 72 99 1 101 73 1 104 75 1 78 105 1 107 77 1 77 108 1
		 108 79 1 110 81 1 81 111 1 113 89 1 114 83 1 22 95 1 97 749 1 763 765 1 98 97 1 116 98 1
		 99 101 1 118 80 1 106 102 1 102 103 1 119 87 1 120 88 1 121 104 1 122 106 1 125 93 1
		 112 113 1 127 748 1 95 131 1 131 99 1 100 133 1 135 119 1 107 138 1 108 138 1 139 116 1
		 140 110 1 141 111 1 142 125 1;
	setAttr ".ed[1826:1991]" 144 112 1 145 122 1 146 22 1 129 127 1 748 148 1 130 129 1
		 150 130 1 152 101 1 99 153 1 154 118 1 134 135 1 137 134 1 156 120 1 120 157 1 121 158 1
		 159 137 1 161 141 1 143 145 1 162 144 1 20 131 1 163 147 1 747 164 1 168 151 1 153 152 1
		 133 169 1 170 155 1 172 119 1 175 139 1 160 175 1 176 150 1 177 140 1 178 161 1 179 142 1
		 180 162 1 181 143 1 145 184 1 185 146 1 186 28 1 166 163 1 147 187 1 148 189 1 190 781 1
		 149 190 1 167 166 1 150 191 1 192 153 1 194 154 1 193 169 1 134 195 1 174 171 1 197 156 1
		 198 173 1 173 199 1 157 200 1 159 201 1 204 179 1 183 181 1 206 145 1 205 183 1 209 188 1
		 210 164 1 746 211 1 191 212 1 213 192 1 214 193 1 215 194 1 195 196 1 171 217 1 218 172 1
		 221 174 1 175 222 1 202 223 1 203 222 1 225 177 1 226 178 1 227 204 1 228 180 1 229 205 1
		 181 206 1 207 232 1 184 233 1 234 185 1 235 186 1 236 24 1 212 209 1 187 238 1 188 239 1
		 189 241 1 745 242 1 203 243 1 192 246 1 247 215 1 217 218 1 249 197 1 250 198 1 251 220 1
		 252 199 1 199 253 1 201 254 1 248 225 1 256 228 1 231 230 1 258 181 1 260 207 1 257 231 1
		 262 208 1 209 264 1 265 210 1 266 211 1 243 244 1 244 263 1 267 213 1 268 245 1 246 247 1
		 269 248 1 221 217 1 270 219 1 254 273 1 222 277 1 275 222 1 276 223 1 277 243 1 279 226 1
		 280 227 1 227 281 1 228 283 1 283 257 1 230 285 1 259 260 1 261 287 1 232 288 1 233 289 1
		 744 290 1 291 235 1 235 292 1 292 262 1 238 293 1 239 294 1 264 239 1 241 295 1 266 290 1
		 298 246 1 299 269 1 217 301 1 270 302 1 249 303 1 250 304 1 251 305 1 252 306 1 306 253 1
		 278 279 1 300 278 1 310 256 1 286 284 1 312 258 1 313 259 1 315 261 1 257 316 1 318 254 1
		 297 264 1 321 265 1 322 266 1 296 297 1 243 324 1 292 268 1 325 298 1;
	setAttr ".ed[1992:2157]" 326 299 1 327 300 1 329 273 1 275 321 1 274 295 1 330 275 1
		 295 308 1 279 333 1 280 334 1 281 335 1 309 336 1 310 283 1 337 311 1 284 339 1 285 340 1
		 314 315 1 341 286 1 287 342 1 317 343 1 288 344 1 289 345 1 743 346 1 291 343 1 319 325 1
		 294 308 1 320 324 1 264 348 1 349 290 1 348 296 1 317 326 1 352 327 1 301 353 1 303 354 1
		 304 355 1 355 306 1 356 328 1 307 357 1 359 279 1 332 331 1 351 332 1 361 310 1 311 362 1
		 364 312 1 365 313 1 366 314 1 316 368 1 369 318 1 322 358 1 322 349 1 326 367 1 372 351 1
		 374 358 1 331 375 1 333 378 1 360 379 1 334 380 1 336 381 1 335 382 1 383 363 1 341 339 1
		 384 340 1 340 385 1 368 384 1 342 386 1 344 387 1 387 369 1 349 390 1 389 291 1 346 741 1
		 367 352 1 366 372 1 353 369 1 354 392 1 392 356 1 393 373 1 394 359 1 395 360 1 332 396 1
		 391 377 1 398 361 1 399 337 1 400 362 1 401 368 1 385 391 1 391 365 1 349 374 1 404 370 1
		 376 407 1 375 408 1 377 410 1 378 411 1 379 412 1 397 413 1 380 414 1 381 415 1 416 398 1
		 362 418 1 420 383 1 383 421 1 403 402 1 402 422 1 368 423 1 386 424 1 424 392 1 425 389 1
		 424 406 1 426 394 1 427 395 1 428 397 1 396 429 1 431 382 1 433 399 1 421 417 1 434 401 1
		 435 419 1 385 410 1 438 403 1 407 439 1 408 440 1 410 441 1 411 442 1 412 443 1 413 444 1
		 430 445 1 414 446 1 415 447 1 448 416 1 432 433 1 435 432 1 417 450 1 418 451 1 419 453 1
		 454 420 1 455 436 1 436 456 1 403 457 1 437 458 1 423 459 1 461 426 1 462 427 1 463 428 1
		 464 430 1 429 465 1 467 431 1 456 449 1 468 434 1 448 452 1 468 454 1 422 470 1 471 438 1
		 472 460 1 439 473 1 440 474 1 441 475 1 442 476 1 443 477 1 444 478 1 445 479 1 466 503 1
		 446 481 1 447 482 1 467 483 1 449 485 1 450 486 1 451 487 1 452 488 1;
	setAttr ".ed[2158:2323]" 453 489 1 490 455 1 490 491 1 492 456 1 493 458 1 460 493 1
		 458 494 1 459 495 1 497 461 1 498 462 1 499 463 1 500 464 1 466 515 1 465 501 1 475 502 1
		 504 467 1 456 506 1 488 502 1 487 490 1 491 492 1 470 509 1 510 471 1 511 472 1 512 496 1
		 473 513 1 474 514 1 476 516 1 477 517 1 478 518 1 479 519 1 503 520 1 503 541 1 481 522 1
		 482 523 1 524 483 1 506 525 1 526 525 1 486 525 1 526 487 1 489 509 1 492 530 1 531 508 1
		 496 531 1 508 532 1 494 533 1 509 510 1 535 497 1 536 498 1 537 499 1 538 500 1 539 515 1
		 515 541 1 501 524 1 523 542 1 504 524 1 530 506 1 544 490 1 545 491 1 510 547 1 547 512 1
		 547 548 1 513 542 1 514 549 1 550 516 1 551 522 1 518 550 1 519 552 1 520 553 1 521 554 1
		 541 555 1 522 556 1 557 525 1 526 544 1 544 545 1 545 530 1 531 559 1 560 531 1 532 561 1
		 562 561 1 561 510 1 548 560 1 549 564 1 535 565 1 565 537 1 566 538 1 567 539 1 568 540 1
		 569 541 1 556 549 1 530 557 1 559 562 1 574 547 1 578 551 1 579 570 1 580 550 1 581 552 1
		 582 553 1 555 582 1 570 584 1 557 544 1 586 559 1 560 586 1 561 574 1 588 548 1 577 589 1
		 564 590 1 565 591 1 566 592 1 567 593 1 593 569 1 584 577 1 597 562 1 574 588 1 588 586 1
		 599 578 1 600 579 1 601 595 1 602 580 1 603 581 1 604 582 1 595 606 1 586 597 1 597 574 1
		 598 608 1 589 609 1 590 610 1 591 611 1 592 612 1 593 613 1 606 598 1 600 618 1 601 619 1
		 620 602 1 621 603 1 622 604 1 623 606 1 624 609 1 625 608 1 611 628 1 612 629 1 613 622 1
		 616 617 1 606 631 1 626 627 1 635 616 1 637 618 1 638 619 1 639 623 1 640 620 1 641 621 1
		 642 632 1 625 643 1 624 644 1 631 645 1 627 648 1 628 649 1 629 641 1 634 636 1 636 637 1
		 650 639 1 632 651 1 644 646 1 645 653 1 646 647 1 654 634 1 656 635 1;
	setAttr ".ed[2324:2489]" 658 638 1 659 650 1 656 642 1 652 640 1 653 661 1 643 662 1
		 651 664 1 647 667 1 648 664 1 649 652 1 668 636 1 669 637 1 671 639 1 650 672 1 674 644 1
		 675 646 1 676 645 1 677 653 1 656 678 1 678 655 1 657 679 1 680 658 1 681 659 1 659 682 1
		 660 683 1 684 661 1 661 685 1 662 686 1 687 663 1 664 688 1 689 665 1 666 688 1 690 668 1
		 691 669 1 693 670 1 690 671 1 672 694 1 695 673 1 697 674 1 698 675 1 698 676 1 699 677 1
		 679 700 1 701 680 1 702 681 1 703 692 1 704 682 1 682 705 1 683 700 1 706 684 1 684 707 1
		 685 708 1 696 709 1 686 710 1 711 687 1 711 689 1 712 691 1 714 693 1 694 712 1 715 695 1
		 717 697 1 717 699 1 712 718 1 702 719 1 719 704 1 704 720 1 713 721 1 716 722 1 707 724 1
		 723 707 1 724 708 1 725 717 1 721 712 1 717 722 1 727 718 1 726 720 1 729 723 1 728 725 1
		 3 1 1 767 771 1 94 791 1 63 791 1 788 50 1 788 63 1 733 772 1 737 777 1 146 798 1
		 114 798 1 795 94 1 795 114 1 771 786 1 738 787 1 739 790 1 795 796 1 798 782 1 741 370 1
		 742 291 1 743 234 1 744 190 1 745 165 1 780 746 1 780 747 1 748 784 1 747 784 1 770 748 1
		 770 749 1 750 776 1 749 776 1 751 70 1 752 57 1 753 51 1 753 78 1 78 755 1 733 800 1
		 801 760 1 733 804 1 736 805 1 806 764 1 736 809 1 760 759 1 730 758 1 764 763 1 734 762 1
		 732 766 1 769 768 1 96 766 1 115 766 1 731 772 1 775 774 1 70 772 1 769 776 1 736 777 1
		 780 779 1 165 777 1 782 781 1 115 783 1 128 783 1 785 779 1 735 783 1 774 788 1 731 787 1
		 789 761 1 789 791 1 792 789 1 732 790 1 793 767 1 739 794 1 786 793 1 735 794 1 796 765 1
		 740 797 1 799 796 1 737 797 1 732 800 1 802 801 1 792 801 1 730 803 1 759 802 1 731 804 1
		 737 805 1 807 806 1 799 806 1 734 808 1 763 807 1 735 809 1 90 811 1;
	setAttr ".ed[2490:2655]" 92 810 1 109 814 1 815 105 1 816 109 1 817 104 1 818 136 1
		 107 819 1 820 158 1 160 821 1 822 200 1 202 823 1 823 276 1 328 822 1 824 330 1 373 825 1
		 826 374 1 828 388 1 829 390 1 830 741 1 406 827 1 404 834 1 405 833 1 425 832 1 831 405 1
		 755 812 1 837 838 0 838 839 0 839 840 0 840 841 0 841 842 0 842 843 0 843 844 0 844 837 0
		 845 846 0 846 847 0 847 848 0 848 849 0 849 850 0 850 851 0 851 852 0 852 845 0 853 854 0
		 854 855 0 855 856 0 856 857 0 857 858 0 858 859 0 859 860 0 860 853 0 861 862 0 862 863 0
		 863 864 0 864 865 0 865 866 0 866 867 0 867 868 0 868 861 0 869 870 0 870 871 0 871 872 0
		 872 873 0 873 874 0 874 875 0 875 876 0 876 869 0 877 878 0 878 879 0 879 880 0 880 881 0
		 881 882 0 882 883 0 883 884 0 884 877 0 885 886 0 886 887 0 887 888 0 888 889 0 889 890 0
		 890 891 0 891 892 0 892 885 0 893 894 0 894 895 0 895 896 0 896 897 0 897 898 0 898 899 0
		 899 900 0 900 893 0 901 902 0 902 903 0 903 904 0 904 905 0 905 906 0 906 907 0 907 908 0
		 908 901 0 837 845 0 838 846 0 839 847 0 840 848 0 841 849 0 842 850 0 843 851 0 844 852 0
		 845 853 0 846 854 0 847 855 0 848 856 0 849 857 0 850 858 0 851 859 0 852 860 0 853 861 0
		 854 862 0 855 863 0 856 864 0 857 865 0 858 866 0 859 867 0 860 868 0 861 869 0 862 870 0
		 863 871 0 864 872 0 865 873 0 866 874 0 867 875 0 868 876 0 869 877 0 870 878 0 871 879 0
		 872 880 0 873 881 0 874 882 0 875 883 0 876 884 0 877 885 0 878 886 0 879 887 0 880 888 0
		 881 889 0 882 890 0 883 891 0 884 892 0 885 893 0 886 894 0 887 895 0 888 896 0 889 897 0
		 890 898 0 891 899 0 892 900 0 893 901 0 894 902 0 895 903 0 896 904 0 897 905 0 898 906 0
		 899 907 0 900 908 0 909 837 0 909 838 0 909 839 0 909 840 0 909 841 0;
	setAttr ".ed[2656:2821]" 909 842 0 909 843 0 909 844 0 901 910 0 902 910 0 903 910 0
		 904 910 0 905 910 0 906 910 0 907 910 0 908 910 0 838 845 1 839 846 1 840 847 1 841 848 1
		 842 849 1 843 850 1 844 851 1 837 852 1 846 853 1 847 854 1 848 855 1 849 856 1 850 857 1
		 851 858 1 852 859 1 845 860 1 854 861 1 855 862 1 856 863 1 857 864 1 858 865 1 859 866 1
		 860 867 1 853 868 1 862 869 1 863 870 1 864 871 1 865 872 1 866 873 1 867 874 1 868 875 1
		 861 876 1 870 877 1 871 878 1 872 879 1 873 880 1 874 881 1 875 882 1 876 883 1 869 884 1
		 878 885 1 879 886 1 880 887 1 881 888 1 882 889 1 883 890 1 884 891 1 877 892 1 886 893 1
		 887 894 1 888 895 1 889 896 1 890 897 1 891 898 1 892 899 1 885 900 1 894 901 1 895 902 1
		 896 903 1 897 904 1 898 905 1 899 906 1 900 907 1 893 908 1 911 912 0 912 913 0 913 914 0
		 914 915 0 915 916 0 916 917 0 917 918 0 918 911 0 919 920 0 920 921 0 921 922 0 922 923 0
		 923 924 0 924 925 0 925 926 0 926 919 0 927 928 0 928 929 0 929 930 0 930 931 0 931 932 0
		 932 933 0 933 934 0 934 927 0 935 936 0 936 937 0 937 938 0 938 939 0 939 940 0 940 941 0
		 941 942 0 942 935 0 943 944 0 944 945 0 945 946 0 946 947 0 947 948 0 948 949 0 949 950 0
		 950 943 0 951 952 0 952 953 0 953 954 0 954 955 0 955 956 0 956 957 0 957 958 0 958 951 0
		 959 960 0 960 961 0 961 962 0 962 963 0 963 964 0 964 965 0 965 966 0 966 959 0 967 968 0
		 968 969 0 969 970 0 970 971 0 971 972 0 972 973 0 973 974 0 974 967 0 975 976 0 976 977 0
		 977 978 0 978 979 0 979 980 0 980 981 0 981 982 0 982 975 0 911 919 0 912 920 0 913 921 0
		 914 922 0 915 923 0 916 924 0 917 925 0 918 926 0 919 927 0 920 928 0 921 929 0 922 930 0
		 923 931 0 924 932 0 925 933 0 926 934 0 927 935 0 928 936 0 929 937 0;
	setAttr ".ed[2822:2987]" 930 938 0 931 939 0 932 940 0 933 941 0 934 942 0 935 943 0
		 936 944 0 937 945 0 938 946 0 939 947 0 940 948 0 941 949 0 942 950 0 943 951 0 944 952 0
		 945 953 0 946 954 0 947 955 0 948 956 0 949 957 0 950 958 0 951 959 0 952 960 0 953 961 0
		 954 962 0 955 963 0 956 964 0 957 965 0 958 966 0 959 967 0 960 968 0 961 969 0 962 970 0
		 963 971 0 964 972 0 965 973 0 966 974 0 967 975 0 968 976 0 969 977 0 970 978 0 971 979 0
		 972 980 0 973 981 0 974 982 0 983 911 0 983 912 0 983 913 0 983 914 0 983 915 0 983 916 0
		 983 917 0 983 918 0 975 984 0 976 984 0 977 984 0 978 984 0 979 984 0 980 984 0 981 984 0
		 982 984 0 912 919 1 913 920 1 914 921 1 915 922 1 916 923 1 917 924 1 918 925 1 911 926 1
		 920 927 1 921 928 1 922 929 1 923 930 1 924 931 1 925 932 1 926 933 1 919 934 1 928 935 1
		 929 936 1 930 937 1 931 938 1 932 939 1 933 940 1 934 941 1 927 942 1 936 943 1 937 944 1
		 938 945 1 939 946 1 940 947 1 941 948 1 942 949 1 935 950 1 944 951 1 945 952 1 946 953 1
		 947 954 1 948 955 1 949 956 1 950 957 1 943 958 1 952 959 1 953 960 1 954 961 1 955 962 1
		 956 963 1 957 964 1 958 965 1 951 966 1 960 967 1 961 968 1 962 969 1 963 970 1 964 971 1
		 965 972 1 966 973 1 959 974 1 968 975 1 969 976 1 970 977 1 971 978 1 972 979 1 973 980 1
		 974 981 1 967 982 1 985 986 0 986 987 0 987 988 0 988 989 0 989 990 0 990 991 0 991 992 0
		 992 993 0 993 994 0 994 985 0 995 996 1 996 997 1 997 998 1 998 999 1 999 1000 1
		 1000 1001 1 1001 1002 1 1002 1003 1 1003 1004 1 1004 995 1 1005 1006 1 1006 1007 1
		 1007 1008 1 1008 1009 1 1009 1010 1 1010 1011 1 1011 1012 1 1012 1013 1 1013 1014 1
		 1014 1005 1 1015 1016 1 1016 1017 1 1017 1018 1 1018 1019 1 1019 1020 1 1020 1021 1
		 1021 1022 1 1022 1023 1 1023 1024 1 1024 1015 1 1025 1026 1;
	setAttr ".ed[2988:3153]" 1026 1027 1 1027 1028 1 1028 1029 1 1029 1030 1 1030 1031 1
		 1031 1032 1 1032 1033 1 1033 1034 1 1034 1025 1 1035 1036 1 1036 1037 1 1037 1038 1
		 1038 1039 1 1039 1040 1 1040 1041 1 1041 1042 1 1042 1043 1 1043 1044 1 1044 1035 1
		 1045 1046 1 1046 1047 1 1047 1048 1 1048 1049 1 1049 1050 1 1050 1051 1 1051 1052 1
		 1052 1053 1 1053 1054 1 1054 1045 1 1055 1056 1 1056 1057 1 1057 1058 1 1058 1059 1
		 1059 1060 1 1060 1061 1 1061 1062 1 1062 1063 1 1063 1064 1 1064 1055 1 1065 1066 1
		 1066 1067 1 1067 1068 1 1068 1069 1 1069 1070 1 1070 1071 1 1071 1072 1 1072 1073 1
		 1073 1074 1 1074 1065 1 1075 1076 1 1076 1077 1 1077 1078 1 1078 1079 1 1079 1080 1
		 1080 1081 1 1081 1082 1 1082 1083 1 1083 1084 1 1084 1075 1 985 995 0 986 996 0 987 997 0
		 988 998 0 989 999 0 990 1000 0 991 1001 0 992 1002 0 993 1003 0 994 1004 0 995 1005 0
		 996 1006 0 997 1007 0 998 1008 0 999 1009 0 1000 1010 0 1001 1011 0 1002 1012 0 1003 1013 0
		 1004 1014 0 1005 1015 0 1006 1016 0 1007 1017 0 1008 1018 0 1009 1019 0 1010 1020 0
		 1011 1021 0 1012 1022 0 1013 1023 0 1014 1024 0 1015 1025 0 1016 1026 0 1017 1027 0
		 1018 1028 0 1019 1029 0 1020 1030 0 1021 1031 0 1022 1032 0 1023 1033 0 1024 1034 0
		 1025 1035 0 1026 1036 0 1027 1037 0 1028 1038 0 1029 1039 0 1030 1040 0 1031 1041 0
		 1032 1042 0 1033 1043 0 1034 1044 0 1035 1045 0 1036 1046 0 1037 1047 0 1038 1048 0
		 1039 1049 0 1040 1050 0 1041 1051 0 1042 1052 0 1043 1053 0 1044 1054 0 1045 1055 0
		 1046 1056 0 1047 1057 0 1048 1058 0 1049 1059 0 1050 1060 0 1051 1061 0 1052 1062 0
		 1053 1063 0 1054 1064 0 1055 1065 1 1056 1066 1 1057 1067 1 1058 1068 1 1059 1069 1
		 1060 1070 1 1061 1071 1 1062 1072 1 1063 1073 1 1064 1074 1 1065 1075 1 1066 1076 1
		 1067 1077 1 1068 1078 1 1069 1079 1 1070 1080 1 1071 1081 1 1072 1082 1 1073 1083 1
		 1074 1084 1 1075 1085 1 1076 1085 1 1077 1085 1 1078 1085 1 1079 1085 1 1080 1085 1
		 1081 1085 1 1082 1085 1 1083 1085 1 1084 1085 1 986 1086 1 1086 991 1 985 1086 1
		 994 1086 1 993 1086 1 992 1086 1 990 1086 1;
	setAttr ".ed[3154:3246]" 989 1086 1 988 1086 1 987 1086 1 986 995 1 987 996 1
		 988 997 1 989 998 1 990 999 1 991 1000 1 992 1001 1 993 1002 1 994 1003 1 985 1004 1
		 996 1005 1 997 1006 1 998 1007 1 999 1008 1 1000 1009 1 1001 1010 1 1002 1011 1 1003 1012 1
		 1004 1013 1 995 1014 1 1006 1015 1 1007 1016 1 1008 1017 1 1009 1018 1 1010 1019 1
		 1011 1020 1 1012 1021 1 1013 1022 1 1014 1023 1 1005 1024 1 1016 1025 1 1017 1026 1
		 1018 1027 1 1019 1028 1 1020 1029 1 1021 1030 1 1022 1031 1 1023 1032 1 1024 1033 1
		 1015 1034 1 1026 1035 1 1027 1036 1 1028 1037 1 1029 1038 1 1040 1029 1 1041 1030 1
		 1042 1031 1 1043 1032 1 1044 1033 1 1025 1044 1 1036 1045 1 1037 1046 1 1048 1037 1
		 1049 1038 1 1050 1039 1 1051 1040 1 1042 1051 1 1043 1052 1 1044 1053 1 1035 1054 1
		 1046 1055 1 1057 1046 1 1058 1047 1 1059 1048 1 1060 1049 1 1061 1050 1 1052 1061 1
		 1053 1062 1 1054 1063 1 1045 1064 1 1066 1055 1 1057 1066 1 1058 1067 1 1059 1068 1
		 1060 1069 1 1061 1070 1 1072 1061 1 1073 1062 1 1074 1063 1 1065 1064 1 1076 1065 1
		 1067 1076 1 1068 1077 1 1069 1078 1 1070 1079 1 1071 1080 1 1082 1071 1 1083 1072 1
		 1084 1073 1 1075 1074 1;
	setAttr -s 2162 -ch 6486 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 3 1 1485 5
		mu 0 3 2 3 7
		f 3 3 1719 -1
		mu 0 3 6 4 5
		f 3 -1720 7 6
		mu 0 3 5 4 8
		f 3 -6 1720 8
		mu 0 3 2 7 9
		f 3 -1721 11 12
		mu 0 3 9 7 12
		f 3 9 1721 -5
		mu 0 3 1 10 7
		f 3 -1722 13 14
		mu 0 3 7 10 13
		f 3 10 1722 -8
		mu 0 3 4 9 8
		f 3 -1723 -13 15
		mu 0 3 8 9 12
		f 3 -7 1723 -10
		mu 0 3 5 8 11
		f 3 -1724 17 16
		mu 0 3 11 8 14
		mc 0 3 -1 -1 0
		f 3 -12 1724 18
		mu 0 3 12 7 15
		mc 0 3 -1 -1 1
		f 3 -1725 -15 19
		mu 0 3 15 7 13
		mc 0 3 1 -1 -1
		f 3 20 1725 -14
		mu 0 3 10 16 13
		f 3 -1726 23 24
		mu 0 3 13 16 19
		f 3 -16 1726 -18
		mu 0 3 8 12 14
		f 3 -1727 -19 21
		mu 0 3 14 12 15
		f 3 -17 1727 -21
		mu 0 3 11 14 17
		f 3 -1728 26 25
		mu 0 3 17 14 20
		f 3 -20 1728 22
		mu 0 3 15 13 18
		f 3 -1729 -25 27
		mu 0 3 18 13 19
		f 3 -22 1729 -27
		mu 0 3 14 15 20
		mc 0 3 3 -1 2
		f 3 -1730 -23 28
		mu 0 3 20 15 18
		mc 0 3 2 -1 -1
		f 3 29 30 -24
		mu 0 3 16 21 19
		f 3 31 -30 -26
		mu 0 3 20 21 17
		f 3 -31 32 -28
		mu 0 3 19 21 18
		f 3 -33 -32 -29
		mu 0 3 18 21 20
		f 3 33 1730 35
		mu 0 3 22 23 25
		f 3 -1731 1486 38
		mu 0 3 25 23 29
		f 3 -36 1731 36
		mu 0 3 26 27 28
		mc 0 3 -1 4 -1
		f 3 -1732 39 40
		mu 0 3 28 27 30
		mc 0 3 -1 4 -1
		f 3 -39 1732 41
		mu 0 3 25 29 31
		f 3 -1733 44 45
		mu 0 3 31 29 34
		f 3 42 1733 -38
		mu 0 3 24 32 29
		mc 0 3 5 -1 -1
		f 3 -1734 46 47
		mu 0 3 29 32 35
		f 3 -41 1734 43
		mu 0 3 28 30 32
		f 3 -1735 48 -47
		mu 0 3 32 30 35
		f 3 -42 1735 -40
		mu 0 3 27 33 30
		f 3 -1736 49 50
		mu 0 3 30 33 36
		f 3 -46 1736 51
		mu 0 3 31 34 37
		f 3 -1737 54 55
		mu 0 3 37 34 40
		f 3 -45 1737 52
		mu 0 3 34 29 38
		f 3 -1738 -48 53
		mu 0 3 38 29 35
		f 3 -51 1738 -49
		mu 0 3 30 36 35
		mc 0 3 -1 6 -1
		f 3 -1739 57 -54
		mu 0 3 35 36 38
		mc 0 3 -1 6 -1
		f 3 -52 1739 -50
		mu 0 3 33 39 36
		f 3 -1740 58 59
		mu 0 3 36 39 42
		f 3 56 1740 -53
		mu 0 3 38 41 34
		f 3 -1741 62 -55
		mu 0 3 34 41 40
		f 3 -56 60 61
		mu 0 3 37 40 43
		f 3 -57 1741 63
		mu 0 3 41 38 42
		f 3 -1742 -58 -60
		mu 0 3 42 38 36
		f 3 -59 -62 64
		mu 0 3 42 39 43
		f 3 -63 65 -61
		mu 0 3 40 41 43
		f 3 -64 -65 -66
		mu 0 3 41 42 43
		f 3 66 1742 67
		mu 0 3 44 45 46
		f 3 -1743 70 71
		mu 0 3 46 45 51
		f 3 -68 1743 68
		mu 0 3 44 46 47
		f 3 -1744 73 74
		mu 0 3 47 46 54
		f 3 69 1744 -67
		mu 0 3 48 49 50
		f 3 -1745 76 77
		mu 0 3 50 49 55
		f 3 72 1745 -69
		mu 0 3 53 52 48
		f 3 -1746 75 -70
		mu 0 3 48 52 49
		f 3 78 1746 -72
		mu 0 3 51 56 46
		f 3 -1747 88 -83
		mu 0 3 46 56 61
		f 3 79 1747 -71
		mu 0 3 45 57 58
		f 3 -1748 90 -88
		mu 0 3 58 57 65
		f 3 -73 1748 80
		mu 0 3 52 53 59
		f 3 -1749 -82 91
		mu 0 3 59 53 68
		f 3 -75 1749 81
		mu 0 3 47 54 60
		f 3 -1750 92 93
		mu 0 3 60 54 69
		f 3 82 1750 -74
		mu 0 3 46 61 54
		f 3 -1751 94 95
		mu 0 3 54 61 70
		f 3 83 1751 -76
		mu 0 3 52 62 49
		f 3 -1752 98 -86
		mu 0 3 49 62 64
		f 3 84 1752 -78
		mu 0 3 55 63 50
		f 3 -1753 89 -80
		mu 0 3 50 63 67
		f 3 85 86 -77
		mu 0 3 49 64 55
		f 3 87 1753 -79
		mu 0 3 58 65 66
		f 3 -1754 101 102
		mu 0 3 66 65 76
		f 3 -84 96 97
		mu 0 3 62 52 71
		f 3 -85 1754 99
		mu 0 3 74 72 73
		f 3 -1755 -87 100
		mu 0 3 73 72 75
		f 3 1518 1755 1532
		mu 0 3 1175 77 1174
		f 3 -1756 116 -1518
		mu 0 3 1174 77 83
		f 3 104 1756 -90
		mu 0 3 63 79 67
		f 3 -1757 118 -106
		mu 0 3 67 79 94
		f 3 105 1757 -91
		mu 0 3 57 80 65
		f 3 -1758 119 -115
		mu 0 3 65 80 90
		f 3 -92 1758 -9
		mu 0 3 59 68 81
		f 3 -1759 -107 120
		mu 0 3 81 68 95
		f 3 1538 1759 1545
		mu 0 3 1151 1183 1185
		f 3 -1760 1546 -1549
		mu 0 3 1185 1183 1182
		f 3 123 1760 122
		mu 0 3 96 1200 70
		f 3 -96 1761 -93
		mu 0 3 54 70 69
		f 3 -1762 -1761 1567
		mu 0 3 69 70 1200
		f 3 1517 1762 1531
		mu 0 3 1174 83 1173
		f 3 -1763 125 124
		mu 0 3 1173 83 97
		f 3 108 -98 109
		mu 0 3 85 86 87
		f 3 110 -99 -109
		mu 0 3 85 75 86
		f 3 111 1763 -100
		mu 0 3 73 88 74
		f 3 -1764 117 -105
		mu 0 3 74 88 79
		f 3 -101 -111 112
		mu 0 3 73 75 85
		f 3 -103 1764 113
		mu 0 3 66 76 89
		f 3 -1765 130 129
		mu 0 3 89 76 100
		f 3 114 1765 -102
		mu 0 3 65 90 76
		f 3 -1766 132 133
		mu 0 3 76 90 104
		f 3 115 1766 1519
		mu 0 3 93 91 1177
		f 3 -1767 -1519 1533
		mu 0 3 1177 91 1176
		f 3 -110 1767 126
		mu 0 3 85 87 98
		f 3 -1768 143 142
		mu 0 3 98 87 118
		f 3 -112 1768 127
		mu 0 3 88 73 99
		f 3 -1769 -113 128
		mu 0 3 99 73 85
		f 3 131 1769 -114
		mu 0 3 101 102 103
		f 3 -1770 148 -136
		mu 0 3 103 102 125
		f 3 134 1770 -116
		mu 0 3 107 105 106
		f 3 -1771 152 151
		mu 0 3 106 105 130
		f 3 -117 1771 137
		mu 0 3 83 77 109
		f 3 -1772 -152 154
		mu 0 3 109 77 134
		f 3 138 1772 -118
		mu 0 3 110 111 79
		f 3 -1773 155 -140
		mu 0 3 79 111 112
		f 3 139 1773 -119
		mu 0 3 79 112 113
		f 3 -1774 157 156
		mu 0 3 113 112 135
		f 3 140 1774 -120
		mu 0 3 80 114 90
		f 3 -1775 158 -150
		mu 0 3 90 114 126
		f 3 -121 1775 141
		mu 0 3 81 95 115
		f 3 -1776 160 159
		mu 0 3 115 95 137
		f 3 -125 1776 1530
		mu 0 3 1173 97 1172
		f 3 -1777 166 165
		mu 0 3 1172 97 141
		f 3 -138 1777 -126
		mu 0 3 83 109 97
		f 3 -1778 168 167
		mu 0 3 97 109 142
		f 3 144 1778 -127
		mu 0 3 98 119 85
		f 3 -1779 145 -129
		mu 0 3 85 119 99
		f 3 -128 1779 -139
		mu 0 3 110 120 111
		f 3 -1780 172 173
		mu 0 3 111 120 147
		f 3 -134 1780 -131
		mu 0 3 76 104 100
		f 3 -1781 176 175
		mu 0 3 100 104 150
		f 3 -130 1781 146
		mu 0 3 89 100 121
		f 3 -1782 178 177
		mu 0 3 121 100 151
		f 3 -132 1782 147
		mu 0 3 124 122 123
		f 3 -1783 -147 179
		mu 0 3 123 122 152
		f 3 149 1783 -133
		mu 0 3 90 126 104
		f 3 -1784 182 183
		mu 0 3 104 126 159
		f 3 150 1784 185
		mu 0 3 129 127 163
		f 3 -1785 -135 -154
		mu 0 3 163 127 128
		f 3 153 1785 1520
		mu 0 3 133 131 1179
		f 3 -1786 -1520 1534
		mu 0 3 1179 131 1178
		f 3 -157 1786 -141
		mu 0 3 113 135 136
		f 3 -1787 191 192
		mu 0 3 136 135 174
		f 3 161 1787 -142
		mu 0 3 115 138 81
		f 3 -1788 -171 196
		mu 0 3 81 138 144
		f 3 169 -144 -3
		mu 0 3 143 144 71
		f 3 170 1788 -143
		mu 0 3 118 145 98
		f 3 -1789 200 201
		mu 0 3 98 145 182
		f 3 -145 1789 171
		mu 0 3 119 98 146
		f 3 -1790 -202 202
		mu 0 3 146 98 182
		f 3 174 1790 -146
		mu 0 3 148 149 120
		f 3 -1791 204 -173
		mu 0 3 120 149 147
		f 3 180 1791 -148
		mu 0 3 153 154 155
		f 3 -1792 208 -182
		mu 0 3 155 154 193
		f 3 -189 1792 209
		mu 0 3 194 156 158
		f 3 -149 181 -1793
		mu 0 3 156 157 158
		f 3 184 1793 -151
		mu 0 3 162 160 161
		f 3 -1794 212 211
		mu 0 3 161 160 196
		f 3 -153 1794 186
		mu 0 3 164 165 166
		f 3 -1795 -212 213
		mu 0 3 166 165 197
		f 3 -187 1795 -155
		mu 0 3 134 168 109
		f 3 -1796 214 -200
		mu 0 3 109 168 181
		f 3 189 1796 -156
		mu 0 3 169 170 171
		f 3 -1797 216 217
		mu 0 3 171 170 198
		f 3 -158 1797 190
		mu 0 3 173 171 172
		f 3 -1798 -218 218
		mu 0 3 172 171 198
		f 3 193 1798 -159
		mu 0 3 114 175 126
		f 3 -1799 221 -211
		mu 0 3 126 175 195
		f 3 194 1799 -160
		mu 0 3 137 176 115
		f 3 -1800 222 -196
		mu 0 3 115 176 177
		f 3 195 1800 -162
		mu 0 3 115 177 138
		f 3 -1801 224 223
		mu 0 3 138 177 202
		f 3 -166 1801 1529
		mu 0 3 1172 141 1171
		f 3 -1802 226 225
		mu 0 3 1171 141 203
		f 3 1542 1802 1551
		mu 0 3 1155 1187 1189
		f 3 -1803 1552 -1555
		mu 0 3 1189 1187 1186
		f 3 -168 1803 -167
		mu 0 3 97 142 141
		f 3 -1804 230 229
		mu 0 3 141 142 206
		f 3 199 1804 -169
		mu 0 3 109 181 142
		f 3 -1805 232 231
		mu 0 3 142 181 207
		f 3 -197 -170 -2
		mu 0 3 81 144 143
		f 3 -172 1805 -175
		mu 0 3 148 183 149
		f 3 -1806 234 235
		mu 0 3 149 183 209
		f 3 203 1806 -174
		mu 0 3 185 184 169
		mc 0 3 -1 -1 7
		f 3 -1807 215 -190
		mu 0 3 169 184 170
		mc 0 3 7 -1 -1
		f 3 -184 1807 -177
		mu 0 3 104 159 150
		f 3 -1808 238 239
		mu 0 3 150 159 212
		f 3 -176 1808 -179
		mu 0 3 100 150 151
		f 3 -1809 241 240
		mu 0 3 151 150 213
		f 3 205 1809 -178
		mu 0 3 151 186 121
		f 3 -1810 242 -207
		mu 0 3 121 186 214
		f 3 206 1810 -180
		mu 0 3 187 188 189
		f 3 -1811 243 -208
		mu 0 3 189 188 215
		f 3 207 1811 -181
		mu 0 3 190 191 192
		f 3 -1812 244 245
		mu 0 3 192 191 216
		f 3 210 1812 -183
		mu 0 3 126 195 159
		f 3 -1813 247 246
		mu 0 3 159 195 217
		f 3 219 1813 -191
		mu 0 3 172 199 173
		f 3 -1814 220 -192
		mu 0 3 173 199 201
		f 3 -193 1814 -194
		mu 0 3 136 174 200
		f 3 -1815 255 254
		mu 0 3 200 174 234
		f 3 -226 1815 1528
		mu 0 3 1171 203 1170
		f 3 -1816 261 262
		mu 0 3 1170 203 240
		f 3 -201 1816 233
		mu 0 3 182 145 208
		f 3 -1817 -224 260
		mu 0 3 208 145 239
		f 3 -234 1817 -203
		mu 0 3 182 208 146
		f 3 -1818 267 266
		mu 0 3 146 208 245
		f 3 -204 1818 236
		mu 0 3 184 185 210
		f 3 -1819 -205 237
		mu 0 3 210 185 211
		f 3 -241 1819 -206
		mu 0 3 151 213 186
		f 3 -1820 273 272
		mu 0 3 186 213 251
		f 3 -213 1820 248
		mu 0 3 218 219 220
		f 3 -1821 279 280
		mu 0 3 220 219 261
		mc 0 3 -1 -1 8
		f 3 249 1821 278
		mu 0 3 223 221 260
		f 3 -1822 -214 -249
		mu 0 3 260 221 222
		f 3 -250 1822 -215
		mu 0 3 168 224 181
		f 3 -1823 281 -266
		mu 0 3 181 224 244
		f 3 250 1823 -216
		mu 0 3 227 225 226
		f 3 -1824 284 283
		mu 0 3 226 225 262
		f 3 251 -217 252
		mu 0 3 228 229 226
		f 3 -252 1824 -219
		mu 0 3 229 230 231
		f 3 -1825 285 -254
		mu 0 3 231 230 232
		f 3 253 1825 -220
		mu 0 3 231 232 233
		f 3 -1826 286 287
		mu 0 3 233 232 263
		f 3 256 1826 -221
		mu 0 3 199 235 201
		f 3 -1827 288 -256
		mu 0 3 201 235 264
		f 3 257 1827 -222
		mu 0 3 175 236 195
		f 3 -1828 289 -278
		mu 0 3 195 236 259
		f 3 258 1828 -223
		mu 0 3 176 237 177
		f 3 -1829 291 -43
		mu 0 3 177 237 268
		f 3 259 -225 -35
		mu 0 3 238 202 177
		f 3 -230 1829 -227
		mu 0 3 141 206 203
		f 3 -1830 294 293
		mu 0 3 203 206 271
		f 3 1527 1830 263
		mu 0 3 1169 1170 241
		f 3 -1831 -263 295
		mu 0 3 241 1170 240
		f 3 -232 1831 -231
		mu 0 3 142 207 206
		f 3 -1832 299 300
		mu 0 3 206 207 274
		f 3 265 1832 -233
		mu 0 3 181 244 207
		f 3 -1833 301 302
		mu 0 3 207 244 275
		f 3 268 1833 -236
		mu 0 3 246 247 211
		f 3 -1834 271 -238
		mu 0 3 211 247 210
		f 3 -235 1834 269
		mu 0 3 209 183 248
		f 3 -1835 -267 304
		mu 0 3 248 183 277
		f 3 270 1835 -237
		mu 0 3 249 250 227
		f 3 -1836 282 -251
		mu 0 3 227 250 225
		f 3 -240 1836 -242
		mu 0 3 150 212 213
		f 3 -1837 307 308
		mu 0 3 213 212 281
		f 3 -247 1837 -239
		mu 0 3 159 217 212
		f 3 -1838 310 309
		mu 0 3 212 217 282
		f 3 274 1838 -243
		mu 0 3 186 252 214
		mc 0 3 -1 9 -1
		f 3 -1839 313 314
		mu 0 3 214 252 284
		mc 0 3 -1 9 10
		f 3 -244 1839 275
		mu 0 3 255 253 254
		f 3 -1840 -315 315
		mu 0 3 254 253 285
		f 3 -245 1840 276
		mu 0 3 258 256 257
		f 3 -1841 -276 316
		mu 0 3 257 256 286
		mc 0 3 -1 -1 11
		f 3 277 1841 -248
		mu 0 3 195 259 217
		f 3 -1842 318 317
		mu 0 3 217 259 287
		f 3 -284 1842 -253
		mu 0 3 226 262 228
		f 3 -1843 324 323
		mu 0 3 228 262 297
		f 3 -255 1843 -258
		mu 0 3 200 234 265
		f 3 -1844 328 327
		mu 0 3 265 234 300
		f 3 -288 1844 -257
		mu 0 3 233 263 266
		f 3 -1845 330 329
		mu 0 3 266 263 301
		f 3 292 -260 -34
		mu 0 3 269 202 238
		f 3 -293 1845 -261
		mu 0 3 239 270 208
		f 3 -1846 334 -304
		mu 0 3 208 270 276
		f 3 -294 1846 -262
		mu 0 3 203 271 240
		f 3 -1847 336 337
		mu 0 3 240 271 306
		f 3 1526 1847 339
		mu 0 3 1168 1169 272
		f 3 -1848 -264 296
		mu 0 3 272 1169 241
		f 3 303 1848 -268
		mu 0 3 208 276 245
		f 3 -1849 343 342
		mu 0 3 245 276 311
		f 3 -270 1849 -269
		mu 0 3 246 278 247
		f 3 -1850 345 344
		mu 0 3 247 278 313
		f 3 -271 1850 305
		mu 0 3 250 249 279
		f 3 -1851 -272 306
		mu 0 3 279 249 280
		f 3 -309 1851 -274
		mu 0 3 213 281 251
		f 3 -1852 349 350
		mu 0 3 251 281 317
		f 3 311 1852 -273
		mu 0 3 251 283 186
		f 3 -1853 312 -275
		mu 0 3 186 283 252
		f 3 319 1853 -279
		mu 0 3 290 288 289
		f 3 -1854 357 -321
		mu 0 3 289 288 327
		f 3 -281 1854 -320
		mu 0 3 291 292 293
		f 3 -1855 358 359
		mu 0 3 293 292 328
		f 3 320 1855 -282
		mu 0 3 224 294 244
		f 3 -1856 360 361
		mu 0 3 244 294 329
		mc 0 3 -1 -1 12
		f 3 321 1856 -283
		mu 0 3 250 295 225
		f 3 -1857 363 -323
		mu 0 3 225 295 296
		f 3 322 1857 -285
		mu 0 3 225 296 262
		f 3 -1858 365 364
		mu 0 3 262 296 330
		f 3 -324 1858 -286
		mu 0 3 228 297 232
		f 3 -1859 366 -326
		mu 0 3 232 297 298
		f 3 325 1859 -287
		mu 0 3 232 298 263
		f 3 -1860 367 368
		mu 0 3 263 298 331
		f 3 326 1860 -289
		mu 0 3 235 299 264
		f 3 -1861 369 -329
		mu 0 3 264 299 333
		f 3 -290 1861 331
		mu 0 3 259 236 302
		f 3 -1862 372 371
		mu 0 3 302 236 335
		f 3 332 1862 -291
		mu 0 3 243 303 267
		f 3 -1863 375 -334
		mu 0 3 267 303 336
		f 3 333 1863 -292
		mu 0 3 237 304 268
		f 3 -1864 377 376
		mu 0 3 268 304 337
		f 3 -301 1864 -295
		mu 0 3 206 274 271
		f 3 -1865 380 381
		mu 0 3 271 274 339
		f 3 -296 1865 335
		mu 0 3 241 240 305
		f 3 -1866 -338 383
		mu 0 3 305 240 306
		f 3 -297 1866 338
		mu 0 3 272 241 307
		f 3 -1867 -336 382
		mu 0 3 307 241 305
		f 3 340 1867 -299
		mu 0 3 273 309 1208
		f 3 -333 1868 374
		mu 0 3 303 243 309
		f 3 -1578 -1868 -1869
		mu 0 3 243 1208 309
		f 3 -303 1869 -300
		mu 0 3 207 275 274
		f 3 -1870 387 388
		mu 0 3 274 275 343
		f 3 -302 1870 341
		mu 0 3 275 244 310
		f 3 -1871 -362 389
		mu 0 3 310 244 329
		f 3 -343 1871 -305
		mu 0 3 277 312 248
		f 3 -1872 392 391
		mu 0 3 248 312 345
		f 3 346 1872 -306
		mu 0 3 279 314 250
		f 3 -1873 362 -322
		mu 0 3 250 314 295
		f 3 -345 1873 -307
		mu 0 3 280 315 279
		f 3 -1874 394 -394
		mu 0 3 279 315 347
		f 3 -308 1874 347
		mu 0 3 281 212 316
		f 3 -1875 -310 348
		mu 0 3 316 212 282
		f 3 -318 1875 -311
		mu 0 3 217 287 282
		f 3 -1876 396 395
		mu 0 3 282 287 348
		f 3 -351 351 -312
		mu 0 3 251 317 283
		f 3 352 1876 -313
		mu 0 3 283 318 252
		f 3 -1877 401 -354
		mu 0 3 252 318 319
		f 3 353 1877 -314
		mu 0 3 252 319 284
		f 3 -1878 403 402
		mu 0 3 284 319 352
		f 3 -316 1878 354
		mu 0 3 322 320 321
		f 3 -1879 -403 404
		mu 0 3 321 320 353
		f 3 -317 1879 355
		mu 0 3 325 323 324
		f 3 -1880 -355 405
		mu 0 3 324 323 354
		f 3 -319 1880 356
		mu 0 3 287 259 326
		f 3 -1881 -332 373
		mu 0 3 326 259 302
		f 3 -365 1881 -325
		mu 0 3 262 330 297
		f 3 -1882 414 413
		mu 0 3 297 330 366
		f 3 -330 1882 -327
		mu 0 3 266 301 332
		mc 0 3 -1 13 -1
		f 3 -1883 417 416
		mu 0 3 332 301 369
		mc 0 3 -1 13 -1
		f 3 370 1883 -328
		mu 0 3 300 334 265
		f 3 -1884 418 -373
		mu 0 3 265 334 371
		f 3 -369 1884 -331
		mu 0 3 263 331 301
		f 3 -1885 420 419
		mu 0 3 301 331 372
		f 3 378 -335 379
		mu 0 3 338 276 270
		f 3 -382 1885 -337
		mu 0 3 271 339 306
		f 3 -1886 432 433
		mu 0 3 306 339 382
		f 3 384 1886 -339
		mu 0 3 307 340 272
		f 3 -1887 435 -386
		mu 0 3 272 340 341
		f 3 1525 1887 386
		mu 0 3 1167 1168 341
		f 3 -1888 -340 385
		mu 0 3 341 1168 272
		f 3 -342 1888 -388
		mu 0 3 275 310 343
		f 3 -1889 438 439
		mu 0 3 343 310 386
		f 3 390 1889 -344
		mu 0 3 276 344 311
		f 3 -1890 442 441
		mu 0 3 311 344 387
		f 3 -392 1890 -346
		mu 0 3 278 346 313
		f 3 -1891 445 444
		mu 0 3 313 346 389
		f 3 393 1891 -347
		mu 0 3 279 347 314
		f 3 -1892 446 447
		mu 0 3 314 347 390
		f 3 -348 1892 -350
		mu 0 3 281 316 317
		f 3 -1893 448 -399
		mu 0 3 317 316 350
		f 3 -349 1893 397
		mu 0 3 316 282 349
		f 3 -1894 -396 450
		mu 0 3 349 282 348
		f 3 398 1894 -352
		mu 0 3 317 350 283
		f 3 -1895 451 -401
		mu 0 3 283 350 351
		f 3 399 -353 400
		mu 0 3 351 318 283
		f 3 406 1895 -357
		mu 0 3 326 355 287
		f 3 -1896 449 -397
		mu 0 3 287 355 348
		f 3 -358 1896 407
		mu 0 3 356 357 358
		f 3 -1897 -409 459
		mu 0 3 358 357 403
		f 3 -360 1897 408
		mu 0 3 359 360 361
		f 3 -1898 460 461
		mu 0 3 361 360 404
		f 3 409 1898 458
		mu 0 3 362 329 402
		f 3 -1899 -361 -408
		mu 0 3 402 329 294
		f 3 410 1899 -363
		mu 0 3 314 363 295
		f 3 -1900 463 -412
		mu 0 3 295 363 364
		f 3 411 1900 -364
		mu 0 3 295 364 296
		f 3 -1901 464 -413
		mu 0 3 296 364 365
		f 3 412 1901 -366
		mu 0 3 296 365 330
		f 3 -1902 466 465
		mu 0 3 330 365 405
		f 3 -414 1902 -367
		mu 0 3 297 367 298
		f 3 -1903 467 -416
		mu 0 3 298 367 368
		f 3 415 1903 -368
		mu 0 3 298 368 331
		f 3 -1904 468 469
		mu 0 3 331 368 406
		f 3 -370 1904 -371
		mu 0 3 333 299 370
		f 3 -1905 472 471
		mu 0 3 370 299 408
		f 3 -372 1905 421
		mu 0 3 302 335 373
		f 3 -1906 475 474
		mu 0 3 373 335 410
		f 3 -374 1906 422
		mu 0 3 326 302 374
		f 3 -1907 -422 476
		mu 0 3 374 302 373
		f 3 423 1907 -375
		mu 0 3 309 375 303
		f 3 -1908 479 -425
		mu 0 3 303 375 376
		f 3 424 1908 -376
		mu 0 3 303 376 336
		f 3 -1909 480 -426
		mu 0 3 336 376 412
		f 3 425 1909 -378
		mu 0 3 304 377 337
		f 3 -1910 482 481
		mu 0 3 337 377 413
		f 3 -380 426 427
		mu 0 3 378 269 337
		f 3 428 -379 429
		mu 0 3 379 276 338
		f 3 -389 1910 -381
		mu 0 3 274 343 339
		f 3 -1911 485 484
		mu 0 3 339 343 415
		f 3 -383 1911 430
		mu 0 3 307 305 380
		f 3 -1912 -432 487
		mu 0 3 380 305 381
		f 3 -384 1912 431
		mu 0 3 305 306 381
		f 3 -1913 -434 488
		mu 0 3 381 306 382
		f 3 -385 1913 434
		mu 0 3 340 307 383
		f 3 -1914 -431 486
		mu 0 3 383 307 380
		f 3 1524 1914 478
		mu 0 3 1166 1167 384
		f 3 -1915 -387 436
		mu 0 3 384 1167 341
		f 3 -390 1915 437
		mu 0 3 310 329 385
		f 3 -1916 -410 462
		mu 0 3 385 329 362
		f 3 -391 -429 440
		mu 0 3 344 276 379
		f 3 -393 1916 443
		mu 0 3 345 312 388
		f 3 -1917 -442 495
		mu 0 3 388 312 421
		f 3 -445 1917 -395
		mu 0 3 315 391 347
		f 3 -1918 497 -497
		mu 0 3 347 391 423
		f 3 -398 1918 -449
		mu 0 3 316 349 350
		f 3 -1919 499 498
		mu 0 3 350 349 424
		f 3 452 1919 -400
		mu 0 3 351 392 318
		f 3 -1920 502 501
		mu 0 3 318 392 425
		f 3 453 1920 -402
		mu 0 3 318 393 319
		f 3 -1921 504 -455
		mu 0 3 319 393 394
		f 3 454 1921 -404
		mu 0 3 319 394 352
		f 3 -1922 505 -456
		mu 0 3 352 394 426
		f 3 455 1922 -405
		mu 0 3 395 396 397
		f 3 -1923 506 507
		mu 0 3 397 396 427
		f 3 -406 1923 456
		mu 0 3 400 398 399
		f 3 -1924 -508 508
		mu 0 3 399 398 428
		f 3 -407 1924 457
		mu 0 3 355 326 401
		f 3 -1925 -423 477
		mu 0 3 401 326 374
		f 3 -448 1925 -411
		mu 0 3 314 390 363
		f 3 -1926 514 515
		mu 0 3 363 390 438
		f 3 -466 1926 -415
		mu 0 3 330 405 366
		f 3 -1927 520 519
		mu 0 3 366 405 442
		f 3 -420 1927 -418
		mu 0 3 301 372 369
		f 3 -1928 523 522
		mu 0 3 369 372 446
		f 3 470 1928 -417
		mu 0 3 369 407 332
		f 3 -1929 525 -473
		mu 0 3 332 407 448
		f 3 473 1929 -419
		mu 0 3 334 409 371
		f 3 -1930 526 -476
		mu 0 3 371 409 450
		f 3 -470 1930 -421
		mu 0 3 331 406 372
		f 3 -1931 528 527
		mu 0 3 372 406 451
		f 3 -482 1931 -428
		mu 0 3 337 413 378
		f 3 -1932 483 -430
		mu 0 3 378 413 414
		f 3 -433 1932 489
		mu 0 3 382 339 416
		f 3 -1933 -485 536
		mu 0 3 416 339 415
		f 3 490 1933 -435
		mu 0 3 383 417 340
		f 3 -1934 540 -492
		mu 0 3 340 417 418
		f 3 491 1934 -436
		mu 0 3 340 418 341
		f 3 -1935 492 -437
		mu 0 3 341 418 384
		f 3 -438 1935 -439
		mu 0 3 310 385 386
		f 3 -1936 541 542
		mu 0 3 386 385 463
		f 3 -440 1936 -486
		mu 0 3 343 386 415
		f 3 -1937 544 543
		mu 0 3 415 386 464
		f 3 493 1937 -441
		mu 0 3 379 419 344
		f 3 -1938 545 -495
		mu 0 3 344 419 420
		f 3 494 1938 -443
		mu 0 3 344 420 387
		f 3 -1939 546 547
		mu 0 3 387 420 465
		f 3 -444 1939 -446
		mu 0 3 346 422 389
		f 3 -1940 549 548
		mu 0 3 389 422 466
		f 3 496 1940 -447
		mu 0 3 347 423 390
		f 3 -1941 550 551
		mu 0 3 390 423 467
		f 3 -450 1941 -451
		mu 0 3 348 355 349
		f 3 -1942 -510 552
		mu 0 3 349 355 429
		f 3 -499 1942 -452
		mu 0 3 350 424 351
		f 3 -1943 500 -453
		mu 0 3 351 424 392
		f 3 503 -454 -502
		mu 0 3 425 393 318
		f 3 -458 1943 509
		mu 0 3 355 401 429
		f 3 -1944 560 561
		mu 0 3 429 401 480
		f 3 -459 1944 -514
		mu 0 3 362 402 437
		f 3 -1945 510 563
		mu 0 3 437 402 430
		f 3 511 1945 -460
		mu 0 3 433 431 432
		f 3 -1946 562 -511
		mu 0 3 432 431 481
		f 3 512 1946 -462
		mu 0 3 436 434 435
		f 3 -1947 564 -512
		mu 0 3 435 434 482
		f 3 513 1947 -463
		mu 0 3 362 437 385
		f 3 -1948 565 566
		mu 0 3 385 437 483
		f 3 516 1948 -464
		mu 0 3 363 439 364
		f 3 -1949 567 -518
		mu 0 3 364 439 440
		f 3 517 1949 -465
		mu 0 3 364 440 365
		f 3 -1950 568 569
		mu 0 3 365 440 484
		f 3 -467 1950 518
		mu 0 3 405 365 441
		f 3 -1951 -570 571
		mu 0 3 441 365 484
		f 3 -468 1951 521
		mu 0 3 443 366 444
		f 3 -1952 -520 573
		mu 0 3 444 366 442
		f 3 -522 1952 -469
		mu 0 3 443 444 445
		f 3 -1953 574 575
		mu 0 3 445 444 486
		f 3 -471 1953 524
		mu 0 3 407 369 447
		f 3 -1954 -523 576
		mu 0 3 447 369 446
		f 3 -472 1954 -474
		mu 0 3 370 408 449
		f 3 -1955 580 579
		mu 0 3 449 408 489
		f 3 -475 1955 529
		mu 0 3 373 410 452
		f 3 -1956 584 585
		mu 0 3 452 410 493
		f 3 -477 1956 530
		mu 0 3 374 373 453
		f 3 -1957 -530 583
		mu 0 3 453 373 452;
	setAttr ".fc[500:999]"
		f 3 -478 1957 531
		mu 0 3 401 374 454
		f 3 -1958 -531 586
		mu 0 3 454 374 453
		f 3 1523 1958 588
		mu 0 3 1164 1166 455
		f 3 -1959 -479 532
		mu 0 3 455 1166 384
		f 3 533 1959 -480
		mu 0 3 375 456 376
		f 3 -1960 589 590
		mu 0 3 376 456 496
		f 3 -481 1960 534
		mu 0 3 412 376 457
		f 3 -1961 -591 591
		mu 0 3 457 376 496
		f 3 -535 1961 -483
		mu 0 3 377 458 413
		f 3 -1962 592 -536
		mu 0 3 413 458 459
		f 3 535 -494 -484
		mu 0 3 413 459 414
		f 3 -487 1962 537
		mu 0 3 383 380 460
		f 3 -1963 -539 594
		mu 0 3 460 380 461
		f 3 -488 1963 538
		mu 0 3 380 381 461
		f 3 -1964 595 596
		mu 0 3 461 381 497
		f 3 -490 1964 -489
		mu 0 3 382 416 381
		f 3 -1965 597 -596
		mu 0 3 381 416 497
		f 3 -491 1965 539
		mu 0 3 417 383 462
		f 3 -1966 -538 593
		mu 0 3 462 383 460
		f 3 -493 1966 -533
		mu 0 3 384 418 455
		f 3 -1967 601 600
		mu 0 3 455 418 500
		f 3 -548 1967 -496
		mu 0 3 421 465 388
		f 3 -1968 604 605
		mu 0 3 388 465 504
		f 3 -549 1968 -498
		mu 0 3 391 468 423
		f 3 -1969 607 -607
		mu 0 3 423 468 506
		f 3 -500 1969 553
		mu 0 3 424 349 469
		f 3 -1970 -553 608
		mu 0 3 469 349 429
		f 3 -501 1970 554
		mu 0 3 392 424 470
		f 3 -1971 -554 609
		mu 0 3 470 424 469
		f 3 -503 1971 555
		mu 0 3 425 392 471
		f 3 -1972 -555 610
		mu 0 3 471 392 470
		f 3 -556 556 -504
		mu 0 3 425 471 393
		f 3 -505 1972 557
		mu 0 3 394 393 472
		f 3 -1973 -557 611
		mu 0 3 472 393 471
		f 3 -506 1973 558
		mu 0 3 426 394 473
		f 3 -1974 -558 612
		mu 0 3 473 394 472
		f 3 -507 1974 559
		mu 0 3 476 474 475
		f 3 -1975 -559 613
		mu 0 3 475 474 507
		f 3 -560 1975 -509
		mu 0 3 477 478 479
		f 3 -1976 614 615
		mu 0 3 479 478 508
		f 3 -516 1976 -517
		mu 0 3 363 438 439
		f 3 -1977 622 623
		mu 0 3 439 438 516
		f 3 -552 1977 -515
		mu 0 3 390 467 438
		mc 0 3 -1 -1 14
		f 3 -1978 624 625
		mu 0 3 438 467 517
		mc 0 3 14 -1 -1
		f 3 570 1978 -519
		mu 0 3 441 485 405
		f 3 -1979 572 -521
		mu 0 3 405 485 442
		f 3 -528 1979 -524
		mu 0 3 372 451 446
		f 3 -1980 633 632
		mu 0 3 446 451 527
		f 3 577 1980 -525
		mu 0 3 447 487 407
		f 3 -1981 636 -579
		mu 0 3 407 487 488
		f 3 578 1981 -526
		mu 0 3 407 488 448
		f 3 -1982 637 -581
		mu 0 3 448 488 530
		f 3 581 1982 -527
		mu 0 3 409 490 450
		f 3 -1983 638 -585
		mu 0 3 450 490 532
		f 3 -529 1983 582
		mu 0 3 492 445 491
		f 3 -1984 -576 630
		mu 0 3 491 445 486
		f 3 587 1984 -532
		mu 0 3 454 494 401
		f 3 -1985 617 -561
		mu 0 3 401 494 480
		f 3 -544 1985 -537
		mu 0 3 415 464 416
		f 3 -1986 647 648
		mu 0 3 416 464 543
		f 3 598 1986 -540
		mu 0 3 462 498 417
		f 3 -1987 652 -600
		mu 0 3 417 498 499
		f 3 599 1987 -541
		mu 0 3 417 499 418
		f 3 -1988 653 -602
		mu 0 3 418 499 500
		f 3 -543 1988 -545
		mu 0 3 386 463 464
		f 3 -1989 655 -648
		mu 0 3 464 463 543
		f 3 -542 1989 602
		mu 0 3 463 385 501
		f 3 -1990 -567 621
		mu 0 3 501 385 483
		f 3 -593 1990 -546
		mu 0 3 459 458 502
		f 3 -1991 -647 -604
		mu 0 3 502 458 503
		f 3 603 1991 -547
		mu 0 3 502 503 465
		f 3 -1992 657 658
		mu 0 3 465 503 548
		f 3 -606 1992 -550
		mu 0 3 422 505 466
		f 3 -1993 659 660
		mu 0 3 466 505 549
		f 3 606 1993 -551
		mu 0 3 423 506 467
		f 3 -1994 662 661
		mu 0 3 467 506 550
		f 3 616 1994 -562
		mu 0 3 480 509 429
		f 3 -1995 664 -609
		mu 0 3 429 509 469
		f 3 -563 1995 618
		mu 0 3 510 511 512
		f 3 -1996 672 673
		mu 0 3 512 511 562
		f 3 -564 1996 619
		mu 0 3 437 430 462
		f 3 -1997 -619 -599
		mu 0 3 462 430 498
		f 3 620 1997 -565
		mu 0 3 515 513 514
		f 3 -1998 674 -673
		mu 0 3 514 513 563
		mc 0 3 -1 -1 16
		f 3 -620 1998 -566
		mu 0 3 437 462 483
		f 3 -1999 -594 649
		mu 0 3 483 462 460
		f 3 -568 1999 626
		mu 0 3 518 519 520
		mc 0 3 -1 -1 15
		f 3 -2000 677 678
		mu 0 3 520 519 566
		mc 0 3 15 -1 -1
		f 3 -569 2000 627
		mu 0 3 521 518 522
		f 3 -2001 -627 676
		mu 0 3 522 518 520
		f 3 -571 2001 628
		mu 0 3 485 523 524
		f 3 -2002 -630 680
		mu 0 3 524 523 525
		f 3 -572 2002 629
		mu 0 3 523 521 525
		f 3 -2003 -628 679
		mu 0 3 525 521 522
		f 3 -573 2003 -574
		mu 0 3 442 485 444
		f 3 -2004 682 -632
		mu 0 3 444 485 526
		f 3 631 2004 -575
		mu 0 3 444 526 486
		f 3 -2005 684 685
		mu 0 3 486 526 569
		f 3 -577 2005 634
		mu 0 3 447 446 528
		f 3 -2006 -633 687
		mu 0 3 528 446 527
		f 3 -578 2006 635
		mu 0 3 487 447 529
		f 3 -2007 -635 688
		mu 0 3 529 447 528
		f 3 -580 2007 -582
		mu 0 3 449 489 531
		f 3 -2008 692 693
		mu 0 3 531 489 574
		f 3 639 2008 -583
		mu 0 3 491 533 492
		f 3 -2009 686 -634
		mu 0 3 492 533 570
		f 3 -584 2009 640
		mu 0 3 453 452 534
		f 3 -2010 -642 696
		mu 0 3 534 452 535
		f 3 -586 2010 641
		mu 0 3 452 493 535
		f 3 -2011 -658 698
		mu 0 3 535 493 542
		f 3 -587 2011 642
		mu 0 3 454 453 536
		f 3 -2012 -641 697
		mu 0 3 536 453 534
		f 3 -588 2012 643
		mu 0 3 494 454 537
		f 3 -2013 -643 699
		mu 0 3 537 454 536
		f 3 1522 2013 702
		mu 0 3 1162 1165 539
		f 3 -2014 -589 644
		mu 0 3 539 1165 538
		f 3 -590 2014 645
		mu 0 3 541 495 535
		f 3 -2015 704 -697
		mu 0 3 535 495 534
		f 3 -592 2015 646
		mu 0 3 457 541 542
		f 3 -2016 -646 -699
		mu 0 3 542 541 535
		f 3 -595 2016 -650
		mu 0 3 460 461 483
		f 3 -2017 -651 -622
		mu 0 3 483 461 501
		f 3 -597 2017 650
		mu 0 3 461 497 501
		f 3 -2018 -652 -657
		mu 0 3 501 497 544
		f 3 -598 2018 651
		mu 0 3 497 416 544
		f 3 -2019 -649 705
		mu 0 3 544 416 543
		f 3 654 2019 -601
		mu 0 3 545 546 547
		f 3 -2020 701 -645
		mu 0 3 547 546 577
		f 3 656 2020 -603
		mu 0 3 501 544 463
		f 3 -2021 -706 -656
		mu 0 3 463 544 543
		f 3 -659 2021 -605
		mu 0 3 465 532 504
		f 3 -2022 -639 694
		mu 0 3 504 532 490
		f 3 663 2022 -608
		mu 0 3 468 551 506
		f 3 -2023 710 -710
		mu 0 3 506 551 586
		f 3 -610 2023 665
		mu 0 3 470 469 552
		f 3 -2024 -665 711
		mu 0 3 552 469 509
		f 3 -666 666 -611
		mu 0 3 470 552 471
		f 3 -612 2024 667
		mu 0 3 472 471 553
		f 3 -2025 -667 712
		mu 0 3 553 471 552
		mc 0 3 -1 -1 17
		f 3 -613 2025 668
		mu 0 3 473 472 554
		f 3 -2026 -668 713
		mu 0 3 554 472 553
		f 3 -669 2026 -614
		mu 0 3 555 556 557
		f 3 -2027 714 -670
		mu 0 3 557 556 587
		f 3 669 2027 -615
		mu 0 3 558 559 560
		f 3 -2028 715 716
		mu 0 3 560 559 588
		f 3 -617 2028 670
		mu 0 3 509 480 561
		f 3 -2029 -618 671
		mu 0 3 561 480 494
		f 3 675 2029 -624
		mu 0 3 564 565 519
		f 3 -2030 722 -678
		mu 0 3 519 565 566
		f 3 -626 2030 -623
		mu 0 3 438 517 516
		f 3 -2031 723 724
		mu 0 3 516 517 594
		f 3 -662 2031 -625
		mu 0 3 467 550 517
		f 3 -2032 726 725
		mu 0 3 517 550 595
		f 3 681 2032 -629
		mu 0 3 524 567 485
		f 3 -2033 732 -683
		mu 0 3 485 567 601
		f 3 -631 2033 683
		mu 0 3 491 486 568
		f 3 -2034 -686 733
		mu 0 3 568 486 569
		f 3 689 2034 -636
		mu 0 3 529 571 487
		f 3 -2035 738 -691
		mu 0 3 487 571 572
		f 3 690 2035 -637
		mu 0 3 487 572 488
		f 3 -2036 739 -692
		mu 0 3 488 572 573
		f 3 691 2036 -638
		mu 0 3 488 573 530
		f 3 -2037 740 -693
		mu 0 3 530 573 610
		f 3 -640 2037 695
		mu 0 3 533 491 575
		f 3 -2038 -684 734
		mu 0 3 575 491 568
		f 3 700 2038 -644
		mu 0 3 537 576 494
		f 3 -2039 718 -672
		mu 0 3 494 576 561
		f 3 706 2039 719
		mu 0 3 581 579 589
		f 3 -2040 -653 -674
		mu 0 3 589 579 580
		f 3 -654 2040 -655
		mu 0 3 582 583 584
		f 3 -2041 -707 748
		mu 0 3 584 583 622
		f 3 -664 -661 707
		mu 0 3 551 468 585
		f 3 -660 2041 708
		mu 0 3 549 505 574
		f 3 -2042 -695 -694
		mu 0 3 574 505 531
		f 3 709 2042 -663
		mu 0 3 506 586 550
		f 3 -2043 749 750
		mu 0 3 550 586 624
		f 3 717 -712 -671
		mu 0 3 561 552 509
		f 3 720 2043 -675
		mu 0 3 592 590 591
		f 3 -2044 755 -720
		mu 0 3 591 590 632
		f 3 -676 2044 721
		mu 0 3 565 564 593
		f 3 -2045 -725 756
		mu 0 3 593 564 633
		f 3 -677 2045 727
		mu 0 3 522 520 596
		f 3 -2046 -729 762
		mu 0 3 596 520 597
		f 3 -679 2046 728
		mu 0 3 520 566 597
		f 3 -2047 764 763
		mu 0 3 597 566 637
		f 3 -680 2047 729
		mu 0 3 525 522 598
		f 3 -2048 -728 761
		mu 0 3 598 522 596
		f 3 -681 2048 730
		mu 0 3 524 525 599
		f 3 -2049 -730 765
		mu 0 3 599 525 598
		f 3 -682 2049 731
		mu 0 3 567 524 600
		f 3 -2050 -731 766
		mu 0 3 600 524 599
		f 3 735 2050 -685
		mu 0 3 602 603 604
		f 3 -2051 772 -770
		mu 0 3 604 603 641
		mc 0 3 -1 -1 19
		f 3 -687 2051 -688
		mu 0 3 570 533 605
		f 3 -2052 -742 -737
		mu 0 3 605 533 606
		f 3 736 2052 -689
		mu 0 3 605 606 607
		f 3 -2053 774 773
		mu 0 3 607 606 643
		f 3 -690 2053 737
		mu 0 3 608 607 609
		f 3 -2054 -774 776
		mu 0 3 609 607 643
		f 3 -696 2054 741
		mu 0 3 533 575 606
		f 3 -2055 779 778
		mu 0 3 606 575 645
		f 3 -698 2055 742
		mu 0 3 536 534 611
		f 3 -2056 -705 747
		mu 0 3 611 534 495
		f 3 -700 2056 743
		mu 0 3 537 536 612
		f 3 -2057 -743 780
		mu 0 3 612 536 611
		f 3 -744 2057 -701
		mu 0 3 537 612 576
		f 3 -2058 781 782
		mu 0 3 576 612 625
		f 3 -702 2058 -747
		mu 0 3 615 613 649
		f 3 -2059 744 784
		mu 0 3 649 613 614
		f 3 745 2059 -704
		mu 0 3 618 616 617
		f 3 -2060 786 -748
		mu 0 3 617 616 653
		f 3 -703 2060 1521
		mu 0 3 1163 620 1161
		f 3 -2061 746 788
		mu 0 3 1161 620 621
		f 3 -709 2061 -708
		mu 0 3 585 610 623
		f 3 -2062 -741 -752
		mu 0 3 623 610 573
		f 3 751 2062 -711
		mu 0 3 551 573 586
		f 3 -2063 -740 777
		mu 0 3 586 573 572
		f 3 -713 2063 752
		mu 0 3 553 552 576
		f 3 -2064 -718 -719
		mu 0 3 576 552 561
		f 3 -714 2064 753
		mu 0 3 554 553 625
		f 3 -2065 -753 -783
		mu 0 3 625 553 576
		f 3 -754 2065 -715
		mu 0 3 626 627 628
		f 3 -2066 789 -755
		mu 0 3 628 627 656
		f 3 754 2066 -716
		mu 0 3 629 630 631
		f 3 -2067 790 791
		mu 0 3 631 630 657
		f 3 757 2067 -722
		mu 0 3 593 634 565
		f 3 -2068 794 -759
		mu 0 3 565 634 635
		f 3 758 2068 -723
		mu 0 3 565 635 566
		f 3 -2069 795 -765
		mu 0 3 566 635 637
		f 3 -724 2069 759
		mu 0 3 594 517 636
		f 3 -2070 -726 760
		mu 0 3 636 517 595
		f 3 -751 2070 -727
		mu 0 3 550 624 595
		f 3 -2071 798 797
		mu 0 3 595 624 661
		f 3 767 2071 -732
		mu 0 3 600 638 639
		f 3 -2072 806 -769
		mu 0 3 639 638 640
		f 3 768 2072 -733
		mu 0 3 639 640 602
		f 3 -2073 771 -736
		mu 0 3 602 640 603
		f 3 769 2073 -734
		mu 0 3 604 641 568
		f 3 -2074 808 807
		mu 0 3 568 641 675
		f 3 770 2074 -735
		mu 0 3 568 642 575
		mc 0 3 18 -1 -1
		f 3 -2075 811 810
		mu 0 3 575 642 677
		f 3 -738 2075 775
		mu 0 3 608 609 624
		f 3 -2076 814 -799
		mu 0 3 624 609 661
		f 3 -776 2076 -739
		mu 0 3 571 644 572
		f 3 -2077 -750 -778
		mu 0 3 572 644 586
		f 3 -745 2077 783
		mu 0 3 648 646 647
		f 3 -2078 -749 -756
		mu 0 3 647 646 655
		f 3 819 2078 -788
		mu 0 3 692 652 651
		f 3 785 -746 -2079
		mu 0 3 652 650 651
		f 3 -757 2079 792
		mu 0 3 593 633 658
		f 3 -2080 -760 796
		mu 0 3 658 633 660
		f 3 -758 2080 793
		mu 0 3 634 593 659
		f 3 -2081 -793 822
		mu 0 3 659 593 658
		f 3 -761 2081 799
		mu 0 3 636 595 662
		f 3 -2082 -798 827
		mu 0 3 662 595 661
		f 3 -762 2082 800
		mu 0 3 663 664 665
		f 3 -2083 -802 830
		mu 0 3 665 664 667
		f 3 -763 2083 801
		mu 0 3 664 666 667
		f 3 -2084 -803 831
		mu 0 3 667 666 669
		mc 0 3 -1 -1 20
		f 3 -764 2084 802
		mu 0 3 666 668 669
		f 3 -2085 832 833
		mu 0 3 669 668 705
		mc 0 3 -1 -1 21
		f 3 -766 2085 803
		mu 0 3 670 663 671
		f 3 -2086 -801 829
		mu 0 3 671 663 665
		f 3 -767 2086 804
		mu 0 3 673 670 672
		f 3 -2087 -804 834
		mu 0 3 672 670 706
		f 3 805 2087 -768
		mu 0 3 600 674 638
		f 3 -2088 837 838
		mu 0 3 638 674 709
		f 3 -771 2088 809
		mu 0 3 642 568 676
		f 3 -2089 -808 840
		mu 0 3 676 568 675
		f 3 812 2089 -772
		mu 0 3 679 678 603
		f 3 -2090 846 845
		mu 0 3 603 678 713
		f 3 -773 2090 813
		mu 0 3 641 603 680
		f 3 -2091 -846 847
		mu 0 3 680 603 713
		f 3 -779 2091 -775
		mu 0 3 606 645 643
		f 3 -2092 849 848
		mu 0 3 643 645 714
		f 3 -777 2092 815
		mu 0 3 609 643 681
		f 3 -2093 -849 851
		mu 0 3 681 643 714
		f 3 -780 2093 816
		mu 0 3 645 575 682
		f 3 -2094 -811 843
		mu 0 3 682 575 677
		f 3 -781 2094 817
		mu 0 3 683 684 685
		f 3 -2095 -787 820
		mu 0 3 685 684 693
		f 3 -818 2095 -782
		mu 0 3 686 687 688
		f 3 -2096 821 -790
		mu 0 3 688 687 694
		f 3 818 2096 -786
		mu 0 3 691 689 690
		f 3 -2097 853 -821
		mu 0 3 690 689 716
		f 3 -822 2097 -791
		mu 0 3 695 696 697
		f 3 -2098 -854 854
		mu 0 3 697 696 717
		f 3 823 2098 -794
		mu 0 3 700 698 699
		f 3 -2099 857 -825
		mu 0 3 699 698 701
		f 3 824 2099 -795
		mu 0 3 699 701 702
		f 3 -2100 858 -826
		mu 0 3 702 701 703
		f 3 825 2100 -796
		mu 0 3 702 703 668
		f 3 -2101 859 -833
		mu 0 3 668 703 705
		f 3 -797 2101 826
		mu 0 3 658 660 704
		f 3 -2102 -800 828
		mu 0 3 704 660 662
		f 3 835 2102 -805
		mu 0 3 672 707 673
		f 3 -2103 836 -806
		mu 0 3 673 707 708
		f 3 839 2103 -807
		mu 0 3 638 710 679
		f 3 -2104 844 -813
		mu 0 3 679 710 678
		f 3 -814 2104 -809
		mu 0 3 641 680 675
		f 3 -2105 871 870
		mu 0 3 675 680 728
		f 3 841 2105 -810
		mu 0 3 676 711 642
		f 3 -2106 874 -843
		mu 0 3 642 711 712
		f 3 842 2106 -812
		mu 0 3 642 712 677
		f 3 -2107 876 875
		mu 0 3 677 712 731
		f 3 -815 2107 -828
		mu 0 3 661 609 662
		f 3 -2108 -816 850
		mu 0 3 662 609 681
		f 3 852 2108 -817
		mu 0 3 682 715 645
		f 3 -2109 885 886
		mu 0 3 645 715 744
		f 3 -823 2109 855
		mu 0 3 659 658 718
		f 3 -2110 -827 860
		mu 0 3 718 658 704
		f 3 -824 2110 856
		mu 0 3 698 700 719
		f 3 -2111 -856 887
		mu 0 3 719 700 745
		f 3 -829 2111 861
		mu 0 3 704 662 720
		f 3 -2112 -851 882
		mu 0 3 720 662 740
		f 3 -830 2112 862
		mu 0 3 671 665 721
		f 3 -2113 -864 895
		mu 0 3 721 665 722
		f 3 -831 2113 863
		mu 0 3 665 667 722
		f 3 -2114 -865 896
		mu 0 3 722 667 723
		f 3 -832 2114 864
		mu 0 3 667 669 723
		f 3 -2115 -866 897
		mu 0 3 723 669 724
		f 3 -834 2115 865
		mu 0 3 669 705 724
		f 3 -2116 899 898
		mu 0 3 724 705 751
		f 3 -835 2116 866
		mu 0 3 672 671 725
		f 3 -2117 -863 894
		mu 0 3 725 671 721
		f 3 -836 2117 867
		mu 0 3 707 672 726
		f 3 -2118 -867 900
		mu 0 3 726 672 725
		f 3 868 2118 -837
		mu 0 3 707 727 708
		f 3 -2119 903 -870
		mu 0 3 708 727 712
		f 3 -839 2119 -840
		mu 0 3 638 709 710
		f 3 -2120 904 905
		mu 0 3 710 709 753
		f 3 869 2120 -838
		mu 0 3 674 712 709
		f 3 -2121 -875 906
		mu 0 3 709 712 711
		f 3 -841 2121 872
		mu 0 3 676 675 729
		f 3 -2122 -871 907
		mu 0 3 729 675 728
		f 3 -842 2122 873
		mu 0 3 711 676 730
		f 3 -2123 -873 908
		mu 0 3 730 676 729
		f 3 -844 2123 877
		mu 0 3 682 677 732
		f 3 -2124 -876 910
		mu 0 3 732 677 731
		f 3 878 2124 -845
		mu 0 3 710 733 678
		f 3 -2125 912 -880
		mu 0 3 678 733 734
		f 3 879 2125 -847
		mu 0 3 678 734 735
		f 3 -2126 914 913
		mu 0 3 735 734 754
		f 3 -848 2126 880
		mu 0 3 737 735 736
		f 3 -2127 -914 915
		mu 0 3 736 735 754
		f 3 -850 2127 881
		mu 0 3 738 645 739
		f 3 -2128 -887 917
		mu 0 3 739 645 744
		f 3 -852 2128 883
		mu 0 3 741 738 742
		f 3 -2129 -882 916
		mu 0 3 742 738 739
		f 3 -853 2129 884
		mu 0 3 715 682 743
		f 3 -2130 -878 911
		mu 0 3 743 682 732
		f 3 888 2130 -857
		mu 0 3 719 746 698
		f 3 -2131 924 -890
		mu 0 3 698 746 747
		f 3 889 2131 -858
		mu 0 3 698 747 701
		f 3 -2132 925 -891
		mu 0 3 701 747 748
		f 3 890 2132 -859
		mu 0 3 701 748 703
		f 3 -2133 926 -892
		mu 0 3 703 748 749
		f 3 891 2133 -860
		mu 0 3 703 749 705
		f 3 -2134 927 -900
		mu 0 3 705 749 751
		f 3 -861 2134 892
		mu 0 3 718 704 750
		f 3 -2135 -862 893
		mu 0 3 750 704 720
		f 3 901 2135 -868
		mu 0 3 726 752 707
		f 3 -2136 902 -869
		mu 0 3 707 752 727
		f 3 -881 2136 -872
		mu 0 3 737 736 728
		f 3 -2137 939 938
		mu 0 3 728 736 772
		f 3 909 2137 -874
		mu 0 3 730 753 711
		f 3 -2138 -905 -907
		mu 0 3 711 753 709
		f 3 -904 2138 -877
		mu 0 3 712 727 731
		f 3 -2139 -938 943
		mu 0 3 731 727 771
		f 3 -906 2139 -879
		mu 0 3 710 753 733
		f 3 -2140 -943 946
		mu 0 3 733 753 775
		f 3 -883 2140 918
		mu 0 3 755 741 756
		f 3 -2141 -884 919
		mu 0 3 756 741 742
		f 3 920 2141 -885
		mu 0 3 743 757 715
		f 3 -2142 955 -922
		mu 0 3 715 757 758
		f 3 921 2142 -886
		mu 0 3 715 758 744
		f 3 -2143 956 957
		mu 0 3 744 758 790
		f 3 -888 2143 922
		mu 0 3 759 745 760
		f 3 -2144 -893 928
		mu 0 3 760 745 762
		f 3 -889 2144 923
		mu 0 3 746 719 761
		f 3 -2145 -923 958
		mu 0 3 761 719 760
		f 3 -894 2145 929
		mu 0 3 750 755 763
		f 3 -2146 -919 952
		mu 0 3 763 755 756
		f 3 -895 2146 930
		mu 0 3 725 721 764
		f 3 -2147 -932 966
		mu 0 3 764 721 765
		f 3 -896 2147 931
		mu 0 3 721 722 765
		f 3 -2148 -933 967
		mu 0 3 765 722 766
		f 3 -897 2148 932
		mu 0 3 722 723 766
		f 3 -2149 -934 968
		mu 0 3 766 723 767
		f 3 -898 2149 933
		mu 0 3 723 724 767
		f 3 -2150 970 969
		mu 0 3 767 724 797
		f 3 -899 2150 -971
		mu 0 3 724 751 797
		f 3 -2151 934 971
		mu 0 3 797 751 768
		f 3 -901 2151 935
		mu 0 3 726 725 769
		f 3 -2152 -931 965
		mu 0 3 769 725 764
		f 3 -902 2152 936
		mu 0 3 752 726 770
		f 3 -2153 -936 972
		mu 0 3 770 726 769
		f 3 -903 2153 937
		mu 0 3 727 752 771
		f 3 -2154 975 974
		mu 0 3 771 752 799
		f 3 -908 2154 940
		mu 0 3 729 728 773
		f 3 -2155 -939 977
		mu 0 3 773 728 772
		f 3 -909 2155 941
		mu 0 3 730 729 774
		f 3 -2156 -941 978
		mu 0 3 774 729 773
		f 3 -910 2156 942
		mu 0 3 753 730 775
		f 3 -2157 -942 979
		mu 0 3 775 730 774
		f 3 -911 2157 944
		mu 0 3 732 731 776
		f 3 -2158 -944 980
		mu 0 3 776 731 771
		f 3 -912 2158 945
		mu 0 3 743 732 777
		f 3 -2159 -982 982
		mu 0 3 777 732 796
		f 3 947 2159 -913
		mu 0 3 778 779 780
		f 3 -2160 2160 -950
		mu 0 3 780 779 781
		f 3 -2161 1025 1026
		mu 0 3 781 779 833
		f 3 948 -915 949
		mu 0 3 781 782 780
		f 3 950 2161 -916
		mu 0 3 783 784 785
		f 3 -2162 984 985
		mu 0 3 785 784 805
		f 3 951 2162 -917
		mu 0 3 739 786 742
		f 3 -2163 987 986
		mu 0 3 742 786 806
		f 3 -918 2163 -952
		mu 0 3 739 744 786
		f 3 -2164 -958 988
		mu 0 3 786 744 790
		f 3 -920 2164 953
		mu 0 3 788 742 787
		f 3 -2165 -987 990
		mu 0 3 787 742 806
		f 3 -921 2165 954
		mu 0 3 757 743 789
		f 3 -2166 -946 983
		mu 0 3 789 743 777
		f 3 959 2166 -924
		mu 0 3 761 791 746
		f 3 -2167 997 -961
		mu 0 3 746 791 792
		f 3 960 2167 -925
		mu 0 3 746 792 747
		f 3 -2168 998 -962
		mu 0 3 747 792 793
		f 3 961 2168 -926
		mu 0 3 747 793 748
		f 3 -2169 999 -963
		mu 0 3 748 793 794
		f 3 962 2169 -927
		mu 0 3 748 794 749
		f 3 -2170 1000 1001
		mu 0 3 749 794 813
		f 3 -935 2170 1002
		mu 0 3 768 751 813
		f 3 -2171 -928 -1002
		mu 0 3 813 751 749
		f 3 -929 2171 963
		mu 0 3 760 762 795
		f 3 -2172 -965 1003
		mu 0 3 795 762 802
		f 3 -930 2172 964
		mu 0 3 750 763 796
		f 3 -2173 1005 -983
		mu 0 3 796 763 777
		f 3 973 2173 -937
		mu 0 3 770 798 752
		f 3 -2174 1014 -976
		mu 0 3 752 798 799
		f 3 -940 2174 976
		mu 0 3 801 785 800
		f 3 -2175 -986 1016
		mu 0 3 800 785 805
		f 3 -945 2175 981
		mu 0 3 732 776 802
		f 3 -2176 1021 -1004
		mu 0 3 802 776 795
		f 3 -947 2176 -948
		mu 0 3 778 803 779
		f 3 -2177 1023 1024
		mu 0 3 779 803 832
		f 3 -949 2177 -951
		mu 0 3 783 804 784
		f 3 -2178 1027 1028
		mu 0 3 784 804 834
		f 3 -953 2178 989
		mu 0 3 763 788 807
		f 3 -2179 -954 991
		mu 0 3 807 788 787
		f 3 992 2179 -955
		mu 0 3 789 808 757
		f 3 -2180 1033 -994
		mu 0 3 757 808 809
		f 3 993 2180 -956
		mu 0 3 757 809 758
		f 3 -2181 1034 -995
		mu 0 3 758 809 810
		f 3 994 2181 -957
		mu 0 3 758 810 790
		f 3 -2182 1035 1036
		mu 0 3 790 810 841
		f 3 -959 2182 995
		mu 0 3 761 760 811
		f 3 -2183 -964 1004
		mu 0 3 811 760 795
		f 3 -960 2183 996
		mu 0 3 791 761 812
		f 3 -2184 -996 1037
		mu 0 3 812 761 811
		f 3 -966 2184 1006
		mu 0 3 769 764 814
		f 3 -2185 -1008 1045
		mu 0 3 814 764 816
		f 3 -967 2185 1007
		mu 0 3 764 815 816
		f 3 -2186 -1009 1047
		mu 0 3 816 815 852
		f 3 -968 2186 1008
		mu 0 3 815 766 817
		f 3 -2187 -1010 1048
		mu 0 3 817 766 820
		f 3 -969 2187 1009
		mu 0 3 818 819 820
		f 3 -2188 -1011 1049
		mu 0 3 820 819 822
		f 3 -970 2188 1010
		mu 0 3 819 821 822
		f 3 -2189 -1012 1050
		mu 0 3 822 821 824
		f 3 1011 2189 1052
		mu 0 3 824 821 853
		f 3 -2190 -972 1051
		mu 0 3 853 821 823
		f 3 -973 2190 1012
		mu 0 3 770 769 825
		f 3 -2191 -1007 1046
		mu 0 3 825 769 814
		f 3 -974 2191 1013
		mu 0 3 798 770 826
		f 3 -2192 -1013 1053
		mu 0 3 826 770 825
		f 3 1015 2192 -975
		mu 0 3 799 827 771
		f 3 -2193 1020 -981
		mu 0 3 771 827 776
		f 3 -977 2193 1017
		mu 0 3 772 800 828
		f 3 -2194 1055 1056
		mu 0 3 828 800 855
		f 3 -978 -1018 1018
		mu 0 3 829 772 828
		f 3 1092 2194 1093
		mu 0 3 875 831 828
		f 3 -1020 2195 -2195
		mu 0 3 831 830 828
		f 3 -979 -1019 -2196
		mu 0 3 830 829 828
		f 3 1019 2196 -980
		mu 0 3 830 831 803
		f 3 -2197 1057 -1024
		mu 0 3 803 831 832
		f 3 -984 2197 1022
		mu 0 3 789 777 807
		f 3 -2198 -1006 -990
		mu 0 3 807 777 763
		f 3 -985 2198 1029
		mu 0 3 805 784 835
		f 3 -2199 -1029 1061
		mu 0 3 835 784 834
		f 3 1030 2199 -988
		mu 0 3 836 837 838
		f 3 -2200 1063 1064
		mu 0 3 838 837 858
		f 3 -989 2200 -1031
		mu 0 3 836 790 837
		f 3 -2201 -1037 1065
		mu 0 3 837 790 841
		f 3 -991 2201 1031
		mu 0 3 787 838 839
		f 3 -2202 -1065 1067
		mu 0 3 839 838 858
		f 3 -992 2202 1032
		mu 0 3 807 787 840
		f 3 -2203 -1032 1066
		mu 0 3 840 787 839
		f 3 -1023 2203 -993
		mu 0 3 789 807 808
		f 3 -2204 -1033 1068
		mu 0 3 808 807 840
		f 3 1038 2204 -997
		mu 0 3 812 842 791;
	setAttr ".fc[1000:1499]"
		f 3 -2205 1074 -1040
		mu 0 3 791 842 843
		f 3 1039 2205 -998
		mu 0 3 791 843 844
		f 3 -2206 1075 -1041
		mu 0 3 844 843 865
		f 3 1040 2206 -999
		mu 0 3 844 845 793
		f 3 -2207 1076 -1042
		mu 0 3 793 845 846
		f 3 1041 2207 -1000
		mu 0 3 848 846 847
		f 3 -2208 1077 -1043
		mu 0 3 847 846 849
		f 3 1042 2208 -1001
		mu 0 3 847 849 850
		f 3 -2209 1078 -1044
		mu 0 3 850 849 851
		f 3 -1003 2209 -1052
		mu 0 3 823 850 853
		f 3 -2210 1043 1079
		mu 0 3 853 850 851
		f 3 -1005 2210 1044
		mu 0 3 811 795 827
		f 3 -2211 -1022 -1021
		mu 0 3 827 795 776
		f 3 -1014 2211 1054
		mu 0 3 798 826 854
		f 3 -2212 1089 1088
		mu 0 3 854 826 864
		f 3 -1015 2212 -1016
		mu 0 3 799 798 827
		f 3 -2213 -1055 -1081
		mu 0 3 827 798 854
		f 3 -1030 2213 -1017
		mu 0 3 805 835 800
		f 3 -2214 1090 -1056
		mu 0 3 800 835 855
		f 3 1058 2214 -1025
		mu 0 3 832 856 779
		f 3 -2215 1059 -1026
		mu 0 3 779 856 833
		f 3 1060 2215 -1027
		mu 0 3 833 857 804
		f 3 -2216 1062 -1028
		mu 0 3 804 857 834
		f 3 -1034 2216 1069
		mu 0 3 861 859 860
		f 3 -2217 1100 1099
		mu 0 3 860 859 886
		f 3 -1070 2217 -1035
		mu 0 3 861 860 862
		f 3 -2218 2218 -1071
		mu 0 3 862 860 863
		f 3 -2219 1127 1128
		mu 0 3 863 860 900
		f 3 1070 1071 -1036
		mu 0 3 862 863 841
		f 3 -1038 2219 1072
		mu 0 3 812 811 854
		f 3 -2220 -1045 1080
		mu 0 3 854 811 827
		f 3 -1039 2220 1073
		mu 0 3 842 812 864
		f 3 -2221 -1073 -1089
		mu 0 3 864 812 854
		f 3 1081 2221 -1046
		mu 0 3 816 866 814
		f 3 -2222 1107 -1083
		mu 0 3 814 866 867
		f 3 1082 2222 -1047
		mu 0 3 814 867 825
		f 3 -2223 1109 1108
		mu 0 3 825 867 893
		f 3 -1048 2223 -1082
		mu 0 3 816 852 866
		f 3 -2224 -1084 1110
		mu 0 3 866 852 868
		f 3 -1049 2224 1083
		mu 0 3 817 820 868
		f 3 -2225 -1085 1111
		mu 0 3 868 820 869
		f 3 -1050 2225 1084
		mu 0 3 820 822 869
		f 3 -2226 -1086 1112
		mu 0 3 869 822 870
		f 3 -1051 2226 1085
		mu 0 3 822 824 870
		f 3 -2227 -1087 1113
		mu 0 3 870 824 871
		f 3 -1053 2227 1086
		mu 0 3 824 853 871
		f 3 -2228 1114 1115
		mu 0 3 871 853 894
		f 3 -1054 2228 1087
		mu 0 3 872 825 873
		f 3 -2229 -1109 1116
		mu 0 3 873 825 895
		f 3 1091 2229 -1057
		mu 0 3 855 874 828
		f 3 -2230 1117 -1094
		mu 0 3 828 874 875
		f 3 -1058 2230 -1059
		mu 0 3 832 831 856
		f 3 -2231 -1093 1118
		mu 0 3 856 831 875
		f 3 -1060 2231 -1061
		mu 0 3 833 856 857
		f 3 -2232 1119 1120
		mu 0 3 857 856 896
		f 3 -1063 2232 -1062
		mu 0 3 834 857 835
		f 3 -2233 -1121 1121
		mu 0 3 835 857 896
		f 3 -1064 2233 1094
		mu 0 3 876 877 878
		f 3 -2234 1122 1123
		mu 0 3 878 877 898
		f 3 1095 2234 -1066
		mu 0 3 880 879 877
		f 3 -2235 1124 -1123
		mu 0 3 877 879 898
		f 3 -1067 2235 1096
		mu 0 3 881 882 883
		f 3 -1098 2236 -2236
		mu 0 3 882 884 883
		f 3 -2237 1152 1153
		mu 0 3 883 884 917
		f 3 1097 -1068 1098
		mu 0 3 884 882 885
		f 3 -1097 2237 -1069
		mu 0 3 881 883 859
		f 3 -2238 1125 -1101
		mu 0 3 859 883 886
		f 3 -1072 2238 -1096
		mu 0 3 841 863 879
		f 3 -2239 1129 1130
		mu 0 3 879 863 901
		f 3 -1074 2239 1101
		mu 0 3 842 864 887
		f 3 -2240 1132 1131
		mu 0 3 887 864 902
		f 3 -1075 2240 1102
		mu 0 3 843 842 888
		f 3 -2241 -1102 1133
		mu 0 3 888 842 887
		f 3 -1103 2241 -1076
		mu 0 3 843 888 865
		f 3 -2242 1134 -1104
		mu 0 3 865 888 889
		f 3 1103 2242 -1077
		mu 0 3 845 889 846
		f 3 -2243 1135 -1105
		mu 0 3 846 889 890
		f 3 1104 2243 -1078
		mu 0 3 846 890 849
		f 3 -2244 1136 -1106
		mu 0 3 849 890 891
		f 3 1105 2244 -1079
		mu 0 3 849 891 851
		f 3 -2245 1137 -1107
		mu 0 3 851 891 892
		f 3 1106 2245 -1080
		mu 0 3 851 892 853
		f 3 -2246 1138 -1115
		mu 0 3 853 892 894
		f 3 -1088 2246 -1090
		mu 0 3 872 873 864
		f 3 -2247 1146 -1133
		mu 0 3 864 873 913
		f 3 -1091 2247 -1092
		mu 0 3 855 835 874
		f 3 -2248 -1122 1148
		mu 0 3 874 835 896
		f 3 -1095 2248 -1099
		mu 0 3 876 878 897
		f 3 -2249 1150 1149
		mu 0 3 897 878 915
		f 3 1126 2249 -1100
		mu 0 3 886 899 860
		f 3 -2250 1154 -1128
		mu 0 3 860 899 900
		f 3 1139 2250 -1108
		mu 0 3 866 903 867
		f 3 -2251 1166 -1141
		mu 0 3 867 903 904
		f 3 1140 2251 -1110
		mu 0 3 867 904 893
		f 3 -2252 1168 1167
		mu 0 3 893 904 929
		f 3 1141 2252 -1111
		mu 0 3 868 905 866
		f 3 -2253 1165 -1140
		mu 0 3 866 905 928
		f 3 1142 2253 -1112
		mu 0 3 907 906 868
		f 3 -2254 1169 -1142
		mu 0 3 868 906 905
		f 3 1143 2254 -1113
		mu 0 3 909 908 907
		f 3 -2255 1170 -1143
		mu 0 3 907 908 906
		f 3 -1114 2255 -1144
		mu 0 3 909 910 908
		f 3 -2256 -1146 1171
		mu 0 3 908 910 912
		f 3 -1116 1144 1145
		mu 0 3 910 911 912
		f 3 -1117 2256 1147
		mu 0 3 873 895 914
		f 3 -2257 -1168 1174
		mu 0 3 914 895 931
		f 3 -1118 2257 -1119
		mu 0 3 875 874 856
		f 3 -2258 -1149 -1120
		mu 0 3 856 874 896
		f 3 1151 2258 -1124
		mu 0 3 898 916 878
		f 3 -2259 1175 -1151
		mu 0 3 878 916 915
		f 3 -1125 2259 -1152
		mu 0 3 898 879 916
		f 3 -2260 -1131 1156
		mu 0 3 916 879 901
		f 3 -1126 2260 -1127
		mu 0 3 886 883 899
		f 3 -2261 -1154 1178
		mu 0 3 899 883 917
		f 3 1155 2261 -1129
		mu 0 3 900 918 863
		f 3 -2262 1157 -1130
		mu 0 3 863 918 901
		f 3 -1132 2262 1158
		mu 0 3 887 902 919
		f 3 -2263 1180 1179
		mu 0 3 919 902 933
		f 3 -1134 2263 1159
		mu 0 3 888 887 920
		f 3 -2264 -1159 1181
		mu 0 3 920 887 919
		f 3 -1135 2264 1160
		mu 0 3 889 888 921
		f 3 -2265 -1160 1182
		mu 0 3 921 888 934
		f 3 -1136 2265 1161
		mu 0 3 923 889 922
		f 3 -2266 -1161 1183
		mu 0 3 922 889 921
		f 3 -1137 2266 1162
		mu 0 3 925 923 924
		f 3 -2267 -1162 1184
		mu 0 3 924 923 922
		f 3 -1163 2267 -1138
		mu 0 3 925 924 926
		f 3 -2268 1185 -1164
		mu 0 3 926 924 927
		f 3 1163 1164 -1139
		mu 0 3 926 927 911
		f 3 -1145 1172 1173
		mu 0 3 912 911 930
		f 3 -1148 2268 -1147
		mu 0 3 873 914 913
		f 3 -2269 1195 -1181
		mu 0 3 913 914 942
		f 3 1176 2269 -1150
		mu 0 3 915 932 897
		f 3 -2270 1177 -1153
		mu 0 3 897 932 917
		f 3 -1155 2270 -1156
		mu 0 3 900 899 918
		f 3 -2271 1198 1197
		mu 0 3 918 899 944
		f 3 -1158 2271 -1157
		mu 0 3 901 918 916
		f 3 -2272 -1198 1199
		mu 0 3 916 918 944
		f 3 -1165 1186 -1173
		mu 0 3 911 927 930
		f 3 1187 2272 -1166
		mu 0 3 905 935 928
		f 3 -2273 1208 -1189
		mu 0 3 928 935 951
		f 3 1188 2273 -1167
		mu 0 3 903 936 904
		f 3 -2274 1209 -1190
		mu 0 3 904 936 937
		f 3 1189 2274 -1169
		mu 0 3 904 937 929
		f 3 -2275 1211 1210
		mu 0 3 929 937 952
		f 3 1190 2275 -1170
		mu 0 3 906 938 905
		f 3 -2276 1207 -1188
		mu 0 3 905 938 935
		f 3 1191 2276 -1171
		mu 0 3 908 939 906
		f 3 -2277 1212 -1191
		mu 0 3 906 939 938
		f 3 1192 2277 -1172
		mu 0 3 912 940 908
		f 3 -2278 1213 -1192
		mu 0 3 908 940 939
		f 3 -1174 1193 1194
		mu 0 3 912 930 941
		f 3 -1175 2278 1196
		mu 0 3 914 931 943
		f 3 -2279 -1211 1215
		mu 0 3 943 931 953
		f 3 -1176 2279 -1177
		mu 0 3 915 916 932
		f 3 -2280 -1200 1216
		mu 0 3 932 916 944
		f 3 -1178 2280 -1179
		mu 0 3 917 932 899
		f 3 -2281 -1217 -1199
		mu 0 3 899 932 944
		f 3 -1180 2281 1200
		mu 0 3 919 933 945
		f 3 -2282 1218 1219
		mu 0 3 945 933 954
		f 3 -1182 2282 1201
		mu 0 3 920 919 946
		f 3 -2283 -1201 1217
		mu 0 3 946 919 945
		f 3 -1183 2283 1202
		mu 0 3 921 934 947
		f 3 -2284 -1202 1220
		mu 0 3 947 934 955
		f 3 -1184 2284 1203
		mu 0 3 922 921 948
		f 3 -2285 -1203 1221
		mu 0 3 948 921 947
		f 3 -1185 2285 1204
		mu 0 3 924 922 949
		f 3 -2286 -1204 1222
		mu 0 3 949 922 948
		f 3 -1186 2286 1205
		mu 0 3 927 924 950
		f 3 -2287 -1205 1223
		mu 0 3 950 924 949
		f 3 1206 -1194 -1187
		mu 0 3 927 941 930
		f 3 -1195 1214 -1193
		mu 0 3 912 941 940
		f 3 -1197 2287 -1196
		mu 0 3 914 943 942
		f 3 -2288 1234 -1219
		mu 0 3 942 943 969
		f 3 -1206 1224 -1207
		mu 0 3 927 950 941
		f 3 -1208 1225 1226
		mu 0 3 956 957 958
		f 3 1227 -1209 1228
		mu 0 3 959 951 956
		f 3 -1210 2288 1229
		mu 0 3 961 951 960
		f 3 -2289 -1246 1247
		mu 0 3 960 951 984
		f 3 -1212 2289 1230
		mu 0 3 952 961 962
		f 3 -2290 -1230 1248
		mu 0 3 962 961 960
		f 3 1231 2290 -1213
		mu 0 3 964 963 957
		f 3 -2291 1244 -1226
		mu 0 3 957 963 958
		f 3 1232 2291 -1214
		mu 0 3 965 966 964
		f 3 -2292 1250 -1232
		mu 0 3 964 966 963
		f 3 1233 2292 -1215
		mu 0 3 967 968 965
		f 3 -2293 1251 -1233
		mu 0 3 965 968 966
		f 3 1235 2293 -1216
		mu 0 3 972 970 971
		f 3 -2294 1254 1253
		mu 0 3 971 970 989
		f 3 1236 2294 -1218
		mu 0 3 973 974 955
		f 3 -2295 1256 1257
		mu 0 3 955 974 990
		f 3 1237 2295 -1220
		mu 0 3 954 975 973
		f 3 -2296 1255 -1237
		mu 0 3 973 975 974
		f 3 1238 -1221 1239
		mu 0 3 976 977 955
		f 3 1240 1241 -1222
		mu 0 3 977 978 979
		f 3 -1223 2296 1242
		mu 0 3 981 979 980
		f 3 -2297 -1242 1260
		mu 0 3 980 979 978
		f 3 -1224 2297 1243
		mu 0 3 983 981 982
		f 3 -2298 -1243 1261
		mu 0 3 982 981 980
		f 3 -1225 2298 -1234
		mu 0 3 967 983 968
		f 3 -2299 -1244 1262
		mu 0 3 968 983 982
		f 3 -1227 2299 -1229
		mu 0 3 956 958 959
		f 3 -2300 1264 1263
		mu 0 3 959 958 993
		f 3 1245 -1228 1246
		mu 0 3 984 951 985
		f 3 -1236 -1231 1249
		mu 0 3 986 952 962
		f 3 -1235 2300 1252
		mu 0 3 987 971 988
		f 3 -2301 -1254 1273
		mu 0 3 988 971 989
		f 3 1258 -1238 -1253
		mu 0 3 991 975 954
		f 3 1259 -1240 -1258
		mu 0 3 990 992 955
		f 3 -1239 2301 -1241
		mu 0 3 977 976 978
		f 3 -2302 1281 1280
		mu 0 3 978 976 1016
		f 3 1265 2302 -1245
		mu 0 3 963 994 958
		f 3 -2303 1284 -1265
		mu 0 3 958 994 993
		f 3 -1247 1266 1267
		mu 0 3 984 985 995
		f 3 1268 2303 -1248
		mu 0 3 996 997 998
		f 3 -2304 1286 -1270
		mu 0 3 998 997 999
		f 3 1269 2304 -1249
		mu 0 3 998 999 1000
		f 3 -2305 1288 1287
		mu 0 3 1000 999 1021
		f 3 1270 2305 -1250
		mu 0 3 962 1001 986
		f 3 -2306 1289 -1275
		mu 0 3 986 1001 1024
		f 3 1271 2306 -1251
		mu 0 3 966 1002 963
		f 3 -2307 1285 -1266
		mu 0 3 963 1002 994
		f 3 1272 2307 -1252
		mu 0 3 1003 1004 1005
		f 3 -2308 1290 -1272
		mu 0 3 1005 1004 1025
		f 3 1274 2308 -1255
		mu 0 3 1008 1006 1007
		f 3 -2309 1293 1292
		mu 0 3 1007 1006 1028
		f 3 -1256 2309 1275
		mu 0 3 1011 1009 1010
		f 3 -2310 1295 1294
		mu 0 3 1010 1009 1029
		f 3 -1257 2310 1276
		mu 0 3 1012 1011 1013
		f 3 -2311 -1276 1296
		mu 0 3 1013 1011 1010
		f 3 -1259 2311 1277
		mu 0 3 975 991 1014
		f 3 -2312 -1292 1297
		mu 0 3 1014 991 1030
		f 3 1278 1279 -1260
		mu 0 3 990 1015 992
		f 3 -1261 2312 1282
		mu 0 3 980 978 1017
		f 3 -2313 -1281 1298
		mu 0 3 1017 978 1016
		f 3 -1262 2313 1283
		mu 0 3 982 980 1018
		f 3 -2314 -1283 1299
		mu 0 3 1018 980 1017
		f 3 -1263 2314 -1273
		mu 0 3 1003 1019 1004
		f 3 -2315 -1284 1300
		mu 0 3 1004 1019 1034
		f 3 -1264 2315 -1267
		mu 0 3 985 1020 995
		f 3 -2316 1302 1303
		mu 0 3 995 1020 1037
		f 3 -1268 2316 -1269
		mu 0 3 996 995 997
		f 3 -2317 1305 1306
		mu 0 3 997 995 1039
		f 3 -1288 2317 -1271
		mu 0 3 962 1022 1023
		f 3 -2318 1309 1310
		mu 0 3 1023 1022 1042
		f 3 -1274 2318 1291
		mu 0 3 1027 1007 1026
		f 3 -2319 -1293 1313
		mu 0 3 1026 1007 1028
		f 3 -1277 2319 -1279
		mu 0 3 1012 1013 1015
		f 3 -2320 1318 1317
		mu 0 3 1015 1013 1047
		f 3 -1278 2320 -1296
		mu 0 3 975 1031 1032
		f 3 -2321 1321 1320
		mu 0 3 1032 1031 1049
		f 3 -1280 2321 -1282
		mu 0 3 992 1015 1033
		f 3 -2322 1322 1323
		mu 0 3 1033 1015 1050
		f 3 1301 2322 -1285
		mu 0 3 1036 1035 1020
		f 3 -2323 1327 -1303
		mu 0 3 1020 1035 1037
		f 3 1304 2323 -1286
		mu 0 3 1002 1038 1036
		f 3 -2324 1326 -1302
		mu 0 3 1036 1038 1035
		f 3 1307 2324 -1287
		mu 0 3 997 1040 999
		f 3 -2325 1332 -1309
		mu 0 3 999 1040 1041
		f 3 1308 2325 -1289
		mu 0 3 999 1041 1021
		f 3 -2326 1334 1333
		mu 0 3 1021 1041 1057
		f 3 1311 2326 -1290
		mu 0 3 1001 1038 1024
		f 3 -2327 -1305 -1315
		mu 0 3 1024 1038 1002
		f 3 1312 2327 -1291
		mu 0 3 1043 1028 1044
		f 3 -2328 -1294 1314
		mu 0 3 1044 1028 1006
		f 3 -1295 2328 1315
		mu 0 3 1010 1029 1045
		f 3 -2329 1340 1339
		mu 0 3 1045 1029 1061
		f 3 -1297 2329 1316
		mu 0 3 1013 1010 1046
		f 3 -2330 -1316 1341
		mu 0 3 1046 1010 1045
		f 3 -1298 2330 1319
		mu 0 3 1014 1030 1048
		f 3 -2331 -1339 -1326
		mu 0 3 1048 1030 1018
		f 3 -1299 2331 1324
		mu 0 3 1052 1033 1051
		f 3 -2332 -1324 1348
		mu 0 3 1051 1033 1050
		f 3 -1300 2332 1325
		mu 0 3 1018 1052 1048
		f 3 -2333 -1325 1350
		mu 0 3 1048 1052 1051
		f 3 -1301 2333 -1313
		mu 0 3 1043 1053 1028
		f 3 -2334 1338 -1314
		mu 0 3 1028 1053 1026
		f 3 1328 2334 -1304
		mu 0 3 1054 1055 995
		f 3 -2335 1330 -1306
		mu 0 3 995 1055 1039
		f 3 1329 2335 -1307
		mu 0 3 1039 1056 997
		f 3 -2336 1331 -1308
		mu 0 3 997 1056 1040
		f 3 1335 2336 -1311
		mu 0 3 1042 1058 1023
		f 3 -2337 1337 -1312
		mu 0 3 1023 1058 1060
		f 3 -1310 2337 1336
		mu 0 3 1042 1022 1059
		f 3 -2338 -1334 1358
		mu 0 3 1059 1022 1074
		f 3 1342 2338 -1317
		mu 0 3 1046 1062 1013
		f 3 -2339 1343 -1319
		mu 0 3 1013 1062 1047
		f 3 1344 2339 -1318
		mu 0 3 1047 1063 1015
		f 3 -2340 1349 -1323
		mu 0 3 1015 1063 1067
		f 3 1345 2340 -1320
		mu 0 3 1065 1064 1031
		f 3 -2341 1346 -1322
		mu 0 3 1031 1064 1049
		f 3 1347 2341 -1321
		mu 0 3 1049 1066 1032
		f 3 -2342 1362 -1341
		mu 0 3 1032 1066 1076
		f 3 -1327 2342 1351
		mu 0 3 1068 1060 1069
		f 3 -2343 -1338 1361
		mu 0 3 1069 1060 1058
		f 3 -1352 2343 -1328
		mu 0 3 1068 1069 1054
		f 3 -2344 1352 -1329
		mu 0 3 1054 1069 1055
		f 3 -1330 2344 1353
		mu 0 3 1056 1039 1070
		f 3 -2345 -1331 1354
		mu 0 3 1070 1039 1055
		f 3 1355 2345 -1332
		mu 0 3 1056 1071 1040
		f 3 -2346 1377 -1357
		mu 0 3 1040 1071 1072
		f 3 1356 2346 -1333
		mu 0 3 1040 1072 1041
		f 3 -2347 1378 1379
		mu 0 3 1041 1072 1086
		f 3 -1335 2347 1357
		mu 0 3 1057 1041 1073
		f 3 -2348 -1380 1380
		mu 0 3 1073 1041 1086
		f 3 -1336 2348 1359
		mu 0 3 1058 1042 1075
		f 3 -2349 -1337 1360
		mu 0 3 1075 1042 1059
		f 3 1363 2349 -1340
		mu 0 3 1061 1077 1045
		f 3 -2350 1388 1387
		mu 0 3 1045 1077 1090
		f 3 -1342 2350 1364
		mu 0 3 1046 1045 1078
		f 3 -2351 -1388 1390
		mu 0 3 1078 1045 1090
		f 3 -1343 2351 1365
		mu 0 3 1062 1046 1079
		f 3 -2352 -1365 1389
		mu 0 3 1079 1046 1078
		f 3 1366 2352 -1344
		mu 0 3 1062 1080 1047
		f 3 -2353 1367 -1345
		mu 0 3 1047 1080 1063
		f 3 -1346 2353 1368
		mu 0 3 1064 1065 1081
		f 3 -2354 -1351 -1372
		mu 0 3 1081 1065 1083
		f 3 1369 2354 -1347
		mu 0 3 1064 1082 1049
		f 3 -2355 1370 -1348
		mu 0 3 1049 1082 1066
		f 3 -1349 2355 1371
		mu 0 3 1083 1067 1081
		f 3 -2356 -1350 1372
		mu 0 3 1081 1067 1063
		f 3 1373 2356 -1353
		mu 0 3 1069 1084 1055
		f 3 -2357 1375 -1355
		mu 0 3 1055 1084 1070
		f 3 1374 2357 -1354
		mu 0 3 1070 1085 1056
		f 3 -2358 1376 -1356
		mu 0 3 1056 1085 1071
		f 3 1381 2358 -1358
		mu 0 3 1073 1087 1074
		f 3 -2359 1382 -1359
		mu 0 3 1074 1087 1059
		f 3 1383 2359 -1360
		mu 0 3 1075 1084 1058
		f 3 -2360 -1374 -1362
		mu 0 3 1058 1084 1069
		f 3 -1361 2360 1384
		mu 0 3 1075 1059 1088
		f 3 -2361 -1383 1404
		mu 0 3 1088 1059 1087
		f 3 1385 2361 -1363
		mu 0 3 1066 1089 1076
		f 3 -2362 1386 -1364
		mu 0 3 1076 1089 1077
		f 3 1391 2362 -1366
		mu 0 3 1079 1091 1062
		f 3 -2363 1392 -1367
		mu 0 3 1062 1091 1080
		f 3 1393 2363 -1368
		mu 0 3 1080 1092 1063
		f 3 -2364 -1395 -1373
		mu 0 3 1063 1092 1081
		f 3 1394 2364 -1369
		mu 0 3 1081 1092 1064
		f 3 -2365 1395 -1370
		mu 0 3 1064 1092 1082
		f 3 1396 2365 -1371
		mu 0 3 1082 1093 1066
		f 3 -2366 1406 -1386
		mu 0 3 1066 1093 1089
		f 3 -1375 2366 1397
		mu 0 3 1085 1070 1094
		f 3 -2367 -1376 1398
		mu 0 3 1094 1070 1084
		f 3 1399 2367 -1377
		mu 0 3 1085 1095 1096
		f 3 -2368 1417 -1401
		mu 0 3 1096 1095 1097
		f 3 1400 2368 -1378
		mu 0 3 1096 1097 1098
		f 3 -2369 1418 -1402
		mu 0 3 1098 1097 1099
		f 3 1401 2369 -1379
		mu 0 3 1098 1099 1100
		f 3 -2370 1419 -1403
		mu 0 3 1100 1099 1101
		f 3 1402 2370 -1381
		mu 0 3 1100 1101 1102
		f 3 -2371 1421 1420
		mu 0 3 1102 1101 1115
		f 3 -1382 2371 1403
		mu 0 3 1087 1073 1103
		f 3 -2372 -1421 1423
		mu 0 3 1103 1073 1115
		f 3 -1384 2372 -1399
		mu 0 3 1084 1075 1094
		f 3 -2373 -1385 1405
		mu 0 3 1094 1075 1088
		f 3 1407 2373 -1387
		mu 0 3 1089 1104 1077
		f 3 -2374 1427 1428
		mu 0 3 1077 1104 1118
		f 3 -1389 2374 1408
		mu 0 3 1105 1106 1107
		f 3 -2375 -1429 1430
		mu 0 3 1107 1106 1118
		f 3 -1390 2375 1409
		mu 0 3 1110 1108 1109
		f 3 -2376 -1411 1431
		mu 0 3 1109 1108 1111
		f 3 -1391 2376 1410
		mu 0 3 1108 1105 1111
		f 3 -2377 -1409 1429
		mu 0 3 1111 1105 1107
		f 3 -1392 2377 1411
		mu 0 3 1091 1110 1112
		f 3 -2378 -1410 1432
		mu 0 3 1112 1110 1109
		f 3 1412 2378 -1393
		mu 0 3 1091 1113 1080
		f 3 -2379 1413 -1394
		mu 0 3 1080 1113 1092
		f 3 -1414 2379 -1396
		mu 0 3 1092 1113 1082
		f 3 -2380 1414 -1397
		mu 0 3 1082 1113 1093
		f 3 1415 2380 -1398
		mu 0 3 1094 1114 1085
		f 3 -2381 1416 -1400
		mu 0 3 1085 1114 1095
		f 3 1422 2381 -1404
		mu 0 3 1103 1116 1087
		f 3 -2382 1424 -1405
		mu 0 3 1087 1116 1088
		f 3 -1406 2382 -1416
		mu 0 3 1094 1088 1114
		f 3 -2383 -1425 1441
		mu 0 3 1114 1088 1127
		f 3 1425 2383 -1407
		mu 0 3 1093 1117 1089
		f 3 -2384 1426 -1408
		mu 0 3 1089 1117 1104
		f 3 1433 2384 -1412
		mu 0 3 1112 1119 1091
		f 3 -2385 1434 -1413
		mu 0 3 1091 1119 1113
		f 3 -1435 2385 -1415
		mu 0 3 1113 1119 1093
		f 3 -2386 1442 -1426
		mu 0 3 1093 1119 1128
		f 3 -1417 2386 1435
		mu 0 3 1122 1120 1121
		f 3 -2387 1450 1449
		mu 0 3 1121 1120 1136
		f 3 -1436 1436 -1418
		mu 0 3 1095 1123 1097
		f 3 -1419 2387 1437
		mu 0 3 1099 1097 1124
		f 3 -2388 -1437 1451
		mu 0 3 1124 1097 1123
		f 3 -1438 2388 -1420
		mu 0 3 1099 1124 1101
		f 3 -2389 1452 1453
		mu 0 3 1101 1124 1137
		f 3 -1422 2389 1438
		mu 0 3 1115 1101 1125
		f 3 -2390 -1454 1454
		mu 0 3 1125 1101 1137
		f 3 1439 -1423 1440
		mu 0 3 1126 1116 1103
		f 3 -1424 2390 -1441
		mu 0 3 1103 1115 1126
		f 3 -2391 -1439 1455
		mu 0 3 1126 1115 1125
		f 3 1443 -1427 1444
		mu 0 3 1129 1104 1117
		f 3 1445 2391 1456
		mu 0 3 1130 1118 1129
		f 3 -2392 -1428 -1444
		mu 0 3 1129 1118 1104
		f 3 -1430 2392 1446
		mu 0 3 1111 1107 1131
		f 3 -2393 1457 1458
		mu 0 3 1131 1107 1138
		f 3 -1446 2393 -1431
		mu 0 3 1118 1130 1107
		f 3 -2394 1459 -1458
		mu 0 3 1107 1130 1138
		f 3 -1447 2394 -1432
		mu 0 3 1111 1131 1109
		f 3 -2395 1460 -1448
		mu 0 3 1109 1131 1132
		f 3 -1433 1447 1448
		mu 0 3 1112 1109 1132
		f 3 -1449 2395 -1434
		mu 0 3 1133 1134 1135
		f 3 -2396 1461 1462
		mu 0 3 1135 1134 1139
		f 3 -1440 2396 -1442
		mu 0 3 1116 1126 1120
		f 3 -2397 1463 -1451
		mu 0 3 1120 1126 1136
		f 3 -1443 2397 -1445
		mu 0 3 1117 1135 1129
		f 3 -2398 -1463 1465
		mu 0 3 1129 1135 1139
		f 3 1464 2398 -1450
		mu 0 3 1136 1137 1121
		f 3 -2399 -1453 -1452
		mu 0 3 1121 1137 1124
		f 3 -1465 2399 -1455
		mu 0 3 1137 1136 1125
		f 3 -2400 -1464 -1456
		mu 0 3 1125 1136 1126
		f 3 -1466 2400 -1457
		mu 0 3 1129 1139 1130
		f 3 -2401 1466 -1460
		mu 0 3 1130 1139 1138
		f 3 -1459 2401 -1461
		mu 0 3 1131 1138 1134
		f 3 -2402 -1467 -1462
		mu 0 3 1134 1138 1139
		f 3 1467 1468 1469
		mu 0 3 1140 1141 1142
		f 3 1470 -1468 1471
		mu 0 3 9 1141 1140
		f 3 -1469 1472 1473
		mu 0 3 1142 1141 1143
		f 3 -1473 -1471 1474
		mu 0 3 1143 1141 9
		f 3 -1472 -1470 1475
		mu 0 3 59 1144 71
		f 3 -1474 -1475 -1476
		mu 0 3 71 52 59
		f 3 1476 1477 1478
		mu 0 3 1145 1146 1147
		f 3 1479 1480 -1477
		mu 0 3 1145 1148 1146
		f 3 -1478 1481 1482
		mu 0 3 1147 1146 32
		f 3 1483 -1482 -1481
		mu 0 3 1148 32 1146
		f 3 -1479 -1483 1484
		mu 0 3 269 1149 268
		f 3 -1480 -1485 -1484
		mu 0 3 337 269 268
		f 3 -1486 2402 4
		mu 0 3 7 3 1
		f 3 -2403 2 0
		mu 0 3 1 3 0
		f 3 -1487 34 37
		mu 0 3 29 23 24
		f 3 -1556 2403 1559
		mu 0 3 116 1191 1196
		f 3 -2404 1560 -1565
		mu 0 3 1196 1191 1190
		f 3 1500 2404 1598
		mu 0 3 1159 140 1219
		f 3 -161 2405 -2405
		mu 0 3 140 82 1219
		f 3 -1500 1503 -2406
		mu 0 3 82 1158 1219
		f 3 1502 2406 -94
		mu 0 3 69 1216 60
		f 3 -2407 2407 106
		mu 0 3 60 1216 82
		f 3 -2408 1592 1499
		mu 0 3 82 1216 1158
		f 3 1557 2408 -1574
		mu 0 3 1193 1153 1197
		f 3 -2409 -1566 1569
		mu 0 3 1197 1153 1198
		f 3 -1491 2409 -1584
		mu 0 3 1209 1157 1203
		f 3 -2410 -1575 1579
		mu 0 3 1203 1157 1204
		f 3 290 2410 1613
		mu 0 3 243 267 1226
		f 3 -259 2411 -2411
		mu 0 3 267 180 1226
		f 3 -1502 1505 -2412
		mu 0 3 180 1160 1226
		f 3 1504 2412 -1501
		mu 0 3 1159 1223 140
		f 3 -2413 2413 -195
		mu 0 3 140 1223 180
		f 3 -2414 1606 1501
		mu 0 3 180 1223 1160
		f 3 -1560 2414 -229
		mu 0 3 116 1196 1214
		f 3 -2415 1587 -1592
		mu 0 3 1214 1196 1210
		f 3 1593 2415 -1598
		mu 0 3 1217 1158 1215
		f 3 -2416 -1593 1594
		mu 0 3 1215 1158 1216
		f 3 1600 2416 -1606
		mu 0 3 1221 1159 1218
		f 3 -2417 -1599 1601
		mu 0 3 1218 1159 1219
		f 3 -1607 2417 1607
		mu 0 3 1160 1223 1224
		f 3 -2418 1608 -1613
		mu 0 3 1224 1223 1222
		f 3 -1614 2418 1578
		mu 0 3 243 1226 1209
		f 3 -2419 1615 -1619
		mu 0 3 1209 1226 1225
		f 3 -1522 2419 -1508
		mu 0 3 1163 1161 619
		f 3 -2420 1506 787
		mu 0 3 619 1161 654
		f 3 -1523 2420 -1509
		mu 0 3 1165 1162 540
		f 3 -2421 1507 703
		mu 0 3 540 1162 578
		f 3 -1524 2421 -1510
		mu 0 3 1166 1164 411
		f 3 -2422 1508 -534
		mu 0 3 411 1164 495
		f 3 -1525 2422 -1511
		mu 0 3 1167 1166 342
		f 3 -2423 1509 -424
		mu 0 3 342 1166 411
		f 3 -1526 2423 -1512
		mu 0 3 1168 1167 308
		f 3 -2424 1510 -341
		mu 0 3 308 1167 342
		f 3 -1577 2424 1511
		mu 0 3 308 1207 1168
		f 3 -2425 2425 -1527
		mu 0 3 1168 1207 1169
		f 3 -1513 -2426 -298
		mu 0 3 242 1169 1207
		f 3 -1514 2426 -228
		mu 0 3 204 1170 1212
		f 3 -1528 2427 -2427
		mu 0 3 1170 1169 1212
		f 3 1512 -1585 -2428
		mu 0 3 1169 242 1212
		f 3 -1559 2428 1513
		mu 0 3 204 1195 1170
		f 3 -2429 2429 -1529
		mu 0 3 1170 1195 1171
		f 3 -199 -1515 -2430
		mu 0 3 1195 178 1171
		f 3 -1516 2430 -164
		mu 0 3 117 1172 1202
		f 3 -1530 2431 -2431
		mu 0 3 1172 1171 1202
		f 3 -1569 -2432 1514
		mu 0 3 178 1202 1171
		f 3 -1517 2432 -123
		mu 0 3 84 1173 117
		f 3 -2433 -1531 1515
		mu 0 3 117 1173 1172
		f 3 107 2433 -95
		mu 0 3 78 1174 84
		f 3 -2434 -1532 1516
		mu 0 3 84 1174 1173
		f 3 103 2434 -89
		mu 0 3 66 1175 78
		f 3 -2435 -1533 -108
		mu 0 3 78 1175 1174
		f 3 -1534 2435 136
		mu 0 3 1177 1176 108
		f 3 -2436 -104 135
		mu 0 3 108 1176 92
		f 3 -137 2436 -1535
		mu 0 3 1178 132 1179
		f 3 -2437 188 187
		mu 0 3 1179 132 167
		f 3 -1536 2437 -1624
		mu 0 3 1230 1153 1228
		f 3 -2438 1491 1621
		mu 0 3 1228 1153 1192
		f 3 -1620 2438 -1537
		mu 0 3 1180 1229 1184
		f 3 -2439 1624 -1627
		mu 0 3 1184 1229 1231
		f 3 1565 2439 -1630
		mu 0 3 1198 1153 1232
		f 3 -2440 1535 1627
		mu 0 3 1232 1153 1230
		f 3 -1540 2440 -1635
		mu 0 3 1235 1156 1233
		f 3 -2441 1495 1632
		mu 0 3 1233 1156 1204;
	setAttr ".fc[1500:1999]"
		f 3 -1631 2441 -1541
		mu 0 3 1181 1234 1188
		f 3 -2442 1635 -1638
		mu 0 3 1188 1234 1236
		f 3 1585 2442 -1641
		mu 0 3 1213 1156 1237
		f 3 -2443 1539 1638
		mu 0 3 1237 1156 1235
		f 3 1536 2443 1543
		mu 0 3 1180 1184 1183
		f 3 -2444 1547 -1547
		mu 0 3 1183 1184 1182
		f 3 1544 2444 -1548
		mu 0 3 1184 1150 1182
		f 3 -2445 1493 1548
		mu 0 3 1182 1150 1185
		f 3 1540 2445 1549
		mu 0 3 1181 1188 1187
		f 3 -2446 1553 -1553
		mu 0 3 1187 1188 1186
		f 3 1550 2446 -1554
		mu 0 3 1188 1154 1186
		f 3 -2447 1497 1554
		mu 0 3 1186 1154 1189
		f 3 -1489 2447 -1561
		mu 0 3 1191 1152 1190
		f 3 -2448 -1557 1561
		mu 0 3 1190 1152 1192
		f 3 -1558 2448 -1492
		mu 0 3 1153 1193 1192
		f 3 -2449 1562 -1562
		mu 0 3 1192 1193 1190
		f 3 -163 2449 -1563
		mu 0 3 1193 139 1190
		f 3 -2450 198 1563
		mu 0 3 1190 139 1194
		f 3 1558 2450 -1564
		mu 0 3 1194 179 1190
		f 3 -2451 197 1564
		mu 0 3 1190 179 1196
		f 3 -1495 2451 -1570
		mu 0 3 1198 1151 1197
		f 3 -2452 -1567 1570
		mu 0 3 1197 1151 1199
		f 3 -1568 2452 -122
		mu 0 3 69 1200 1199
		f 3 -2453 1571 -1571
		mu 0 3 1199 1200 1197
		f 3 -124 2453 -1572
		mu 0 3 1200 96 1197
		f 3 -2454 163 1572
		mu 0 3 1197 96 1201
		f 3 162 2454 1568
		mu 0 3 139 1193 1201
		f 3 -2455 1573 -1573
		mu 0 3 1201 1193 1197
		f 3 -1496 2455 -1580
		mu 0 3 1204 1156 1203
		f 3 -2456 -1576 1580
		mu 0 3 1203 1156 1205
		f 3 297 2456 -265
		mu 0 3 205 1206 1205
		f 3 -2457 1581 -1581
		mu 0 3 1205 1206 1203
		f 3 1576 2457 -1582
		mu 0 3 1206 273 1203
		f 3 -2458 298 1582
		mu 0 3 1203 273 1208
		f 3 -1579 2458 1577
		mu 0 3 243 1209 1208
		f 3 -2459 1583 -1583
		mu 0 3 1208 1209 1203
		f 3 -198 2459 -1588
		mu 0 3 1196 179 1210
		f 3 -2460 227 1588
		mu 0 3 1210 179 1211
		f 3 1584 2460 -1589
		mu 0 3 1211 205 1210
		f 3 -2461 264 1589
		mu 0 3 1210 205 1205
		f 3 -1586 2461 1575
		mu 0 3 1156 1213 1205
		f 3 -2462 1590 -1590
		mu 0 3 1205 1213 1210
		f 3 -1499 2462 -1591
		mu 0 3 1213 1155 1210
		f 3 -2463 -1587 1591
		mu 0 3 1210 1155 1214
		f 3 121 2463 -1503
		mu 0 3 69 1199 1216
		f 3 -2464 1595 -1595
		mu 0 3 1216 1199 1215
		f 3 1566 2464 -1596
		mu 0 3 1199 1151 1215
		f 3 -2465 -1546 1596
		mu 0 3 1215 1151 1185
		f 3 1487 2465 -1494
		mu 0 3 1150 1217 1185
		f 3 -2466 1597 -1597
		mu 0 3 1185 1217 1215
		f 3 -1594 2466 -1504
		mu 0 3 1158 1217 1219
		f 3 -2467 1602 -1602
		mu 0 3 1219 1217 1218
		f 3 -1600 2467 -1488
		mu 0 3 1150 1220 1217
		f 3 -2468 1603 -1603
		mu 0 3 1217 1220 1218
		f 3 -1493 2468 -1604
		mu 0 3 1220 1152 1218
		f 3 -2469 1488 1604
		mu 0 3 1218 1152 1191
		f 3 164 2469 1555
		mu 0 3 116 1221 1191
		f 3 -2470 1605 -1605
		mu 0 3 1191 1221 1218
		f 3 -1505 2470 -1609
		mu 0 3 1223 1159 1222
		f 3 -2471 -1601 1609
		mu 0 3 1222 1159 1221
		f 3 228 2471 -165
		mu 0 3 116 1214 1221
		f 3 -2472 1610 -1610
		mu 0 3 1221 1214 1222
		f 3 1586 2472 -1611
		mu 0 3 1214 1155 1222
		f 3 -2473 -1552 1611
		mu 0 3 1222 1155 1189
		f 3 1489 2473 -1498
		mu 0 3 1154 1224 1189
		f 3 -2474 1612 -1612
		mu 0 3 1189 1224 1222
		f 3 -1506 2474 -1616
		mu 0 3 1226 1160 1225
		f 3 -2475 -1608 1616
		mu 0 3 1225 1160 1224
		f 3 -1615 2475 -1490
		mu 0 3 1154 1227 1224
		f 3 -2476 1617 -1617
		mu 0 3 1224 1227 1225
		f 3 -1497 2476 -1618
		mu 0 3 1227 1157 1225
		f 3 -2477 1490 1618
		mu 0 3 1225 1157 1209
		f 3 1556 2477 -1622
		mu 0 3 1192 1152 1228
		f 3 -2478 1537 1622
		mu 0 3 1228 1152 1229
		f 3 -1621 2478 1619
		mu 0 3 1180 1230 1229
		f 3 -2479 1623 -1623
		mu 0 3 1229 1230 1228
		f 3 1492 2479 -1538
		mu 0 3 1152 1220 1229
		f 3 -2480 1625 -1625
		mu 0 3 1229 1220 1231
		f 3 1599 2480 -1626
		mu 0 3 1220 1150 1231
		f 3 -2481 -1545 1626
		mu 0 3 1231 1150 1184
		f 3 -1544 2481 1620
		mu 0 3 1180 1183 1230
		f 3 -2482 1628 -1628
		mu 0 3 1230 1183 1232
		f 3 -1539 2482 -1629
		mu 0 3 1183 1151 1232
		f 3 -2483 1494 1629
		mu 0 3 1232 1151 1198
		f 3 1574 2483 -1633
		mu 0 3 1204 1157 1233
		f 3 -2484 1541 1633
		mu 0 3 1233 1157 1234
		f 3 -1632 2484 1630
		mu 0 3 1181 1235 1234
		f 3 -2485 1634 -1634
		mu 0 3 1234 1235 1233
		f 3 1496 2485 -1542
		mu 0 3 1157 1227 1234
		f 3 -2486 1636 -1636
		mu 0 3 1234 1227 1236
		f 3 1614 2486 -1637
		mu 0 3 1227 1154 1236
		f 3 -2487 -1551 1637
		mu 0 3 1236 1154 1188
		f 3 -1550 2487 1631
		mu 0 3 1181 1187 1235
		f 3 -2488 1639 -1639
		mu 0 3 1235 1187 1237
		f 3 -1543 2488 -1640
		mu 0 3 1187 1155 1237
		f 3 -2489 1498 1640
		mu 0 3 1237 1155 1213
		f 3 -185 2489 -1643
		mu 0 3 1238 1239 1241
		f 3 -2490 1641 1643
		mu 0 3 1241 1239 1240
		f 3 -186 2490 -1642
		mu 0 3 1242 1243 1245
		f 3 -2491 1644 1645
		mu 0 3 1245 1243 1244
		f 3 -188 2491 -1648
		mu 0 3 1246 1247 1249
		f 3 -2492 1646 1648
		mu 0 3 1249 1247 1248
		f 3 1649 2492 -209
		mu 0 3 1251 1252 1250
		f 3 -2493 1651 -1651
		mu 0 3 1250 1252 1253
		f 3 1650 2493 -210
		mu 0 3 1255 1256 1254
		f 3 -2494 1652 -1647
		mu 0 3 1254 1256 1257
		f 3 1653 2494 -246
		mu 0 3 1259 1260 1258
		f 3 -2495 1654 -1650
		mu 0 3 1258 1260 1261
		f 3 1655 2495 -277
		mu 0 3 1263 1264 1262
		f 3 -2496 1656 -1654
		mu 0 3 1262 1264 1265
		f 3 -280 2496 -1658
		mu 0 3 1266 1267 1269
		f 3 -2497 1642 1658
		mu 0 3 1269 1267 1268
		f 3 1659 2497 -356
		mu 0 3 1271 1272 1270
		f 3 -2498 1660 -1656
		mu 0 3 1270 1272 1273
		f 3 -359 2498 -1662
		mu 0 3 1274 1275 1277
		f 3 -2499 1657 1662
		mu 0 3 1277 1275 1276
		f 3 1663 2499 -457
		mu 0 3 1279 1280 1278
		f 3 -2500 1664 -1660
		mu 0 3 1278 1280 1281
		f 3 -461 2500 -1666
		mu 0 3 1282 1283 1285
		f 3 -2501 1661 1666
		mu 0 3 1285 1283 1284
		f 3 1665 2501 -513
		mu 0 3 1287 1288 1286
		f 3 -2502 1668 -1668
		mu 0 3 1286 1288 1289
		f 3 -616 2502 -1664
		mu 0 3 1290 1291 1293
		f 3 -2503 1669 1670
		mu 0 3 1293 1291 1292
		f 3 1667 2503 -621
		mu 0 3 1295 1296 1294
		f 3 -2504 1672 -1672
		mu 0 3 1294 1296 1297
		f 3 -717 2504 -1670
		mu 0 3 1298 1299 1301
		f 3 -2505 1673 1674
		mu 0 3 1301 1299 1300
		f 3 1671 2505 -721
		mu 0 3 1303 1304 1302
		f 3 -2506 1676 -1676
		mu 0 3 1302 1304 1305
		f 3 1675 2506 -784
		mu 0 3 1307 1308 1306
		f 3 -2507 1678 -1678
		mu 0 3 1306 1308 1309
		f 3 1677 2507 -785
		mu 0 3 1311 1312 1310
		f 3 -2508 1680 -1680
		mu 0 3 1310 1312 1313
		f 3 1679 2508 -789
		mu 0 3 1315 1316 1314
		f 3 -2509 1682 -1682
		mu 0 3 1314 1316 1317
		f 3 -792 2509 -1674
		mu 0 3 1318 1319 1321
		f 3 -2510 1683 1684
		mu 0 3 1321 1319 1320
		f 3 -819 2510 -1687
		mu 0 3 1322 1323 1325
		f 3 -2511 1685 1687
		mu 0 3 1325 1323 1324
		f 3 -820 2511 -1686
		mu 0 3 1326 1327 1329
		f 3 -2512 1688 1689
		mu 0 3 1329 1327 1328
		f 3 -855 2512 -1684
		mu 0 3 1330 1331 1333
		f 3 -2513 1686 1690
		mu 0 3 1333 1331 1332
		f 3 1681 2513 -1507
		mu 0 3 1335 1336 1334
		f 3 -2514 1691 -1689
		mu 0 3 1334 1336 1337
		f 3 -1521 2514 -1645
		mu 0 3 1338 1339 1341
		f 3 -2515 1647 1692
		mu 0 3 1341 1339 1340
		f 3 -1644 1693 -1695
		mu 0 3 1342 1343 1344
		f 3 -1646 1695 -1694
		mu 0 3 1345 1346 1347
		f 3 -1649 1696 -1698
		mu 0 3 1348 1349 1350
		f 3 -1652 1698 -1700
		mu 0 3 1351 1352 1353
		f 3 -1653 1699 -1697
		mu 0 3 1354 1355 1356
		f 3 -1655 1700 -1699
		mu 0 3 1357 1358 1359
		f 3 -1657 1701 -1701
		mu 0 3 1360 1361 1362
		f 3 -1659 1694 -1703
		mu 0 3 1363 1364 1365
		f 3 -1661 1703 -1702
		mu 0 3 1366 1367 1368
		f 3 -1663 1702 -1705
		mu 0 3 1369 1370 1371
		f 3 -1665 1705 -1704
		mu 0 3 1372 1373 1374
		f 3 -1667 1704 -1707
		mu 0 3 1375 1376 1377
		f 3 -1669 1706 -1708
		mu 0 3 1378 1379 1380
		f 3 -1671 1708 -1706
		mu 0 3 1381 1382 1383
		f 3 -1673 1707 -1710
		mu 0 3 1384 1385 1386
		f 3 -1675 1710 -1709
		mu 0 3 1387 1388 1389
		f 3 -1677 1709 -1712
		mu 0 3 1390 1391 1392
		f 3 -1679 1711 -1713
		mu 0 3 1393 1394 1395
		f 3 -1681 1712 -1714
		mu 0 3 1396 1397 1398
		f 3 -1683 1713 -1715
		mu 0 3 1399 1400 1401
		f 3 -1685 1715 -1711
		mu 0 3 1402 1403 1404
		f 3 -1688 1716 -1718
		mu 0 3 1405 1406 1407
		f 3 -1690 1718 -1717
		mu 0 3 1408 1409 1410
		f 3 -1691 1717 -1716
		mu 0 3 1411 1412 1413
		f 3 -1692 1714 -1719
		mu 0 3 1414 1415 1416
		f 3 -1693 1697 -1696
		mu 0 3 1417 1418 1419
		f 3 2515 2667 -2588
		mu 0 3 1420 1421 1422
		f 3 -2668 2588 -2524
		mu 0 3 1422 1421 1423
		f 3 2516 2668 -2589
		mu 0 3 1421 1424 1423
		f 3 -2669 2589 -2525
		mu 0 3 1423 1424 1425
		f 3 2517 2669 -2590
		mu 0 3 1424 1426 1425
		f 3 -2670 2590 -2526
		mu 0 3 1425 1426 1427
		f 3 2518 2670 -2591
		mu 0 3 1426 1428 1427
		f 3 -2671 2591 -2527
		mu 0 3 1427 1428 1429
		f 3 2519 2671 -2592
		mu 0 3 1428 1430 1429
		f 3 -2672 2592 -2528
		mu 0 3 1429 1430 1431
		f 3 2520 2672 -2593
		mu 0 3 1430 1432 1431
		f 3 -2673 2593 -2529
		mu 0 3 1431 1432 1433
		f 3 2521 2673 -2594
		mu 0 3 1432 1434 1433
		f 3 -2674 2594 -2530
		mu 0 3 1433 1434 1435
		f 3 2522 2674 -2595
		mu 0 3 1434 1436 1435
		f 3 -2675 2587 -2531
		mu 0 3 1435 1436 1437
		f 3 2523 2675 -2596
		mu 0 3 1422 1423 1438
		f 3 -2676 2596 -2532
		mu 0 3 1438 1423 1439
		f 3 2524 2676 -2597
		mu 0 3 1423 1425 1439
		f 3 -2677 2597 -2533
		mu 0 3 1439 1425 1440
		f 3 2525 2677 -2598
		mu 0 3 1425 1427 1440
		f 3 -2678 2598 -2534
		mu 0 3 1440 1427 1441
		f 3 2526 2678 -2599
		mu 0 3 1427 1429 1441
		f 3 -2679 2599 -2535
		mu 0 3 1441 1429 1442
		f 3 2527 2679 -2600
		mu 0 3 1429 1431 1442
		f 3 -2680 2600 -2536
		mu 0 3 1442 1431 1443
		f 3 2528 2680 -2601
		mu 0 3 1431 1433 1443
		f 3 -2681 2601 -2537
		mu 0 3 1443 1433 1444
		f 3 2529 2681 -2602
		mu 0 3 1433 1435 1444
		f 3 -2682 2602 -2538
		mu 0 3 1444 1435 1445
		f 3 2530 2682 -2603
		mu 0 3 1435 1437 1445
		f 3 -2683 2595 -2539
		mu 0 3 1445 1437 1446
		f 3 2531 2683 -2604
		mu 0 3 1438 1439 1447
		f 3 -2684 2604 -2540
		mu 0 3 1447 1439 1448
		f 3 2532 2684 -2605
		mu 0 3 1439 1440 1448
		f 3 -2685 2605 -2541
		mu 0 3 1448 1440 1449
		f 3 2533 2685 -2606
		mu 0 3 1440 1441 1449
		f 3 -2686 2606 -2542
		mu 0 3 1449 1441 1450
		f 3 2534 2686 -2607
		mu 0 3 1441 1442 1450
		f 3 -2687 2607 -2543
		mu 0 3 1450 1442 1451
		f 3 2535 2687 -2608
		mu 0 3 1442 1443 1451
		f 3 -2688 2608 -2544
		mu 0 3 1451 1443 1452
		f 3 2536 2688 -2609
		mu 0 3 1443 1444 1452
		f 3 -2689 2609 -2545
		mu 0 3 1452 1444 1453
		f 3 2537 2689 -2610
		mu 0 3 1444 1445 1453
		f 3 -2690 2610 -2546
		mu 0 3 1453 1445 1454
		f 3 2538 2690 -2611
		mu 0 3 1445 1446 1454
		f 3 -2691 2603 -2547
		mu 0 3 1454 1446 1455
		f 3 2539 2691 -2612
		mu 0 3 1447 1448 1456
		f 3 -2692 2612 -2548
		mu 0 3 1456 1448 1457
		f 3 2540 2692 -2613
		mu 0 3 1448 1449 1457
		f 3 -2693 2613 -2549
		mu 0 3 1457 1449 1458
		f 3 2541 2693 -2614
		mu 0 3 1449 1450 1458
		f 3 -2694 2614 -2550
		mu 0 3 1458 1450 1459
		f 3 2542 2694 -2615
		mu 0 3 1450 1451 1459
		f 3 -2695 2615 -2551
		mu 0 3 1459 1451 1460
		f 3 2543 2695 -2616
		mu 0 3 1451 1452 1460
		f 3 -2696 2616 -2552
		mu 0 3 1460 1452 1461
		f 3 2544 2696 -2617
		mu 0 3 1452 1453 1461
		f 3 -2697 2617 -2553
		mu 0 3 1461 1453 1462
		f 3 2545 2697 -2618
		mu 0 3 1453 1454 1462
		f 3 -2698 2618 -2554
		mu 0 3 1462 1454 1463
		f 3 2546 2698 -2619
		mu 0 3 1454 1455 1463
		f 3 -2699 2611 -2555
		mu 0 3 1463 1455 1464
		f 3 2547 2699 -2620
		mu 0 3 1456 1457 1465
		f 3 -2700 2620 -2556
		mu 0 3 1465 1457 1466
		f 3 2548 2700 -2621
		mu 0 3 1457 1458 1466
		f 3 -2701 2621 -2557
		mu 0 3 1466 1458 1467
		f 3 2549 2701 -2622
		mu 0 3 1458 1459 1467
		f 3 -2702 2622 -2558
		mu 0 3 1467 1459 1468
		f 3 2550 2702 -2623
		mu 0 3 1459 1460 1468
		f 3 -2703 2623 -2559
		mu 0 3 1468 1460 1469
		f 3 2551 2703 -2624
		mu 0 3 1460 1461 1469
		f 3 -2704 2624 -2560
		mu 0 3 1469 1461 1470
		f 3 2552 2704 -2625
		mu 0 3 1461 1462 1470
		f 3 -2705 2625 -2561
		mu 0 3 1470 1462 1471
		f 3 2553 2705 -2626
		mu 0 3 1462 1463 1471
		f 3 -2706 2626 -2562
		mu 0 3 1471 1463 1472
		f 3 2554 2706 -2627
		mu 0 3 1463 1464 1472
		f 3 -2707 2619 -2563
		mu 0 3 1472 1464 1473
		f 3 2555 2707 -2628
		mu 0 3 1465 1466 1474
		f 3 -2708 2628 -2564
		mu 0 3 1474 1466 1475
		f 3 2556 2708 -2629
		mu 0 3 1466 1467 1475
		f 3 -2709 2629 -2565
		mu 0 3 1475 1467 1476
		f 3 2557 2709 -2630
		mu 0 3 1467 1468 1476
		f 3 -2710 2630 -2566
		mu 0 3 1476 1468 1477
		f 3 2558 2710 -2631
		mu 0 3 1468 1469 1477
		f 3 -2711 2631 -2567
		mu 0 3 1477 1469 1478
		f 3 2559 2711 -2632
		mu 0 3 1469 1470 1478
		f 3 -2712 2632 -2568
		mu 0 3 1478 1470 1479
		f 3 2560 2712 -2633
		mu 0 3 1470 1471 1479
		f 3 -2713 2633 -2569
		mu 0 3 1479 1471 1480
		f 3 2561 2713 -2634
		mu 0 3 1471 1472 1480
		f 3 -2714 2634 -2570
		mu 0 3 1480 1472 1481
		f 3 2562 2714 -2635
		mu 0 3 1472 1473 1481
		f 3 -2715 2627 -2571
		mu 0 3 1481 1473 1482
		f 3 2563 2715 -2636
		mu 0 3 1474 1475 1483
		f 3 -2716 2636 -2572
		mu 0 3 1483 1475 1484
		f 3 2564 2716 -2637
		mu 0 3 1475 1476 1484
		f 3 -2717 2637 -2573
		mu 0 3 1484 1476 1485
		f 3 2565 2717 -2638
		mu 0 3 1476 1477 1485
		f 3 -2718 2638 -2574
		mu 0 3 1485 1477 1486
		f 3 2566 2718 -2639
		mu 0 3 1477 1478 1486
		f 3 -2719 2639 -2575
		mu 0 3 1486 1478 1487
		f 3 2567 2719 -2640
		mu 0 3 1478 1479 1487
		f 3 -2720 2640 -2576
		mu 0 3 1487 1479 1488
		f 3 2568 2720 -2641
		mu 0 3 1479 1480 1488
		f 3 -2721 2641 -2577
		mu 0 3 1488 1480 1489
		f 3 2569 2721 -2642
		mu 0 3 1480 1481 1489
		f 3 -2722 2642 -2578
		mu 0 3 1489 1481 1490
		f 3 2570 2722 -2643
		mu 0 3 1481 1482 1490
		f 3 -2723 2635 -2579
		mu 0 3 1490 1482 1491
		f 3 2571 2723 -2644
		mu 0 3 1483 1484 1492
		f 3 -2724 2644 -2580
		mu 0 3 1492 1484 1493
		f 3 2572 2724 -2645
		mu 0 3 1484 1485 1493
		f 3 -2725 2645 -2581
		mu 0 3 1493 1485 1494
		f 3 2573 2725 -2646
		mu 0 3 1485 1486 1494
		f 3 -2726 2646 -2582
		mu 0 3 1494 1486 1495
		f 3 2574 2726 -2647
		mu 0 3 1486 1487 1495
		f 3 -2727 2647 -2583
		mu 0 3 1495 1487 1496
		f 3 2575 2727 -2648
		mu 0 3 1487 1488 1496
		f 3 -2728 2648 -2584
		mu 0 3 1496 1488 1497
		f 3 2576 2728 -2649
		mu 0 3 1488 1489 1497
		f 3 -2729 2649 -2585
		mu 0 3 1497 1489 1498
		f 3 2577 2729 -2650
		mu 0 3 1489 1490 1498
		f 3 -2730 2650 -2586
		mu 0 3 1498 1490 1499
		f 3 2578 2730 -2651
		mu 0 3 1490 1491 1499
		f 3 -2731 2643 -2587
		mu 0 3 1499 1491 1500
		f 3 -2516 -2652 2652
		mu 0 3 1421 1420 1501
		f 3 -2517 -2653 2653
		mu 0 3 1424 1421 1502
		f 3 -2518 -2654 2654
		mu 0 3 1426 1424 1503
		f 3 -2519 -2655 2655
		mu 0 3 1428 1426 1504
		f 3 -2520 -2656 2656
		mu 0 3 1430 1428 1505
		f 3 -2521 -2657 2657
		mu 0 3 1432 1430 1506
		f 3 -2522 -2658 2658
		mu 0 3 1434 1432 1507
		f 3 -2523 -2659 2651
		mu 0 3 1436 1434 1508
		f 3 2579 2660 -2660
		mu 0 3 1492 1493 1509
		f 3 2580 2661 -2661
		mu 0 3 1493 1494 1510
		f 3 2581 2662 -2662
		mu 0 3 1494 1495 1511
		f 3 2582 2663 -2663
		mu 0 3 1495 1496 1512
		f 3 2583 2664 -2664
		mu 0 3 1496 1497 1513
		f 3 2584 2665 -2665
		mu 0 3 1497 1498 1514
		f 3 2585 2666 -2666
		mu 0 3 1498 1499 1515
		f 3 2586 2659 -2667
		mu 0 3 1499 1500 1516
		f 3 2731 2883 -2804
		mu 0 3 1517 1518 1519
		f 3 -2884 2804 -2740
		mu 0 3 1519 1518 1520
		f 3 2732 2884 -2805
		mu 0 3 1518 1521 1520
		f 3 -2885 2805 -2741
		mu 0 3 1520 1521 1522
		f 3 2733 2885 -2806
		mu 0 3 1521 1523 1522
		f 3 -2886 2806 -2742
		mu 0 3 1522 1523 1524
		f 3 2734 2886 -2807
		mu 0 3 1523 1525 1524
		f 3 -2887 2807 -2743
		mu 0 3 1524 1525 1526
		f 3 2735 2887 -2808
		mu 0 3 1525 1527 1526
		f 3 -2888 2808 -2744
		mu 0 3 1526 1527 1528
		f 3 2736 2888 -2809
		mu 0 3 1527 1529 1528
		f 3 -2889 2809 -2745
		mu 0 3 1528 1529 1530
		f 3 2737 2889 -2810
		mu 0 3 1529 1531 1530
		f 3 -2890 2810 -2746
		mu 0 3 1530 1531 1532
		f 3 2738 2890 -2811
		mu 0 3 1531 1533 1532
		f 3 -2891 2803 -2747
		mu 0 3 1532 1533 1534
		f 3 2739 2891 -2812
		mu 0 3 1519 1520 1535
		f 3 -2892 2812 -2748
		mu 0 3 1535 1520 1536
		f 3 2740 2892 -2813
		mu 0 3 1520 1522 1536
		f 3 -2893 2813 -2749
		mu 0 3 1536 1522 1537
		f 3 2741 2893 -2814
		mu 0 3 1522 1524 1537
		f 3 -2894 2814 -2750
		mu 0 3 1537 1524 1538
		f 3 2742 2894 -2815
		mu 0 3 1524 1526 1538
		f 3 -2895 2815 -2751
		mu 0 3 1538 1526 1539
		f 3 2743 2895 -2816
		mu 0 3 1526 1528 1539
		f 3 -2896 2816 -2752
		mu 0 3 1539 1528 1540
		f 3 2744 2896 -2817
		mu 0 3 1528 1530 1540
		f 3 -2897 2817 -2753
		mu 0 3 1540 1530 1541
		f 3 2745 2897 -2818
		mu 0 3 1530 1532 1541
		f 3 -2898 2818 -2754
		mu 0 3 1541 1532 1542
		f 3 2746 2898 -2819
		mu 0 3 1532 1534 1542
		f 3 -2899 2811 -2755
		mu 0 3 1542 1534 1543
		f 3 2747 2899 -2820
		mu 0 3 1535 1536 1544
		f 3 -2900 2820 -2756
		mu 0 3 1544 1536 1545
		f 3 2748 2900 -2821
		mu 0 3 1536 1537 1545
		f 3 -2901 2821 -2757
		mu 0 3 1545 1537 1546
		f 3 2749 2901 -2822
		mu 0 3 1537 1538 1546
		f 3 -2902 2822 -2758
		mu 0 3 1546 1538 1547
		f 3 2750 2902 -2823
		mu 0 3 1538 1539 1547
		f 3 -2903 2823 -2759
		mu 0 3 1547 1539 1548
		f 3 2751 2903 -2824
		mu 0 3 1539 1540 1548
		f 3 -2904 2824 -2760
		mu 0 3 1548 1540 1549
		f 3 2752 2904 -2825
		mu 0 3 1540 1541 1549
		f 3 -2905 2825 -2761
		mu 0 3 1549 1541 1550
		f 3 2753 2905 -2826
		mu 0 3 1541 1542 1550
		f 3 -2906 2826 -2762
		mu 0 3 1550 1542 1551
		f 3 2754 2906 -2827
		mu 0 3 1542 1543 1551
		f 3 -2907 2819 -2763
		mu 0 3 1551 1543 1552
		f 3 2755 2907 -2828
		mu 0 3 1544 1545 1553
		f 3 -2908 2828 -2764
		mu 0 3 1553 1545 1554
		f 3 2756 2908 -2829
		mu 0 3 1545 1546 1554
		f 3 -2909 2829 -2765
		mu 0 3 1554 1546 1555
		f 3 2757 2909 -2830
		mu 0 3 1546 1547 1555
		f 3 -2910 2830 -2766
		mu 0 3 1555 1547 1556
		f 3 2758 2910 -2831
		mu 0 3 1547 1548 1556
		f 3 -2911 2831 -2767
		mu 0 3 1556 1548 1557
		f 3 2759 2911 -2832
		mu 0 3 1548 1549 1557
		f 3 -2912 2832 -2768
		mu 0 3 1557 1549 1558
		f 3 2760 2912 -2833
		mu 0 3 1549 1550 1558
		f 3 -2913 2833 -2769
		mu 0 3 1558 1550 1559
		f 3 2761 2913 -2834
		mu 0 3 1550 1551 1559
		f 3 -2914 2834 -2770
		mu 0 3 1559 1551 1560
		f 3 2762 2914 -2835
		mu 0 3 1551 1552 1560
		f 3 -2915 2827 -2771
		mu 0 3 1560 1552 1561
		f 3 2763 2915 -2836
		mu 0 3 1553 1554 1562
		f 3 -2916 2836 -2772
		mu 0 3 1562 1554 1563
		f 3 2764 2916 -2837
		mu 0 3 1554 1555 1563
		f 3 -2917 2837 -2773
		mu 0 3 1563 1555 1564
		f 3 2765 2917 -2838
		mu 0 3 1555 1556 1564
		f 3 -2918 2838 -2774
		mu 0 3 1564 1556 1565
		f 3 2766 2918 -2839
		mu 0 3 1556 1557 1565
		f 3 -2919 2839 -2775
		mu 0 3 1565 1557 1566
		f 3 2767 2919 -2840
		mu 0 3 1557 1558 1566
		f 3 -2920 2840 -2776
		mu 0 3 1566 1558 1567
		f 3 2768 2920 -2841
		mu 0 3 1558 1559 1567
		f 3 -2921 2841 -2777
		mu 0 3 1567 1559 1568
		f 3 2769 2921 -2842
		mu 0 3 1559 1560 1568
		f 3 -2922 2842 -2778
		mu 0 3 1568 1560 1569
		f 3 2770 2922 -2843
		mu 0 3 1560 1561 1569
		f 3 -2923 2835 -2779
		mu 0 3 1569 1561 1570
		f 3 2771 2923 -2844
		mu 0 3 1562 1563 1571
		f 3 -2924 2844 -2780
		mu 0 3 1571 1563 1572
		f 3 2772 2924 -2845
		mu 0 3 1563 1564 1572
		f 3 -2925 2845 -2781
		mu 0 3 1572 1564 1573
		f 3 2773 2925 -2846
		mu 0 3 1564 1565 1573
		f 3 -2926 2846 -2782
		mu 0 3 1573 1565 1574
		f 3 2774 2926 -2847
		mu 0 3 1565 1566 1574
		f 3 -2927 2847 -2783
		mu 0 3 1574 1566 1575
		f 3 2775 2927 -2848
		mu 0 3 1566 1567 1575
		f 3 -2928 2848 -2784
		mu 0 3 1575 1567 1576
		f 3 2776 2928 -2849
		mu 0 3 1567 1568 1576
		f 3 -2929 2849 -2785
		mu 0 3 1576 1568 1577
		f 3 2777 2929 -2850
		mu 0 3 1568 1569 1577
		f 3 -2930 2850 -2786
		mu 0 3 1577 1569 1578
		f 3 2778 2930 -2851
		mu 0 3 1569 1570 1578
		f 3 -2931 2843 -2787
		mu 0 3 1578 1570 1579
		f 3 2779 2931 -2852
		mu 0 3 1571 1572 1580
		f 3 -2932 2852 -2788
		mu 0 3 1580 1572 1581
		f 3 2780 2932 -2853
		mu 0 3 1572 1573 1581
		f 3 -2933 2853 -2789
		mu 0 3 1581 1573 1582
		f 3 2781 2933 -2854
		mu 0 3 1573 1574 1582
		f 3 -2934 2854 -2790
		mu 0 3 1582 1574 1583
		f 3 2782 2934 -2855
		mu 0 3 1574 1575 1583
		f 3 -2935 2855 -2791
		mu 0 3 1583 1575 1584
		f 3 2783 2935 -2856
		mu 0 3 1575 1576 1584
		f 3 -2936 2856 -2792
		mu 0 3 1584 1576 1585
		f 3 2784 2936 -2857
		mu 0 3 1576 1577 1585
		f 3 -2937 2857 -2793
		mu 0 3 1585 1577 1586
		f 3 2785 2937 -2858
		mu 0 3 1577 1578 1586
		f 3 -2938 2858 -2794
		mu 0 3 1586 1578 1587
		f 3 2786 2938 -2859
		mu 0 3 1578 1579 1587
		f 3 -2939 2851 -2795
		mu 0 3 1587 1579 1588
		f 3 2787 2939 -2860
		mu 0 3 1580 1581 1589
		f 3 -2940 2860 -2796
		mu 0 3 1589 1581 1590
		f 3 2788 2940 -2861
		mu 0 3 1581 1582 1590
		f 3 -2941 2861 -2797
		mu 0 3 1590 1582 1591
		f 3 2789 2941 -2862
		mu 0 3 1582 1583 1591
		f 3 -2942 2862 -2798
		mu 0 3 1591 1583 1592
		f 3 2790 2942 -2863
		mu 0 3 1583 1584 1592
		f 3 -2943 2863 -2799
		mu 0 3 1592 1584 1593
		f 3 2791 2943 -2864
		mu 0 3 1584 1585 1593
		f 3 -2944 2864 -2800
		mu 0 3 1593 1585 1594
		f 3 2792 2944 -2865
		mu 0 3 1585 1586 1594
		f 3 -2945 2865 -2801
		mu 0 3 1594 1586 1595
		f 3 2793 2945 -2866
		mu 0 3 1586 1587 1595
		f 3 -2946 2866 -2802
		mu 0 3 1595 1587 1596
		f 3 2794 2946 -2867
		mu 0 3 1587 1588 1596
		f 3 -2947 2859 -2803
		mu 0 3 1596 1588 1597
		f 3 -2732 -2868 2868
		mu 0 3 1518 1517 1598
		f 3 -2733 -2869 2869
		mu 0 3 1521 1518 1599
		f 3 -2734 -2870 2870
		mu 0 3 1523 1521 1600
		f 3 -2735 -2871 2871
		mu 0 3 1525 1523 1601
		f 3 -2736 -2872 2872
		mu 0 3 1527 1525 1602
		f 3 -2737 -2873 2873
		mu 0 3 1529 1527 1603
		f 3 -2738 -2874 2874
		mu 0 3 1531 1529 1604
		f 3 -2739 -2875 2867
		mu 0 3 1533 1531 1605
		f 3 2795 2876 -2876
		mu 0 3 1589 1590 1606
		f 3 2796 2877 -2877
		mu 0 3 1590 1591 1607
		f 3 2797 2878 -2878
		mu 0 3 1591 1592 1608
		f 3 2798 2879 -2879
		mu 0 3 1592 1593 1609
		f 3 2799 2880 -2880
		mu 0 3 1593 1594 1610
		f 3 2800 2881 -2881
		mu 0 3 1594 1595 1611
		f 3 2801 2882 -2882
		mu 0 3 1595 1596 1612
		f 3 2802 2875 -2883
		mu 0 3 1596 1597 1613
		f 3 2947 3157 -3048
		mu 0 3 1614 1615 1616
		f 3 -3158 3048 -2958
		mu 0 3 1616 1615 1617
		f 3 2948 3158 -3049
		mu 0 3 1615 1618 1617
		f 3 -3159 3049 -2959
		mu 0 3 1617 1618 1619
		f 3 2949 3159 -3050
		mu 0 3 1618 1620 1619
		f 3 -3160 3050 -2960
		mu 0 3 1619 1620 1621
		f 3 2950 3160 -3051
		mu 0 3 1620 1622 1621
		f 3 -3161 3051 -2961
		mu 0 3 1621 1622 1623
		f 3 2951 3161 -3052
		mu 0 3 1622 1624 1623
		f 3 -3162 3052 -2962
		mu 0 3 1623 1624 1625
		f 3 2952 3162 -3053
		mu 0 3 1624 1626 1625
		f 3 -3163 3053 -2963
		mu 0 3 1625 1626 1627
		f 3 2953 3163 -3054
		mu 0 3 1626 1628 1627
		f 3 -3164 3054 -2964
		mu 0 3 1627 1628 1629
		f 3 2954 3164 -3055
		mu 0 3 1628 1630 1629
		f 3 -3165 3055 -2965
		mu 0 3 1629 1630 1631
		f 3 2955 3165 -3056
		mu 0 3 1630 1632 1631
		f 3 -3166 3056 -2966
		mu 0 3 1631 1632 1633
		f 3 2956 3166 -3057
		mu 0 3 1632 1634 1633
		f 3 -3167 3047 -2967
		mu 0 3 1633 1634 1635
		f 3 2957 3167 -3058
		mu 0 3 1616 1617 1636
		f 3 -3168 3058 -2968
		mu 0 3 1636 1617 1637
		f 3 2958 3168 -3059
		mu 0 3 1617 1619 1637
		f 3 -3169 3059 -2969
		mu 0 3 1637 1619 1638
		f 3 2959 3169 -3060
		mu 0 3 1619 1621 1638
		f 3 -3170 3060 -2970
		mu 0 3 1638 1621 1639
		f 3 2960 3170 -3061
		mu 0 3 1621 1623 1639
		f 3 -3171 3061 -2971
		mu 0 3 1639 1623 1640
		f 3 2961 3171 -3062
		mu 0 3 1623 1625 1640
		f 3 -3172 3062 -2972
		mu 0 3 1640 1625 1641
		f 3 2962 3172 -3063
		mu 0 3 1625 1627 1641
		f 3 -3173 3063 -2973
		mu 0 3 1641 1627 1642
		f 3 2963 3173 -3064
		mu 0 3 1627 1629 1642
		f 3 -3174 3064 -2974
		mu 0 3 1642 1629 1643
		f 3 2964 3174 -3065
		mu 0 3 1629 1631 1643
		f 3 -3175 3065 -2975
		mu 0 3 1643 1631 1644
		f 3 2965 3175 -3066
		mu 0 3 1631 1633 1644
		f 3 -3176 3066 -2976
		mu 0 3 1644 1633 1645;
	setAttr ".fc[2000:2161]"
		f 3 2966 3176 -3067
		mu 0 3 1633 1635 1645
		f 3 -3177 3057 -2977
		mu 0 3 1645 1635 1646
		f 3 2967 3177 -3068
		mu 0 3 1636 1637 1647
		f 3 -3178 3068 -2978
		mu 0 3 1647 1637 1648
		f 3 2968 3178 -3069
		mu 0 3 1637 1638 1648
		f 3 -3179 3069 -2979
		mu 0 3 1648 1638 1649
		f 3 2969 3179 -3070
		mu 0 3 1638 1639 1649
		f 3 -3180 3070 -2980
		mu 0 3 1649 1639 1650
		f 3 2970 3180 -3071
		mu 0 3 1639 1640 1650
		f 3 -3181 3071 -2981
		mu 0 3 1650 1640 1651
		f 3 2971 3181 -3072
		mu 0 3 1640 1641 1651
		f 3 -3182 3072 -2982
		mu 0 3 1651 1641 1652
		f 3 2972 3182 -3073
		mu 0 3 1641 1642 1652
		f 3 -3183 3073 -2983
		mu 0 3 1652 1642 1653
		f 3 2973 3183 -3074
		mu 0 3 1642 1643 1653
		f 3 -3184 3074 -2984
		mu 0 3 1653 1643 1654
		f 3 2974 3184 -3075
		mu 0 3 1643 1644 1654
		f 3 -3185 3075 -2985
		mu 0 3 1654 1644 1655
		f 3 2975 3185 -3076
		mu 0 3 1644 1645 1655
		f 3 -3186 3076 -2986
		mu 0 3 1655 1645 1656
		f 3 2976 3186 -3077
		mu 0 3 1645 1646 1656
		f 3 -3187 3067 -2987
		mu 0 3 1656 1646 1657
		f 3 2977 3187 -3078
		mu 0 3 1647 1648 1658
		f 3 -3188 3078 -2988
		mu 0 3 1658 1648 1659
		f 3 2978 3188 -3079
		mu 0 3 1648 1649 1659
		f 3 -3189 3079 -2989
		mu 0 3 1659 1649 1660
		f 3 2979 3189 -3080
		mu 0 3 1649 1650 1660
		f 3 -3190 3080 -2990
		mu 0 3 1660 1650 1661
		f 3 2980 3190 -3081
		mu 0 3 1650 1651 1661
		f 3 -3191 3081 -2991
		mu 0 3 1661 1651 1662
		f 3 2981 3191 -3082
		mu 0 3 1651 1652 1662
		f 3 -3192 3082 -2992
		mu 0 3 1662 1652 1663
		f 3 2982 3192 -3083
		mu 0 3 1652 1653 1663
		f 3 -3193 3083 -2993
		mu 0 3 1663 1653 1664
		f 3 2983 3193 -3084
		mu 0 3 1653 1654 1664
		f 3 -3194 3084 -2994
		mu 0 3 1664 1654 1665
		f 3 2984 3194 -3085
		mu 0 3 1654 1655 1665
		f 3 -3195 3085 -2995
		mu 0 3 1665 1655 1666
		f 3 2985 3195 -3086
		mu 0 3 1655 1656 1666
		f 3 -3196 3086 -2996
		mu 0 3 1666 1656 1667
		f 3 2986 3196 -3087
		mu 0 3 1656 1657 1667
		f 3 -3197 3077 -2997
		mu 0 3 1667 1657 1668
		f 3 2987 3197 -3088
		mu 0 3 1658 1659 1669
		f 3 -3198 3088 -2998
		mu 0 3 1669 1659 1670
		f 3 2988 3198 -3089
		mu 0 3 1659 1660 1670
		f 3 -3199 3089 -2999
		mu 0 3 1670 1660 1671
		f 3 2989 3199 -3090
		mu 0 3 1660 1661 1671
		f 3 -3200 3090 -3000
		mu 0 3 1671 1661 1672
		f 3 2990 3200 -3091
		mu 0 3 1661 1662 1672
		f 3 -3201 3091 -3001
		mu 0 3 1672 1662 1673
		f 3 3092 3201 2991
		mu 0 3 1663 1674 1662
		f 3 -3202 -3002 -3092
		mu 0 3 1662 1674 1673
		f 3 3093 3202 2992
		mu 0 3 1664 1675 1663
		f 3 -3203 -3003 -3093
		mu 0 3 1663 1675 1674
		f 3 3094 3203 2993
		mu 0 3 1665 1676 1664
		f 3 -3204 -3004 -3094
		mu 0 3 1664 1676 1675
		f 3 3095 3204 2994
		mu 0 3 1666 1677 1665
		f 3 -3205 -3005 -3095
		mu 0 3 1665 1677 1676
		f 3 3096 3205 2995
		mu 0 3 1667 1678 1666
		f 3 -3206 -3006 -3096
		mu 0 3 1666 1678 1677
		f 3 2996 3206 -3097
		mu 0 3 1667 1668 1678
		f 3 -3207 3087 -3007
		mu 0 3 1678 1668 1679
		f 3 2997 3207 -3098
		mu 0 3 1669 1670 1680
		f 3 -3208 3098 -3008
		mu 0 3 1680 1670 1681
		f 3 2998 3208 -3099
		mu 0 3 1670 1671 1681
		f 3 -3209 3099 -3009
		mu 0 3 1681 1671 1682
		f 3 3100 3209 2999
		mu 0 3 1672 1683 1671
		f 3 -3210 -3010 -3100
		mu 0 3 1671 1683 1682
		f 3 3101 3210 3000
		mu 0 3 1673 1684 1672
		f 3 -3211 -3011 -3101
		mu 0 3 1672 1684 1683
		f 3 3102 3211 3001
		mu 0 3 1674 1685 1673
		f 3 -3212 -3012 -3102
		mu 0 3 1673 1685 1684
		f 3 3103 3212 3002
		mu 0 3 1675 1686 1674
		f 3 -3213 -3013 -3103
		mu 0 3 1674 1686 1685
		f 3 3003 3213 -3104
		mu 0 3 1675 1676 1686
		f 3 -3214 3104 -3014
		mu 0 3 1686 1676 1687
		f 3 3004 3214 -3105
		mu 0 3 1676 1677 1687
		f 3 -3215 3105 -3015
		mu 0 3 1687 1677 1688
		f 3 3005 3215 -3106
		mu 0 3 1677 1678 1688
		f 3 -3216 3106 -3016
		mu 0 3 1688 1678 1689
		f 3 3006 3216 -3107
		mu 0 3 1678 1679 1689
		f 3 -3217 3097 -3017
		mu 0 3 1689 1679 1690
		f 3 3007 3217 -3108
		mu 0 3 1680 1681 1691
		f 3 -3218 3108 -3018
		mu 0 3 1691 1681 1692
		f 3 3109 3218 3008
		mu 0 3 1682 1693 1681
		f 3 -3219 -3019 -3109
		mu 0 3 1681 1693 1692
		f 3 3110 3219 3009
		mu 0 3 1683 1694 1682
		f 3 -3220 -3020 -3110
		mu 0 3 1682 1694 1693
		f 3 3111 3220 3010
		mu 0 3 1684 1695 1683
		f 3 -3221 -3021 -3111
		mu 0 3 1683 1695 1694
		f 3 3112 3221 3011
		mu 0 3 1685 1696 1684
		f 3 -3222 -3022 -3112
		mu 0 3 1684 1696 1695
		f 3 3113 3222 3012
		mu 0 3 1686 1697 1685
		f 3 -3223 -3023 -3113
		mu 0 3 1685 1697 1696
		f 3 3013 3223 -3114
		mu 0 3 1686 1687 1697
		f 3 -3224 3114 -3024
		mu 0 3 1697 1687 1698
		f 3 3014 3224 -3115
		mu 0 3 1687 1688 1698
		f 3 -3225 3115 -3025
		mu 0 3 1698 1688 1699
		f 3 3015 3225 -3116
		mu 0 3 1688 1689 1699
		f 3 -3226 3116 -3026
		mu 0 3 1699 1689 1700
		f 3 3016 3226 -3117
		mu 0 3 1689 1690 1700
		f 3 -3227 3107 -3027
		mu 0 3 1700 1690 1701
		f 3 3118 3227 3017
		mu 0 3 1702 1703 1704
		f 3 -3228 -3028 -3118
		mu 0 3 1704 1703 1705
		f 3 3018 3228 -3119
		mu 0 3 1702 1706 1703
		f 3 -3229 3119 -3029
		mu 0 3 1703 1706 1707
		f 3 3019 3229 -3120
		mu 0 3 1706 1708 1707
		f 3 -3230 3120 -3030
		mu 0 3 1707 1708 1709
		f 3 3020 3230 -3121
		mu 0 3 1708 1710 1709
		f 3 -3231 3121 -3031
		mu 0 3 1709 1710 1711
		f 3 3021 3231 -3122
		mu 0 3 1710 1712 1711
		f 3 -3232 3122 -3032
		mu 0 3 1711 1712 1713
		f 3 3022 3232 -3123
		mu 0 3 1712 1714 1713
		f 3 -3233 3123 -3033
		mu 0 3 1713 1714 1715
		f 3 3124 3233 3023
		mu 0 3 1716 1717 1714
		f 3 -3234 -3034 -3124
		mu 0 3 1714 1717 1715
		f 3 3125 3234 3024
		mu 0 3 1718 1719 1716
		f 3 -3235 -3035 -3125
		mu 0 3 1716 1719 1717
		f 3 3126 3235 3025
		mu 0 3 1720 1721 1718
		f 3 -3236 -3036 -3126
		mu 0 3 1718 1721 1719
		f 3 3117 3236 3026
		mu 0 3 1704 1705 1720
		f 3 -3237 -3037 -3127
		mu 0 3 1720 1705 1721
		f 3 3128 3237 3027
		mu 0 3 1703 1722 1705
		f 3 -3238 -3038 -3128
		mu 0 3 1705 1722 1723
		f 3 3028 3238 -3129
		mu 0 3 1703 1707 1722
		f 3 -3239 3129 -3039
		mu 0 3 1722 1707 1724
		f 3 3029 3239 -3130
		mu 0 3 1707 1709 1724
		f 3 -3240 3130 -3040
		mu 0 3 1724 1709 1725
		f 3 3030 3240 -3131
		mu 0 3 1709 1711 1725
		f 3 -3241 3131 -3041
		mu 0 3 1725 1711 1726
		f 3 3031 3241 -3132
		mu 0 3 1711 1713 1726
		f 3 -3242 3132 -3042
		mu 0 3 1726 1713 1727
		f 3 3032 3242 -3133
		mu 0 3 1713 1715 1727
		f 3 -3243 3133 -3043
		mu 0 3 1727 1715 1728
		f 3 3134 3243 3033
		mu 0 3 1717 1729 1715
		f 3 -3244 -3044 -3134
		mu 0 3 1715 1729 1728
		f 3 3135 3244 3034
		mu 0 3 1719 1730 1717
		f 3 -3245 -3045 -3135
		mu 0 3 1717 1730 1729
		f 3 3136 3245 3035
		mu 0 3 1721 1731 1719
		f 3 -3246 -3046 -3136
		mu 0 3 1719 1731 1730
		f 3 3127 3246 3036
		mu 0 3 1705 1723 1721
		f 3 -3247 -3047 -3137
		mu 0 3 1721 1723 1731
		f 3 3037 3138 -3138
		mu 0 3 1723 1722 1732
		f 3 3038 3139 -3139
		mu 0 3 1722 1724 1732
		f 3 3039 3140 -3140
		mu 0 3 1724 1725 1732
		f 3 3040 3141 -3141
		mu 0 3 1725 1726 1732
		f 3 3041 3142 -3142
		mu 0 3 1726 1727 1732
		f 3 3042 3143 -3143
		mu 0 3 1727 1728 1732
		f 3 3043 3144 -3144
		mu 0 3 1728 1729 1732
		f 3 3044 3145 -3145
		mu 0 3 1729 1730 1732
		f 3 3045 3146 -3146
		mu 0 3 1730 1731 1732
		f 3 3046 3137 -3147
		mu 0 3 1731 1723 1732
		f 3 3153 3148 -2953
		mu 0 3 1624 1733 1626
		f 3 -2948 3149 -3148
		mu 0 3 1615 1634 1733
		f 3 -3150 -2957 3150
		mu 0 3 1733 1634 1632
		f 3 -3151 -2956 3151
		mu 0 3 1733 1632 1630
		f 3 -3152 -2955 3152
		mu 0 3 1733 1630 1628
		f 3 -3153 -2954 -3149
		mu 0 3 1733 1628 1626
		f 3 3154 -3154 -2952
		mu 0 3 1622 1733 1624
		f 3 3155 -3155 -2951
		mu 0 3 1620 1733 1622
		f 3 3156 -3156 -2950
		mu 0 3 1618 1733 1620
		f 3 3147 -3157 -2949
		mu 0 3 1615 1733 1618;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 812 
		0 0 
		1 0 
		2 0 
		3 0 
		4 0 
		5 0 
		6 0 
		7 0 
		8 0 
		9 0 
		10 0 
		11 0 
		12 0 
		13 0 
		14 0 
		15 0 
		16 0 
		17 0 
		18 0 
		19 0 
		20 0 
		21 0 
		22 0 
		23 0 
		24 0 
		25 0 
		26 0 
		27 0 
		28 0 
		29 0 
		30 0 
		31 0 
		32 0 
		33 0 
		34 0 
		35 0 
		36 0 
		37 0 
		38 0 
		39 0 
		40 0 
		41 0 
		42 0 
		43 0 
		44 0 
		45 0 
		46 0 
		47 0 
		48 0 
		49 0 
		50 0 
		51 0 
		52 0 
		53 0 
		54 0 
		55 0 
		56 0 
		57 0 
		58 0 
		59 0 
		60 0 
		61 0 
		62 0 
		63 0 
		64 0 
		65 0 
		66 0 
		67 0 
		68 0 
		69 0 
		70 0 
		71 0 
		72 0 
		73 0 
		74 0 
		75 0 
		76 0 
		77 0 
		78 0 
		79 0 
		80 0 
		81 0 
		82 0 
		83 0 
		84 0 
		85 0 
		86 0 
		87 0 
		88 0 
		89 0 
		90 0 
		91 0 
		92 0 
		93 0 
		94 0 
		95 0 
		96 0 
		97 0 
		98 0 
		99 0 
		100 0 
		101 0 
		102 0 
		103 0 
		104 0 
		105 0 
		106 0 
		107 0 
		108 0 
		109 0 
		110 0 
		111 0 
		112 0 
		113 0 
		114 0 
		115 0 
		116 0 
		117 0 
		118 0 
		119 0 
		120 0 
		121 0 
		122 0 
		123 0 
		124 0 
		125 0 
		126 0 
		127 0 
		128 0 
		129 0 
		130 0 
		131 0 
		132 0 
		133 0 
		134 0 
		135 0 
		136 0 
		137 0 
		138 0 
		139 0 
		140 0 
		141 0 
		142 0 
		143 0 
		144 0 
		145 0 
		146 0 
		147 0 
		148 0 
		149 0 
		150 0 
		151 0 
		152 0 
		153 0 
		154 0 
		155 0 
		156 0 
		157 0 
		158 0 
		159 0 
		160 0 
		161 0 
		162 0 
		163 0 
		164 0 
		165 0 
		166 0 
		167 0 
		168 0 
		169 0 
		170 0 
		171 0 
		172 0 
		173 0 
		174 0 
		175 0 
		176 0 
		177 0 
		178 0 
		179 0 
		180 0 
		181 0 
		182 0 
		183 0 
		184 0 
		185 0 
		186 0 
		187 0 
		188 0 
		189 0 
		190 0 
		191 0 
		192 0 
		193 0 
		194 0 
		195 0 
		196 0 
		197 0 
		198 0 
		199 0 
		200 0 
		201 0 
		202 0 
		203 0 
		204 0 
		205 0 
		206 0 
		207 0 
		208 0 
		209 0 
		210 0 
		211 0 
		212 0 
		213 0 
		214 0 
		215 0 
		216 0 
		217 0 
		218 0 
		220 0 
		221 0 
		222 0 
		223 0 
		224 0 
		225 0 
		226 0 
		227 0 
		228 0 
		229 0 
		230 0 
		231 0 
		232 0 
		233 0 
		234 0 
		235 0 
		236 0 
		237 0 
		238 0 
		239 0 
		240 0 
		241 0 
		242 0 
		243 0 
		244 0 
		245 0 
		246 0 
		247 0 
		248 0 
		249 0 
		250 0 
		251 0 
		252 0 
		254 0 
		255 0 
		257 0 
		258 0 
		259 0 
		260 0 
		261 0 
		262 0 
		263 0 
		264 0 
		265 0 
		266 0 
		267 0 
		268 0 
		269 0 
		270 0 
		271 0 
		272 0 
		273 0 
		274 0 
		275 0 
		276 0 
		277 0 
		278 0 
		279 0 
		280 0 
		281 0 
		282 0 
		283 0 
		284 0 
		288 0 
		289 0 
		290 0 
		291 0 
		292 0 
		293 0 
		294 0 
		295 0 
		296 0 
		297 0 
		298 0 
		299 0 
		300 0 
		301 0 
		302 0 
		303 0 
		304 0 
		305 0 
		306 0 
		307 0 
		308 0 
		309 0 
		310 0 
		311 0 
		312 0 
		313 0 
		314 0 
		315 0 
		316 0 
		317 0 
		318 0 
		319 0 
		320 0 
		321 0 
		322 0 
		324 0 
		327 0 
		328 0 
		331 0 
		332 0 
		333 0 
		334 0 
		335 0 
		336 0 
		337 0 
		338 0 
		339 0 
		340 0 
		341 0 
		342 0 
		343 0 
		344 0 
		345 0 
		346 0 
		347 0 
		348 0 
		349 0 
		350 0 
		351 0 
		352 0 
		353 0 
		354 0 
		355 0 
		356 0 
		357 0 
		358 0 
		359 0 
		360 0 
		361 0 
		362 0 
		363 0 
		364 0 
		365 0 
		369 0 
		370 0 
		371 0 
		372 0 
		373 0 
		374 0 
		375 0 
		376 0 
		377 0 
		378 0 
		379 0 
		380 0 
		381 0 
		382 0 
		383 0 
		384 0 
		385 0 
		386 0 
		387 0 
		388 0 
		389 0 
		390 0 
		391 0 
		392 0 
		393 0 
		394 0 
		395 0 
		396 0 
		397 0 
		398 0 
		399 0 
		400 0 
		401 0 
		402 0 
		403 0 
		404 0 
		405 0 
		406 0 
		407 0 
		408 0 
		409 0 
		410 0 
		411 0 
		412 0 
		418 0 
		421 0 
		422 0 
		423 0 
		424 0 
		425 0 
		426 0 
		427 0 
		428 0 
		429 0 
		430 0 
		431 0 
		432 0 
		433 0 
		434 0 
		435 0 
		436 0 
		437 0 
		438 0 
		439 0 
		440 0 
		441 0 
		442 0 
		443 0 
		444 0 
		445 0 
		446 0 
		447 0 
		448 0 
		449 0 
		450 0 
		451 0 
		452 0 
		453 0 
		454 0 
		455 0 
		456 0 
		457 0 
		458 0 
		459 0 
		460 0 
		461 0 
		462 0 
		463 0 
		464 0 
		466 0 
		471 0 
		472 0 
		473 0 
		474 0 
		475 0 
		476 0 
		477 0 
		478 0 
		479 0 
		480 0 
		481 0 
		482 0 
		483 0 
		484 0 
		485 0 
		486 0 
		487 0 
		488 0 
		489 0 
		490 0 
		491 0 
		492 0 
		493 0 
		494 0 
		495 0 
		496 0 
		497 0 
		498 0 
		499 0 
		500 0 
		501 0 
		502 0 
		503 0 
		504 0 
		505 0 
		506 0 
		507 0 
		508 0 
		509 0 
		510 0 
		511 0 
		512 0 
		513 0 
		514 0 
		515 0 
		516 0 
		517 0 
		525 0 
		526 0 
		529 0 
		530 0 
		531 0 
		532 0 
		533 0 
		534 0 
		535 0 
		536 0 
		537 0 
		538 0 
		539 0 
		540 0 
		541 0 
		542 0 
		543 0 
		544 0 
		545 0 
		546 0 
		547 0 
		548 0 
		549 0 
		550 0 
		551 0 
		552 0 
		553 0 
		554 0 
		555 0 
		556 0 
		557 0 
		558 0 
		559 0 
		560 0 
		561 0 
		562 0 
		563 0 
		564 0 
		565 0 
		566 0 
		567 0 
		568 0 
		569 0 
		571 0 
		579 0 
		580 0 
		581 0 
		583 0 
		584 0 
		585 0 
		586 0 
		587 0 
		588 0 
		589 0 
		590 0 
		591 0 
		592 0 
		593 0 
		594 0 
		595 0 
		596 0 
		597 0 
		598 0 
		599 0 
		600 0 
		601 0 
		602 0 
		603 0 
		604 0 
		605 0 
		606 0 
		607 0 
		608 0 
		609 0 
		610 0 
		611 0 
		612 0 
		613 0 
		614 0 
		615 0 
		616 0 
		617 0 
		618 0 
		619 0 
		620 0 
		621 0 
		622 0 
		623 0 
		624 0 
		625 0 
		626 0 
		627 0 
		637 0 
		638 0 
		640 0 
		641 0 
		642 0 
		643 0 
		644 0 
		645 0 
		646 0 
		647 0 
		648 0 
		649 0 
		650 0 
		651 0 
		652 0 
		653 0 
		654 0 
		655 0 
		656 0 
		657 0 
		658 0 
		659 0 
		660 0 
		661 0 
		662 0 
		663 0 
		664 0 
		665 0 
		666 0 
		667 0 
		668 0 
		669 0 
		670 0 
		671 0 
		672 0 
		673 0 
		674 0 
		675 0 
		676 0 
		677 0 
		678 0 
		692 0 
		693 0 
		694 0 
		696 0 
		697 0 
		698 0 
		699 0 
		700 0 
		701 0 
		702 0 
		703 0 
		704 0 
		705 0 
		706 0 
		707 0 
		708 0 
		709 0 
		710 0 
		711 0 
		712 0 
		713 0 
		714 0 
		715 0 
		716 0 
		717 0 
		718 0 
		719 0 
		720 0 
		721 0 
		722 0 
		723 0 
		724 0 
		725 0 
		726 0 
		728 0 
		740 0 
		742 0 
		743 0 
		744 0 
		745 0 
		746 0 
		747 0 
		748 0 
		750 0 
		751 0 
		752 0 
		753 0 
		754 0 
		755 0 
		756 0 
		757 0 
		758 0 
		759 0 
		762 0 
		763 0 
		764 0 
		780 0 
		781 0 
		782 0 
		783 0 
		784 0 
		785 0 
		786 0 
		787 0 
		788 0 
		789 0 
		790 0 
		791 0 
		792 0 
		793 0 
		794 0 
		795 0 
		802 0 
		817 0 
		818 0 
		819 0 
		820 0 
		821 0 
		824 0 
		825 0 
		831 0 
		832 0 
		833 0 
		853 0 
		854 0 
		857 0 
		858 0 
		859 0 
		865 0 
		890 0 
		891 0 
		898 0 
		899 0 
		925 0 
		926 0 
		927 0 
		934 0 
		962 0 
		999 0 
		1000 0 
		1001 0 
		1002 0 
		1042 0 
		1043 0 
		1077 0 
		1467 0 
		1468 0 
		1469 0 
		1470 0 
		1471 0 
		1472 0 
		1473 0 
		1474 0 
		1475 0 
		1476 0 
		1477 0 
		1478 0 
		1479 0 
		1480 0 
		1481 0 
		1482 0 
		1483 0 
		1484 0 
		1485 0 
		1486 0 
		1487 0 
		1488 0 
		1489 0 
		1490 0 
		1491 0 
		1492 0 
		1493 0 
		1494 0 
		1495 0 
		1496 0 
		1497 0 
		1498 0 
		1499 0 
		1500 0 
		1501 0 
		1502 0 
		1503 0 
		1504 0 
		1505 0 
		1506 0 
		1507 0 
		1508 0 
		1509 0 
		1510 0 
		1511 0 
		1512 0 
		1513 0 
		1514 0 
		1515 0 
		1516 0 
		1517 0 
		1518 0 
		1519 0 
		1520 0 
		1545 0 
		1551 0 
		1555 0 
		1556 0 
		1557 0 
		1558 0 
		1559 0 
		1565 0 
		1566 0 
		1567 0 
		1568 0 
		1574 0 
		1575 0 
		1576 0 
		1577 0 
		1578 0 
		1584 0 
		1585 0 
		1586 0 
		1592 0 
		1593 0 
		1598 0 
		1599 0 
		1600 0 
		1606 0 
		1607 0 
		1613 0 
		1614 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "F9834F93-4EC9-0DFB-C765-1BB4A8679DE6";
	setAttr -s 5 ".lnk";
	setAttr -s 5 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "47323E3F-47DE-FCE6-7D75-B68E75D02FC4";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "5CB890D9-46CF-5BA9-661B-D69B2AAA5E2F";
createNode displayLayerManager -n "layerManager";
	rename -uid "14321751-42DD-479C-6664-A98D5CE4E990";
createNode displayLayer -n "defaultLayer";
	rename -uid "C95BFF3D-4528-7582-933E-1AA3836A5922";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "BBC14FCB-41C3-2AD0-B5F0-008619332F0F";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "22F699A7-4D95-3334-86CC-1D890DBFDD46";
	setAttr ".g" yes;
createNode cameraView -n "cameraView1";
	rename -uid "5374D763-451D-6226-B351-AD92AEF5D1C8";
	setAttr ".e" -type "double3" 100 40 4.6321296794669508e-015 ;
	setAttr ".coi" -type "double3" 65.944675152451168 27.604875437226035 -3.4149710257011902e-015 ;
	setAttr ".u" -type "double3" -0.34202014332566866 0.93969262078590843 2.7755575615628914e-017 ;
	setAttr ".tp" -type "double3" -0.8135606050491333 -1.3399490714073181 -4.0308306217193604 ;
	setAttr ".fl" 34.999999999999993;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "DE104825-4A4B-2AB6-E1E3-E69C025BF65E";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 1\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 642\n                -height 401\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 1\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 642\n            -height 401\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 1\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 641\n                -height 400\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 1\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 641\n            -height 400\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 1\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 642\n                -height 400\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 1\n            -jointXray 1\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 642\n            -height 400\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 0\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1290\n                -height 846\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1290\n            -height 846\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n"
		+ "                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n"
		+ "                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -renderFilterIndex 0\n                -selectionOrder \"chronological\" \n                -expandAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n"
		+ "                -showShapes 0\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n"
		+ "                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n"
		+ "            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n"
		+ "                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n"
		+ "                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n"
		+ "                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n"
		+ "                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n"
		+ "                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n"
		+ "                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"timeEditorPanel\" -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n"
		+ "                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n"
		+ "            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n"
		+ "                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n"
		+ "                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tshapePanel -unParent -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tposePanel -unParent -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels ;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"contentBrowserPanel\" -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n"
		+ "                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n"
		+ "            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n"
		+ "                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n"
		+ "                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n"
		+ "                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1290\\n    -height 846\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1290\\n    -height 846\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "602B8EDA-4CFE-7B7A-D8AC-CFA77F75578F";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode blinn -n "blinn1";
	rename -uid "F31F48FF-4FDE-B06F-8B05-28BB5905C175";
	setAttr ".c" -type "float3" 0 0 0 ;
createNode shadingEngine -n "blinn1SG";
	rename -uid "8EEE25A9-4577-A995-55BE-C1A613250325";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "41AC1A03-4D6B-8E9D-8997-55AE6F65B377";
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "2611E6AE-4F6A-FDBF-05A9-01A20E77A437";
	setAttr ".version" -type "string" "1.3.0.0";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "B76D1661-4C87-41D2-7E05-E2AE4BC8E056";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "B7C90719-478E-AC34-5792-4D973F077B88";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "F15B2496-466E-4294-C080-0484AC335037";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode lambert -n "lambert2";
	rename -uid "71B26F35-461E-3146-C060-D58A84901EFA";
	setAttr ".c" -type "float3" 1 1 1 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "2DE4100F-4D54-90FA-FB54-ABA4F15F730B";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "27EAAE66-4C9E-D6A9-5525-1F8FD24D3F6A";
createNode lambert -n "lambert3";
	rename -uid "80B0ED0F-4705-4985-CA04-25BED7597A7F";
	setAttr ".c" -type "float3" 1 0.18599999 0.18599999 ;
createNode shadingEngine -n "lambert3SG";
	rename -uid "018DBEEC-4C7B-D691-04F8-8C95A19B64B3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
	rename -uid "1BFDB76D-4796-1584-A331-4087B83C38BC";
createNode groupId -n "groupId1";
	rename -uid "4E9EEABA-40B8-1F1E-322C-489EF510AAC5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "91E8D560-4ADF-6CAE-FD26-8C884C927D50";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:1673]";
createNode groupId -n "groupId2";
	rename -uid "4D50E33C-4DDD-9944-B48A-6C8D54AB800E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "6A2E2CAE-4C35-0C0C-4A65-84BBC683F27B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[1674:1961]";
createNode groupId -n "groupId3";
	rename -uid "32DE0F19-4A48-5468-0BE1-CEB2B3A832C0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "CCE6B167-437C-6A53-3BD6-8ABB5AAEE913";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[1962:2161]";
createNode groupId -n "groupId7";
	rename -uid "DBAFA4B7-47C3-9207-E4CD-0992EC66FFE6";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "E29B8D04-42FF-A969-F06F-10A85FF4B6D4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[1962:2161]";
createNode skinCluster -n "skinCluster1";
	rename -uid "F00BC736-4AA0-3644-E184-9C978C97A955";
	setAttr ".skm" 2;
	setAttr -s 1087 ".wl";
	setAttr -s 3 ".wl[0].w";
	setAttr ".wl[0].w[22]" 0.89865639574421552;
	setAttr ".wl[0].w[24]" 0.10133405808437253;
	setAttr ".wl[0].w[25]" 9.5461714119524571e-006;
	setAttr -s 3 ".wl[1].w[22:24]"  0.34678224454527951 0.0012318620056168531 
		0.65198589344910374;
	setAttr -s 3 ".wl[2].w[22:24]"  0.49966050241933002 0.00067899516133991685 
		0.49966050241933002;
	setAttr -s 3 ".wl[3].w[22:24]"  0.50153196862624583 0.0003821732794853495 
		0.49808585809426875;
	setAttr -s 3 ".wl[4].w[22:24]"  0.66275323862302993 0.0070082178501072456 
		0.33023854352686277;
	setAttr -s 2 ".wl[5].w";
	setAttr ".wl[5].w[22]" 0.00021053288850392788;
	setAttr ".wl[5].w[24]" 0.99978946711149608;
	setAttr -s 2 ".wl[6].w";
	setAttr ".wl[6].w[22]" 0.017318264534827495;
	setAttr ".wl[6].w[24]" 0.98268173546517246;
	setAttr -s 3 ".wl[7].w";
	setAttr ".wl[7].w[22]" 0.49999999271861983;
	setAttr ".wl[7].w[24]" 0.49999999271861983;
	setAttr ".wl[7].w[25]" 1.4562760295725196e-008;
	setAttr -s 3 ".wl[8].w";
	setAttr ".wl[8].w[22]" 3.2340502922477929e-006;
	setAttr ".wl[8].w[24]" 0.99937239037910475;
	setAttr ".wl[8].w[25]" 0.00062437557060299892;
	setAttr -s 2 ".wl[9].w";
	setAttr ".wl[9].w[22]" 8.0501240947965571e-010;
	setAttr ".wl[9].w[24]" 0.99999999919498761;
	setAttr -s 2 ".wl[10].w";
	setAttr ".wl[10].w[22]" 1.6411341033297409e-005;
	setAttr ".wl[10].w[24]" 0.99998358865896675;
	setAttr -s 2 ".wl[11].w";
	setAttr ".wl[11].w[22]" 1.1453713605057407e-005;
	setAttr ".wl[11].w[24]" 0.999988546286395;
	setAttr ".wl[12].w[24]"  1;
	setAttr -s 2 ".wl[13].w[24:25]"  0.99914466041089733 0.00085533958910271126;
	setAttr ".wl[14].w[24]"  1;
	setAttr -s 2 ".wl[15].w";
	setAttr ".wl[15].w[22]" 8.3561833711546567e-008;
	setAttr ".wl[15].w[24]" 0.99999991643816633;
	setAttr ".wl[16].w[24]"  1;
	setAttr ".wl[17].w[24]"  1;
	setAttr -s 2 ".wl[18].w";
	setAttr ".wl[18].w[22]" 0.99414566142508087;
	setAttr ".wl[18].w[24]" 0.0058543385749191806;
	setAttr -s 2 ".wl[19].w";
	setAttr ".wl[19].w[22]" 0.99337948602764381;
	setAttr ".wl[19].w[24]" 0.0066205139723561363;
	setAttr -s 3 ".wl[20].w";
	setAttr ".wl[20].w[22]" 0.99998481620575563;
	setAttr ".wl[20].w[24]" 3.0970599169036781e-006;
	setAttr ".wl[20].w[27]" 1.2086734327498205e-005;
	setAttr -s 3 ".wl[21].w";
	setAttr ".wl[21].w[22]" 0.76498839661587203;
	setAttr ".wl[21].w[23]" 0.00088553251400229978;
	setAttr ".wl[21].w[26]" 0.23412607087012569;
	setAttr -s 3 ".wl[22].w";
	setAttr ".wl[22].w[22]" 0.99693649480527446;
	setAttr ".wl[22].w[23]" 0.0027387941788267899;
	setAttr ".wl[22].w[27]" 0.00032471101589875409;
	setAttr -s 3 ".wl[23].w";
	setAttr ".wl[23].w[22]" 0.48274571330561677;
	setAttr ".wl[23].w[23]" 0.0016865439859847743;
	setAttr ".wl[23].w[26]" 0.51556774270839834;
	setAttr -s 3 ".wl[24].w";
	setAttr ".wl[24].w[22]" 0.76082634526923587;
	setAttr ".wl[24].w[23]" 0.0092220138743673297;
	setAttr ".wl[24].w[26]" 0.22995164085639683;
	setAttr -s 2 ".wl[25].w";
	setAttr ".wl[25].w[22]" 0.34407327597480425;
	setAttr ".wl[25].w[26]" 0.65592672402519581;
	setAttr -s 2 ".wl[26].w";
	setAttr ".wl[26].w[22]" 0.024111106147953414;
	setAttr ".wl[26].w[26]" 0.97588889385204658;
	setAttr -s 3 ".wl[27].w";
	setAttr ".wl[27].w[22]" 4.4948419066403416e-006;
	setAttr ".wl[27].w[26]" 0.99915203026482602;
	setAttr ".wl[27].w[27]" 0.00084347489326731708;
	setAttr -s 3 ".wl[28].w";
	setAttr ".wl[28].w[22]" 0.49987324039108938;
	setAttr ".wl[28].w[26]" 0.50012674090948572;
	setAttr ".wl[28].w[27]" 1.8699424857539653e-008;
	setAttr -s 2 ".wl[29].w";
	setAttr ".wl[29].w[22]" 9.6367109996538402e-006;
	setAttr ".wl[29].w[26]" 0.99999036328900037;
	setAttr -s 2 ".wl[30].w";
	setAttr ".wl[30].w[22]" 1.4565716365569551e-009;
	setAttr ".wl[30].w[26]" 0.99999999854342836;
	setAttr -s 2 ".wl[31].w";
	setAttr ".wl[31].w[22]" 2.4625751293006182e-005;
	setAttr ".wl[31].w[26]" 0.99997537424870697;
	setAttr -s 2 ".wl[32].w[26:27]"  0.99866538288041196 0.0013346171195880762;
	setAttr ".wl[33].w[26]"  1;
	setAttr -s 2 ".wl[34].w";
	setAttr ".wl[34].w[22]" 2.3399502210022347e-008;
	setAttr ".wl[34].w[26]" 0.99999997660049778;
	setAttr ".wl[35].w[26]"  1;
	setAttr -s 2 ".wl[36].w";
	setAttr ".wl[36].w[22]" 2.0576096601485396e-010;
	setAttr ".wl[36].w[26]" 0.99999999979423904;
	setAttr ".wl[37].w[26]"  1;
	setAttr ".wl[38].w[22]"  1;
	setAttr ".wl[39].w[22]"  1;
	setAttr -s 3 ".wl[40].w[22:24]"  0.84005327791918283 0.019236771080522658 
		0.14070995100029465;
	setAttr -s 3 ".wl[41].w[22:24]"  0.90040970271168297 0.010238878617707859 
		0.089351418670609153;
	setAttr -s 3 ".wl[42].w";
	setAttr ".wl[42].w[22]" 0.87683464639003084;
	setAttr ".wl[42].w[28]" 0.12316490377464678;
	setAttr ".wl[42].w[35]" 4.4983532241145111e-007;
	setAttr -s 3 ".wl[43].w[22:24]"  0.61475974083487095 0.071824532726040849 
		0.31341572643908833;
	setAttr -s 3 ".wl[44].w[22:24]"  0.98524291765747973 1.1725092612741359e-005 
		0.014745357249907548;
	setAttr ".wl[45].w[22]"  1;
	setAttr -s 2 ".wl[46].w[22:23]"  0.6690923509604354 0.33090764903956466;
	setAttr -s 3 ".wl[47].w[22:24]"  0.95977230586560414 0.00018087831757858527 
		0.040046815816817349;
	setAttr -s 3 ".wl[48].w";
	setAttr ".wl[48].w[23]" 0.43413004184794862;
	setAttr ".wl[48].w[28]" 0.56586988280305905;
	setAttr ".wl[48].w[35]" 7.5348992260806043e-008;
	setAttr -s 3 ".wl[49].w[22:24]"  0.98858117355547559 0.0035856516669681522 
		0.007833174777556344;
	setAttr -s 3 ".wl[50].w[22:24]"  0.44052002480353825 0.31754503218007402 
		0.24193494301638774;
	setAttr ".wl[51].w[23]"  1;
	setAttr -s 3 ".wl[52].w";
	setAttr ".wl[52].w[22]" 0.96328028452688819;
	setAttr ".wl[52].w[24]" 0.036710064079282387;
	setAttr ".wl[52].w[25]" 9.6513938294271926e-006;
	setAttr -s 3 ".wl[53].w[22:24]"  0.99234295934217553 0.00021551573402028921 
		0.0074415249238040711;
	setAttr -s 3 ".wl[54].w";
	setAttr ".wl[54].w[22]" 0.9889790190933121;
	setAttr ".wl[54].w[24]" 0.011013061929021262;
	setAttr ".wl[54].w[25]" 7.9189776666909087e-006;
	setAttr -s 2 ".wl[55].w";
	setAttr ".wl[55].w[22]" 0.44416608193698298;
	setAttr ".wl[55].w[28]" 0.55583391806301696;
	setAttr -s 3 ".wl[56].w[22:24]"  0.52834021260845321 0.46462643223925687 
		0.0070333551522899287;
	setAttr -s 2 ".wl[57].w[22:23]"  0.5540107132996489 0.44598928670035115;
	setAttr -s 3 ".wl[58].w";
	setAttr ".wl[58].w[22]" 0.98331913044172492;
	setAttr ".wl[58].w[24]" 0.01663007992459923;
	setAttr ".wl[58].w[25]" 5.0789633675895208e-005;
	setAttr -s 2 ".wl[59].w";
	setAttr ".wl[59].w[22]" 0.26836037842510335;
	setAttr ".wl[59].w[28]" 0.7316396215748967;
	setAttr ".wl[60].w[23]"  1;
	setAttr -s 3 ".wl[61].w";
	setAttr ".wl[61].w[13]" 0.17470875641117653;
	setAttr ".wl[61].w[22]" 0.8196164844390329;
	setAttr ".wl[61].w[24]" 0.0056747591497905078;
	setAttr -s 3 ".wl[62].w[22:24]"  0.99570866346242271 0.0015898492460495609 
		0.0027014872915277319;
	setAttr -s 3 ".wl[63].w[22:24]"  0.49999067637379568 0.49999067637379568 
		1.8647252408661625e-005;
	setAttr ".wl[64].w[23]"  1;
	setAttr -s 3 ".wl[65].w";
	setAttr ".wl[65].w[22]" 0.95375110579441091;
	setAttr ".wl[65].w[24]" 0.046127934136556067;
	setAttr ".wl[65].w[25]" 0.00012096006903308695;
	setAttr -s 3 ".wl[66].w[22:24]"  0.99015176489925216 0.00013427399544538716 
		0.00971396110530247;
	setAttr ".wl[67].w[28]"  1;
	setAttr -s 2 ".wl[68].w";
	setAttr ".wl[68].w[22]" 0.41748611801093594;
	setAttr ".wl[68].w[28]" 0.58251388198906406;
	setAttr ".wl[69].w[23]"  1;
	setAttr -s 2 ".wl[70].w[22:23]"  0.48892833268087554 0.51107166731912457;
	setAttr ".wl[71].w[23]"  1;
	setAttr -s 3 ".wl[72].w";
	setAttr ".wl[72].w[22]" 0.96712221370747775;
	setAttr ".wl[72].w[24]" 0.032803015805864977;
	setAttr ".wl[72].w[25]" 7.4770486657274907e-005;
	setAttr -s 3 ".wl[73].w";
	setAttr ".wl[73].w[13]" 0.31798448392549106;
	setAttr ".wl[73].w[22]" 0.66816056400498847;
	setAttr ".wl[73].w[24]" 0.013854952069520454;
	setAttr ".wl[74].w[28]"  1;
	setAttr -s 3 ".wl[75].w";
	setAttr ".wl[75].w[23]" 0.30434507414979922;
	setAttr ".wl[75].w[25]" 0.01698586815803587;
	setAttr ".wl[75].w[28]" 0.6786690576921649;
	setAttr -s 2 ".wl[76].w";
	setAttr ".wl[76].w[22]" 0.053343458531028176;
	setAttr ".wl[76].w[28]" 0.94665654146897182;
	setAttr ".wl[77].w[23]"  1;
	setAttr -s 3 ".wl[78].w";
	setAttr ".wl[78].w[23]" 0.61421025331135681;
	setAttr ".wl[78].w[25]" 0.095775987762884623;
	setAttr ".wl[78].w[28]" 0.29001375892575848;
	setAttr ".wl[79].w[23]"  1;
	setAttr -s 3 ".wl[80].w";
	setAttr ".wl[80].w[13]" 0.5675056127912842;
	setAttr ".wl[80].w[22]" 0.42804795691049413;
	setAttr ".wl[80].w[24]" 0.0044464302982216214;
	setAttr -s 3 ".wl[81].w";
	setAttr ".wl[81].w[13]" 0.47978160901881667;
	setAttr ".wl[81].w[22]" 0.5183377691724812;
	setAttr ".wl[81].w[24]" 0.0018806218087021305;
	setAttr -s 2 ".wl[82].w[22:23]"  0.99976908948657073 0.00023091051342924868;
	setAttr -s 3 ".wl[83].w[22:24]"  0.43169234922983574 0.26399210249792759 
		0.30431554827223672;
	setAttr -s 2 ".wl[84].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[85].w";
	setAttr ".wl[85].w[22]" 0.76233805424386258;
	setAttr ".wl[85].w[24]" 0.2376473393494575;
	setAttr ".wl[85].w[25]" 1.4606406679888259e-005;
	setAttr -s 3 ".wl[86].w";
	setAttr ".wl[86].w[22]" 0.98545228416193298;
	setAttr ".wl[86].w[24]" 0.01433919629573294;
	setAttr ".wl[86].w[25]" 0.00020851954233409823;
	setAttr ".wl[87].w[28]"  1;
	setAttr -s 3 ".wl[88].w";
	setAttr ".wl[88].w[23]" 0.085557262059350658;
	setAttr ".wl[88].w[25]" 0.0033201225149281418;
	setAttr ".wl[88].w[28]" 0.91112261542572126;
	setAttr -s 2 ".wl[89].w";
	setAttr ".wl[89].w[22]" 0.35050268111562133;
	setAttr ".wl[89].w[28]" 0.64949731888437867;
	setAttr -s 3 ".wl[90].w";
	setAttr ".wl[90].w[23]" 0.52756275853662693;
	setAttr ".wl[90].w[25]" 0.24906808900865807;
	setAttr ".wl[90].w[27]" 0.22336915245471509;
	setAttr ".wl[91].w[23]"  1;
	setAttr ".wl[92].w[23]"  1;
	setAttr ".wl[93].w[22]"  1;
	setAttr -s 3 ".wl[94].w[22:24]"  0.49999999977454834 0.49999999977454834 
		4.5090339461539606e-010;
	setAttr -s 3 ".wl[95].w[22:24]"  0.80598967793903087 0.00077223754645762335 
		0.19323808451451155;
	setAttr -s 2 ".wl[96].w[22:23]"  0.5 0.5;
	setAttr ".wl[97].w[23]"  1;
	setAttr ".wl[98].w[23]"  1;
	setAttr -s 3 ".wl[99].w";
	setAttr ".wl[99].w[13]" 0.2895932835100522;
	setAttr ".wl[99].w[22]" 0.69856566219872329;
	setAttr ".wl[99].w[24]" 0.011841054291224543;
	setAttr -s 3 ".wl[100].w";
	setAttr ".wl[100].w[13]" 0.99997280643885733;
	setAttr ".wl[100].w[23]" 1.5870291805993061e-005;
	setAttr ".wl[100].w[25]" 1.1323269336734303e-005;
	setAttr -s 3 ".wl[101].w";
	setAttr ".wl[101].w[13]" 0.65824070435100557;
	setAttr ".wl[101].w[22]" 0.33603973866018455;
	setAttr ".wl[101].w[24]" 0.0057195569888099373;
	setAttr ".wl[102].w[28]"  1;
	setAttr ".wl[103].w[28]"  1;
	setAttr -s 3 ".wl[104].w";
	setAttr ".wl[104].w[23]" 0.20854652828124584;
	setAttr ".wl[104].w[25]" 0.0029847123173395189;
	setAttr ".wl[104].w[28]" 0.7884687594014147;
	setAttr -s 3 ".wl[105].w";
	setAttr ".wl[105].w[23]" 0.48183988172482317;
	setAttr ".wl[105].w[28]" 0.51815919754224737;
	setAttr ".wl[105].w[35]" 9.2073292942690995e-007;
	setAttr -s 2 ".wl[106].w";
	setAttr ".wl[106].w[22]" 0.01564996844271668;
	setAttr ".wl[106].w[28]" 0.98435003155728329;
	setAttr -s 2 ".wl[107].w";
	setAttr ".wl[107].w[23]" 0.67059579388848467;
	setAttr ".wl[107].w[25]" 0.32940420611151533;
	setAttr -s 3 ".wl[108].w";
	setAttr ".wl[108].w[23]" 0.66805193415898445;
	setAttr ".wl[108].w[25]" 0.33194802791195521;
	setAttr ".wl[108].w[32]" 3.792906020929144e-008;
	setAttr -s 3 ".wl[109].w";
	setAttr ".wl[109].w[23]" 0.733987571112937;
	setAttr ".wl[109].w[28]" 0.26601182567221088;
	setAttr ".wl[109].w[35]" 6.0321485215056231e-007;
	setAttr ".wl[110].w[13]"  1;
	setAttr -s 3 ".wl[111].w";
	setAttr ".wl[111].w[13]" 0.61549411349988892;
	setAttr ".wl[111].w[22]" 0.38416720048390074;
	setAttr ".wl[111].w[24]" 0.00033868601621038003;
	setAttr ".wl[112].w[22]"  1;
	setAttr -s 2 ".wl[113].w[22:23]"  0.99997945711144387 2.0542888556163544e-005;
	setAttr -s 3 ".wl[114].w[22:24]"  0.4999997568039119 0.4999997568039119 
		4.8639217618275933e-007;
	setAttr -s 2 ".wl[115].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[116].w";
	setAttr ".wl[116].w[23]" 0.51156575966050155;
	setAttr ".wl[116].w[25]" 0.25239956208303321;
	setAttr ".wl[116].w[27]" 0.23603467825646529;
	setAttr -s 3 ".wl[117].w[22:24]"  0.96178322359536228 0.00036919859727350588 
		0.037847577807364259;
	setAttr ".wl[118].w[13]"  1;
	setAttr ".wl[119].w[28]"  1;
	setAttr ".wl[120].w[28]"  1;
	setAttr ".wl[121].w[28]"  1;
	setAttr -s 2 ".wl[122].w";
	setAttr ".wl[122].w[22]" 0.20478094758521123;
	setAttr ".wl[122].w[28]" 0.79521905241478874;
	setAttr -s 3 ".wl[123].w";
	setAttr ".wl[123].w[23]" 0.66998942028222541;
	setAttr ".wl[123].w[25]" 0.33001020898192046;
	setAttr ".wl[123].w[32]" 3.7073585410165591e-007;
	setAttr ".wl[124].w[13]"  1;
	setAttr -s 3 ".wl[125].w";
	setAttr ".wl[125].w[13]" 0.4197771798002492;
	setAttr ".wl[125].w[22]" 0.58014564693125326;
	setAttr ".wl[125].w[24]" 7.7173268497536732e-005;
	setAttr -s 3 ".wl[126].w";
	setAttr ".wl[126].w[22]" 0.99996933772894292;
	setAttr ".wl[126].w[24]" 8.3713477945644087e-006;
	setAttr ".wl[126].w[27]" 2.2290923262563703e-005;
	setAttr ".wl[127].w[23]"  1;
	setAttr -s 2 ".wl[128].w[22:23]"  0.5 0.5;
	setAttr ".wl[129].w[23]"  1;
	setAttr -s 3 ".wl[130].w";
	setAttr ".wl[130].w[23]" 0.51204549162797264;
	setAttr ".wl[130].w[25]" 0.25219390293639671;
	setAttr ".wl[130].w[27]" 0.23576060543563068;
	setAttr -s 3 ".wl[131].w";
	setAttr ".wl[131].w[22]" 0.99948619817703488;
	setAttr ".wl[131].w[24]" 0.00042863552901186309;
	setAttr ".wl[131].w[27]" 8.5166293953345461e-005;
	setAttr -s 3 ".wl[132].w";
	setAttr ".wl[132].w[13]" 0.99997438762072066;
	setAttr ".wl[132].w[23]" 1.7368526815358898e-005;
	setAttr ".wl[132].w[27]" 8.2438524640492985e-006;
	setAttr -s 3 ".wl[133].w";
	setAttr ".wl[133].w[13]" 0.99997647695129976;
	setAttr ".wl[133].w[14]" 1.1793074733884688e-005;
	setAttr ".wl[133].w[23]" 1.1729973966328318e-005;
	setAttr ".wl[134].w[28]"  1;
	setAttr ".wl[135].w[28]"  1;
	setAttr ".wl[136].w[28]"  1;
	setAttr -s 2 ".wl[137].w";
	setAttr ".wl[137].w[22]" 0.0033827403095869958;
	setAttr ".wl[137].w[28]" 0.99661725969041304;
	setAttr -s 3 ".wl[138].w";
	setAttr ".wl[138].w[23]" 0.50259827250136146;
	setAttr ".wl[138].w[25]" 0.25290959783485994;
	setAttr ".wl[138].w[27]" 0.24449212966377853;
	setAttr -s 3 ".wl[139].w";
	setAttr ".wl[139].w[23]" 0.50075010600315006;
	setAttr ".wl[139].w[25]" 0.254134490736875;
	setAttr ".wl[139].w[27]" 0.24511540325997483;
	setAttr ".wl[140].w[13]"  1;
	setAttr ".wl[141].w[13]"  1;
	setAttr -s 3 ".wl[142].w";
	setAttr ".wl[142].w[13]" 0.79340589218026736;
	setAttr ".wl[142].w[22]" 0.20651978679966998;
	setAttr ".wl[142].w[24]" 7.4321020062613201e-005;
	setAttr ".wl[143].w[22]"  1;
	setAttr -s 3 ".wl[144].w";
	setAttr ".wl[144].w[13]" 0.38459901333987273;
	setAttr ".wl[144].w[22]" 0.61538102092108282;
	setAttr ".wl[144].w[24]" 1.9965739044464813e-005;
	setAttr -s 2 ".wl[145].w[22:23]"  0.99999839329217377 1.6067078262407024e-006;
	setAttr -s 3 ".wl[146].w";
	setAttr ".wl[146].w[22]" 0.57755768155228793;
	setAttr ".wl[146].w[23]" 0.42059613555712294;
	setAttr ".wl[146].w[27]" 0.001846182890589091;
	setAttr ".wl[147].w[23]"  1;
	setAttr ".wl[148].w[23]"  1;
	setAttr -s 3 ".wl[149].w";
	setAttr ".wl[149].w[22]" 0.5310706297418567;
	setAttr ".wl[149].w[23]" 0.46875046800361847;
	setAttr ".wl[149].w[27]" 0.00017890225452496424;
	setAttr -s 3 ".wl[150].w";
	setAttr ".wl[150].w[23]" 0.50168724850656288;
	setAttr ".wl[150].w[25]" 0.25445920840582564;
	setAttr ".wl[150].w[27]" 0.24385354308761151;
	setAttr -s 3 ".wl[151].w";
	setAttr ".wl[151].w[22]" 0.99819283017504312;
	setAttr ".wl[151].w[24]" 0.0015806149855387981;
	setAttr ".wl[151].w[27]" 0.00022655483941798801;
	setAttr -s 3 ".wl[152].w";
	setAttr ".wl[152].w[13]" 0.99998827570336146;
	setAttr ".wl[152].w[14]" 4.6072638829847342e-006;
	setAttr ".wl[152].w[23]" 7.1170327556205407e-006;
	setAttr -s 3 ".wl[153].w";
	setAttr ".wl[153].w[13]" 0.65824453026539176;
	setAttr ".wl[153].w[22]" 0.33975735335482027;
	setAttr ".wl[153].w[24]" 0.001998116379788029;
	setAttr -s 3 ".wl[154].w";
	setAttr ".wl[154].w[13]" 0.99998166758063822;
	setAttr ".wl[154].w[14]" 1.2713272050303707e-005;
	setAttr ".wl[154].w[23]" 5.6191473114970164e-006;
	setAttr ".wl[155].w[28]"  1;
	setAttr ".wl[156].w[28]"  1;
	setAttr ".wl[157].w[28]"  1;
	setAttr ".wl[158].w[28]"  1;
	setAttr -s 3 ".wl[159].w";
	setAttr ".wl[159].w[22]" 0.052980419437026881;
	setAttr ".wl[159].w[28]" 0.94701957181230711;
	setAttr ".wl[159].w[29]" 8.7506659696146016e-009;
	setAttr -s 3 ".wl[160].w";
	setAttr ".wl[160].w[23]" 0.50304917158887386;
	setAttr ".wl[160].w[25]" 0.2527970703018797;
	setAttr ".wl[160].w[27]" 0.24415375810924628;
	setAttr ".wl[161].w[13]"  1;
	setAttr -s 3 ".wl[162].w";
	setAttr ".wl[162].w[13]" 0.72516405978146636;
	setAttr ".wl[162].w[22]" 0.27480587338286594;
	setAttr ".wl[162].w[24]" 3.0066835667762385e-005;
	setAttr ".wl[163].w[23]"  1;
	setAttr ".wl[164].w[23]"  1;
	setAttr -s 2 ".wl[165].w[22:23]"  0.51150414522060328 0.48849585477939678;
	setAttr -s 3 ".wl[166].w";
	setAttr ".wl[166].w[23]" 0.51308669980150556;
	setAttr ".wl[166].w[25]" 0.25073460468078601;
	setAttr ".wl[166].w[27]" 0.23617869551770843;
	setAttr -s 3 ".wl[167].w";
	setAttr ".wl[167].w[23]" 0.50253542474853774;
	setAttr ".wl[167].w[25]" 0.25430101450037607;
	setAttr ".wl[167].w[27]" 0.24316356075108611;
	setAttr -s 3 ".wl[168].w";
	setAttr ".wl[168].w[22]" 0.99973533374264478;
	setAttr ".wl[168].w[24]" 0.00012683888350620425;
	setAttr ".wl[168].w[27]" 0.00013782737384909329;
	setAttr -s 3 ".wl[169].w";
	setAttr ".wl[169].w[13]" 0.99999249594624273;
	setAttr ".wl[169].w[14]" 4.0548946411222983e-006;
	setAttr ".wl[169].w[18]" 3.4491591162060682e-006;
	setAttr ".wl[170].w[28]"  1;
	setAttr ".wl[171].w[28]"  1;
	setAttr ".wl[172].w[28]"  1;
	setAttr ".wl[173].w[28]"  1;
	setAttr -s 2 ".wl[174].w";
	setAttr ".wl[174].w[22]" 0.00024630227699638959;
	setAttr ".wl[174].w[28]" 0.9997536977230036;
	setAttr -s 2 ".wl[175].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[176].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[177].w";
	setAttr ".wl[177].w[13]" 0.99997841586181768;
	setAttr ".wl[177].w[14]" 1.5609160260300584e-005;
	setAttr ".wl[177].w[18]" 5.9749779221150615e-006;
	setAttr ".wl[178].w[13]"  1;
	setAttr ".wl[179].w[13]"  1;
	setAttr ".wl[180].w[13]"  1;
	setAttr -s 3 ".wl[181].w";
	setAttr ".wl[181].w[13]" 0.40945056708911226;
	setAttr ".wl[181].w[22]" 0.59054581956718821;
	setAttr ".wl[181].w[24]" 3.6133436995301952e-006;
	setAttr ".wl[182].w[22]"  1;
	setAttr -s 3 ".wl[183].w";
	setAttr ".wl[183].w[13]" 0.75320227336840861;
	setAttr ".wl[183].w[22]" 0.2467887856451895;
	setAttr ".wl[183].w[24]" 8.9409864019595644e-006;
	setAttr -s 2 ".wl[184].w";
	setAttr ".wl[184].w[22]" 0.21331468275902696;
	setAttr ".wl[184].w[28]" 0.78668531724097313;
	setAttr -s 2 ".wl[185].w[22:23]"  0.73432077717447952 0.26567922282552048;
	setAttr -s 3 ".wl[186].w";
	setAttr ".wl[186].w[22]" 0.90220410967416143;
	setAttr ".wl[186].w[23]" 0.09665047242207607;
	setAttr ".wl[186].w[27]" 0.0011454179037625103;
	setAttr ".wl[187].w[23]"  1;
	setAttr -s 3 ".wl[188].w";
	setAttr ".wl[188].w[23]" 0.55000697676355015;
	setAttr ".wl[188].w[25]" 0.22401057156676965;
	setAttr ".wl[188].w[27]" 0.22598245166968031;
	setAttr ".wl[189].w[23]"  1;
	setAttr -s 2 ".wl[190].w[22:23]"  0.61496022564033437 0.38503977435966563;
	setAttr -s 3 ".wl[191].w";
	setAttr ".wl[191].w[23]" 0.49545284572419518;
	setAttr ".wl[191].w[25]" 0.25426675850830083;
	setAttr ".wl[191].w[27]" 0.25028039576750388;
	setAttr -s 3 ".wl[192].w";
	setAttr ".wl[192].w[13]" 0.31800026165679329;
	setAttr ".wl[192].w[22]" 0.68143522666640988;
	setAttr ".wl[192].w[24]" 0.00056451167679678774;
	setAttr -s 3 ".wl[193].w";
	setAttr ".wl[193].w[13]" 0.99997611495000016;
	setAttr ".wl[193].w[18]" 1.1564608140927862e-005;
	setAttr ".wl[193].w[23]" 1.2320441858916085e-005;
	setAttr -s 3 ".wl[194].w";
	setAttr ".wl[194].w[13]" 0.99999238919835787;
	setAttr ".wl[194].w[14]" 4.1216539273485042e-006;
	setAttr ".wl[194].w[18]" 3.4891477147360494e-006;
	setAttr ".wl[195].w[28]"  1;
	setAttr ".wl[196].w[28]"  1;
	setAttr ".wl[197].w[28]"  1;
	setAttr ".wl[198].w[28]"  1;
	setAttr ".wl[199].w[28]"  1;
	setAttr ".wl[200].w[28]"  1;
	setAttr -s 2 ".wl[201].w";
	setAttr ".wl[201].w[22]" 0.0038451312593086829;
	setAttr ".wl[201].w[28]" 0.99615486874069137;
	setAttr -s 2 ".wl[202].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[203].w[22:23]"  0.5 0.5;
	setAttr ".wl[204].w[13]"  1;
	setAttr ".wl[205].w[13]"  1;
	setAttr ".wl[206].w[22]"  1;
	setAttr -s 3 ".wl[207].w";
	setAttr ".wl[207].w[22]" 0.99996319437301306;
	setAttr ".wl[207].w[23]" 2.3494366499918019e-005;
	setAttr ".wl[207].w[29]" 1.3311260487002768e-005;
	setAttr -s 3 ".wl[208].w";
	setAttr ".wl[208].w[22]" 0.99998545488287793;
	setAttr ".wl[208].w[24]" 3.2192759885729623e-006;
	setAttr ".wl[208].w[27]" 1.1325841133503243e-005;
	setAttr -s 3 ".wl[209].w";
	setAttr ".wl[209].w[23]" 0.51512636759663866;
	setAttr ".wl[209].w[25]" 0.24598329814696551;
	setAttr ".wl[209].w[27]" 0.2388903342563958;
	setAttr -s 2 ".wl[210].w";
	setAttr ".wl[210].w[23]" 0.68222635149539035;
	setAttr ".wl[210].w[27]" 0.31777364850460976;
	setAttr ".wl[211].w[23]"  1;
	setAttr -s 3 ".wl[212].w";
	setAttr ".wl[212].w[23]" 0.50363344767292417;
	setAttr ".wl[212].w[25]" 0.25333979551768576;
	setAttr ".wl[212].w[27]" 0.24302675680938993;
	setAttr -s 3 ".wl[213].w";
	setAttr ".wl[213].w[22]" 0.99989663639016879;
	setAttr ".wl[213].w[24]" 4.6784001732478145e-005;
	setAttr ".wl[213].w[27]" 5.6579608098633244e-005;
	setAttr -s 3 ".wl[214].w";
	setAttr ".wl[214].w[13]" 0.99997062539111881;
	setAttr ".wl[214].w[23]" 1.6926732266564432e-005;
	setAttr ".wl[214].w[27]" 1.2447876614677919e-005;
	setAttr -s 3 ".wl[215].w";
	setAttr ".wl[215].w[13]" 0.99998040351420958;
	setAttr ".wl[215].w[14]" 7.0116547954513412e-006;
	setAttr ".wl[215].w[18]" 1.2584830995103257e-005;
	setAttr ".wl[216].w[28]"  1;
	setAttr ".wl[217].w[28]"  1;
	setAttr ".wl[218].w[28]"  1;
	setAttr ".wl[219].w[28]"  1;
	setAttr ".wl[220].w[28]"  1;
	setAttr -s 2 ".wl[221].w";
	setAttr ".wl[221].w[22]" 9.9495261620079884e-006;
	setAttr ".wl[221].w[28]" 0.99999005047383804;
	setAttr -s 2 ".wl[222].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[223].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[224].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[225].w";
	setAttr ".wl[225].w[13]" 0.9999907684366528;
	setAttr ".wl[225].w[14]" 5.0112311310786918e-006;
	setAttr ".wl[225].w[18]" 4.2203322161086922e-006;
	setAttr -s 3 ".wl[226].w";
	setAttr ".wl[226].w[13]" 0.99997537737539111;
	setAttr ".wl[226].w[14]" 1.7962625877061615e-005;
	setAttr ".wl[226].w[18]" 6.6599987317281497e-006;
	setAttr -s 3 ".wl[227].w";
	setAttr ".wl[227].w[1]" 0.23571926591467987;
	setAttr ".wl[227].w[13]" 0.7569114331709843;
	setAttr ".wl[227].w[14]" 0.0073693009143359503;
	setAttr ".wl[228].w[13]"  1;
	setAttr -s 2 ".wl[229].w";
	setAttr ".wl[229].w[13]" 0.99985465869709744;
	setAttr ".wl[229].w[18]" 0.00014534130290264522;
	setAttr ".wl[230].w[13]"  1;
	setAttr ".wl[231].w[13]"  1;
	setAttr -s 2 ".wl[232].w";
	setAttr ".wl[232].w[22]" 0.35453448146987671;
	setAttr ".wl[232].w[28]" 0.64546551853012335;
	setAttr -s 2 ".wl[233].w";
	setAttr ".wl[233].w[22]" 0.018152764916445508;
	setAttr ".wl[233].w[28]" 0.98184723508355454;
	setAttr -s 3 ".wl[234].w";
	setAttr ".wl[234].w[22]" 0.50198731509165273;
	setAttr ".wl[234].w[23]" 0.38359448506473437;
	setAttr ".wl[234].w[28]" 0.11441819984361286;
	setAttr ".wl[235].w[22]"  1;
	setAttr -s 3 ".wl[236].w";
	setAttr ".wl[236].w[22]" 0.97800921992402157;
	setAttr ".wl[236].w[23]" 0.02148528099105098;
	setAttr ".wl[236].w[27]" 0.00050549908492744525;
	setAttr -s 3 ".wl[237].w";
	setAttr ".wl[237].w[22]" 0.99998596816000118;
	setAttr ".wl[237].w[23]" 4.9340480259018467e-006;
	setAttr ".wl[237].w[27]" 9.097791972870398e-006;
	setAttr -s 2 ".wl[238].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[239].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[240].w";
	setAttr ".wl[240].w[23]" 0.51519106616743793;
	setAttr ".wl[240].w[25]" 0.24148749896172098;
	setAttr ".wl[240].w[27]" 0.24332143487084099;
	setAttr ".wl[241].w[23]"  1;
	setAttr ".wl[242].w[23]"  1;
	setAttr -s 2 ".wl[243].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[244].w";
	setAttr ".wl[244].w[23]" 0.49556020298597192;
	setAttr ".wl[244].w[25]" 0.25416219869619028;
	setAttr ".wl[244].w[27]" 0.25027759831783769;
	setAttr -s 3 ".wl[245].w[22:24]"  0.99965864462241893 0.00014260402106134091 
		0.00019875135651975149;
	setAttr -s 3 ".wl[246].w";
	setAttr ".wl[246].w[13]" 0.56751846249685178;
	setAttr ".wl[246].w[22]" 0.43223104014471042;
	setAttr ".wl[246].w[23]" 0.00025049735843783521;
	setAttr ".wl[247].w[13]"  1;
	setAttr -s 3 ".wl[248].w";
	setAttr ".wl[248].w[13]" 0.99997626470895873;
	setAttr ".wl[248].w[14]" 8.1896359344463097e-006;
	setAttr ".wl[248].w[18]" 1.5545655106884261e-005;
	setAttr ".wl[249].w[28]"  1;
	setAttr ".wl[250].w[28]"  1;
	setAttr ".wl[251].w[28]"  1;
	setAttr ".wl[252].w[28]"  1;
	setAttr ".wl[253].w[28]"  1;
	setAttr -s 2 ".wl[254].w";
	setAttr ".wl[254].w[22]" 6.691423141414821e-005;
	setAttr ".wl[254].w[28]" 0.99993308576858586;
	setAttr -s 2 ".wl[255].w[22:23]"  0.5 0.5;
	setAttr ".wl[256].w[13]"  1;
	setAttr -s 2 ".wl[257].w";
	setAttr ".wl[257].w[13]" 0.99982270783994698;
	setAttr ".wl[257].w[18]" 0.00017729216005298341;
	setAttr -s 3 ".wl[258].w";
	setAttr ".wl[258].w[13]" 0.75320231438135943;
	setAttr ".wl[258].w[22]" 0.24679569013282618;
	setAttr ".wl[258].w[23]" 1.9954858143749012e-006;
	setAttr -s 3 ".wl[259].w";
	setAttr ".wl[259].w[13]" 0.38459819905647541;
	setAttr ".wl[259].w[22]" 0.61539711291977328;
	setAttr ".wl[259].w[23]" 4.688023751381541e-006;
	setAttr ".wl[260].w[22]"  1;
	setAttr -s 3 ".wl[261].w";
	setAttr ".wl[261].w[22]" 0.9996625644960313;
	setAttr ".wl[261].w[23]" 0.00026113296083291813;
	setAttr ".wl[261].w[29]" 7.6302543135797109e-005;
	setAttr -s 3 ".wl[262].w";
	setAttr ".wl[262].w[22]" 0.99998103538655081;
	setAttr ".wl[262].w[23]" 1.2681982127231956e-005;
	setAttr ".wl[262].w[27]" 6.2826313220887272e-006;
	setAttr -s 3 ".wl[263].w";
	setAttr ".wl[263].w[23]" 0.50408289595164468;
	setAttr ".wl[263].w[25]" 0.25164260684764012;
	setAttr ".wl[263].w[27]" 0.24427449720071515;
	setAttr -s 3 ".wl[264].w";
	setAttr ".wl[264].w[23]" 0.50517034704861441;
	setAttr ".wl[264].w[25]" 0.24652675859290665;
	setAttr ".wl[264].w[27]" 0.24830289435847894;
	setAttr -s 2 ".wl[265].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[266].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[267].w";
	setAttr ".wl[267].w[22]" 0.99970064998143915;
	setAttr ".wl[267].w[23]" 0.00019572852721573631;
	setAttr ".wl[267].w[27]" 0.00010362149134500566;
	setAttr -s 3 ".wl[268].w";
	setAttr ".wl[268].w[22]" 0.99971513075810059;
	setAttr ".wl[268].w[23]" 0.00023094524569719353;
	setAttr ".wl[268].w[27]" 5.3923996202248337e-005;
	setAttr ".wl[269].w[13]"  1;
	setAttr ".wl[270].w[28]"  1;
	setAttr ".wl[271].w[28]"  1;
	setAttr ".wl[272].w[28]"  1;
	setAttr -s 2 ".wl[273].w";
	setAttr ".wl[273].w[28]" 0.99998503820859108;
	setAttr ".wl[273].w[35]" 1.4961791408950268e-005;
	setAttr -s 2 ".wl[274].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[275].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[276].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[277].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[278].w";
	setAttr ".wl[278].w[13]" 0.99997294700945016;
	setAttr ".wl[278].w[14]" 9.2060786390074843e-006;
	setAttr ".wl[278].w[18]" 1.7846911910913794e-005;
	setAttr -s 3 ".wl[279].w";
	setAttr ".wl[279].w[1]" 0.46562636878880237;
	setAttr ".wl[279].w[13]" 0.53310416300918462;
	setAttr ".wl[279].w[18]" 0.0012694682020130499;
	setAttr -s 3 ".wl[280].w";
	setAttr ".wl[280].w[1]" 0.53041632246816528;
	setAttr ".wl[280].w[13]" 0.46663272907269632;
	setAttr ".wl[280].w[14]" 0.00295094845913829;
	setAttr -s 2 ".wl[281].w";
	setAttr ".wl[281].w[1]" 0.49104068730036948;
	setAttr ".wl[281].w[13]" 0.50895931269963046;
	setAttr ".wl[282].w[13]"  1;
	setAttr -s 3 ".wl[283].w[13:15]"  0.48792631259209246 0.51196565823753004 
		0.00010802917037749986;
	setAttr ".wl[284].w[13]"  1;
	setAttr ".wl[285].w[13]"  1;
	setAttr -s 2 ".wl[286].w";
	setAttr ".wl[286].w[13]" 0.99669107403236035;
	setAttr ".wl[286].w[18]" 0.0033089259676397495;
	setAttr -s 3 ".wl[287].w";
	setAttr ".wl[287].w[22]" 0.41969768956920239;
	setAttr ".wl[287].w[23]" 0.00063442927950647552;
	setAttr ".wl[287].w[28]" 0.57966788115129109;
	setAttr -s 2 ".wl[288].w";
	setAttr ".wl[288].w[22]" 0.057156864297577152;
	setAttr ".wl[288].w[28]" 0.94284313570242284;
	setAttr -s 2 ".wl[289].w";
	setAttr ".wl[289].w[22]" 0.00026777196988810492;
	setAttr ".wl[289].w[28]" 0.99973222803011186;
	setAttr -s 3 ".wl[290].w";
	setAttr ".wl[290].w[22]" 0.35889060474470913;
	setAttr ".wl[290].w[23]" 0.57761170635021786;
	setAttr ".wl[290].w[28]" 0.063497688905072958;
	setAttr -s 3 ".wl[291].w";
	setAttr ".wl[291].w[22]" 0.38166990794278333;
	setAttr ".wl[291].w[23]" 0.1456823535529593;
	setAttr ".wl[291].w[28]" 0.47264773850425751;
	setAttr -s 3 ".wl[292].w";
	setAttr ".wl[292].w[22]" 0.98809059525522414;
	setAttr ".wl[292].w[23]" 0.011155827939288671;
	setAttr ".wl[292].w[27]" 0.00075357680548720845;
	setAttr -s 2 ".wl[293].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[294].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[295].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[296].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[297].w";
	setAttr ".wl[297].w[23]" 0.49709500354559605;
	setAttr ".wl[297].w[25]" 0.25348366886814061;
	setAttr ".wl[297].w[27]" 0.24942132758626329;
	setAttr -s 3 ".wl[298].w";
	setAttr ".wl[298].w[13]" 0.17455281749513168;
	setAttr ".wl[298].w[22]" 0.82431401380360081;
	setAttr ".wl[298].w[23]" 0.0011331687012674683;
	setAttr ".wl[299].w[13]"  1;
	setAttr ".wl[300].w[13]"  1;
	setAttr ".wl[301].w[28]"  1;
	setAttr ".wl[302].w[28]"  1;
	setAttr ".wl[303].w[28]"  1;
	setAttr ".wl[304].w[28]"  1;
	setAttr ".wl[305].w[28]"  1;
	setAttr ".wl[306].w[28]"  1;
	setAttr -s 2 ".wl[307].w";
	setAttr ".wl[307].w[28]" 0.99992889866364121;
	setAttr ".wl[307].w[35]" 7.1101336358746082e-005;
	setAttr -s 2 ".wl[308].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[309].w";
	setAttr ".wl[309].w[1]" 0.65404998434214268;
	setAttr ".wl[309].w[13]" 0.34595001565785738;
	setAttr -s 2 ".wl[310].w";
	setAttr ".wl[310].w[1]" 0.29118931875565673;
	setAttr ".wl[310].w[13]" 0.70881068124434321;
	setAttr -s 3 ".wl[311].w[13:15]"  0.48119808199799574 0.51874933426463687 
		5.258373736741815e-005;
	setAttr ".wl[312].w[13]"  1;
	setAttr -s 3 ".wl[313].w";
	setAttr ".wl[313].w[13]" 0.72516239856373188;
	setAttr ".wl[313].w[22]" 0.27483031655234363;
	setAttr ".wl[313].w[23]" 7.2848839244917721e-006;
	setAttr -s 3 ".wl[314].w";
	setAttr ".wl[314].w[13]" 0.41977252461916931;
	setAttr ".wl[314].w[22]" 0.58020862762500069;
	setAttr ".wl[314].w[23]" 1.8847755829981486e-005;
	setAttr ".wl[315].w[22]"  1;
	setAttr -s 3 ".wl[316].w";
	setAttr ".wl[316].w[13]" 0.49994196072341068;
	setAttr ".wl[316].w[14]" 0.49994196072341068;
	setAttr ".wl[316].w[18]" 0.00011607855317859658;
	setAttr -s 3 ".wl[317].w";
	setAttr ".wl[317].w[22]" 0.99800776116403911;
	setAttr ".wl[317].w[23]" 0.0017574043758177658;
	setAttr ".wl[317].w[29]" 0.00023483446014313408;
	setAttr -s 2 ".wl[318].w";
	setAttr ".wl[318].w[28]" 0.99992194339793028;
	setAttr ".wl[318].w[35]" 7.8056602069674524e-005;
	setAttr ".wl[319].w[22]"  1;
	setAttr -s 2 ".wl[320].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[321].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[322].w[22:23]"  0.50115148922497221 0.49884851077502779;
	setAttr -s 2 ".wl[323].w";
	setAttr ".wl[323].w[22]" 0.99943307341762455;
	setAttr ".wl[323].w[35]" 0.00056692658237546603;
	setAttr -s 2 ".wl[324].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[325].w";
	setAttr ".wl[325].w[22]" 0.99576865272930237;
	setAttr ".wl[325].w[23]" 0.0039561091435624859;
	setAttr ".wl[325].w[29]" 0.0002752381271351966;
	setAttr -s 3 ".wl[326].w";
	setAttr ".wl[326].w[13]" 0.47966530267197249;
	setAttr ".wl[326].w[22]" 0.5198741095870919;
	setAttr ".wl[326].w[23]" 0.00046058774093562333;
	setAttr ".wl[327].w[13]"  1;
	setAttr ".wl[328].w[28]"  1;
	setAttr ".wl[329].w[28]"  1;
	setAttr -s 2 ".wl[330].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[331].w";
	setAttr ".wl[331].w[1]" 0.52787613725376414;
	setAttr ".wl[331].w[13]" 0.46362869514771876;
	setAttr ".wl[331].w[18]" 0.0084951675985171886;
	setAttr -s 3 ".wl[332].w";
	setAttr ".wl[332].w[1]" 0.22973236994850021;
	setAttr ".wl[332].w[13]" 0.74967083902671061;
	setAttr ".wl[332].w[18]" 0.020596791024789118;
	setAttr -s 3 ".wl[333].w";
	setAttr ".wl[333].w[1]" 0.83284999464036835;
	setAttr ".wl[333].w[13]" 0.1666580765351085;
	setAttr ".wl[333].w[14]" 0.00049192882452311264;
	setAttr ".wl[334].w[1]"  1;
	setAttr -s 2 ".wl[335].w";
	setAttr ".wl[335].w[1]" 0.76146554815137013;
	setAttr ".wl[335].w[13]" 0.2385344518486299;
	setAttr ".wl[336].w[1]"  1;
	setAttr -s 3 ".wl[337].w[13:15]"  0.24121197506740508 0.44269887536995739 
		0.31608914956263756;
	setAttr ".wl[338].w[13]"  1;
	setAttr -s 2 ".wl[339].w[13:14]"  0.98194622191212899 0.018053778087871033;
	setAttr -s 2 ".wl[340].w[13:14]"  0.9997286742437469 0.00027132575625311075;
	setAttr -s 3 ".wl[341].w";
	setAttr ".wl[341].w[13]" 0.44921786525870677;
	setAttr ".wl[341].w[14]" 0.44921786525870677;
	setAttr ".wl[341].w[18]" 0.10156426948258654;
	setAttr -s 2 ".wl[342].w";
	setAttr ".wl[342].w[22]" 0.18114436316772614;
	setAttr ".wl[342].w[28]" 0.81885563683227391;
	setAttr -s 3 ".wl[343].w";
	setAttr ".wl[343].w[22]" 0.44342789469650357;
	setAttr ".wl[343].w[23]" 0.0048415082925523717;
	setAttr ".wl[343].w[28]" 0.55173059701094407;
	setAttr -s 2 ".wl[344].w";
	setAttr ".wl[344].w[22]" 0.0015644809368035575;
	setAttr ".wl[344].w[28]" 0.9984355190631965;
	setAttr -s 2 ".wl[345].w";
	setAttr ".wl[345].w[28]" 0.99986127909947653;
	setAttr ".wl[345].w[35]" 0.00013872090052350234;
	setAttr -s 3 ".wl[346].w";
	setAttr ".wl[346].w[22]" 0.22684733399024554;
	setAttr ".wl[346].w[23]" 0.54256877907808609;
	setAttr ".wl[346].w[28]" 0.23058388693166842;
	setAttr ".wl[347].w[23]"  1;
	setAttr -s 2 ".wl[348].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[349].w[22:23]"  0.50477737145458501 0.49522262854541499;
	setAttr ".wl[350].w[13]"  1;
	setAttr ".wl[351].w[13]"  1;
	setAttr ".wl[352].w[13]"  1;
	setAttr ".wl[353].w[28]"  1;
	setAttr ".wl[354].w[28]"  1;
	setAttr ".wl[355].w[28]"  1;
	setAttr ".wl[356].w[28]"  1;
	setAttr ".wl[357].w[28]"  1;
	setAttr -s 2 ".wl[358].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[359].w";
	setAttr ".wl[359].w[1]" 0.83225041767376107;
	setAttr ".wl[359].w[13]" 0.16612214302615055;
	setAttr ".wl[359].w[18]" 0.001627439300088373;
	setAttr ".wl[360].w[1]"  1;
	setAttr -s 3 ".wl[361].w";
	setAttr ".wl[361].w[1]" 0.28513394735807984;
	setAttr ".wl[361].w[14]" 0.28823713404107953;
	setAttr ".wl[361].w[15]" 0.42662891860084068;
	setAttr -s 3 ".wl[362].w[13:15]"  0.18325183732641767 0.41409710515339931 
		0.40265105752018293;
	setAttr -s 3 ".wl[363].w[13:15]"  0.00028880369933610592 0.99816648847321776 
		0.0015447078274462651;
	setAttr -s 2 ".wl[364].w[13:14]"  0.99992150178695138 7.8498213048615689e-005;
	setAttr ".wl[365].w[13]"  1;
	setAttr -s 3 ".wl[366].w";
	setAttr ".wl[366].w[13]" 0.79340135332538719;
	setAttr ".wl[366].w[22]" 0.20658046863646168;
	setAttr ".wl[366].w[23]" 1.81780381512127e-005;
	setAttr -s 3 ".wl[367].w";
	setAttr ".wl[367].w[13]" 0.61547319676942147;
	setAttr ".wl[367].w[22]" 0.38444386764142574;
	setAttr ".wl[367].w[23]" 8.293558915280291e-005;
	setAttr -s 3 ".wl[368].w";
	setAttr ".wl[368].w[13]" 0.34453722507973927;
	setAttr ".wl[368].w[14]" 0.30839664592213917;
	setAttr ".wl[368].w[18]" 0.34706612899812161;
	setAttr ".wl[369].w[28]"  1;
	setAttr -s 3 ".wl[370].w";
	setAttr ".wl[370].w[22]" 0.19272440932871543;
	setAttr ".wl[370].w[23]" 0.18168342097780846;
	setAttr ".wl[370].w[28]" 0.62559216969347609;
	setAttr -s 2 ".wl[371].w";
	setAttr ".wl[371].w[23]" 0.99982822557872653;
	setAttr ".wl[371].w[35]" 0.00017177442127348295;
	setAttr ".wl[372].w[13]"  1;
	setAttr ".wl[373].w[28]"  1;
	setAttr -s 2 ".wl[374].w[22:23]"  0.5 0.5;
	setAttr ".wl[375].w[1]"  1;
	setAttr -s 3 ".wl[376].w";
	setAttr ".wl[376].w[1]" 0.63964280744228974;
	setAttr ".wl[376].w[13]" 0.33289781061258966;
	setAttr ".wl[376].w[18]" 0.027459381945120702;
	setAttr ".wl[377].w[13]"  1;
	setAttr ".wl[378].w[1]"  1;
	setAttr ".wl[379].w[1]"  1;
	setAttr ".wl[380].w[1]"  1;
	setAttr ".wl[381].w[1]"  1;
	setAttr -s 2 ".wl[382].w";
	setAttr ".wl[382].w[1]" 0.67672567525133198;
	setAttr ".wl[382].w[15]" 0.32327432474866807;
	setAttr -s 3 ".wl[383].w[13:15]"  0.034684856730631854 0.24774542499881472 
		0.71756971827055338;
	setAttr -s 3 ".wl[384].w";
	setAttr ".wl[384].w[13]" 0.4992648990372861;
	setAttr ".wl[384].w[14]" 0.0014702019254278947;
	setAttr ".wl[384].w[18]" 0.4992648990372861;
	setAttr -s 3 ".wl[385].w";
	setAttr ".wl[385].w[13]" 0.49999990826354518;
	setAttr ".wl[385].w[14]" 1.8347290964230382e-007;
	setAttr ".wl[385].w[18]" 0.49999990826354518;
	setAttr ".wl[386].w[28]"  1;
	setAttr ".wl[387].w[28]"  1;
	setAttr -s 2 ".wl[388].w[22:23]"  0.50063164720799314 0.49936835279200681;
	setAttr ".wl[389].w[28]"  1;
	setAttr -s 3 ".wl[390].w";
	setAttr ".wl[390].w[23]" 0.76327289558833977;
	setAttr ".wl[390].w[28]" 0.23672679477159983;
	setAttr ".wl[390].w[35]" 3.0964006041599521e-007;
	setAttr -s 3 ".wl[391].w";
	setAttr ".wl[391].w[13]" 0.90703469038629547;
	setAttr ".wl[391].w[14]" 0.00015469314161801323;
	setAttr ".wl[391].w[18]" 0.092810616472086535;
	setAttr ".wl[392].w[28]"  1;
	setAttr ".wl[393].w[28]"  1;
	setAttr ".wl[394].w[1]"  1;
	setAttr ".wl[395].w[1]"  1;
	setAttr -s 3 ".wl[396].w";
	setAttr ".wl[396].w[1]" 0.44060553000918401;
	setAttr ".wl[396].w[13]" 0.45118951888851883;
	setAttr ".wl[396].w[18]" 0.10820495110229718;
	setAttr ".wl[397].w[1]"  1;
	setAttr -s 3 ".wl[398].w[14:16]"  0.49775115367816125 0.50212282515368334 
		0.00012602116815546086;
	setAttr -s 3 ".wl[399].w[14:16]"  0.096101865265100991 0.90384532239241355 
		5.2812342485555326e-005;
	setAttr -s 3 ".wl[400].w[13:15]"  0.043822527181482163 0.35967549785141306 
		0.59650197496710478;
	setAttr -s 3 ".wl[401].w";
	setAttr ".wl[401].w[1]" 0.17495341362042888;
	setAttr ".wl[401].w[14]" 0.24475048299491933;
	setAttr ".wl[401].w[15]" 0.58029610338465187;
	setAttr -s 3 ".wl[402].w";
	setAttr ".wl[402].w[13]" 0.49841541739780809;
	setAttr ".wl[402].w[18]" 0.50157049592001335;
	setAttr ".wl[402].w[19]" 1.4086682178665807e-005;
	setAttr -s 3 ".wl[403].w";
	setAttr ".wl[403].w[13]" 0.1664131228366667;
	setAttr ".wl[403].w[18]" 0.41242215991859421;
	setAttr ".wl[403].w[19]" 0.42116471724473908;
	setAttr -s 3 ".wl[404].w";
	setAttr ".wl[404].w[22]" 0.14755510455374449;
	setAttr ".wl[404].w[23]" 0.030280575227144856;
	setAttr ".wl[404].w[28]" 0.82216432021911068;
	setAttr -s 3 ".wl[405].w";
	setAttr ".wl[405].w[22]" 0.41067838183437733;
	setAttr ".wl[405].w[28]" 0.58932143793558434;
	setAttr ".wl[405].w[35]" 1.8023003838278017e-007;
	setAttr ".wl[406].w[28]"  1;
	setAttr ".wl[407].w[1]"  1;
	setAttr ".wl[408].w[1]"  1;
	setAttr -s 3 ".wl[409].w";
	setAttr ".wl[409].w[13]" 0.93879812381439254;
	setAttr ".wl[409].w[14]" 7.8025202729497333e-005;
	setAttr ".wl[409].w[18]" 0.061123850982877932;
	setAttr -s 3 ".wl[410].w";
	setAttr ".wl[410].w[1]" 0.18851135077788708;
	setAttr ".wl[410].w[13]" 0.45630688892553684;
	setAttr ".wl[410].w[18]" 0.35518176029657611;
	setAttr ".wl[411].w[1]"  1;
	setAttr ".wl[412].w[1]"  1;
	setAttr ".wl[413].w[1]"  1;
	setAttr -s 2 ".wl[414].w";
	setAttr ".wl[414].w[1]" 0.74544278585686419;
	setAttr ".wl[414].w[7]" 0.25455721414313581;
	setAttr -s 2 ".wl[415].w";
	setAttr ".wl[415].w[1]" 0.66045317144853977;
	setAttr ".wl[415].w[17]" 0.33954682855146018;
	setAttr -s 2 ".wl[416].w";
	setAttr ".wl[416].w[1]" 0.54233006678314821;
	setAttr ".wl[416].w[15]" 0.45766993321685179;
	setAttr -s 3 ".wl[417].w[14:16]"  0.13048408378165466 0.86655445075837489 
		0.0029614654599703322;
	setAttr -s 3 ".wl[418].w[14:16]"  0.035168691521483476 0.96482533839174589 
		5.9700867705810154e-006;
	setAttr -s 3 ".wl[419].w";
	setAttr ".wl[419].w[1]" 0.40233921817343909;
	setAttr ".wl[419].w[15]" 0.29973991722706278;
	setAttr ".wl[419].w[18]" 0.29792086459949824;
	setAttr -s 3 ".wl[420].w[14:16]"  0.00084631147141690517 0.99618333717735297 
		0.002970351351230153;
	setAttr -s 3 ".wl[421].w[14:16]"  0.0006031578416302155 0.98211442375122993 
		0.017282418407139812;
	setAttr -s 3 ".wl[422].w";
	setAttr ".wl[422].w[13]" 0.26160644810275258;
	setAttr ".wl[422].w[18]" 0.36743688463080926;
	setAttr ".wl[422].w[19]" 0.37095666726643811;
	setAttr -s 3 ".wl[423].w";
	setAttr ".wl[423].w[13]" 4.0106262089745014e-007;
	setAttr ".wl[423].w[18]" 0.49999979946868955;
	setAttr ".wl[423].w[19]" 0.49999979946868955;
	setAttr ".wl[424].w[28]"  1;
	setAttr ".wl[425].w[28]"  1;
	setAttr ".wl[426].w[1]"  1;
	setAttr ".wl[427].w[1]"  1;
	setAttr ".wl[428].w[1]"  1;
	setAttr -s 3 ".wl[429].w";
	setAttr ".wl[429].w[1]" 0.58574665188642705;
	setAttr ".wl[429].w[13]" 0.16826157476627573;
	setAttr ".wl[429].w[18]" 0.24599177334729716;
	setAttr ".wl[430].w[1]"  1;
	setAttr ".wl[431].w[1]"  1;
	setAttr -s 3 ".wl[432].w[14:16]"  0.46182432929410144 0.53789383097139742 
		0.0002818397345011592;
	setAttr -s 3 ".wl[433].w[14:16]"  0.0030603916277532396 0.99482629573084858 
		0.0021133126413982717;
	setAttr -s 3 ".wl[434].w[14:16]"  0.4235946985515695 0.57556040224752769 
		0.00084489920090282083;
	setAttr -s 2 ".wl[435].w";
	setAttr ".wl[435].w[1]" 0.44205122665254681;
	setAttr ".wl[435].w[15]" 0.55794877334745319;
	setAttr -s 3 ".wl[436].w[14:16]"  0.00040899755095612434 0.98915155857900761 
		0.010439443870036381;
	setAttr -s 3 ".wl[437].w";
	setAttr ".wl[437].w[13]" 0.1988435242808172;
	setAttr ".wl[437].w[18]" 0.32531162714374101;
	setAttr ".wl[437].w[19]" 0.47584484857544179;
	setAttr -s 3 ".wl[438].w[18:20]"  0.016939332730636674 0.98305857526251628 
		2.0920068470950155e-006;
	setAttr -s 2 ".wl[439].w";
	setAttr ".wl[439].w[1]" 0.88416514855147299;
	setAttr ".wl[439].w[18]" 0.11583485144852697;
	setAttr ".wl[440].w[1]"  1;
	setAttr -s 3 ".wl[441].w";
	setAttr ".wl[441].w[1]" 0.23715452712347959;
	setAttr ".wl[441].w[18]" 0.46687087705043107;
	setAttr ".wl[441].w[19]" 0.29597459582608943;
	setAttr ".wl[442].w[1]"  1;
	setAttr ".wl[443].w[1]"  1;
	setAttr ".wl[444].w[1]"  1;
	setAttr ".wl[445].w[1]"  1;
	setAttr -s 2 ".wl[446].w";
	setAttr ".wl[446].w[1]" 0.6553728871056701;
	setAttr ".wl[446].w[7]" 0.3446271128943299;
	setAttr ".wl[447].w[1]"  1;
	setAttr ".wl[448].w[1]"  1;
	setAttr -s 3 ".wl[449].w[14:16]"  0.00052033087220918351 0.98697389101338806 
		0.012505778114402779;
	setAttr -s 3 ".wl[450].w[14:16]"  0.00010062038817085562 0.99948420579978148 
		0.00041517381204772189;
	setAttr -s 3 ".wl[451].w[14:16]"  0.0038002415319463487 0.99355632768039914 
		0.0026434307876545267;
	setAttr -s 3 ".wl[452].w";
	setAttr ".wl[452].w[1]" 0.74006349038078312;
	setAttr ".wl[452].w[15]" 0.15061231243578974;
	setAttr ".wl[452].w[18]" 0.10932419718342722;
	setAttr -s 3 ".wl[453].w";
	setAttr ".wl[453].w[1]" 0.37295768180485556;
	setAttr ".wl[453].w[18]" 0.32464528563034456;
	setAttr ".wl[453].w[19]" 0.30239703256479999;
	setAttr -s 3 ".wl[454].w[14:16]"  5.0782394717036938e-005 0.76333476751297258 
		0.23661445009231047;
	setAttr -s 3 ".wl[455].w[15:17]"  0.62526162384324147 0.37467743324338915 
		6.0942913369392997e-005;
	setAttr -s 3 ".wl[456].w[15:17]"  0.45616390632554782 0.48056073315882486 
		0.06327536051562728;
	setAttr -s 3 ".wl[457].w[18:20]"  0.19680659079315543 0.79568960012196122 
		0.007503809084883295;
	setAttr -s 3 ".wl[458].w";
	setAttr ".wl[458].w[13]" 0.066290549222890086;
	setAttr ".wl[458].w[18]" 0.14588092811295328;
	setAttr ".wl[458].w[19]" 0.78782852266415659;
	setAttr -s 3 ".wl[459].w[18:20]"  0.4119881522260927 0.58723234664870472 
		0.00077950112520265514;
	setAttr -s 3 ".wl[460].w[18:20]"  0.098196613460220558 0.89895846972619609 
		0.002844916813583414;
	setAttr ".wl[461].w[1]"  1;
	setAttr ".wl[462].w[1]"  1;
	setAttr ".wl[463].w[1]"  1;
	setAttr ".wl[464].w[1]"  1;
	setAttr -s 2 ".wl[465].w";
	setAttr ".wl[465].w[1]" 0.63100134340455005;
	setAttr ".wl[465].w[18]" 0.36899865659544995;
	setAttr ".wl[466].w[1]"  1;
	setAttr ".wl[467].w[1]"  1;
	setAttr -s 3 ".wl[468].w[14:16]"  0.0028112019655510152 0.99551914789919427 
		0.0016696501352547156;
	setAttr -s 3 ".wl[469].w[15:17]"  0.18151515747302385 0.81794386524782037 
		0.00054097727915579418;
	setAttr -s 3 ".wl[470].w[18:20]"  0.12079243234093141 0.87908577312804959 
		0.00012179453101909752;
	setAttr -s 3 ".wl[471].w[18:20]"  0.0022268280480256626 0.99584692280123077 
		0.0019262491507435564;
	setAttr -s 3 ".wl[472].w[18:20]"  2.9866138277456188e-005 0.99980279020491514 
		0.0001673436568073537;
	setAttr ".wl[473].w[1]"  1;
	setAttr ".wl[474].w[1]"  1;
	setAttr -s 3 ".wl[475].w[18:20]"  0.4963705390273016 0.50346182376690962 
		0.00016763720578881627;
	setAttr -s 3 ".wl[476].w";
	setAttr ".wl[476].w[1]" 0.77916762520949001;
	setAttr ".wl[476].w[3]" 0.15875604405029017;
	setAttr ".wl[476].w[7]" 0.062076330740219787;
	setAttr ".wl[477].w[1]"  1;
	setAttr -s 2 ".wl[478].w[1:2]"  0.86079655959822587 0.13920344040177421;
	setAttr -s 2 ".wl[479].w[1:2]"  0.73782453443126761 0.26217546556873234;
	setAttr ".wl[480].w[1]"  1;
	setAttr ".wl[481].w[1]"  1;
	setAttr ".wl[482].w[1]"  1;
	setAttr -s 2 ".wl[483].w";
	setAttr ".wl[483].w[1]" 0.99707548682578762;
	setAttr ".wl[483].w[21]" 0.0029245131742123408;
	setAttr -s 3 ".wl[484].w[15:17]"  0.50657447055862104 0.49328179930649524 
		0.00014373013488368276;
	setAttr -s 3 ".wl[485].w[15:17]"  0.66749583849883842 0.33248828215593118 
		1.5879345230484508e-005;
	setAttr -s 3 ".wl[486].w[14:16]"  0.00014758328933654633 0.70747316158209284 
		0.29237925512857066;
	setAttr -s 3 ".wl[487].w[14:16]"  3.0832334945150343e-005 0.7840319968459909 
		0.21593717081906391;
	setAttr ".wl[488].w[1]"  1;
	setAttr -s 3 ".wl[489].w[18:20]"  0.45556062904005229 0.54413590288356872 
		0.00030346807637899296;
	setAttr -s 3 ".wl[490].w[15:17]"  0.49998880836708171 0.49998880836708159 
		2.2383265836750639e-005;
	setAttr -s 3 ".wl[491].w[15:17]"  0.068327896306765945 0.92353359774636168 
		0.008138505946872529;
	setAttr -s 3 ".wl[492].w[15:17]"  0.00062288279729474602 0.88873567256055785 
		0.11064144464214737;
	setAttr -s 3 ".wl[493].w[18:20]"  0.00068751103799610629 0.97693961120304906 
		0.022372877758954905;
	setAttr -s 3 ".wl[494].w[18:20]"  0.0015468947108276307 0.99228845769727003 
		0.0061646475919023324;
	setAttr -s 3 ".wl[495].w[18:20]"  0.0023230108311248182 0.99592569068514647 
		0.0017512984837286904;
	setAttr -s 3 ".wl[496].w[18:20]"  0.00036420042241905076 0.98850023359545536 
		0.011135565982125619;
	setAttr -s 2 ".wl[497].w";
	setAttr ".wl[497].w[1]" 0.63718785262095556;
	setAttr ".wl[497].w[8]" 0.36281214737904444;
	setAttr -s 2 ".wl[498].w";
	setAttr ".wl[498].w[1]" 0.71155275412504837;
	setAttr ".wl[498].w[8]" 0.28844724587495163;
	setAttr -s 3 ".wl[499].w";
	setAttr ".wl[499].w[1]" 0.70327186335468928;
	setAttr ".wl[499].w[2]" 0.11402563733884702;
	setAttr ".wl[499].w[8]" 0.18270249930646368;
	setAttr -s 3 ".wl[500].w";
	setAttr ".wl[500].w[1]" 0.67421508702562527;
	setAttr ".wl[500].w[2]" 0.2398685705782935;
	setAttr ".wl[500].w[8]" 0.085916342396081161;
	setAttr ".wl[501].w[1]"  1;
	setAttr -s 3 ".wl[502].w";
	setAttr ".wl[502].w[1]" 0.29993468924678818;
	setAttr ".wl[502].w[18]" 0.35116691761262658;
	setAttr ".wl[502].w[19]" 0.34889839314058529;
	setAttr -s 3 ".wl[503].w[1:3]"  0.66125745102476829 0.31380107551653857 
		0.024941473458693186;
	setAttr ".wl[504].w[1]"  1;
	setAttr ".wl[505].w[1]"  1;
	setAttr -s 3 ".wl[506].w[15:17]"  0.0016210466932292072 0.83851140248428968 
		0.15986755082248111;
	setAttr -s 3 ".wl[507].w[15:17]"  3.8754817649068544e-005 0.76504903255689083 
		0.23491221262546011;
	setAttr -s 3 ".wl[508].w[18:20]"  0.00069778184187846036 0.98140225338566989 
		0.017899964772451666;
	setAttr -s 3 ".wl[509].w[18:20]"  0.0037957181270752566 0.99289859823283078 
		0.0033056836400940132;
	setAttr -s 3 ".wl[510].w[18:20]"  2.193447701057282e-005 0.76899813411335971 
		0.23097993140962966;
	setAttr -s 3 ".wl[511].w[18:20]"  5.7192917772820676e-005 0.72178847517524736 
		0.2781543319069798;
	setAttr -s 3 ".wl[512].w[19:21]"  0.68658819269155835 0.3134079058993493 
		3.9014090923895513e-006;
	setAttr ".wl[513].w[1]"  1;
	setAttr -s 2 ".wl[514].w";
	setAttr ".wl[514].w[1]" 0.70647645452876073;
	setAttr ".wl[514].w[8]" 0.29352354547123932;
	setAttr -s 3 ".wl[515].w";
	setAttr ".wl[515].w[1]" 0.66200885478322669;
	setAttr ".wl[515].w[2]" 0.31434507220760211;
	setAttr ".wl[515].w[8]" 0.023646073009171265;
	setAttr -s 3 ".wl[516].w";
	setAttr ".wl[516].w[1]" 0.4841699684269728;
	setAttr ".wl[516].w[3]" 0.50298508777730477;
	setAttr ".wl[516].w[7]" 0.012844943795722404;
	setAttr -s 3 ".wl[517].w";
	setAttr ".wl[517].w[1]" 0.53546289572429739;
	setAttr ".wl[517].w[3]" 0.44955117652443616;
	setAttr ".wl[517].w[7]" 0.014985927751266417;
	setAttr -s 2 ".wl[518].w";
	setAttr ".wl[518].w[1]" 0.41367802262930031;
	setAttr ".wl[518].w[3]" 0.5863219773706998;
	setAttr -s 2 ".wl[519].w[1:2]"  0.469312869400203 0.530687130599797;
	setAttr -s 2 ".wl[520].w[1:2]"  0.34814819377575301 0.6518518062242471;
	setAttr -s 3 ".wl[521].w[1:3]"  0.26940230347467697 0.71183969075178077 
		0.018758005773542374;
	setAttr -s 2 ".wl[522].w";
	setAttr ".wl[522].w[1]" 0.59011019682314081;
	setAttr ".wl[522].w[3]" 0.40988980317685925;
	setAttr -s 2 ".wl[523].w";
	setAttr ".wl[523].w[1]" 0.7985985081126622;
	setAttr ".wl[523].w[8]" 0.20140149188733783;
	setAttr ".wl[524].w[1]"  1;
	setAttr -s 3 ".wl[525].w[15:17]"  0.060360823214061742 0.93291279364159874 
		0.0067263831443394725;
	setAttr -s 3 ".wl[526].w[15:17]"  0.49996802514893174 0.49996802514893174 
		6.394970213659067e-005;
	setAttr -s 3 ".wl[527].w[15:17]"  0.49999683180720955 0.49999683180720955 
		6.3363855808582585e-006;
	setAttr -s 3 ".wl[528].w[15:17]"  0.064895693143540406 0.93031675519777479 
		0.0047875516586848117;
	setAttr -s 3 ".wl[529].w[15:17]"  0.00020967736557324793 0.65463061513344634 
		0.34515970750098046;
	setAttr -s 2 ".wl[530].w[16:17]"  0.50000000000001565 0.49999999999998435;
	setAttr -s 3 ".wl[531].w[19:21]"  0.48452538442386045 0.45909049949120179 
		0.056384116084937777;
	setAttr -s 3 ".wl[532].w[19:21]"  0.61484537249742555 0.38511501141621318 
		3.9616086361254439e-005;
	setAttr -s 3 ".wl[533].w[18:20]"  8.799666058335397e-005 0.71046790460566978 
		0.28944409873374694;
	setAttr -s 3 ".wl[534].w[19:21]"  0.45334529730382017 0.54661271787667542 
		4.1984819504473847e-005;
	setAttr -s 2 ".wl[535].w";
	setAttr ".wl[535].w[2]" 0.0021709477775211693;
	setAttr ".wl[535].w[8]" 0.99782905222247886;
	setAttr -s 2 ".wl[536].w";
	setAttr ".wl[536].w[2]" 0.021563815157282971;
	setAttr ".wl[536].w[8]" 0.9784361848427171;
	setAttr -s 2 ".wl[537].w";
	setAttr ".wl[537].w[1]" 0.4161365581066192;
	setAttr ".wl[537].w[8]" 0.58386344189338091;
	setAttr -s 3 ".wl[538].w";
	setAttr ".wl[538].w[1]" 0.3534120506404857;
	setAttr ".wl[538].w[2]" 0.39971199705907484;
	setAttr ".wl[538].w[8]" 0.24687595230043935;
	setAttr -s 3 ".wl[539].w";
	setAttr ".wl[539].w[1]" 0.3265977533753503;
	setAttr ".wl[539].w[2]" 0.61112871686423698;
	setAttr ".wl[539].w[8]" 0.062273529760412727;
	setAttr -s 3 ".wl[540].w";
	setAttr ".wl[540].w[1]" 0.26995052780276391;
	setAttr ".wl[540].w[2]" 0.71284666134045149;
	setAttr ".wl[540].w[8]" 0.017202810856784607;
	setAttr -s 3 ".wl[541].w[1:3]"  0.35966387562143443 0.63484565452887876 
		0.005490469849686869;
	setAttr -s 2 ".wl[542].w";
	setAttr ".wl[542].w[1]" 0.84471288326339866;
	setAttr ".wl[542].w[8]" 0.15528711673660134;
	setAttr -s 3 ".wl[543].w[15:17]"  0.00047422378987163779 0.62685079498547169 
		0.37267498122465664;
	setAttr -s 3 ".wl[544].w[15:17]"  0.036485095202683428 0.9616034609926476 
		0.0019114438046690353;
	setAttr -s 3 ".wl[545].w[15:17]"  6.7505060219972629e-005 0.58721640334577729 
		0.4127160915940028;
	setAttr -s 3 ".wl[546].w[19:21]"  0.47333071580550429 0.52625478908468504 
		0.00041449510981070526;
	setAttr -s 3 ".wl[547].w[19:21]"  0.49998781102866385 0.49998781102866385 
		2.4377942672374307e-005;
	setAttr -s 3 ".wl[548].w[19:21]"  0.027459652381158058 0.96995121237787063 
		0.0025891352409713841;
	setAttr -s 2 ".wl[549].w";
	setAttr ".wl[549].w[1]" 0.49072532582469525;
	setAttr ".wl[549].w[8]" 0.50927467417530481;
	setAttr -s 2 ".wl[550].w[2:3]"  0.00055813831006815964 0.99944186168993188;
	setAttr -s 2 ".wl[551].w[2:3]"  1.8297287157517048e-005 0.99998170271284248;
	setAttr -s 3 ".wl[552].w[2:4]"  0.32349395302249467 0.64370889858613667 
		0.032797148391368616;
	setAttr ".wl[553].w[2]"  1;
	setAttr ".wl[554].w[2]"  1;
	setAttr ".wl[555].w[2]"  1;
	setAttr -s 2 ".wl[556].w";
	setAttr ".wl[556].w[1]" 0.51979281783756237;
	setAttr ".wl[556].w[8]" 0.48020718216243763;
	setAttr -s 3 ".wl[557].w[15:17]"  0.0002065131055508879 0.56650057069612525 
		0.43329291619832389;
	setAttr -s 3 ".wl[558].w[15:17]"  0.098886193416394486 0.89137310372966294 
		0.0097407028539426323;
	setAttr -s 3 ".wl[559].w[19:21]"  0.0012454740133824411 0.85797361990651322 
		0.14078090608010438;
	setAttr -s 3 ".wl[560].w[19:21]"  0.00056181851963017023 0.897426654580601 
		0.10201152689976893;
	setAttr -s 3 ".wl[561].w[19:21]"  0.49997430678063787 0.49997430678063776 
		5.1386438724391179e-005;
	setAttr -s 3 ".wl[562].w[19:21]"  0.061925813179150635 0.92979146970505466 
		0.0082827171157946397;
	setAttr -s 3 ".wl[563].w[19:21]"  0.49999716539137867 0.49999716539137867 
		5.6692172426804812e-006;
	setAttr -s 3 ".wl[564].w";
	setAttr ".wl[564].w[2]" 1.0585718469123354e-005;
	setAttr ".wl[564].w[8]" 0.49999470714076544;
	setAttr ".wl[564].w[9]" 0.49999470714076544;
	setAttr -s 3 ".wl[565].w";
	setAttr ".wl[565].w[2]" 0.00072908816644941146;
	setAttr ".wl[565].w[8]" 0.99535077907359204;
	setAttr ".wl[565].w[9]" 0.0039201327599585056;
	setAttr -s 2 ".wl[566].w";
	setAttr ".wl[566].w[2]" 0.33935594608368119;
	setAttr ".wl[566].w[8]" 0.66064405391631875;
	setAttr ".wl[567].w[2]"  1;
	setAttr ".wl[568].w[2]"  1;
	setAttr ".wl[569].w[2]"  1;
	setAttr -s 3 ".wl[570].w";
	setAttr ".wl[570].w[1]" 0.24373452906516763;
	setAttr ".wl[570].w[3]" 0.46070318678317784;
	setAttr ".wl[570].w[4]" 0.29556228415165453;
	setAttr ".wl[571].w[2]"  1;
	setAttr -s 3 ".wl[572].w[15:17]"  7.3749777414085834e-006 0.49999631251112925 
		0.49999631251112925;
	setAttr -s 3 ".wl[573].w[19:21]"  2.9997649531475657e-005 0.79224463021360114 
		0.2077253721368674;
	setAttr -s 3 ".wl[574].w[19:21]"  0.030426491859237161 0.96772724945315935 
		0.0018462586876035146;
	setAttr -s 3 ".wl[575].w[19:21]"  0.060717042598299388 0.93398101307811188 
		0.0053019443235887195;
	setAttr -s 3 ".wl[576].w[19:21]"  0.000130080566797172 0.68410688663463359 
		0.31576303279856932;
	setAttr -s 3 ".wl[577].w";
	setAttr ".wl[577].w[2]" 1.8342365009026034e-005;
	setAttr ".wl[577].w[8]" 0.95292329139592014;
	setAttr ".wl[577].w[9]" 0.047058366239070892;
	setAttr -s 3 ".wl[578].w[3:5]"  0.49527339639156898 0.50467269835881645 
		5.3905249614528733e-005;
	setAttr -s 3 ".wl[579].w[3:5]"  0.49824102815680288 0.50175128903000987 
		7.6828131872686501e-006;
	setAttr -s 3 ".wl[580].w[2:4]"  0.1468389426805348 0.73987675311024659 
		0.11328430420921859;
	setAttr ".wl[581].w[2]"  1;
	setAttr ".wl[582].w[2]"  1;
	setAttr ".wl[583].w[2]"  1;
	setAttr -s 3 ".wl[584].w";
	setAttr ".wl[584].w[4]" 0.25954053876380412;
	setAttr ".wl[584].w[8]" 0.27305638799069659;
	setAttr ".wl[584].w[9]" 0.46740307324549929;
	setAttr -s 3 ".wl[585].w[19:21]"  0.00055325573552272541 0.63088202861867448 
		0.3685647156458029;
	setAttr -s 2 ".wl[586].w[20:21]"  0.50000000000000022 0.49999999999999972;
	setAttr -s 3 ".wl[587].w[19:21]"  0.08038049978824878 0.91130223786345266 
		0.0083172623482986752;
	setAttr -s 3 ".wl[588].w[19:21]"  7.1017761350512814e-005 0.59649771722291134 
		0.40343126501573817;
	setAttr -s 3 ".wl[589].w[8:10]"  0.49680506174315364 0.50318379978185079 
		1.1138474995619379e-005;
	setAttr -s 3 ".wl[590].w[8:10]"  0.49192555888222622 0.50797298372706001 
		0.00010145739071377865;
	setAttr -s 3 ".wl[591].w";
	setAttr ".wl[591].w[2]" 0.15566185650551831;
	setAttr ".wl[591].w[8]" 0.71494313617539362;
	setAttr ".wl[591].w[9]" 0.12939500731908807;
	setAttr ".wl[592].w[2]"  1;
	setAttr ".wl[593].w[2]"  1;
	setAttr ".wl[594].w[2]"  1;
	setAttr -s 3 ".wl[595].w";
	setAttr ".wl[595].w[3]" 0.18980874873458853;
	setAttr ".wl[595].w[4]" 0.58462116803048625;
	setAttr ".wl[595].w[9]" 0.22557008323492522;
	setAttr ".wl[596].w[2]"  1;
	setAttr -s 3 ".wl[597].w[19:21]"  0.00016151853926688002 0.57926863107775139 
		0.42056985038298167;
	setAttr -s 3 ".wl[598].w[8:10]"  0.49418262823054937 0.50579433229069282 
		2.3039478757811597e-005;
	setAttr -s 3 ".wl[599].w[3:5]"  0.26904900536359377 0.69418544317270359 
		0.036765551463702667;
	setAttr -s 3 ".wl[600].w[3:5]"  0.12275943428604111 0.875552095462236 
		0.0016884702517229561;
	setAttr -s 3 ".wl[601].w[3:5]"  0.1071172961130813 0.89261260509086804 
		0.00027009879605054767;
	setAttr -s 3 ".wl[602].w[2:4]"  0.39511543275153743 0.17738972303683229 
		0.42749484421163025;
	setAttr -s 2 ".wl[603].w";
	setAttr ".wl[603].w[2]" 0.79701836773924661;
	setAttr ".wl[603].w[4]" 0.20298163226075339;
	setAttr -s 2 ".wl[604].w";
	setAttr ".wl[604].w[2]" 0.93445029500735022;
	setAttr ".wl[604].w[4]" 0.065549704992649838;
	setAttr ".wl[605].w[2]"  1;
	setAttr -s 3 ".wl[606].w";
	setAttr ".wl[606].w[4]" 0.30432002815538584;
	setAttr ".wl[606].w[8]" 0.13271317791286841;
	setAttr ".wl[606].w[9]" 0.56296679393174576;
	setAttr -s 3 ".wl[607].w[19:21]"  6.4320455117976984e-006 0.49999678397724406 
		0.49999678397724406;
	setAttr -s 3 ".wl[608].w[8:10]"  0.11103278720042896 0.88859485715754194 
		0.00037235564202912994;
	setAttr -s 3 ".wl[609].w[8:10]"  0.13003987733641389 0.86724221755925068 
		0.0027179051043354991;
	setAttr -s 3 ".wl[610].w[8:10]"  0.25429979181106244 0.69731618467007261 
		0.048384023518864991;
	setAttr -s 3 ".wl[611].w";
	setAttr ".wl[611].w[2]" 0.37283290670158531;
	setAttr ".wl[611].w[8]" 0.17153249896969139;
	setAttr ".wl[611].w[9]" 0.45563459432872339;
	setAttr -s 3 ".wl[612].w";
	setAttr ".wl[612].w[2]" 0.68272085139088379;
	setAttr ".wl[612].w[9]" 0.23547572533901545;
	setAttr ".wl[612].w[10]" 0.081803423270100761;
	setAttr -s 3 ".wl[613].w";
	setAttr ".wl[613].w[2]" 0.82982387565929649;
	setAttr ".wl[613].w[9]" 0.10921986572882404;
	setAttr ".wl[613].w[10]" 0.060956258611879297;
	setAttr -s 3 ".wl[614].w[3:5]"  0.14444846996506924 0.85475146749098208 
		0.00080006254394872443;
	setAttr -s 3 ".wl[615].w[8:10]"  0.13376210255460816 0.86555510952552839 
		0.00068278791986346407;
	setAttr ".wl[616].w[4]"  1;
	setAttr -s 3 ".wl[617].w[3:5]"  0.00096383666868388011 0.70846434624841592 
		0.29057181708290025;
	setAttr -s 3 ".wl[618].w[3:5]"  0.00014897993544559147 0.78221677040493764 
		0.21763424965961672;
	setAttr -s 3 ".wl[619].w[3:5]"  0.00010601544769193904 0.74066726884394085 
		0.25922671570836725;
	setAttr -s 2 ".wl[620].w";
	setAttr ".wl[620].w[2]" 0.45141327540719511;
	setAttr ".wl[620].w[4]" 0.54858672459280489;
	setAttr -s 2 ".wl[621].w";
	setAttr ".wl[621].w[2]" 0.79264916203436264;
	setAttr ".wl[621].w[4]" 0.2073508379656373;
	setAttr -s 3 ".wl[622].w";
	setAttr ".wl[622].w[2]" 0.78796935392980694;
	setAttr ".wl[622].w[9]" 0.12460780325617535;
	setAttr ".wl[622].w[10]" 0.087422842814017784;
	setAttr -s 3 ".wl[623].w";
	setAttr ".wl[623].w[4]" 0.36716147808366462;
	setAttr ".wl[623].w[5]" 0.38499088989805874;
	setAttr ".wl[623].w[9]" 0.24784763201827661;
	setAttr -s 3 ".wl[624].w[8:10]"  0.00017730702584553063 0.72492680163676715 
		0.27489589133738729;
	setAttr -s 3 ".wl[625].w[8:10]"  8.7970635483401659e-005 0.70241841460495591 
		0.29749361475956065;
	setAttr -s 3 ".wl[626].w[8:10]"  0.0011022364352783907 0.67161645802573777 
		0.32728130553898377;
	setAttr ".wl[627].w[9]"  1;
	setAttr -s 3 ".wl[628].w";
	setAttr ".wl[628].w[2]" 0.26830646361153793;
	setAttr ".wl[628].w[9]" 0.51237248658468981;
	setAttr ".wl[628].w[10]" 0.21932104980377221;
	setAttr -s 3 ".wl[629].w";
	setAttr ".wl[629].w[2]" 0.50825304863508924;
	setAttr ".wl[629].w[9]" 0.29523566577775362;
	setAttr ".wl[629].w[10]" 0.1965112855871573;
	setAttr -s 3 ".wl[630].w[3:5]"  0.00058894232325048168 0.68698442791260672 
		0.31242662976414276;
	setAttr -s 3 ".wl[631].w[8:10]"  8.6983667177537593e-006 0.93432818214357205 
		0.065663119489710223;
	setAttr -s 2 ".wl[632].w[9:10]"  0.77371977660046598 0.22628022339953405;
	setAttr -s 3 ".wl[633].w[8:10]"  0.0006978002051142801 0.64358581753822919 
		0.35571638225665653;
	setAttr -s 3 ".wl[634].w[3:5]"  2.1082000734340844e-005 0.49998945899963282 
		0.49998945899963282;
	setAttr -s 3 ".wl[635].w[3:5]"  9.7938249597423564e-006 0.49999510308752015 
		0.49999510308752015;
	setAttr -s 3 ".wl[636].w[4:6]"  0.17025663898401375 0.81131510270594787 
		0.018428258310038352;
	setAttr -s 3 ".wl[637].w[4:6]"  0.01205594264463552 0.77764324073109081 
		0.21030081662427372;
	setAttr -s 3 ".wl[638].w[4:6]"  0.0013829392842136848 0.8952292360304237 
		0.1033878246853626;
	setAttr -s 3 ".wl[639].w[4:6]"  0.0078225652050416487 0.9920285190204966 
		0.00014891577446176697;
	setAttr -s 3 ".wl[640].w";
	setAttr ".wl[640].w[2]" 0.32900037600688015;
	setAttr ".wl[640].w[4]" 0.27997604344496668;
	setAttr ".wl[640].w[5]" 0.39102358054815323;
	setAttr -s 3 ".wl[641].w";
	setAttr ".wl[641].w[2]" 0.4806866774707178;
	setAttr ".wl[641].w[9]" 0.28974257352376415;
	setAttr ".wl[641].w[10]" 0.22957074900551802;
	setAttr -s 3 ".wl[642].w";
	setAttr ".wl[642].w[4]" 0.13174682515738181;
	setAttr ".wl[642].w[5]" 0.63017944880466037;
	setAttr ".wl[642].w[9]" 0.2380737260379579;
	setAttr -s 3 ".wl[643].w[9:11]"  0.0013466627344383979 0.88330338368144634 
		0.11534995358411529;
	setAttr -s 3 ".wl[644].w[9:11]"  0.012410593248478574 0.75907816044802023 
		0.22851124630350125;
	setAttr -s 3 ".wl[645].w[9:11]"  0.0039479908988244794 0.99596076899815489 
		9.1240103020596693e-005;
	setAttr -s 3 ".wl[646].w[9:11]"  0.17213930962956725 0.80354003210103608 
		0.024320658269396622;
	setAttr -s 3 ".wl[647].w[8:10]"  2.5651401469247473e-005 0.49998717429926537 
		0.49998717429926537;
	setAttr -s 3 ".wl[648].w[8:10]"  1.0633495317886751e-005 0.49999468325234103 
		0.49999468325234103;
	setAttr -s 3 ".wl[649].w[8:10]"  2.1779993886195349e-005 0.4999891100030569 
		0.4999891100030569;
	setAttr -s 3 ".wl[650].w[4:6]"  0.10714460534493755 0.55264626464179878 
		0.34020913001326369;
	setAttr -s 3 ".wl[651].w[8:10]"  1.5042206804236793e-006 0.49999924788965977 
		0.49999924788965977;
	setAttr -s 2 ".wl[652].w[9:10]"  0.60074719005035981 0.39925280994964019;
	setAttr -s 3 ".wl[653].w[9:11]"  0.10111529942114494 0.55635681885151134 
		0.34252788172734377;
	setAttr -s 3 ".wl[654].w[4:6]"  0.17122159007683679 0.82670132150233999 
		0.0020770884208231627;
	setAttr -s 3 ".wl[655].w[4:6]"  0.22822691114806457 0.76782505826539105 
		0.0039480305865443391;
	setAttr -s 3 ".wl[656].w[4:6]"  0.080064829094481274 0.9197245474515614 
		0.00021062345395734636;
	setAttr -s 3 ".wl[657].w[4:6]"  0.016422130548036262 0.80618476643458403 
		0.17739310301737973;
	setAttr -s 3 ".wl[658].w[5:7]"  0.25164460555748763 0.7377136815662575 
		0.010641712876254863;
	setAttr -s 3 ".wl[659].w[5:7]"  0.025719288165996581 0.97384915826835294 
		0.0004315535656505134;
	setAttr -s 3 ".wl[660].w[4:6]"  7.5494515225426131e-005 0.99223641133937845 
		0.0076880941453962102;
	setAttr -s 3 ".wl[661].w[10:12]"  0.033540857190050559 0.96596365777884219 
		0.00049548503110729322;
	setAttr -s 3 ".wl[662].w[10:12]"  0.26836801945285427 0.72034469079848618 
		0.0112872897486595;
	setAttr -s 3 ".wl[663].w[9:11]"  0.017709269170927775 0.77697619353782899 
		0.2053145372912433;
	setAttr -s 3 ".wl[664].w[9:11]"  0.070725190087433762 0.92902540452396898 
		0.00024940538859734415;
	setAttr -s 3 ".wl[665].w[9:11]"  3.2712477897920382e-005 0.99412955239672229 
		0.005837735125379862;
	setAttr -s 3 ".wl[666].w[9:11]"  0.23028565927259639 0.76395414237979553 
		0.0057601983476080797;
	setAttr -s 3 ".wl[667].w[9:11]"  0.17163330525127618 0.82535942630834247 
		0.003007268440381519;
	setAttr -s 3 ".wl[668].w[4:6]"  0.011033987055181358 0.89764655058335829 
		0.091319462361460255;
	setAttr -s 3 ".wl[669].w[5:7]"  0.5426918784290008 0.45453422969452989 
		0.0027738918764693915;
	setAttr -s 3 ".wl[670].w[5:7]"  0.021126133890750177 0.97882282476876048 
		5.1041340489258053e-005;
	setAttr -s 3 ".wl[671].w[4:6]"  0.00030834004800960296 0.99251930691718393 
		0.007172353034806434;
	setAttr -s 3 ".wl[672].w[5:7]"  0.68262759666425044 0.31736397414722256 
		8.4291885269070374e-006;
	setAttr -s 3 ".wl[673].w[10:12]"  0.013780502539667882 0.98620005646877551 
		1.9440991556735059e-005;
	setAttr -s 3 ".wl[674].w[10:12]"  0.53174262492585644 0.46508308992277997 
		0.0031742851513636527;
	setAttr -s 3 ".wl[675].w[9:11]"  0.012536749319470986 0.87125459178322595 
		0.11620865889730302;
	setAttr -s 3 ".wl[676].w[9:11]"  0.00027175311777048836 0.99085094254895723 
		0.0088773043332723341;
	setAttr -s 3 ".wl[677].w[10:12]"  0.67595168359360625 0.32404482380150224 
		3.4926048915753152e-006;
	setAttr -s 3 ".wl[678].w[4:6]"  0.0034476737254763947 0.95300056988169146 
		0.04355175639283218;
	setAttr -s 3 ".wl[679].w[4:6]"  0.001109234008283617 0.63420938904680546 
		0.36468137694491087;
	setAttr -s 3 ".wl[680].w[5:7]"  0.46158155037166243 0.53356612569539696 
		0.0048523239329405794;
	setAttr -s 3 ".wl[681].w[5:7]"  0.0053632394913253143 0.71118111446809817 
		0.28345564604057649;
	setAttr -s 3 ".wl[682].w[5:7]"  6.82641277823521e-006 0.9804467943601235 
		0.019546379227098357;
	setAttr -s 3 ".wl[683].w[4:6]"  5.6478163631305961e-006 0.86587383698721532 
		0.13412051519642151;
	setAttr -s 3 ".wl[684].w[10:12]"  3.7167280542221435e-006 0.9905024675649492 
		0.0094938157069966393;
	setAttr -s 3 ".wl[685].w[10:12]"  0.0074441734756810199 0.71183031210926095 
		0.28072551441505811;
	setAttr -s 3 ".wl[686].w[10:12]"  0.46663636659957658 0.52776724538885544 
		0.0055963880115678958;
	setAttr -s 3 ".wl[687].w[9:11]"  0.0013333350004985068 0.60723736735267664 
		0.39142929764682488;
	setAttr -s 3 ".wl[688].w[9:11]"  0.0040120990887277299 0.93622129187607317 
		0.059766609035199189;
	setAttr -s 3 ".wl[689].w[9:11]"  4.3182335005612444e-006 0.84252295183792136 
		0.15747272992857805;
	setAttr -s 3 ".wl[690].w[4:6]"  7.5854120095498226e-005 0.75437592002875387 
		0.2455482258511506;
	setAttr -s 3 ".wl[691].w[4:6]"  0.00018432673469470976 0.5018922791116569 
		0.49792339415364839;
	setAttr -s 3 ".wl[692].w[5:7]"  6.7752696848464916e-005 0.89612722018368551 
		0.10380502711946599;
	setAttr -s 3 ".wl[693].w[5:7]"  0.34371322848808544 0.65627036447004161 
		1.6407041872888343e-005;
	setAttr -s 3 ".wl[694].w[4:6]"  2.0554923525800456e-007 0.50919813035959327 
		0.49080166409117154;
	setAttr -s 3 ".wl[695].w[10:12]"  0.33326353660812619 0.6667292682925956 
		7.1950992782472456e-006;
	setAttr -s 3 ".wl[696].w[10:12]"  0.00012354362218242818 0.8946798390138675 
		0.10519661736395004;
	setAttr -s 3 ".wl[697].w[9:11]"  0.00024283156269335303 0.50008662551305871 
		0.49967054292424795;
	setAttr -s 3 ".wl[698].w[9:11]"  0.00010031641848867527 0.70839196174904995 
		0.29150772183246149;
	setAttr -s 3 ".wl[699].w[9:11]"  1.456831598635487e-007 0.50110734710839777 
		0.4988925072084423;
	setAttr -s 3 ".wl[700].w[4:6]"  6.3582839470528498e-006 0.50509139528285563 
		0.49490224643319736;
	setAttr -s 3 ".wl[701].w[5:7]"  0.49967283414813363 0.49967283414813385 
		0.00065433170373244089;
	setAttr -s 3 ".wl[702].w[5:7]"  0.35017006924410238 0.63325915138099398 
		0.016570779374903675;
	setAttr -s 3 ".wl[703].w[5:7]"  0.0016616185029389942 0.53544436202331258 
		0.46289401947374836;
	setAttr -s 3 ".wl[704].w[5:7]"  2.5741142832412935e-007 0.49999987129428586 
		0.49999987129428586;
	setAttr -s 3 ".wl[705].w[5:7]"  0.11929490879675839 0.88047112803536165 
		0.00023396316787996282;
	setAttr -s 3 ".wl[706].w[10:12]"  0.10642405946245312 0.89341711358280829 
		0.00015882695473865715;
	setAttr -s 3 ".wl[707].w[10:12]"  6.6169201561814992e-007 0.49999966915399219 
		0.49999966915399219;
	setAttr -s 3 ".wl[708].w[10:12]"  0.35974227858099878 0.62114588532750581 
		0.019111836091495422;
	setAttr -s 3 ".wl[709].w[10:12]"  0.0022962085068217938 0.53792771507809611 
		0.45977607641508211;
	setAttr -s 3 ".wl[710].w[10:12]"  0.49954138320078234 0.49954138320078234 
		0.00091723359843528466;
	setAttr -s 3 ".wl[711].w[9:11]"  9.6548140721766001e-006 0.50067052306711812 
		0.49931982211880965;
	setAttr -s 3 ".wl[712].w[5:7]"  0.49998711159689202 0.49998711159689191 
		2.5776806216093091e-005;
	setAttr -s 3 ".wl[713].w[5:7]"  5.3705123428630998e-007 0.72601894909181586 
		0.27398051385694988;
	setAttr -s 3 ".wl[714].w[5:7]"  0.49999613891953704 0.49999613891953693 
		7.7221609260734668e-006;
	setAttr -s 3 ".wl[715].w[10:12]"  0.49999565941595697 0.49999565941595697 
		8.6811680860845022e-006;
	setAttr -s 3 ".wl[716].w[10:12]"  1.9682277755111286e-007 0.80168992156555885 
		0.19830988161166371;
	setAttr -s 3 ".wl[717].w[10:12]"  0.49997753743417928 0.49997753743417928 
		4.4925131641443346e-005;
	setAttr -s 3 ".wl[718].w[5:7]"  0.20494130787194448 0.7755749450470123 
		0.019483747081043432;
	setAttr -s 3 ".wl[719].w[5:7]"  0.00078792739725622857 0.5308648018540042 
		0.46834727074873966;
	setAttr -s 3 ".wl[720].w[5:7]"  3.0354247940929714e-007 0.68415133686087926 
		0.31584835959664137;
	setAttr -s 3 ".wl[721].w[5:7]"  0.079537261425446065 0.91794546834723267 
		0.0025172702273212975;
	setAttr -s 3 ".wl[722].w[10:12]"  0.077116065381086313 0.92034287779478352 
		0.0025410568241302101;
	setAttr -s 3 ".wl[723].w[10:12]"  1.4642491617171282e-007 0.73301969271696277 
		0.26698016085812115;
	setAttr -s 3 ".wl[724].w[10:12]"  0.0010871358556323904 0.53180168382949722 
		0.46711118031487048;
	setAttr -s 3 ".wl[725].w[10:12]"  0.21576235358972271 0.76071603070076821 
		0.023521615709509065;
	setAttr -s 3 ".wl[726].w[5:7]"  0.11398352029103136 0.88057896938560332 
		0.0054375103233653514;
	setAttr -s 3 ".wl[727].w[5:7]"  8.3602457758154265e-006 0.54473990036004138 
		0.45525173939418284;
	setAttr -s 3 ".wl[728].w[10:12]"  1.3507905702493965e-005 0.54587908880209357 
		0.45410740329220406;
	setAttr -s 3 ".wl[729].w[10:12]"  0.12499284077610913 0.86795101446574929 
		0.0070561447581415455;
	setAttr -s 3 ".wl[730].w[22:24]"  0.49999753783507983 0.49999753783507983 
		4.9243298403409522e-006;
	setAttr -s 3 ".wl[731].w[22:24]"  0.51029890836874825 0.48954989426223522 
		0.00015119736901660197;
	setAttr -s 3 ".wl[732].w[22:24]"  0.49999966287667519 0.49999966287667519 
		6.7424664961763309e-007;
	setAttr -s 3 ".wl[733].w[22:24]"  0.51675598372117382 0.48322314261439603 
		2.0873664430103596e-005;
	setAttr -s 2 ".wl[734].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[735].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[736].w[22:23]"  0.51548327385751713 0.48451672614248287;
	setAttr -s 2 ".wl[737].w[22:23]"  0.50958295017482469 0.49041704982517531;
	setAttr -s 3 ".wl[738].w[22:24]"  0.49999679723793389 0.49999679723793389 
		6.4055241322189959e-006;
	setAttr -s 3 ".wl[739].w[22:24]"  0.49999999737565765 0.49999999737565765 
		5.2486847582000672e-009;
	setAttr -s 3 ".wl[740].w[22:24]"  0.49999977921498651 0.49999977921498651 
		4.4157002696563973e-007;
	setAttr -s 3 ".wl[741].w";
	setAttr ".wl[741].w[23]" 0.53259898138676876;
	setAttr ".wl[741].w[28]" 0.46740090875761703;
	setAttr ".wl[741].w[35]" 1.0985561430463526e-007;
	setAttr -s 3 ".wl[742].w";
	setAttr ".wl[742].w[22]" 0.21644293328622924;
	setAttr ".wl[742].w[23]" 0.38153984001971147;
	setAttr ".wl[742].w[28]" 0.4020172266940592;
	setAttr -s 3 ".wl[743].w";
	setAttr ".wl[743].w[22]" 0.28097301584260331;
	setAttr ".wl[743].w[23]" 0.50516619401095153;
	setAttr ".wl[743].w[28]" 0.21386079014644518;
	setAttr ".wl[744].w[23]"  1;
	setAttr ".wl[745].w[23]"  1;
	setAttr ".wl[746].w[23]"  1;
	setAttr ".wl[747].w[23]"  1;
	setAttr ".wl[748].w[23]"  1;
	setAttr ".wl[749].w[23]"  1;
	setAttr ".wl[750].w[23]"  1;
	setAttr ".wl[751].w[23]"  1;
	setAttr ".wl[752].w[23]"  1;
	setAttr -s 2 ".wl[753].w";
	setAttr ".wl[753].w[23]" 0.67192844264495699;
	setAttr ".wl[753].w[25]" 0.32807155735504301;
	setAttr -s 2 ".wl[754].w";
	setAttr ".wl[754].w[23]" 0.99215686321258545;
	setAttr ".wl[754].w[35]" 0.0078431367874145508;
	setAttr -s 2 ".wl[755].w";
	setAttr ".wl[755].w[23]" 0.675377521797212;
	setAttr ".wl[755].w[25]" 0.32462247820278795;
	setAttr -s 3 ".wl[756].w[22:24]"  0.59106005386574278 0.40893152206948996 
		8.4240647673058671e-006;
	setAttr -s 3 ".wl[757].w[22:24]"  0.58966361851695726 0.41033580217599064 
		5.7930705217112465e-007;
	setAttr -s 3 ".wl[758].w[22:24]"  0.50564539587683044 0.49433587866661388 
		1.8725456555767846e-005;
	setAttr -s 3 ".wl[759].w[22:24]"  0.54404162133014178 0.45592566386713157 
		3.2714802726724514e-005;
	setAttr -s 3 ".wl[760].w[22:24]"  0.50742816830354043 0.49256625653964126 
		5.5751568183018964e-006;
	setAttr -s 3 ".wl[761].w[22:24]"  0.49997617107951708 0.49997617107951708 
		4.7657840965820474e-005;
	setAttr -s 3 ".wl[762].w[22:24]"  0.50108148437752242 0.49891830791852837 
		2.077039492585905e-007;
	setAttr -s 3 ".wl[763].w[22:24]"  0.53761516614815186 0.46238460223711891 
		2.3161472932760248e-007;
	setAttr -s 3 ".wl[764].w[22:24]"  0.50731520320338275 0.4926843641879658 
		4.3260865140401299e-007;
	setAttr -s 2 ".wl[765].w[22:23]"  0.50000000000000011 0.49999999999999994;
	setAttr -s 2 ".wl[766].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[767].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[768].w[22:24]"  0.50250472330962936 0.49749186719160871 
		3.409498761988397e-006;
	setAttr -s 2 ".wl[769].w[22:23]"  0.50188089039468375 0.4981191096053163;
	setAttr -s 2 ".wl[770].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[771].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[772].w[22:24]"  0.51704536222927056 0.48272660683263829 
		0.00022803093809108108;
	setAttr -s 3 ".wl[773].w[22:24]"  0.51870194229589528 0.4812045797932839 
		9.3477910820850355e-005;
	setAttr -s 3 ".wl[774].w[22:24]"  0.51094140457829507 0.48846972768314556 
		0.00058886773855942735;
	setAttr -s 3 ".wl[775].w[22:24]"  0.53654088691592705 0.46242650061832447 
		0.0010326124657485112;
	setAttr -s 2 ".wl[776].w[22:23]"  0.50642453912028951 0.49357546087971049;
	setAttr -s 2 ".wl[777].w[22:23]"  0.51589096592157391 0.48410903407842609;
	setAttr -s 2 ".wl[778].w[22:23]"  0.51735234143748265 0.4826476585625174;
	setAttr -s 2 ".wl[779].w[22:23]"  0.50164919058949176 0.4983508094105083;
	setAttr -s 2 ".wl[780].w[22:23]"  0.50625592364322403 0.49374407635677597;
	setAttr -s 2 ".wl[781].w[22:23]"  0.53598374351167144 0.46401625648832856;
	setAttr -s 2 ".wl[782].w[22:23]"  0.51040769064450087 0.48959230935549919;
	setAttr -s 2 ".wl[783].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[784].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[785].w[22:23]"  0.5022711563450396 0.4977288436549604;
	setAttr -s 2 ".wl[786].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[787].w[22:24]"  0.49993238893273473 0.49993238893273473 
		0.00013522213453053943;
	setAttr -s 3 ".wl[788].w[22:24]"  0.49986088730059819 0.49986088730059819 
		0.00027822539880362717;
	setAttr -s 3 ".wl[789].w[22:24]"  0.49999788715075172 0.49999788715075172 
		4.2256984965090004e-006;
	setAttr -s 3 ".wl[790].w[22:24]"  0.49999996281411302 0.49999996281411302 
		7.4371773959367406e-008;
	setAttr -s 3 ".wl[791].w[22:24]"  0.49999996838969185 0.49999996838969185 
		6.3220616279942845e-008;
	setAttr -s 3 ".wl[792].w[22:24]"  0.49999972962243533 0.49999972962243522 
		5.407551294370593e-007;
	setAttr -s 3 ".wl[793].w[22:24]"  0.499999992404836 0.499999992404836 
		1.5190328103975267e-008;
	setAttr -s 3 ".wl[794].w[22:24]"  0.49999998507418697 0.49999998507418697 
		2.9851626032664444e-008;
	setAttr -s 3 ".wl[795].w[22:24]"  0.49999999057475752 0.49999999057475752 
		1.8850484992833439e-008;
	setAttr -s 2 ".wl[796].w[22:23]"  0.5 0.5;
	setAttr -s 2 ".wl[797].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[798].w";
	setAttr ".wl[798].w[22]" 0.49999572684154597;
	setAttr ".wl[798].w[23]" 0.49999572684154597;
	setAttr ".wl[798].w[27]" 8.5463169080323437e-006;
	setAttr -s 2 ".wl[799].w[22:23]"  0.5 0.5;
	setAttr -s 3 ".wl[800].w[22:24]"  0.53708555272671288 0.46291039110125565 
		4.0561720314774599e-006;
	setAttr -s 3 ".wl[801].w[22:24]"  0.53770866324949029 0.46228935856305736 
		1.9781874523196132e-006;
	setAttr -s 3 ".wl[802].w[22:24]"  0.56651973791025556 0.43346915878998515 
		1.1103299759304105e-005;
	setAttr -s 3 ".wl[803].w[22:24]"  0.50108695396588787 0.49891120386348248 
		1.842170629726015e-006;
	setAttr -s 3 ".wl[804].w[22:24]"  0.54288401657205043 0.45708625615938658 
		2.9727268563020098e-005;
	setAttr -s 2 ".wl[805].w[22:23]"  0.54215853998001584 0.4578414600199841;
	setAttr -s 3 ".wl[806].w[22:24]"  0.54317806439650729 0.45682007551778642 
		1.8600857063420489e-006;
	setAttr -s 2 ".wl[807].w[22:23]"  0.56563999943708543 0.43436000056291452;
	setAttr -s 3 ".wl[808].w[22:24]"  0.5055612855052346 0.49443752044674577 
		1.1940480196288609e-006;
	setAttr -s 2 ".wl[809].w[22:23]"  0.53691374250174184 0.46308625749825821;
	setAttr -s 3 ".wl[810].w";
	setAttr ".wl[810].w[23]" 0.83276394002150689;
	setAttr ".wl[810].w[28]" 0.16723595182468484;
	setAttr ".wl[810].w[35]" 1.0815380826408424e-007;
	setAttr -s 2 ".wl[811].w[22:23]"  0.59054901383680836 0.40945098616319175;
	setAttr -s 3 ".wl[812].w[22:24]"  0.4855769248561293 0.25410487464261378 
		0.26031820050125692;
	setAttr -s 3 ".wl[813].w[22:24]"  0.54711420760000029 0.19750397180023552 
		0.25538182059976422;
	setAttr -s 3 ".wl[814].w";
	setAttr ".wl[814].w[23]" 0.74426414459875645;
	setAttr ".wl[814].w[28]" 0.25573555212314397;
	setAttr ".wl[814].w[35]" 3.0327809967033016e-007;
	setAttr -s 3 ".wl[815].w";
	setAttr ".wl[815].w[23]" 0.37671539004672294;
	setAttr ".wl[815].w[28]" 0.62328430986748806;
	setAttr ".wl[815].w[35]" 3.0008578907550447e-007;
	setAttr -s 3 ".wl[816].w[22:24]"  0.5626689782938058 0.22191392960295908 
		0.21541709210323512;
	setAttr -s 3 ".wl[817].w";
	setAttr ".wl[817].w[23]" 0.21031838984124412;
	setAttr ".wl[817].w[25]" 0.00059694254049944895;
	setAttr ".wl[817].w[28]" 0.78908466761825646;
	setAttr ".wl[818].w[28]"  1;
	setAttr -s 2 ".wl[819].w";
	setAttr ".wl[819].w[23]" 0.74439876375815028;
	setAttr ".wl[819].w[25]" 0.25560123624184972;
	setAttr ".wl[820].w[28]"  1;
	setAttr -s 3 ".wl[821].w";
	setAttr ".wl[821].w[23]" 0.59676153613991301;
	setAttr ".wl[821].w[25]" 0.20329192873074692;
	setAttr ".wl[821].w[27]" 0.19994653512934005;
	setAttr ".wl[822].w[28]"  1;
	setAttr -s 3 ".wl[823].w";
	setAttr ".wl[823].w[23]" 0.59642537061967338;
	setAttr ".wl[823].w[25]" 0.20081844843870678;
	setAttr ".wl[823].w[27]" 0.20275618094161985;
	setAttr -s 2 ".wl[824].w";
	setAttr ".wl[824].w[23]" 0.7439006872466617;
	setAttr ".wl[824].w[27]" 0.25609931275333836;
	setAttr ".wl[825].w[28]"  1;
	setAttr -s 3 ".wl[826].w";
	setAttr ".wl[826].w[23]" 0.5991790290535377;
	setAttr ".wl[826].w[25]" 0.19250781243488774;
	setAttr ".wl[826].w[27]" 0.20831315851157453;
	setAttr ".wl[827].w[28]"  1;
	setAttr -s 3 ".wl[828].w";
	setAttr ".wl[828].w[23]" 0.77893547477138725;
	setAttr ".wl[828].w[28]" 0.22106415793378639;
	setAttr ".wl[828].w[35]" 3.6729482637556127e-007;
	setAttr -s 3 ".wl[829].w";
	setAttr ".wl[829].w[23]" 0.73813764139138005;
	setAttr ".wl[829].w[28]" 0.26186208866300342;
	setAttr ".wl[829].w[35]" 2.6994561643714315e-007;
	setAttr -s 3 ".wl[830].w[22:24]"  0.5284491935867428 0.28682581322385464 
		0.18472499318940244;
	setAttr -s 3 ".wl[831].w[22:24]"  0.61474064834137676 0.21908480246864098 
		0.16617454918998226;
	setAttr ".wl[832].w[28]"  1;
	setAttr -s 3 ".wl[833].w";
	setAttr ".wl[833].w[22]" 0.28192346268502927;
	setAttr ".wl[833].w[28]" 0.71807640922332361;
	setAttr ".wl[833].w[35]" 1.2809164712238969e-007;
	setAttr ".wl[834].w[28]"  1;
	setAttr -s 3 ".wl[835].w[22:24]"  0.59806190372935075 0.21800291511888306 
		0.18393518115176613;
	setAttr -s 3 ".wl[836].w";
	setAttr ".wl[836].w[23]" 0.46629474134682752;
	setAttr ".wl[836].w[28]" 0.53370518011835455;
	setAttr ".wl[836].w[35]" 7.8534817987991081e-008;
	setAttr -s 3 ".wl[837].w[22:24]"  0.6065721927221005 0.3934271152295572 
		6.9204834228235571e-007;
	setAttr -s 3 ".wl[838].w[22:24]"  0.60520497993479716 0.3947946389303591 
		3.8113484380571279e-007;
	setAttr -s 3 ".wl[839].w[22:24]"  0.63806148357834858 0.36193829004651584 
		2.26375135604605e-007;
	setAttr -s 3 ".wl[840].w[22:24]"  0.69105524819978925 0.30894454104955177 
		2.1075065908683968e-007;
	setAttr -s 3 ".wl[841].w[22:24]"  0.72426881477137561 0.27573086267419389 
		3.2255443059193967e-007;
	setAttr -s 3 ".wl[842].w[22:24]"  0.71705747772312756 0.28294193074764973 
		5.9152922276415013e-007;
	setAttr -s 3 ".wl[843].w[22:24]"  0.6799786915957684 0.32002039621981737 
		9.1218441431936713e-007;
	setAttr -s 3 ".wl[844].w[22:24]"  0.63596798803888477 0.36403104155630384 
		9.7040481143527185e-007;
	setAttr -s 3 ".wl[845].w[22:24]"  0.55477410661217685 0.4452249357290296 
		9.5765879360047381e-007;
	setAttr -s 3 ".wl[846].w[22:24]"  0.54666567160893786 0.45333401356168745 
		3.1482937474476687e-007;
	setAttr -s 3 ".wl[847].w[22:24]"  0.59194057738064532 0.4080593134252975 
		1.0919405726290041e-007;
	setAttr -s 3 ".wl[848].w[22:24]"  0.6937774046415387 0.30622249651319555 
		9.8845265730509223e-008;
	setAttr -s 3 ".wl[849].w[22:24]"  0.75664944070888018 0.24335031329198467 
		2.459991352980234e-007;
	setAttr -s 3 ".wl[850].w[22:24]"  0.74011600804356026 0.25988323120664236 
		7.6074979735978348e-007;
	setAttr -s 3 ".wl[851].w[22:24]"  0.67415700973720993 0.325841408776682 
		1.5814861080758146e-006;
	setAttr -s 3 ".wl[852].w[22:24]"  0.6011272882976435 0.3988709848847633 
		1.7268175930885587e-006;
	setAttr -s 3 ".wl[853].w[22:24]"  0.52096264220131949 0.47903613747357887 
		1.2203251016812725e-006;
	setAttr -s 3 ".wl[854].w[22:24]"  0.51028693899611355 0.48971278642220262 
		2.7458168380789497e-007;
	setAttr -s 3 ".wl[855].w[22:24]"  0.54323980426623064 0.45676013520995157 
		6.0523817755232858e-008;
	setAttr -s 3 ".wl[856].w[22:24]"  0.66900173758641013 0.33099820417721493 
		5.8236375018959552e-008;
	setAttr -s 3 ".wl[857].w[22:24]"  0.75659980858316078 0.2433999623533426 
		2.2906349661122328e-007;
	setAttr -s 3 ".wl[858].w[22:24]"  0.73647364635217361 0.26352535363340623 
		1.00001442021621e-006;
	setAttr -s 3 ".wl[859].w[22:24]"  0.65480495022773377 0.34519263246505333 
		2.4173072129516416e-006;
	setAttr -s 3 ".wl[860].w[22:24]"  0.56927451217666791 0.43072287602590126 
		2.611797430840725e-006;
	setAttr -s 3 ".wl[861].w[22:24]"  0.50441432747537684 0.49558424070384494 
		1.431820778304819e-006;
	setAttr -s 3 ".wl[862].w[22:24]"  0.49999986847682265 0.49999986847682254 
		2.6304635481873302e-007;
	setAttr -s 3 ".wl[863].w[22:24]"  0.51015627268807173 0.48984368180968679 
		4.5502241492466676e-008;
	setAttr -s 3 ".wl[864].w[22:24]"  0.62004940986136403 0.37995054038454884 
		4.9754087180941148e-008;
	setAttr -s 3 ".wl[865].w[22:24]"  0.7240925433924833 0.27590719361835636 
		2.6298916038034529e-007;
	setAttr -s 3 ".wl[866].w[22:24]"  0.70951866898372862 0.29048002383795296 
		1.3071783185202509e-006;
	setAttr -s 3 ".wl[867].w";
	setAttr ".wl[867].w[22]" 0.62639726933744277;
	setAttr ".wl[867].w[23]" 0.37359933553596619;
	setAttr ".wl[867].w[29]" 3.3951265911029614e-006;
	setAttr -s 3 ".wl[868].w[22:24]"  0.54324421816077206 0.45675240305729403 
		3.378781933885203e-006;
	setAttr -s 3 ".wl[869].w[22:24]"  0.50001425210012596 0.49998418572835129 
		1.5621715227902568e-006;
	setAttr -s 3 ".wl[870].w[22:24]"  0.49999985655311641 0.49999985655311641 
		2.8689376716053297e-007;
	setAttr -s 3 ".wl[871].w[22:24]"  0.50003443432458616 0.49996551452935312 
		5.1146060692996213e-008;
	setAttr -s 3 ".wl[872].w[22:24]"  0.56648370431700579 0.43351623312088805 
		6.2562106218738862e-008;
	setAttr -s 3 ".wl[873].w[22:24]"  0.66822540056992707 0.33177425375178426 
		3.4567828866565714e-007;
	setAttr -s 3 ".wl[874].w";
	setAttr ".wl[874].w[22]" 0.6656584111857885;
	setAttr ".wl[874].w[23]" 0.33433926386853896;
	setAttr ".wl[874].w[29]" 2.3249456725968262e-006;
	setAttr -s 3 ".wl[875].w";
	setAttr ".wl[875].w[22]" 0.59384475563672712;
	setAttr ".wl[875].w[23]" 0.4061493996238007;
	setAttr ".wl[875].w[29]" 5.844739472133202e-006;
	setAttr -s 3 ".wl[876].w";
	setAttr ".wl[876].w[22]" 0.52403544360461796;
	setAttr ".wl[876].w[23]" 0.47595994404169056;
	setAttr ".wl[876].w[29]" 4.6123536914266148e-006;
	setAttr -s 3 ".wl[877].w";
	setAttr ".wl[877].w[22]" 0.49999907275199801;
	setAttr ".wl[877].w[23]" 0.49999907275199801;
	setAttr ".wl[877].w[29]" 1.8544960040351196e-006;
	setAttr -s 3 ".wl[878].w[22:24]"  0.49999982345094407 0.49999982345094407 
		3.5309811185619756e-007;
	setAttr -s 3 ".wl[879].w[22:24]"  0.49999995886076565 0.49999995886076565 
		8.2278468784225554e-008;
	setAttr -s 3 ".wl[880].w[22:24]"  0.52808461956096253 0.47191527634081226 
		1.0409822521883731e-007;
	setAttr -s 3 ".wl[881].w";
	setAttr ".wl[881].w[22]" 0.6061251739982112;
	setAttr ".wl[881].w[23]" 0.39387414884276584;
	setAttr ".wl[881].w[29]" 6.7715902302082712e-007;
	setAttr -s 3 ".wl[882].w";
	setAttr ".wl[882].w[22]" 0.61429945793076934;
	setAttr ".wl[882].w[23]" 0.38569673110344521;
	setAttr ".wl[882].w[29]" 3.8109657855271453e-006;
	setAttr -s 3 ".wl[883].w";
	setAttr ".wl[883].w[22]" 0.56197777654665526;
	setAttr ".wl[883].w[23]" 0.43801383984116071;
	setAttr ".wl[883].w[29]" 8.3836121841014032e-006;
	setAttr -s 3 ".wl[884].w";
	setAttr ".wl[884].w[22]" 0.51138865358983376;
	setAttr ".wl[884].w[23]" 0.48860499063698137;
	setAttr ".wl[884].w[29]" 6.3557731849176496e-006;
	setAttr -s 3 ".wl[885].w";
	setAttr ".wl[885].w[22]" 0.49999872649341803;
	setAttr ".wl[885].w[23]" 0.49999872649341803;
	setAttr ".wl[885].w[29]" 2.5470131639320517e-006;
	setAttr -s 3 ".wl[886].w";
	setAttr ".wl[886].w[22]" 0.4999997167699563;
	setAttr ".wl[886].w[23]" 0.4999997167699563;
	setAttr ".wl[886].w[29]" 5.6646008740432319e-007;
	setAttr -s 3 ".wl[887].w";
	setAttr ".wl[887].w[22]" 0.49999990901578184;
	setAttr ".wl[887].w[23]" 0.49999990901578184;
	setAttr ".wl[887].w[29]" 1.8196843630766553e-007;
	setAttr -s 3 ".wl[888].w";
	setAttr ".wl[888].w[22]" 0.50868079550757317;
	setAttr ".wl[888].w[23]" 0.49131893156236889;
	setAttr ".wl[888].w[29]" 2.729300580505335e-007;
	setAttr -s 3 ".wl[889].w";
	setAttr ".wl[889].w[22]" 0.5543444555409911;
	setAttr ".wl[889].w[23]" 0.44565426651795137;
	setAttr ".wl[889].w[29]" 1.2779410575305931e-006;
	setAttr -s 3 ".wl[890].w";
	setAttr ".wl[890].w[22]" 0.56593423920833341;
	setAttr ".wl[890].w[23]" 0.43406056746369298;
	setAttr ".wl[890].w[29]" 5.1933279735590956e-006;
	setAttr -s 3 ".wl[891].w";
	setAttr ".wl[891].w[22]" 0.53481715896716209;
	setAttr ".wl[891].w[23]" 0.46517310702118764;
	setAttr ".wl[891].w[29]" 9.7340116503447822e-006;
	setAttr -s 3 ".wl[892].w";
	setAttr ".wl[892].w[22]" 0.50423664467288387;
	setAttr ".wl[892].w[23]" 0.49575596737963007;
	setAttr ".wl[892].w[29]" 7.3879474861965283e-006;
	setAttr -s 3 ".wl[893].w";
	setAttr ".wl[893].w[22]" 0.49999839936257368;
	setAttr ".wl[893].w[23]" 0.49999839936257356;
	setAttr ".wl[893].w[29]" 3.2012748528135366e-006;
	setAttr -s 3 ".wl[894].w";
	setAttr ".wl[894].w[22]" 0.49999943712061445;
	setAttr ".wl[894].w[23]" 0.49999943712061445;
	setAttr ".wl[894].w[29]" 1.1257587710474143e-006;
	setAttr -s 3 ".wl[895].w";
	setAttr ".wl[895].w[22]" 0.49999971921021041;
	setAttr ".wl[895].w[23]" 0.4999997192102103;
	setAttr ".wl[895].w[29]" 5.61579579324554e-007;
	setAttr -s 3 ".wl[896].w";
	setAttr ".wl[896].w[22]" 0.50171059006384688;
	setAttr ".wl[896].w[23]" 0.49828865895658125;
	setAttr ".wl[896].w[29]" 7.509795718404047e-007;
	setAttr -s 3 ".wl[897].w";
	setAttr ".wl[897].w[22]" 0.52066206873932463;
	setAttr ".wl[897].w[23]" 0.47933581715506113;
	setAttr ".wl[897].w[29]" 2.1141056142626861e-006;
	setAttr -s 3 ".wl[898].w";
	setAttr ".wl[898].w[22]" 0.52881171204147404;
	setAttr ".wl[898].w[23]" 0.47118259120711642;
	setAttr ".wl[898].w[29]" 5.6967514095574634e-006;
	setAttr -s 3 ".wl[899].w";
	setAttr ".wl[899].w[22]" 0.51501425951832502;
	setAttr ".wl[899].w[23]" 0.48497680780761854;
	setAttr ".wl[899].w[29]" 8.9326740564791512e-006;
	setAttr -s 3 ".wl[900].w";
	setAttr ".wl[900].w[22]" 0.50104205946712843;
	setAttr ".wl[900].w[23]" 0.49895082202767899;
	setAttr ".wl[900].w[29]" 7.1185051926654019e-006;
	setAttr -s 3 ".wl[901].w";
	setAttr ".wl[901].w[22]" 0.49999820282376067;
	setAttr ".wl[901].w[23]" 0.49999820282376067;
	setAttr ".wl[901].w[29]" 3.5943524787248044e-006;
	setAttr -s 3 ".wl[902].w";
	setAttr ".wl[902].w[22]" 0.49999894452715093;
	setAttr ".wl[902].w[23]" 0.49999894452715093;
	setAttr ".wl[902].w[29]" 2.1109456982163377e-006;
	setAttr -s 3 ".wl[903].w";
	setAttr ".wl[903].w[22]" 0.49999922595504243;
	setAttr ".wl[903].w[23]" 0.49999922595504243;
	setAttr ".wl[903].w[29]" 1.5480899151284845e-006;
	setAttr -s 3 ".wl[904].w";
	setAttr ".wl[904].w[22]" 0.50014734679579975;
	setAttr ".wl[904].w[23]" 0.49985085718806738;
	setAttr ".wl[904].w[29]" 1.7960161329055774e-006;
	setAttr -s 3 ".wl[905].w";
	setAttr ".wl[905].w[22]" 0.5042792707464232;
	setAttr ".wl[905].w[23]" 0.49571775587446398;
	setAttr ".wl[905].w[29]" 2.9733791128593211e-006;
	setAttr -s 3 ".wl[906].w";
	setAttr ".wl[906].w[22]" 0.50692742907574206;
	setAttr ".wl[906].w[23]" 0.49306758361887953;
	setAttr ".wl[906].w[29]" 4.9873053784415586e-006;
	setAttr -s 3 ".wl[907].w";
	setAttr ".wl[907].w[22]" 0.50361171025986562;
	setAttr ".wl[907].w[23]" 0.49638191254834618;
	setAttr ".wl[907].w[29]" 6.3771917882258647e-006;
	setAttr -s 3 ".wl[908].w";
	setAttr ".wl[908].w[22]" 0.5001126702997003;
	setAttr ".wl[908].w[23]" 0.49988174120511408;
	setAttr ".wl[908].w[29]" 5.5884951855818029e-006;
	setAttr -s 3 ".wl[909].w[22:24]"  0.66856271108342225 0.33143681759280097 
		4.7132377682350031e-007;
	setAttr -s 3 ".wl[910].w";
	setAttr ".wl[910].w[22]" 0.5000073693881909;
	setAttr ".wl[910].w[23]" 0.49998909913458511;
	setAttr ".wl[910].w[29]" 3.531477224024915e-006;
	setAttr -s 3 ".wl[911].w[22:24]"  0.64042494779904546 0.35957236912879148 
		2.6830721631451934e-006;
	setAttr -s 3 ".wl[912].w[22:24]"  0.60670884261716584 0.39328553381202819 
		5.6235708058719295e-006;
	setAttr -s 3 ".wl[913].w[22:24]"  0.60799599899376211 0.3919914681924237 
		1.2532813814161505e-005;
	setAttr -s 3 ".wl[914].w[22:24]"  0.63790210073109022 0.36207905421166153 
		1.8845057248280931e-005;
	setAttr -s 3 ".wl[915].w[22:24]"  0.68270138752400011 0.31728259335333042 
		1.6019122669456038e-005;
	setAttr -s 3 ".wl[916].w[22:24]"  0.72048643027756021 0.27950515718791674 
		8.4125345230256324e-006;
	setAttr -s 3 ".wl[917].w[22:24]"  0.72805623343685111 0.27194002190179584 
		3.7446613530291094e-006;
	setAttr -s 3 ".wl[918].w[22:24]"  0.69451551357207431 0.30548222314050733 
		2.2632874183452103e-006;
	setAttr -s 3 ".wl[919].w[22:24]"  0.5931822601698028 0.40681672239124328 
		1.0174389539341252e-006;
	setAttr -s 3 ".wl[920].w[22:24]"  0.54682368157918859 0.45317188349765553 
		4.4349231558302853e-006;
	setAttr -s 3 ".wl[921].w[22:24]"  0.55505530493510058 0.44492492930897404 
		1.9765755925358873e-005;
	setAttr -s 3 ".wl[922].w[22:24]"  0.60219339130491634 0.39776621886474289 
		4.0389830340804383e-005;
	setAttr -s 3 ".wl[923].w[22:24]"  0.67643319999139573 0.32353610385683379 
		3.0696151770477918e-005;
	setAttr -s 3 ".wl[924].w[22:24]"  0.7434154101617565 0.25657453918481082 
		1.00506534327745e-005;
	setAttr -s 3 ".wl[925].w[22:24]"  0.76051983904950327 0.23947792530890749 
		2.2356415892399219e-006;
	setAttr -s 3 ".wl[926].w[22:24]"  0.6972287983839599 0.30277043075335142 
		7.7086268868469026e-007;
	setAttr -s 3 ".wl[927].w[22:24]"  0.54320668994254029 0.45679285794795216 
		4.521095075729253e-007;
	setAttr -s 3 ".wl[928].w[22:24]"  0.50990830360222417 0.49008812652261274 
		3.5698751630465221e-006;
	setAttr -s 3 ".wl[929].w[22:24]"  0.52070138739796246 0.47927223939177444 
		2.637321026321101e-005;
	setAttr -s 3 ".wl[930].w[22:24]"  0.56965750395865045 0.43027607911150428 
		6.6416929845204627e-005;
	setAttr -s 3 ".wl[931].w[22:24]"  0.65646411365668 0.34348782683750084 
		4.8059505819219695e-005;
	setAttr -s 3 ".wl[932].w[22:24]"  0.7392016582214771 0.26078631988723877 
		1.2021891284179577e-005;
	setAttr -s 3 ".wl[933].w[22:24]"  0.75979471605276705 0.24020360023403547 
		1.6837131976563521e-006;
	setAttr -s 3 ".wl[934].w[22:24]"  0.67149933760364888 0.3285003156688206 
		3.4672753047862536e-007;
	setAttr -s 3 ".wl[935].w[22:24]"  0.50964306733108933 0.49035664541557494 
		2.8725333577448135e-007;
	setAttr -s 3 ".wl[936].w[22:24]"  0.4999984515510501 0.49999845155104999 
		3.0968978999268997e-006;
	setAttr -s 3 ".wl[937].w[22:24]"  0.50413017313935604 0.49583997414401354 
		2.9852716630497187e-005;
	setAttr -s 3 ".wl[938].w[22:24]"  0.54318225537192599 0.45673322488384877 
		8.451974422529815e-005;
	setAttr -s 3 ".wl[939].w[22:24]"  0.62739554407619347 0.3725429112345644 
		6.154468924209087e-005;
	setAttr -s 3 ".wl[940].w[22:24]"  0.71144490458044629 0.28854090450011893 
		1.4190919434788209e-005;
	setAttr -s 3 ".wl[941].w[22:24]"  0.72614984146718187 0.2738485092583744 
		1.6492744436803618e-006;
	setAttr -s 3 ".wl[942].w[22:24]"  0.62094303226542613 0.37905672218085429 
		2.455537196199059e-007;
	setAttr -s 3 ".wl[943].w[22:24]"  0.50000194455479974 0.4999977629580285 
		2.9248717166731627e-007;
	setAttr -s 3 ".wl[944].w[22:24]"  0.49999847518268314 0.49999847518268314 
		3.0496346337567851e-006;
	setAttr -s 3 ".wl[945].w[22:24]"  0.49998621877012001 0.49998441034222868 
		2.9370887651241154e-005;
	setAttr -s 3 ".wl[946].w[22:24]"  0.52375172486605048 0.47616254146306364 
		8.5733670885933871e-005;
	setAttr -s 3 ".wl[947].w[22:24]"  0.59424616252063744 0.40568853524648868 
		6.5302232873784918e-005;
	setAttr -s 3 ".wl[948].w[22:24]"  0.66670789547153442 0.33327606405984705 
		1.6040468618633524e-005;
	setAttr -s 3 ".wl[949].w[22:24]"  0.66904717550721016 0.3309508586430312 
		1.965849758620484e-006;
	setAttr -s 3 ".wl[950].w[22:24]"  0.56614662044750119 0.43385309907244762 
		2.8048005118332123e-007;
	setAttr -s 3 ".wl[951].w[22:24]"  0.49999977175554294 0.49999977175554283 
		4.5648891425105528e-007;
	setAttr -s 3 ".wl[952].w[22:24]"  0.49999829477508662 0.49999829477508662 
		3.4104498268139577e-006;
	setAttr -s 3 ".wl[953].w[22:24]"  0.49998697285995597 0.49998697285995586 
		2.6054280088211564e-005;
	setAttr -s 3 ".wl[954].w[22:24]"  0.51105606679907967 0.48887209325891995 
		7.1839942000424714e-005;
	setAttr -s 3 ".wl[955].w[22:24]"  0.56192728682401627 0.4380142822192083 
		5.8430956775463877e-005;
	setAttr -s 3 ".wl[956].w[22:24]"  0.61457412359077868 0.38540898466005452 
		1.6891749166880815e-005;
	setAttr -s 3 ".wl[957].w[22:24]"  0.60603038474959769 0.39396701528708861 
		2.599963313800427e-006;
	setAttr -s 3 ".wl[958].w[22:24]"  0.5274038087207078 0.47259572949251094 
		4.6178678126065425e-007;
	setAttr -s 3 ".wl[959].w[22:24]"  0.49999953774740941 0.4999995377474093 
		9.2450518132853533e-007;
	setAttr -s 3 ".wl[960].w[22:24]"  0.49999791228603263 0.49999791228603263 
		4.1754279346965014e-006;
	setAttr -s 3 ".wl[961].w[22:24]"  0.49998926180254927 0.49998926180254927 
		2.1476394901452446e-005;
	setAttr -s 3 ".wl[962].w[22:24]"  0.50396568896459137 0.49598289492866754 
		5.1416106741089963e-005;
	setAttr -s 3 ".wl[963].w[22:24]"  0.53450369509498108 0.46545142820020025 
		4.4876704818803773e-005;
	setAttr -s 3 ".wl[964].w[22:24]"  0.56568974419964202 0.43429390637320098 
		1.6349427157144524e-005;
	setAttr -s 3 ".wl[965].w[22:24]"  0.55383367224187008 0.44616274832639263 
		3.5794317373246879e-006;
	setAttr -s 3 ".wl[966].w[22:24]"  0.50817315025999132 0.49182591340356752 
		9.363364412628269e-007;
	setAttr -s 3 ".wl[967].w[22:24]"  0.49999897227033752 0.49999897227033752 
		2.055459324965822e-006;
	setAttr -s 3 ".wl[968].w[22:24]"  0.49999730551305999 0.49999730551305988 
		5.3889738801895631e-006;
	setAttr -s 3 ".wl[969].w[22:24]"  0.49999160536164178 0.49999160536164178 
		1.6789276716428066e-005;
	setAttr -s 3 ".wl[970].w[22:24]"  0.50088179641107167 0.49908589374208978 
		3.2309846838543881e-005;
	setAttr -s 3 ".wl[971].w[22:24]"  0.5146338584282899 0.48533609708633818 
		3.0044485372004028e-005;
	setAttr -s 3 ".wl[972].w[22:24]"  0.52836477893213241 0.47162066693290117 
		1.4554134966478894e-005;
	setAttr -s 3 ".wl[973].w[22:24]"  0.52013532020434627 0.47985970876686596 
		4.9710287878378679e-006;
	setAttr -s 3 ".wl[974].w[22:24]"  0.50146767171668183 0.49853027415564977 
		2.054127668474241e-006;
	setAttr -s 3 ".wl[975].w[22:24]"  0.49999773640726364 0.49999773640726364 
		4.5271854727197267e-006;
	setAttr -s 3 ".wl[976].w[22:24]"  0.49999644459236575 0.49999644459236575 
		7.1108152684855316e-006;
	setAttr -s 3 ".wl[977].w[22:24]"  0.49999366387286021 0.49999366387286009 
		1.2672254279745512e-005;
	setAttr -s 3 ".wl[978].w[22:24]"  0.50005553770256317 0.49992624426287746 
		1.8218034559455711e-005;
	setAttr -s 3 ".wl[979].w[22:24]"  0.50334291583294755 0.4966393242837549 
		1.7759883297537053e-005;
	setAttr -s 3 ".wl[980].w[22:24]"  0.50657500514956999 0.49341296100523091 
		1.2033845199224253e-005;
	setAttr -s 3 ".wl[981].w[22:24]"  0.50396408246220648 0.49602904867021352 
		6.8688675800319807e-006;
	setAttr -s 3 ".wl[982].w[22:24]"  0.50008071941323839 0.49991478543130752 
		4.495155454183044e-006;
	setAttr -s 3 ".wl[983].w[22:24]"  0.67139000339768706 0.32860302102664501 
		6.9755756678669421e-006;
	setAttr -s 3 ".wl[984].w[22:24]"  0.4999953147525919 0.49999530179860879 
		9.3834487992589866e-006;
	setAttr -s 3 ".wl[985].w";
	setAttr ".wl[985].w[22]" 0.5001391825342737;
	setAttr ".wl[985].w[30]" 0.24208812884342298;
	setAttr ".wl[985].w[31]" 0.25777268862230346;
	setAttr -s 3 ".wl[986].w";
	setAttr ".wl[986].w[22]" 0.5001082083248326;
	setAttr ".wl[986].w[30]" 0.23963848809665694;
	setAttr ".wl[986].w[31]" 0.26025330357851051;
	setAttr -s 3 ".wl[987].w";
	setAttr ".wl[987].w[22]" 0.50005892964041898;
	setAttr ".wl[987].w[30]" 0.23647127889834357;
	setAttr ".wl[987].w[31]" 0.2634697914612375;
	setAttr -s 3 ".wl[988].w";
	setAttr ".wl[988].w[22]" 0.50004060971232112;
	setAttr ".wl[988].w[30]" 0.23709288651707405;
	setAttr ".wl[988].w[31]" 0.26286650377060483;
	setAttr -s 3 ".wl[989].w";
	setAttr ".wl[989].w[22]" 0.50005164558151705;
	setAttr ".wl[989].w[30]" 0.24137972493919829;
	setAttr ".wl[989].w[31]" 0.25856862947928466;
	setAttr -s 3 ".wl[990].w";
	setAttr ".wl[990].w[22]" 0.50006266528796162;
	setAttr ".wl[990].w[30]" 0.24435256028636992;
	setAttr ".wl[990].w[31]" 0.25558477442566846;
	setAttr -s 3 ".wl[991].w";
	setAttr ".wl[991].w[22]" 0.50005186186213668;
	setAttr ".wl[991].w[30]" 0.24370153849482207;
	setAttr ".wl[991].w[31]" 0.25624659964304125;
	setAttr -s 3 ".wl[992].w";
	setAttr ".wl[992].w[22]" 0.50004086926063507;
	setAttr ".wl[992].w[30]" 0.24132430684182146;
	setAttr ".wl[992].w[31]" 0.25863482389754344;
	setAttr -s 3 ".wl[993].w";
	setAttr ".wl[993].w[22]" 0.50005923230016136;
	setAttr ".wl[993].w[30]" 0.24057312752783711;
	setAttr ".wl[993].w[31]" 0.25936764017200165;
	setAttr -s 3 ".wl[994].w";
	setAttr ".wl[994].w[22]" 0.50010849933275725;
	setAttr ".wl[994].w[30]" 0.24180555696170161;
	setAttr ".wl[994].w[31]" 0.25808594370554128;
	setAttr -s 3 ".wl[995].w";
	setAttr ".wl[995].w[22]" 0.25023935676192521;
	setAttr ".wl[995].w[30]" 0.36382918766638334;
	setAttr ".wl[995].w[31]" 0.38593145557169151;
	setAttr -s 3 ".wl[996].w";
	setAttr ".wl[996].w[22]" 0.25010357268754524;
	setAttr ".wl[996].w[30]" 0.35580384507513474;
	setAttr ".wl[996].w[31]" 0.39409258223731997;
	setAttr -s 3 ".wl[997].w";
	setAttr ".wl[997].w[22]" 0.25004225747739578;
	setAttr ".wl[997].w[30]" 0.34982117480285208;
	setAttr ".wl[997].w[31]" 0.40013656771975215;
	setAttr -s 3 ".wl[998].w";
	setAttr ".wl[998].w[22]" 0.25005021586227377;
	setAttr ".wl[998].w[30]" 0.35779225394497638;
	setAttr ".wl[998].w[31]" 0.39215753019274985;
	setAttr -s 3 ".wl[999].w";
	setAttr ".wl[999].w[22]" 0.25010473704502884;
	setAttr ".wl[999].w[30]" 0.36766092394757094;
	setAttr ".wl[999].w[31]" 0.38223433900740017;
	setAttr -s 3 ".wl[1000].w";
	setAttr ".wl[1000].w[22]" 0.25010508195112546;
	setAttr ".wl[1000].w[30]" 0.36902061405025827;
	setAttr ".wl[1000].w[31]" 0.38087430399861633;
	setAttr -s 3 ".wl[1001].w";
	setAttr ".wl[1001].w[22]" 0.25005069281096132;
	setAttr ".wl[1001].w[30]" 0.36381021129566055;
	setAttr ".wl[1001].w[31]" 0.38613909589337814;
	setAttr -s 3 ".wl[1002].w";
	setAttr ".wl[1002].w[22]" 0.25004255932991643;
	setAttr ".wl[1002].w[30]" 0.35853665689078751;
	setAttr ".wl[1002].w[31]" 0.39142078377929607;
	setAttr -s 3 ".wl[1003].w";
	setAttr ".wl[1003].w[22]" 0.25010423357749784;
	setAttr ".wl[1003].w[30]" 0.36119911694487489;
	setAttr ".wl[1003].w[31]" 0.38869664947762739;
	setAttr -s 3 ".wl[1004].w";
	setAttr ".wl[1004].w[22]" 0.2502398482518533;
	setAttr ".wl[1004].w[30]" 0.36516741149237292;
	setAttr ".wl[1004].w[31]" 0.38459274025577378;
	setAttr -s 3 ".wl[1005].w[30:32]"  0.48827739103606094 0.51103294315499836 
		0.00068966580894071975;
	setAttr -s 3 ".wl[1006].w[30:32]"  0.45653988531522888 0.54345933085781417 
		7.8382695679757371e-007;
	setAttr -s 3 ".wl[1007].w[30:32]"  0.45522659552631362 0.54477332060004902 
		8.3873637473646485e-008;
	setAttr -s 3 ".wl[1008].w[30:32]"  0.49548343837636338 0.50426851390832195 
		0.00024804771531461238;
	setAttr -s 3 ".wl[1009].w[30:32]"  0.49740948612904373 0.50140390828248083 
		0.0011866055884756175;
	setAttr -s 3 ".wl[1010].w[30:32]"  0.49665700879720476 0.50308806390496674 
		0.00025492729782860039;
	setAttr -s 3 ".wl[1011].w[30:32]"  0.47352833976251274 0.52647150141163024 
		1.5882585707440052e-007;
	setAttr -s 3 ".wl[1012].w[30:32]"  0.47078436860862483 0.52921447831959356 
		1.1530717816179055e-006;
	setAttr -s 3 ".wl[1013].w[30:32]"  0.49007446743914806 0.50922592880100559 
		0.00069960375984631155;
	setAttr -s 3 ".wl[1014].w[30:32]"  0.49077081705177128 0.50654076793093561 
		0.0026884150172931187;
	setAttr -s 3 ".wl[1015].w[30:32]"  0.021176149756328758 0.88102486298372085 
		0.097798987259950371;
	setAttr -s 3 ".wl[1016].w[30:32]"  6.6587295515987852e-005 0.99903222465827679 
		0.00090118804620731568;
	setAttr -s 3 ".wl[1017].w[30:32]"  2.2260163628042245e-005 0.99972789945472185 
		0.00024984038165008727;
	setAttr -s 3 ".wl[1018].w[30:32]"  0.019587783649019305 0.91648714877545767 
		0.063925067575522881;
	setAttr -s 3 ".wl[1019].w[30:32]"  0.05198715723813073 0.82643952213068317 
		0.12157332063118605;
	setAttr -s 3 ".wl[1020].w[30:32]"  0.18357634784983415 0.60749280160332819 
		0.20893085054683769;
	setAttr -s 3 ".wl[1021].w[30:32]"  8.4266349404455411e-005 0.99923906386605388 
		0.00067666978454174588;
	setAttr -s 3 ".wl[1022].w[30:32]"  0.00018633766017697555 0.99799917278029038 
		0.0018144895595326792;
	setAttr -s 3 ".wl[1023].w[30:32]"  0.023571034176431211 0.877572437532955 
		0.098856528290613796;
	setAttr -s 3 ".wl[1024].w[30:32]"  0.049421484421337931 0.78537949000732676 
		0.16519902557133528;
	setAttr -s 3 ".wl[1025].w";
	setAttr ".wl[1025].w[28]" 0.11630228502804528;
	setAttr ".wl[1025].w[31]" 0.62734358925459788;
	setAttr ".wl[1025].w[32]" 0.25635412571735677;
	setAttr -s 3 ".wl[1026].w[31:33]"  0.4972874003267127 0.50270058329983358 
		1.201637345377028e-005;
	setAttr -s 3 ".wl[1027].w[31:33]"  0.50632188974960779 0.33289052746453984 
		0.16078758278585234;
	setAttr -s 3 ".wl[1028].w[31:33]"  0.12404173340675705 0.82484731519969456 
		0.051110951393548418;
	setAttr -s 3 ".wl[1029].w[31:33]"  0.13433859325322939 0.7094012824895729 
		0.15626012425719779;
	setAttr -s 3 ".wl[1030].w[31:33]"  0.39328623175596555 0.33153860206534858 
		0.27517516617868576;
	setAttr -s 3 ".wl[1031].w[31:33]"  0.54637769344415676 0.23251257420253357 
		0.22110973235330963;
	setAttr -s 3 ".wl[1032].w[31:33]"  0.73300884775571873 0.11978476005277305 
		0.14720639219150813;
	setAttr -s 3 ".wl[1033].w";
	setAttr ".wl[1033].w[28]" 0.089759146067948312;
	setAttr ".wl[1033].w[31]" 0.71868460225173503;
	setAttr ".wl[1033].w[32]" 0.19155625168031667;
	setAttr -s 3 ".wl[1034].w[30:32]"  0.00076239869367103258 0.4996188006531645 
		0.4996188006531645;
	setAttr -s 3 ".wl[1035].w";
	setAttr ".wl[1035].w[28]" 0.37068343743160437;
	setAttr ".wl[1035].w[31]" 0.38385635301217169;
	setAttr ".wl[1035].w[32]" 0.245460209556224;
	setAttr -s 3 ".wl[1036].w[31:33]"  0.34114970472656292 0.30522032844435526 
		0.35362996682908182;
	setAttr -s 3 ".wl[1037].w[31:33]"  0.14930845736614251 0.29387203205778789 
		0.5568195105760696;
	setAttr -s 3 ".wl[1038].w[32:34]"  0.44349291484357506 0.55641023664000944 
		9.6848516415534228e-005;
	setAttr -s 3 ".wl[1039].w[32:34]"  0.48253132926758069 0.51690176708508329 
		0.00056690364733605878;
	setAttr -s 3 ".wl[1040].w[32:34]"  0.47522546870111959 0.52467789456770708 
		9.6636731173359828e-005;
	setAttr -s 3 ".wl[1041].w[32:34]"  0.30156210951212142 0.69843778473088858 
		1.0575699011271785e-007;
	setAttr -s 3 ".wl[1042].w[31:33]"  0.4173811395564172 0.15922241109051924 
		0.42339644935306353;
	setAttr -s 3 ".wl[1043].w";
	setAttr ".wl[1043].w[28]" 0.17655632172724944;
	setAttr ".wl[1043].w[31]" 0.62741208302501894;
	setAttr ".wl[1043].w[33]" 0.19603159524773164;
	setAttr -s 3 ".wl[1044].w";
	setAttr ".wl[1044].w[28]" 0.3245696276354213;
	setAttr ".wl[1044].w[31]" 0.48942957465806192;
	setAttr ".wl[1044].w[32]" 0.18600079770651687;
	setAttr -s 3 ".wl[1045].w";
	setAttr ".wl[1045].w[28]" 0.52572898698992476;
	setAttr ".wl[1045].w[33]" 0.28968860699956334;
	setAttr ".wl[1045].w[34]" 0.18458240601051196;
	setAttr -s 3 ".wl[1046].w";
	setAttr ".wl[1046].w[28]" 0.13213613599927582;
	setAttr ".wl[1046].w[33]" 0.58568611159959838;
	setAttr ".wl[1046].w[34]" 0.28217775240112569;
	setAttr -s 3 ".wl[1047].w[32:34]"  3.2906702051613424e-007 0.99984700373729496 
		0.00015266719568452638;
	setAttr -s 3 ".wl[1048].w[32:34]"  0.0057014587866132992 0.91561544757746305 
		0.078683093635923601;
	setAttr -s 3 ".wl[1049].w[32:34]"  0.026102471760007753 0.84194577312293672 
		0.13195175511705551;
	setAttr -s 3 ".wl[1050].w[32:34]"  0.0094592826181369803 0.9205717414002329 
		0.069968975981630072;
	setAttr -s 3 ".wl[1051].w[32:34]"  0.17949659606922558 0.61197468715447056 
		0.20852871677630377;
	setAttr -s 3 ".wl[1052].w";
	setAttr ".wl[1052].w[31]" 0.27773740422089388;
	setAttr ".wl[1052].w[33]" 0.34125943707383155;
	setAttr ".wl[1052].w[34]" 0.38100315870527457;
	setAttr -s 3 ".wl[1053].w";
	setAttr ".wl[1053].w[28]" 0.40694215270642031;
	setAttr ".wl[1053].w[31]" 0.28223395044949123;
	setAttr ".wl[1053].w[34]" 0.3108238968440884;
	setAttr -s 3 ".wl[1054].w";
	setAttr ".wl[1054].w[28]" 0.92151182197944081;
	setAttr ".wl[1054].w[29]" 0.078487841266178804;
	setAttr ".wl[1054].w[35]" 3.3675438043207664e-007;
	setAttr -s 3 ".wl[1055].w";
	setAttr ".wl[1055].w[28]" 0.25525159598738523;
	setAttr ".wl[1055].w[33]" 0.2616807322769083;
	setAttr ".wl[1055].w[34]" 0.48306767173570642;
	setAttr -s 3 ".wl[1056].w";
	setAttr ".wl[1056].w[29]" 2.5187976529220013e-005;
	setAttr ".wl[1056].w[33]" 0.49998740601173541;
	setAttr ".wl[1056].w[34]" 0.49998740601173541;
	setAttr -s 3 ".wl[1057].w[33:35]"  0.49999999522307365 0.49999999522307365 
		9.5538527527433984e-009;
	setAttr -s 3 ".wl[1058].w[33:35]"  0.47898841992726149 0.52051282513708796 
		0.00049875493565055672;
	setAttr -s 3 ".wl[1059].w[33:35]"  0.43200887107376917 0.56296188054523533 
		0.0050292483809955971;
	setAttr -s 3 ".wl[1060].w[33:35]"  0.32611480535623544 0.67230701764031919 
		0.001578177003445289;
	setAttr -s 3 ".wl[1061].w";
	setAttr ".wl[1061].w[31]" 0.037543285880657681;
	setAttr ".wl[1061].w[33]" 0.3151343218061427;
	setAttr ".wl[1061].w[34]" 0.64732239231319955;
	setAttr -s 3 ".wl[1062].w";
	setAttr ".wl[1062].w[28]" 0.16461476291659596;
	setAttr ".wl[1062].w[33]" 0.13012780892640563;
	setAttr ".wl[1062].w[34]" 0.70525742815699843;
	setAttr -s 3 ".wl[1063].w";
	setAttr ".wl[1063].w[28]" 0.42353675833586357;
	setAttr ".wl[1063].w[29]" 0.12687565084326266;
	setAttr ".wl[1063].w[34]" 0.44958759082087374;
	setAttr -s 3 ".wl[1064].w";
	setAttr ".wl[1064].w[28]" 0.49999473935969013;
	setAttr ".wl[1064].w[29]" 0.49999473935969013;
	setAttr ".wl[1064].w[35]" 1.0521280619745067e-005;
	setAttr -s 3 ".wl[1065].w";
	setAttr ".wl[1065].w[28]" 0.18415319102417982;
	setAttr ".wl[1065].w[34]" 0.57879626115669092;
	setAttr ".wl[1065].w[35]" 0.23705054781912924;
	setAttr -s 3 ".wl[1066].w[33:35]"  0.00045281269977998962 0.99423704679455671 
		0.0053101405056633581;
	setAttr -s 3 ".wl[1067].w[33:35]"  6.4982853144046818e-006 0.99905867845458529 
		0.00093482326010026488;
	setAttr -s 3 ".wl[1068].w[33:35]"  0.0026420730933191324 0.82577337419062713 
		0.17158455271605372;
	setAttr -s 3 ".wl[1069].w[33:35]"  0.0058016960037256642 0.68779523640811446 
		0.30640306758815994;
	setAttr -s 3 ".wl[1070].w[33:35]"  0.0011504268416419685 0.76728627779401104 
		0.23156329536434719;
	setAttr -s 3 ".wl[1071].w[33:35]"  1.1881832524999483e-007 0.99931514537154953 
		0.00068473581012528967;
	setAttr -s 3 ".wl[1072].w[33:35]"  9.9007253485462569e-005 0.98508942379177478 
		0.014811568954739861;
	setAttr -s 3 ".wl[1073].w";
	setAttr ".wl[1073].w[29]" 0.081269115847863144;
	setAttr ".wl[1073].w[34]" 0.78418626106805001;
	setAttr ".wl[1073].w[35]" 0.13454462308408685;
	setAttr -s 3 ".wl[1074].w";
	setAttr ".wl[1074].w[28]" 0.2093220317929339;
	setAttr ".wl[1074].w[34]" 0.53758190503643322;
	setAttr ".wl[1074].w[35]" 0.25309606317063282;
	setAttr -s 3 ".wl[1075].w[33:35]"  1.3152183283854613e-005 0.51676632881932905 
		0.48322051899738722;
	setAttr -s 3 ".wl[1076].w[33:35]"  7.9185247363738566e-009 0.49999999604073764 
		0.49999999604073764;
	setAttr -s 2 ".wl[1077].w[34:35]"  0.5000000000087782 0.4999999999912218;
	setAttr -s 3 ".wl[1078].w[33:35]"  3.240072240025206e-007 0.49999983799638797 
		0.49999983799638797;
	setAttr -s 3 ".wl[1079].w[33:35]"  2.4810707246420357e-006 0.49999875946463768 
		0.49999875946463768;
	setAttr -s 3 ".wl[1080].w[33:35]"  4.8437288349919799e-007 0.49999975781355827 
		0.49999975781355827;
	setAttr -s 3 ".wl[1081].w[33:35]"  2.0465285960268674e-009 0.49999999897673569 
		0.49999999897673569;
	setAttr -s 3 ".wl[1082].w[33:35]"  5.0807784839625086e-008 0.49999997459610757 
		0.49999997459610757;
	setAttr -s 3 ".wl[1083].w[33:35]"  1.4952079674255658e-005 0.50191138289496962 
		0.4980736650253561;
	setAttr -s 3 ".wl[1084].w[33:35]"  7.4281701777473185e-005 0.51542500078480313 
		0.48450071751341933;
	setAttr -s 3 ".wl[1085].w[33:35]"  3.0668503376739643e-009 0.49999999846657484 
		0.49999999846657484;
	setAttr ".wl[1086].w[22]"  1;
	setAttr -s 36 ".pm";
	setAttr ".pm[0]" -type "matrix" -1 -3.7450797331474061e-019 -1.2246410727391313e-016 0
		 -1.2246467991473532e-016 0.0030580896759415666 0.99999532403283464 0 9.1481672358393703e-034 0.99999532403283464 -0.0030580896759415666 0
		 -13.559594067844024 0.15796486088918799 31.2386755036257 1;
	setAttr ".pm[1]" -type "matrix" -1 -1.2246144130787181e-016 8.9062866405723911e-019 0
		 -1.2246467991473532e-016 0.99997355476807059 -0.0072725349437677009 0 9.6296497219361776e-034 -0.0072725349437677009 -0.99997355476807059 0
		 -13.559594067844019 -0.12405315898345098 -0.15744343808347555 1;
	setAttr ".pm[2]" -type "matrix" 0.022404427953783879 0.023154628266791721 -0.99948081762377505 0
		 -1.4201205315866873e-014 0.99973176099206784 0.023160441785476684 0 0.9997489893008461 -0.00051889644934632201 0.022398418212256549 0
		 26.830177477302698 0.3690921167420203 -12.956659968317489 1;
	setAttr ".pm[3]" -type "matrix" -0.98625234986514654 -0.11881830684130333 -0.11484124844689635 0
		 -1.512678871051774e-015 0.69497063980719076 -0.71903811429296527 0 0.16524618720404974 -0.70915302986404072 -0.68541642659712665 0
		 -15.464896764634457 -21.157910031597197 -20.334729004821593 1;
	setAttr ".pm[4]" -type "matrix" -0.98625234986514654 -0.11881830684130333 -0.11484124844689635 0
		 -1.512678871051774e-015 0.69497063980719076 -0.71903811429296527 0 0.16524618720404974 -0.70915302986404072 -0.68541642659712665 0
		 -17.042075356287249 -14.114699442329904 -29.519406310659761 1;
	setAttr ".pm[5]" -type "matrix" -0.99999999999999956 4.1553702904685868e-015 1.9198092074638635e-015 0
		 -1.556614099966041e-015 0.10223554176714029 -0.99476021934915471 0 -4.4010493124387186e-015 -0.9947602193491546 -0.10223554176714018 0
		 -22.480379638794165 -38.857531608099592 -18.88338143091762 1;
	setAttr ".pm[6]" -type "matrix" 0.99999999999999956 7.6758738729096921e-010 3.2937465736116359e-009 0
		 3.4576308079217089e-010 0.94559672659070704 -0.32534109894223157 0 -3.3642836311624461e-009 0.32534109894223145 0.94559672659070693 0
		 22.480379507377648 38.121074621151081 31.014847371310928 1;
	setAttr ".pm[7]" -type "matrix" 0.99999999999999956 -3.4576300910136812e-010 3.364283702341962e-009 0
		 3.457630807921711e-010 0.99999999999999978 6.6729705987267406e-016 0 -3.3642836311624466e-009 -7.2048172117457002e-016 0.99999999999999956 0
		 22.480379538578315 29.038613461827222 32.772548016774287 1;
	setAttr ".pm[8]" -type "matrix" -0.98305508367291605 0.13180902621271626 0.12739341848655666 0
		 -9.4368957093138227e-016 0.69496012786847428 -0.71904827422992501 0 -0.1833103992272001 -0.70686406138796498 -0.68318408665108332 0
		 -12.098529638886721 -17.76741079880254 -17.089578917359542 1;
	setAttr ".pm[9]" -type "matrix" -0.98305508367291605 0.13180902621271626 0.12739341848655666 0
		 -9.4368957093138227e-016 0.69496012786847428 -0.71904827422992501 0 -0.1833103992272001 -0.70686406138796498 -0.68318408665108332 0
		 -10.432300249606794 -10.61470173008442 -26.238072339551454 1;
	setAttr ".pm[10]" -type "matrix" -0.99999999999999978 1.1780397681485087e-014 2.5221566248064097e-015 0
		 -1.3574501930496881e-015 0.10224000072939651 -0.99475976107342245 0 -1.2039629242611935e-014 -0.99475976107342201 -0.10224000072939646 0
		 -4.7577523045071999 -38.769265198875132 -18.772565221073872 1;
	setAttr ".pm[11]" -type "matrix" 0.99999999999999978 -2.9112737176527958e-015 -1.2042389941789899e-015 0
		 2.3688519670712212e-015 0.93841625194685607 -0.34550678442255589 0 2.1990406285938943e-015 0.34550678442255567 0.93841625194685563 0
		 4.7577523045068144 38.611715453740239 30.142867454898802 1;
	setAttr ".pm[12]" -type "matrix" 0.99999999999999978 -2.3159138279561003e-015 -2.1359422641258066e-015 0
		 2.3688519670712212e-015 0.99999999999999978 -4.6074255521944004e-015 0 2.1990406285938935e-015 4.5519144009631331e-015 0.99999999999999933 0
		 4.7577523045067833 29.103936836466616 32.705893603945022 1;
	setAttr ".pm[13]" -type "matrix" 0.0017216250816360916 -0.0013724570887204733 -0.99999757618137131 0
		 6.6948984401540987e-015 0.99999905817753443 -0.0013724591227015314 0 0.99999851800244077 2.3628600424680733e-006 0.0017216234601709209 0
		 -18.98678052127649 -9.3361687146559049 -13.579501608724001 1;
	setAttr ".pm[14]" -type "matrix" 0.90804780778936856 -0.39119331644330418 -0.14972297064582504 0
		 6.106226635438358e-016 0.35744791278676702 -0.93393307557039851 0 0.4188665405220639 0.84805588189368286 0.32457979360490924 0
		 4.32137723261358 -35.002934548377475 -17.388889019432408 1;
	setAttr ".pm[15]" -type "matrix" 0.99999999999999978 3.2152199360080789e-015 2.0912752565246543e-015 0
		 3.8449616831878722e-016 0.47678331984112376 -0.87902085636307659 0 -3.5989775082409092e-015 0.87902085636307636 0.47678331984112349 0
		 22.59932344640675 -15.333272522199854 -25.286674428674715 1;
	setAttr ".pm[16]" -type "matrix" 0.99999999999999978 -5.5256443326868122e-017 3.8351034094176547e-015 0
		 3.8449616831878722e-016 0.99787010592873038 -0.065232290269345877 0 -3.5989775082409092e-015 0.065232290269346044 0.99787010592873004 0
		 22.59932344640675 24.223222396686683 -33.386887630540834 1;
	setAttr ".pm[17]" -type "matrix" 0.99999999999999978 -3.0531133177191746e-016 3.8233305410528805e-015 0
		 3.8449616831878732e-016 1 4.1633363423444825e-017 0 -3.5989775082409092e-015 1.3877787807814558e-016 0.99999999999999956 0
		 22.599323446406757 26.979333910883945 -41.36982435709178 1;
	setAttr ".pm[18]" -type "matrix" 1 1.204872267222162e-015 5.9607597955647789e-016 0
		 2.5648636118618768e-016 0.32589619120077767 -0.94540555983177177 0 -1.2946314526049248e-015 0.94540555983177177 0.32589619120077773 0
		 4.7741344956547813 -30.47229183726175 -14.461449788388457 1;
	setAttr ".pm[19]" -type "matrix" 1 1.0952949060114477e-015 7.7932844333481252e-016 0
		 2.5648636118618768e-016 0.47147453247360488 -0.88187967729662287 0 -1.294631452604925e-015 0.88187967729662287 0.47147453247360493 0
		 4.7741344956547724 -15.451286554210121 -25.14910263652089 1;
	setAttr ".pm[20]" -type "matrix" 1 -8.0366879368924533e-017 1.3418513026508522e-015 0
		 2.5648636118618768e-016 0.99771361518459678 -0.06758359324038915 0 -1.2946314526049248e-015 0.067583593240389095 0.99771361518459678 0
		 4.7741344956547733 24.096046616545863 -33.280827998021827 1;
	setAttr ".pm[21]" -type "matrix" 1 -1.7087026238342505e-016 1.3333518317227106e-015 0
		 2.5648636118618768e-016 1 -2.1788126858268697e-013 0 -1.294631452604925e-015 2.1782575743145571e-013 1 0
		 4.7741344956547689 26.959719398679709 -41.460247298719182 1;
	setAttr ".pm[22]" -type "matrix" 1 -1.2300573099055915e-015 1.8758427334616073e-016 0
		 5.2075288676871125e-016 0.57929677791156109 0.81511670520317703 0 -1.1377280178611338e-015 -0.81511670520317703 0.57929677791156109 0
		 13.559594067844033 9.7925236571143621 -38.761241417296013 1;
	setAttr ".pm[23]" -type "matrix" -1.6967427792063343e-015 5.6819178117461709e-016 1 0
		 0.548195810435682 0.83635001848554147 3.9278671220087846e-016 0 -0.83635001848554147 0.548195810435682 -1.7525898810279603e-015 0
		 11.94938008932275 -57.059998451008553 13.559594067844067 1;
	setAttr ".pm[24]" -type "matrix" -0.12848003334551053 0.88146023125097772 -0.45444553221976897 0
		 -4.0668857170800247e-014 0.45824340968204519 0.88882674210611645 0 0.9917120958380693 0.11419648946419372 -0.058875128556273769 0
		 -33.268987834514178 -4.3820873521069927 -44.657761378451269 1;
	setAttr ".pm[25]" -type "matrix" 0.99999999999999978 -2.9087843245194961e-014 -4.3533232574333033e-013 0
		 2.9087843245163274e-014 0.99999999999999978 -3.6345926268678703e-014 0 4.3519007841830235e-013 3.64638874650198e-014 0.99999999999999956 0
		 25.1480551455126 -50.388505805809054 -30.28899717661394 1;
	setAttr ".pm[26]" -type "matrix" -0.1344476464366319 -0.90192643795570193 0.41042969054905076 0
		 4.0287218006085349e-014 0.41419024876872268 0.91019033054900289 0 -0.99092069832436636 0.12237294775171008 -0.055686904123921577 0
		 29.729503994626857 -26.749417817374653 -33.623290286521346 1;
	setAttr ".pm[27]" -type "matrix" -2.8976820942734439e-014 -4.9599213625128767e-013 1 0
		 -0.99999999999999978 3.6026737149100775e-014 -2.9143354396392474e-014 0 -3.6012859361264133e-014 -1 -4.96193364174503e-013 0
		 50.423856318491083 30.325825852425719 2.387431426524897 1;
	setAttr ".pm[28]" -type "matrix" 1 -7.2789855023836332e-016 1.0091543716497876e-015 0
		 5.2075288676871115e-016 0.98748967924184261 0.15768364972578136 0 -1.1377280178611342e-015 -0.15768364972578136 0.98748967924184261 0
		 13.559594067844033 -22.381813094153408 -44.401871360433915 1;
	setAttr ".pm[29]" -type "matrix" -2.1107056767596492e-015 1.8214686070805692e-015 1 0
		 0.56109379366851575 0.82775223026378653 -2.674811328803908e-016 0 -0.82775223026378653 0.56109379366851586 -2.7450346338271665e-015 0
		 28.772275477477233 -57.728078760431714 13.559594067844149 1;
	setAttr ".pm[30]" -type "matrix" 1 -5.9066454027979248e-016 1.0951457647797617e-015 0
		 5.2075288676871125e-016 0.99960530565396177 0.028093289384640882 0 -1.137728017861134e-015 -0.028093289384640882 0.99960530565396177 0
		 13.721603235622847 -31.15607308572076 -39.200518706345264 1;
	setAttr ".pm[31]" -type "matrix" 0.99974740863347267 -0.00063812659218360904 0.022465790106483464 0
		 -1.9002670530871008e-013 0.99959683984874592 0.028392917504203002 0 -0.02247485107083522 -0.028385745698375181 0.99934435031699198 0
		 14.682830140228214 -31.152001962469051 -43.506018025412267 1;
	setAttr ".pm[32]" -type "matrix" 0.91972792027426242 -0.0048794193930970925 -0.39252610605457344 0
		 -1.6335890973273586e-014 0.99992274637276513 -0.012429854638985011 0 0.39255643246287525 0.011432084356431485 0.91965686795635204 0
		 -6.1154567106770026 -33.111178218164312 -48.613339271302664 1;
	setAttr ".pm[33]" -type "matrix" 0.85391475219052426 0.10310383169003855 -0.51009724159441172 0
		 -2.5951463200613022e-014 0.98017791765591644 0.19811928159498018 0 0.52041290913215776 -0.16917697724732897 0.83698838365777872 0
		 -13.535398801595038 -21.765720209926258 -56.18281402728303 1;
	setAttr ".pm[34]" -type "matrix" 0.58785084996131087 -0.64968936864240689 -0.48201151695036792 0
		 -3.3417713041217199e-014 0.59583410469289966 -0.80310753930268275 0 0.80896933081530609 0.47210744958943507 0.35026158487963605 0
		 -33.766721218240875 -56.677784577893952 -0.48800193016566928 1;
	setAttr ".pm[35]" -type "matrix" 1 1.6264767310757559e-013 1.2548295735827099e-013 0
		 -1.6286971771252021e-013 0.99999999999999978 7.7993167479896774e-014 0 -1.255107129338612e-013 -7.8104189782400134e-014 0.99999999999999978 0
		 19.027442219731739 -30.201499022732889 -55.679647814712233 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 36 ".ma";
	setAttr -s 36 ".dpf[0:35]"  10 10 10 10 10 10 10 10 10 10 10 10 10 
		10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10;
	setAttr -s 36 ".lw";
	setAttr -s 36 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".bm" 1;
	setAttr ".ucm" yes;
	setAttr -s 36 ".ifcl";
	setAttr -s 36 ".ifcl";
createNode tweak -n "tweak1";
	rename -uid "1EF3375C-4978-B3E8-7288-6B98088F5745";
createNode objectSet -n "skinCluster1Set";
	rename -uid "8E37D43B-4240-DED2-9D9C-2EA94975328C";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster1GroupId";
	rename -uid "62B5FA88-44D0-48B1-A967-D9BB2B213D15";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster1GroupParts";
	rename -uid "3AE6E36E-446D-6714-584C-1C92DD42D4B0";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	rename -uid "7387A50B-496A-F580-0EF2-88B6D6EAC665";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId9";
	rename -uid "6B3BEF3F-48B5-2B55-056F-BD81F265C103";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "64D280A6-40D2-7273-186E-44AB222A8836";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose1";
	rename -uid "18031BF1-425C-C5EC-E15D-A1B0337588AC";
	setAttr -s 36 ".wm";
	setAttr -s 36 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 -13.559594067844019 -31.239012503314999
		 -0.062433451202958734 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 4.3363956556246231e-017 -0.70818715382162278 -0.70602475534646036 4.3231547837691793e-017 1
		 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 -4.2188474935755949e-015
		 4.4408920985006262e-016 31.362064016760339 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0.70859526252070804 0 0 0.70561515993720603 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.1688295699958685e-014
		 -1.6424575253205046e-015 26.375398654648283 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.010638802834020009 0.71496149992555813 0.0056804578238059122 0.69905979851323652 1
		 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 1.5291301710646735e-018 5.6012043212022006e-017
		 -2.384338568295384e-016 0 -1.5543122344752199e-015 1.6682326168187062e-014 6.5453076270635853 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.30328663973065623 -0.60549908653408757 0.24862966786619328 0.69251090866788434 1
		 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.5771785916527938 -7.0432105892672938
		 9.1846773058381679 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 -5.983723589155572e-015 1.252592348539364e-015
		 -9.5147842238551722e-017 0 -1.5771785916527892 7.0432105892672814 11.529797540308335 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.32573762234688342 -0.034968348344401018 0.075173397432147099 0.94181801656178377 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 -1.7639915219954499e-016
		 2.125621499848794e-015 11.203647653956066 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-1.0513079958235253e-009 -0.62170620500730323 0.78325053121936472 1.3244801798210601e-009 1
		 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 -3.1200668628256211e-008
		 3.2257113264326829e-015 9.4726876600419665 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.16492918693986949 0 0 0.98630541076045686 1 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 -1.7023035874546865e-018 5.6228151327472197e-017
		 -2.383818123667687e-016 0 0.21933664359010335 0.27145783837047865 -6.1770854123746943 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.25288403112511926 0.71811640082150685 -0.29487865546255521 -0.57741240047791698 1
		 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 -1.6662293892799274 -7.1527090687181216
		 9.1484934221919083 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 7.6716942020272868e-016 1.612509198056218e-016
		 1.6007486952294729e-031 0 1.6662293892799327 7.1527090687181181 11.392314718181581 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.32546635920027306 0.038821817751756568 -0.083458520415773854 0.94106279857689534 1
		 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 -2.5061417847857109e-015
		 7.2867459224140048e-016 11.16739310594264 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-3.0334597063569017e-015 -0.6132892596328815 0.78985839491579257 3.9068083731631882e-015 1
		 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 0 0 0 3.1194547900250197e-014
		 -1.1102230246251565e-015 9.5067329929166426 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.17547613520524982 0 0 0.98448368497066974 1 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 0 0 -1.0451493343957269e-014
		 9.0549710440854536 -19.234855950469605 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.0020842148608331001 0.7077085942573178 -0.0030591101890780625 0.70649475830157127 1
		 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 -2.2151147640743484e-017 -9.9975057568023607e-018
		 -2.4372067586340518e-016 0 14.496803979766035 -13.036667014546724 6.703719802302798 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.47720005362814011 0.44328152495517664 -0.30542899471295948 0.69461840433387911 1
		 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 1.8192798495301425e-015 -1.7231967738062567e-015
		 -3.2127970705643971e-018 0 -5.5043282479443567 -9.8395236036149409 8.2140957981916891 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.064153944330599455 0.089631633455113879 -0.1947877470209165 0.97463232827641721 1
		 1 1 yes;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0 0 0 0 -3.1519695106906887e-015
		 1.2239713727797744e-015 13.006138509181579 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.48316230941061655 0 0 0.87553080058042487 1 1 1 yes;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0 0 0 0 -1.2738905830479637e-015
		 -8.1350188384908484e-016 9.6547471238717115 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.032633526251920099 0 0 0.99946738464262319 1 1 1 yes;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 -7.2130351397457953e-033 -1.759011765708104e-017
		 -2.4429690760892647e-016 0 14.526556059135771 -13.070664875829475 -8.7425325525782114 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.4104768281957567 0.57496040994798392 -0.4105602446068406 0.57651503541119253 1
		 1 1 yes;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 1.0623071630060296e-014 -3.7765413681111213e-030
		 -7.3522625577352782e-031 0 8.739830146978205e-015 -11.233969039428214 7.9239596456365184 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.079417553474684197 0 0 0.99684143784259671 1
		 1 1 yes;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0 0 0 0 9.0111742976083552e-016
		 5.6621374255882984e-015 12.923075113121305 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.48476945156481938 0 0 0.87464197179734304 1 1 1 yes;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.6758688460888269e-015
		 -4.1217029789208937e-015 9.9066599305408491 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.033811128459343941 0 0 0.99942824034159938 1 1 1 yes;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 0 0 0 0 11.426141714842744 16.604646740479271
		 -0.0031176405186609021 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.32501873107805446 0.62803146112742292 0.32359800879074768 0.62866814535908278 1
		 1 1 yes;
	setAttr ".xm[23]" -type "matrix" "xform" 1 1 1 0 0 0 0 -3.0429151716230374e-015
		 -1.0470273515473241e-012 18.708207755051525 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0.49049686336753717 0.50932585544679332 0.49049686336753717 0.50932585544679287 1
		 1 1 yes;
	setAttr ".xm[24]" -type "matrix" "xform" 1 1 1 0 0 0 0 -7.146691005129262 8.7916972783649356
		 13.109675265037701 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.04677913240447773 -0.39066102460620206 -0.64157928623544902 0.65846161323096775 1
		 1 1 yes;
	setAttr ".xm[25]" -type "matrix" "xform" 1 1 1 0 0 0 0 -9.3476941858463187e-015
		 -2.3906918663683836e-015 9.7740427788075124 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0.34356647301832488 0.64140443005626535 0.39094804485600976 0.56366839714960881 1
		 1 1 yes;
	setAttr ".xm[26]" -type "matrix" "xform" 1 1 1 0 0 0 0 7.2306454077744071 8.7843195399833167
		 13.092672073809931 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.062668596147905126 0.3758278827466538 0.6526693081250895 0.65486550057132753 1
		 1 1 yes;
	setAttr ".xm[27]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.3094521886690241e-015
		 2.2923222153842519e-015 9.6033920652487019 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0.42188365019344942 0.34175918484354634 -0.61903203436904231 0.56746293773201695 1
		 1 1 yes;
	setAttr ".xm[28]" -type "matrix" "xform" 1 1 1 0 0 0 0 9.2038368447298817e-016
		 -6.2112297915028938 8.3169022721700081 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.38692365756398817 0 0 0.92211175202212103 1
		 1 1 yes;
	setAttr ".xm[29]" -type "matrix" "xform" 1 1 1 0 0 0 0 -1.9627649019491387e-015
		 -6.6917625036924008e-015 16.091346078720633 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0.26032601333195193 0.65744229159880885 0.2603260133319511 0.65744229159880796 1 1
		 1 yes;
	setAttr ".xm[30]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.16200916777881708
		 -2.7549576593474003 9.7149617094170182 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.44611240610962088 0 0 0.89497693887333474 1
		 1 1 yes;
	setAttr ".xm[31]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.6181278345831304e-015
		 -6.2966483662331905e-015 4.6152666471701664 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.00014986438235213006 0.0112336521496134 0.00031739981906145538 0.99993683893404151 1
		 1 1 yes;
	setAttr ".xm[32]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.6244874364002338e-014
		 -2.4031876522595784e-015 4.5665621184235077 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0.019950993054329853 -0.21133072360305638 -0.0016871480776203514 0.97720951523658361 1
		 1 1 yes;
	setAttr ".xm[33]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.4090076216436444e-015
		 6.6196266887466204e-016 3.1526075975036725 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		-0.1054627962640442 -0.071586282544028962 -0.0067132020230097266 0.9918205158567216 1
		 1 1 yes;
	setAttr ".xm[34]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.1188681142563042e-016
		 -1.8331261410719564e-015 4.6106665063842707 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0.52580362399277625 -0.18321376002274883 0.070330791829265188 0.8276574453571105 1
		 1 1 yes;
	setAttr ".xm[35]" -type "matrix" "xform" 1 1 1 0 0 0 0 0.091108133005783284
		 -0.033998000844557022 3.9308344835626743 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.40054811208796726 0.40550020648947727 -0.2040690019488178 0.79591873635652655 1
		 1 1 yes;
	setAttr -s 36 ".m";
	setAttr -s 36 ".p";
	setAttr ".bp" yes;
createNode animCurveTA -n "Corgi_center_rotateX";
	rename -uid "E913FC9A-4695-0BEB-0CF8-898AA5D7819C";
	setAttr ".tan" 18;
	setAttr -s 6 ".ktv[0:5]"  1 0 12 14.999999999999998 24 0 36 0 48 16.447266257400358
		 60 0;
	setAttr -s 6 ".kit[5]"  1;
	setAttr -s 6 ".kot[5]"  1;
	setAttr -s 6 ".kix[5]"  0.4583333432674408;
	setAttr -s 6 ".kiy[5]"  0;
	setAttr -s 6 ".kox[5]"  0.4583333432674408;
	setAttr -s 6 ".koy[5]"  0;
createNode animCurveTA -n "Corgi_center_rotateY";
	rename -uid "F044CF6A-453D-D054-6294-D9A328F289D6";
	setAttr ".tan" 18;
	setAttr -s 3 ".ktv[0:2]"  1 0 36 0 60 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1.4583333730697632;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1.4583333730697632;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "Corgi_center_rotateZ";
	rename -uid "361704B1-489E-4BBB-65F5-AB8580C1DCCD";
	setAttr ".tan" 18;
	setAttr -s 3 ".ktv[0:2]"  1 0 36 0 60 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1.4583333730697632;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1.4583333730697632;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "Corgi_Leg_R_rotateX";
	rename -uid "E008900C-4A2A-8B54-0C46-A7814669E2F5";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 12 35.6918130254587 36 35.6918130254587
		 60 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.4583333432674408;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.4583333432674408;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Corgi_Leg_L_rotateX";
	rename -uid "D0403B6E-487B-D7C4-BB46-02A87CCA4F84";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 0 12 35.612748222216723 36 -0.57422153790946451
		 48 50.18164143221157 60 0;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  0.4583333432674408;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  0.4583333432674408;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Corgi_Leg_R_rotateY";
	rename -uid "5AAE8901-4665-475B-B047-14B9CDC07412";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 12 2.7641268877835077 36 2.7641268877835077
		 60 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.4583333432674408;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.4583333432674408;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Corgi_Leg_L_rotateY";
	rename -uid "E044E839-4812-5E67-F9B1-FE8F710E9E1E";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 0 12 -3.0711219951647135 36 0.077370524407071845
		 48 -3.2151766434517368 60 0;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  0.4583333432674408;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  0.4583333432674408;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Corgi_Leg_R_rotateZ";
	rename -uid "BF30E18B-4246-A9AE-E1E1-37B94F3C279D";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 12 5.1678991095295288 36 5.1678991095295288
		 60 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.4583333432674408;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.4583333432674408;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Corgi_Leg_L_rotateZ";
	rename -uid "14C75A5B-49E4-C2C0-5C6C-EEBB86A22D9A";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 0 12 -5.7315152444375119 36 0.074032347128516418
		 48 -8.4133342934786626 60 0;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  0.4583333432674408;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  0.4583333432674408;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "joint2_rotateX";
	rename -uid "E656187F-45D4-4061-4BBE-5E8EC04CC79F";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 -33.644607078103427 12 -33.644607078103427
		 24 -15.236990049587417 60 -33.644607078103427;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.4583333432674408;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.4583333432674408;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "joint1_rotateX";
	rename -uid "9D96D1DE-42E9-1D71-29B7-4589DE857339";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 -33.515662426657585 12 -33.515662426657585
		 24 -15.181823647520414 48 -40.214659649121856 60 -33.515662426657585;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  0.4583333432674408;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  0.4583333432674408;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "joint2_rotateY";
	rename -uid "2DDCC58E-4357-E6CD-03A3-5CAF294EC7E5";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 -4.9545621292825439 12 -4.9545621292825439
		 24 -2.0520401709296223 60 -4.9545621292825439;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.4583333432674408;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.4583333432674408;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "joint1_rotateY";
	rename -uid "3673CA4F-4EB1-D82C-7536-BE9B3A8C15A3";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 5.4937529434372649 12 5.4937529434372649
		 24 2.275653172013187 48 6.7355094484267388 60 5.4937529434372649;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  0.4583333432674408;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  0.4583333432674408;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "joint2_rotateZ";
	rename -uid "F393DDD4-41A3-A539-1CB7-489896E03499";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 -2.5477486263200642 12 -2.5477486263200642
		 24 -1.513321159107716 60 -2.5477486263200642;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.4583333432674408;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.4583333432674408;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "joint1_rotateZ";
	rename -uid "642B6FD7-4C92-5446-5FF6-B7A4A2724BCE";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 2.833029349768502 12 2.833029349768502
		 24 1.6798885577367875 48 2.9898835597528173 60 2.833029349768502;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  0.4583333432674408;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  0.4583333432674408;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Corgi_Shin_R_rotateX";
	rename -uid "6A283F08-4A94-B7A5-488C-5DA8CA4FD964";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  12 44.535547512682655 24 21.574185801767381;
createNode animCurveTA -n "Corgi_Shin_L_rotateX";
	rename -uid "309299B4-434F-77B8-8E67-F2BED1607A9A";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  12 44.53554751268063 24 21.574185801764365
		 36 15.986895021448904 48 34.58700917412537;
createNode animCurveTA -n "Corgi_Shin_R_rotateY";
	rename -uid "777AC891-41F0-6D7F-6486-09A7454B11E5";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTA -n "Corgi_Shin_L_rotateY";
	rename -uid "B0256CC6-43ED-C9E9-DE79-CBB527E00F9A";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTA -n "Corgi_Shin_R_rotateZ";
	rename -uid "F720B208-4FA7-9C91-A2F4-348625B41F78";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTA -n "Corgi_Shin_L_rotateZ";
	rename -uid "E4D28D1F-41BF-9BAC-DEFD-D9AE28BA3D26";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTA -n "Corgi_Foot_R_rotateX";
	rename -uid "0CF36759-4E4A-3EEE-0126-6780B7D135C2";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  12 20.156717540296007;
createNode animCurveTA -n "Corgi_Foot_L_rotateX";
	rename -uid "116D3187-45D2-2D07-0184-1F82F620366F";
	setAttr ".tan" 18;
	setAttr -s 3 ".ktv[0:2]"  12 20.156717540296189 36 12.73533385874903
		 48 23.192488027728341;
createNode animCurveTA -n "Corgi_Foot_R_rotateY";
	rename -uid "F990303E-4BCD-29ED-303A-04A66FDE0A9A";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTA -n "Corgi_Foot_L_rotateY";
	rename -uid "61F28B8D-4463-B3F8-9A5E-7099F6513E9C";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTA -n "Corgi_Foot_R_rotateZ";
	rename -uid "C3795A3D-43F5-C65E-BB3E-36B2D58BFCAD";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTA -n "Corgi_Foot_L_rotateZ";
	rename -uid "020EA473-4637-B9A4-8EBC-49B7A5D9E24D";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTA -n "Corgi_Tongue5_rotateX";
	rename -uid "55CDADD6-46AE-C6D2-1EE5-79A0971887BF";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_Hips_rotateX";
	rename -uid "A0D36FAE-4F8D-6DC5-5053-8C9953CDE676";
	setAttr ".tan" 18;
	setAttr -s 3 ".ktv[0:2]"  1 0 48 -0.14808379737990285 60 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1.9583333730697632;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1.9583333730697632;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "Corgi_Shoulders_rotateX";
	rename -uid "523645E9-4391-D8FC-9D9E-E5A5948E42A9";
	setAttr ".tan" 18;
	setAttr -s 6 ".ktv[0:5]"  1 0 12 0.060894395926983973 24 0.058789075021519088
		 36 0.046967945136045172 48 -4.9522611925363949 60 0;
	setAttr -s 6 ".kit[5]"  1;
	setAttr -s 6 ".kot[5]"  1;
	setAttr -s 6 ".kix[5]"  0.4583333432674408;
	setAttr -s 6 ".kiy[5]"  0;
	setAttr -s 6 ".kox[5]"  0.4583333432674408;
	setAttr -s 6 ".koy[5]"  0;
createNode animCurveTA -n "Corgi_Arm_R_rotateX";
	rename -uid "1BF170EB-4110-5344-4587-88BF725B107E";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 12 0 36 -0.16978399269257582 60 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.4583333432674408;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.4583333432674408;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Corgi_Forearm_R_rotateX";
	rename -uid "A2984744-4CB5-EF9C-E919-FF9F175AB907";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 0 12 0 24 -35.490200143843502 36 -28.649938294511593
		 60 0;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  0.4583333432674408;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  0.4583333432674408;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Corgi_Hand_R_rotateX";
	rename -uid "9D05BE53-41D8-70F7-F4CC-59BA699C08F2";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Fingers_R_rotateX";
	rename -uid "7E3E085B-4754-3780-A096-628DA8868F0C";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Arm_L_rotateX";
	rename -uid "BBD83231-41FA-C370-50E5-74A3F6C8A840";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 12 0 36 -0.18699651991999261 60 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.4583333432674408;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.4583333432674408;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Corgi_Forearm_L_rotateX";
	rename -uid "7F1D237F-41D3-7FB0-9367-99B4004AB5AC";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 0 12 0 24 -35.490200143847389 36 -28.649938294515128
		 60 0;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  0.4583333432674408;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  0.4583333432674408;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Corgi_Hand_L_rotateX";
	rename -uid "6786430C-4C4F-C130-948E-C39B0CD92BFB";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Fingers_L_rotateX";
	rename -uid "8E825090-4A57-D680-6112-07AC7437A99A";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_head_rotateX";
	rename -uid "FE4F7B42-47B2-2D49-C1BA-A78212943F51";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 0 12 -32.213523925801063 36 -32.213523925801063
		 48 -19.849103327588583 60 0;
	setAttr -s 5 ".kit[2:4]"  1 18 1;
	setAttr -s 5 ".kot[2:4]"  1 18 1;
	setAttr -s 5 ".kix[2:4]"  0.4583333432674408 0.5 0.4583333432674408;
	setAttr -s 5 ".kiy[2:4]"  -0.19573433697223663 0.28111603856086731 
		0;
	setAttr -s 5 ".kox[2:4]"  1 0.5 0.4583333432674408;
	setAttr -s 5 ".koy[2:4]"  -0.42705672979354858 0.28111603856086731 
		0;
createNode animCurveTA -n "Corgi_Hat_rotateX";
	rename -uid "9B60FB7A-44BB-9AA3-4ECE-1787DCEE0D7A";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Ear_R_rotateX";
	rename -uid "FA26ABC6-4469-7036-768D-2BAC4C624120";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Eartip_R_rotateX";
	rename -uid "3CF560EE-4926-B0F4-E984-5CA016FB1109";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Ear_L_rotateX";
	rename -uid "E0482E01-4B47-AA0E-0EDF-E494D7008757";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Eartip_L_rotateX";
	rename -uid "F9A51CA5-4402-991A-361C-27A296E3F453";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_lowerJaw_rotateX";
	rename -uid "6FA0CC1E-431E-6FBD-0CCE-DCB978A8ADDE";
	setAttr ".tan" 18;
	setAttr -s 6 ".ktv[0:5]"  1 11.55201174121515 12 -1.2772637720965745
		 24 -1.0776913077064847 48 -0.20844235398358241 54 14.576123965012277 60 0;
createNode animCurveTA -n "Corgi_Jawtip_rotateX";
	rename -uid "853F2BE1-44F9-45D3-30A9-D3ACCB307C4D";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Tongue_root_rotateX";
	rename -uid "B4C74EDB-4C8F-3157-9F6F-1FAB8F7ACE0A";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue1_rotateX";
	rename -uid "7D95E42F-47BB-A536-32FE-D1B35D067D52";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue2_rotateX";
	rename -uid "5F9C7748-453B-1CFB-9D63-7A82A1780CBA";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue3_rotateX";
	rename -uid "C89444CF-41AD-D74A-3732-B6ACE96CCF30";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue4_rotateX";
	rename -uid "470138C8-40D4-A769-3004-76A421D5D995";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 -68.426254509787583 60 -68.426254509787583;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_Tongue5_rotateY";
	rename -uid "DACBC5A1-4482-1956-21FE-848DEFCB84E5";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_Hips_rotateY";
	rename -uid "D019FB79-42FA-8711-FF17-DF8779A32B35";
	setAttr ".tan" 18;
	setAttr -s 3 ".ktv[0:2]"  1 0 48 -0.1371789680768174 60 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1.9583333730697632;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1.9583333730697632;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "Corgi_Shoulders_rotateY";
	rename -uid "4880A004-4BD1-9578-8FC7-5EB05F8B6DDD";
	setAttr ".tan" 18;
	setAttr -s 6 ".ktv[0:5]"  1 0 12 -0.026254423461991999 24 -0.025937212102780157
		 36 -0.02346865274542688 48 8.0474402426723888 60 0;
	setAttr -s 6 ".kit[5]"  1;
	setAttr -s 6 ".kot[5]"  1;
	setAttr -s 6 ".kix[5]"  0.4583333432674408;
	setAttr -s 6 ".kiy[5]"  0;
	setAttr -s 6 ".kox[5]"  0.4583333432674408;
	setAttr -s 6 ".koy[5]"  0;
createNode animCurveTA -n "Corgi_Arm_R_rotateY";
	rename -uid "A40F32AB-415D-F954-DF37-D79575FCDCAF";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 12 0 36 0.073193165751428507 60 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.4583333432674408;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.4583333432674408;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Corgi_Forearm_R_rotateY";
	rename -uid "6015F0EC-4137-698A-A185-2380B36D37BF";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 24 -0.00029704367392438237 36 -0.00029109719898369962
		 60 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.95833331346511841;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.95833331346511841;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Corgi_Hand_R_rotateY";
	rename -uid "4FD14776-4807-DDB8-7B66-6BBAB83ED3EE";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Fingers_R_rotateY";
	rename -uid "712857BF-4065-F6D8-8D14-7A842F31FBE7";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Arm_L_rotateY";
	rename -uid "4F35A5F5-4819-218E-A577-52AB178674E4";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Forearm_L_rotateY";
	rename -uid "1BA51ABC-4598-2EC9-E416-CE911BC5C526";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 24 -0.00029722221883147891 36 -0.00029148775078281276
		 60 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.95833331346511841;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.95833331346511841;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Corgi_Hand_L_rotateY";
	rename -uid "4E5328C6-4B80-D5CF-476B-778AB3C7BCAD";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Fingers_L_rotateY";
	rename -uid "49063803-48A5-63D7-AAA5-C29D707E8122";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_head_rotateY";
	rename -uid "6223D449-40EE-AD5D-B5A2-C0A5A121C00D";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 0 12 5.0284490579436572 36 5.0284490579436572
		 48 36.257286513604818 60 0;
	setAttr -s 5 ".kit[2:4]"  1 18 1;
	setAttr -s 5 ".kot[2:4]"  1 18 1;
	setAttr -s 5 ".kix[2:4]"  0.4583333432674408 0.5 0.4583333432674408;
	setAttr -s 5 ".kiy[2:4]"  0.1481042355298996 0 0;
	setAttr -s 5 ".kox[2:4]"  1.5 0.5 0.4583333432674408;
	setAttr -s 5 ".koy[2:4]"  0.48470476269721985 0 0;
createNode animCurveTA -n "Corgi_Hat_rotateY";
	rename -uid "C071FC33-4839-9C34-278F-CF96044F1258";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Ear_R_rotateY";
	rename -uid "29D13C8C-4244-5425-BE95-59B8707D7A3B";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Eartip_R_rotateY";
	rename -uid "FA8AE7B0-4ACF-FE6B-3383-6A925CB6E4C5";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Ear_L_rotateY";
	rename -uid "11000938-42FA-F1BE-A2A5-6B87B6585983";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Eartip_L_rotateY";
	rename -uid "848B587F-4BC9-B787-ABE6-35AA5BA996B6";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_lowerJaw_rotateY";
	rename -uid "27001ADA-4276-FA8A-3267-A181BA3B6798";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 24 0 48 0 60 0;
createNode animCurveTA -n "Corgi_Jawtip_rotateY";
	rename -uid "47F57D34-4282-B73A-3617-5BB15CB7933C";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Tongue_root_rotateY";
	rename -uid "6263DCFB-4E55-17E6-6D67-5E83E4B55699";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 38.569300417548213 60 38.569300417548213;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue1_rotateY";
	rename -uid "3DD9F5A8-4218-30DE-30DF-50B0F474833B";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 -79.76911151019408 60 -79.76911151019408;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue2_rotateY";
	rename -uid "476BA7AC-4EA6-83A6-CB32-16BF0802040D";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 119.51628477535888 60 119.51628477535888;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue3_rotateY";
	rename -uid "C5A46ED7-44A3-24BA-EFF0-568C8D859C49";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 -89.414066549734486 60 -89.414066549734486;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue4_rotateY";
	rename -uid "B0CDAD73-41A8-82F7-C295-A3AB48627C76";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_Tongue5_rotateZ";
	rename -uid "25F4B53D-4FCA-C8DA-DCE6-9E8C3FA72331";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_Hips_rotateZ";
	rename -uid "D0183F10-4552-CDA9-891F-649530F277D2";
	setAttr ".tan" 18;
	setAttr -s 3 ".ktv[0:2]"  1 0 48 6.2651567978610903 60 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1.9583333730697632;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1.9583333730697632;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "Corgi_Shoulders_rotateZ";
	rename -uid "CADC4DB1-46BB-6711-F0A6-59BAD2EC6DF0";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 0 12 -30.476525903015069 24 -29.509512454936136
		 36 -24.022597406201811 60 0;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  0.4583333432674408;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  0.4583333432674408;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "Corgi_Arm_R_rotateZ";
	rename -uid "DB57A045-47E1-A7A2-B1AA-20B1707E85B8";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 12 0 36 0.027889252387169199 60 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.4583333432674408;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.4583333432674408;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Corgi_Forearm_R_rotateZ";
	rename -uid "10CA3EFE-473A-0015-BA81-D5943E74BE56";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 24 -3.2175326183453887e-005 36 -6.7324710005915546e-005
		 60 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.95833331346511841;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.95833331346511841;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Corgi_Hand_R_rotateZ";
	rename -uid "01DCAF1C-4328-4DDB-1152-0CB7E059C203";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Fingers_R_rotateZ";
	rename -uid "03C0AFE6-4AE3-46C1-6DE5-28A7A72BDA7C";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Arm_L_rotateZ";
	rename -uid "4A172361-4FC6-1FF5-2EA4-74A453A00ACA";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Forearm_L_rotateZ";
	rename -uid "A986EE9A-4F0B-9ABF-51C9-879069A8F8DD";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 24 -3.0384606420196731e-005 36 -6.5568001479559117e-005
		 60 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  0.95833331346511841;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  0.95833331346511841;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "Corgi_Hand_L_rotateZ";
	rename -uid "DFF65123-4D68-731B-3675-5099346CE73E";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Fingers_L_rotateZ";
	rename -uid "19F3995E-4017-7548-ABC6-CD818270B7B2";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_head_rotateZ";
	rename -uid "967CD298-4628-1BAC-C711-278E04EC65DE";
	setAttr ".tan" 18;
	setAttr -s 5 ".ktv[0:4]"  1 0 12 1.4251903753822217 36 1.4251903753822217
		 48 10.276237301247848 60 0;
	setAttr -s 5 ".kit[2:4]"  1 18 1;
	setAttr -s 5 ".kot[2:4]"  1 18 1;
	setAttr -s 5 ".kix[2:4]"  0.4583333432674408 0.5 0.4583333432674408;
	setAttr -s 5 ".kiy[2:4]"  0.041976507753133774 0 0;
	setAttr -s 5 ".kox[2:4]"  1.5 0.5 0.4583333432674408;
	setAttr -s 5 ".koy[2:4]"  0.13737766444683075 0 0;
createNode animCurveTA -n "Corgi_Hat_rotateZ";
	rename -uid "F9EE3C14-4B93-7E89-6AB6-90931F383BFF";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Ear_R_rotateZ";
	rename -uid "945833BA-44FE-700C-F1E0-C4A1F5E20781";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Eartip_R_rotateZ";
	rename -uid "4693FBD9-471F-7F9D-3990-3EA924B9C001";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Ear_L_rotateZ";
	rename -uid "8A1B7A19-4E7F-958C-7462-58A12BCE71B5";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Eartip_L_rotateZ";
	rename -uid "DB4FDEDA-4D24-B77A-F6B5-17A705224EAC";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_lowerJaw_rotateZ";
	rename -uid "A8457589-436D-8416-15F3-55872A0FAE99";
	setAttr ".tan" 18;
	setAttr -s 4 ".ktv[0:3]"  1 0 24 0 48 0 60 0;
createNode animCurveTA -n "Corgi_Jawtip_rotateZ";
	rename -uid "019C19AA-4CD3-6719-797B-E0863EDCF392";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
createNode animCurveTA -n "Corgi_Tongue_root_rotateZ";
	rename -uid "7DB3B39E-42FA-68DE-6DCB-36BCAC59BA6E";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue1_rotateZ";
	rename -uid "3ADADB98-44C5-9EFD-28C3-ECBCAF884566";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue2_rotateZ";
	rename -uid "AE95556E-4C19-28DF-0466-92952449A47E";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue3_rotateZ";
	rename -uid "643E57BC-4DDB-37D7-596E-8AB8724AF4FF";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 0 60 0;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "Corgi_tongue4_rotateZ";
	rename -uid "A9A8C51B-487F-3F27-ACA1-88A2D118F22B";
	setAttr ".tan" 18;
	setAttr -s 2 ".ktv[0:1]"  1 -79.718513006178455 60 -79.718513006178455;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  2.4583332538604736;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[1]"  2.4583332538604736;
	setAttr -s 2 ".koy[1]"  0;
select -ne :time1;
	setAttr ".o" 60;
	setAttr ".unw" 60;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 5 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 7 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :lambert1;
	setAttr ".c" -type "float3" 0.4759036 0.4759036 0.4759036 ;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
	setAttr ".outf" 51;
	setAttr ".imfkey" -type "string" "exr";
select -ne :defaultResolution;
	setAttr ".w" 1280;
	setAttr ".h" 720;
	setAttr ".pa" 1;
	setAttr ".dar" 1.7769999504089355;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "cameraView1.msg" ":perspShape.b" -na;
connectAttr "Corgi_Root.s" "Corgi_center.is";
connectAttr "Corgi_center_rotateX.o" "Corgi_center.rx";
connectAttr "Corgi_center_rotateY.o" "Corgi_center.ry";
connectAttr "Corgi_center_rotateZ.o" "Corgi_center.rz";
connectAttr "Corgi_center.s" "Corgi_Hips.is";
connectAttr "Corgi_Hips_rotateX.o" "Corgi_Hips.rx";
connectAttr "Corgi_Hips_rotateY.o" "Corgi_Hips.ry";
connectAttr "Corgi_Hips_rotateZ.o" "Corgi_Hips.rz";
connectAttr "Corgi_Hips.s" "Corgi_Leg_R.is";
connectAttr "Corgi_Leg_R_rotateX.o" "Corgi_Leg_R.rx";
connectAttr "Corgi_Leg_R_rotateY.o" "Corgi_Leg_R.ry";
connectAttr "Corgi_Leg_R_rotateZ.o" "Corgi_Leg_R.rz";
connectAttr "Corgi_Leg_R.s" "joint2.is";
connectAttr "joint2_rotateX.o" "joint2.rx";
connectAttr "joint2_rotateY.o" "joint2.ry";
connectAttr "joint2_rotateZ.o" "joint2.rz";
connectAttr "joint2.s" "Corgi_Shin_R.is";
connectAttr "Corgi_Shin_R_rotateX.o" "Corgi_Shin_R.rx";
connectAttr "Corgi_Shin_R_rotateY.o" "Corgi_Shin_R.ry";
connectAttr "Corgi_Shin_R_rotateZ.o" "Corgi_Shin_R.rz";
connectAttr "Corgi_Shin_R.s" "Corgi_Foot_R.is";
connectAttr "Corgi_Foot_R_rotateX.o" "Corgi_Foot_R.rx";
connectAttr "Corgi_Foot_R_rotateY.o" "Corgi_Foot_R.ry";
connectAttr "Corgi_Foot_R_rotateZ.o" "Corgi_Foot_R.rz";
connectAttr "Corgi_Foot_R.s" "Corgi_Toes_R.is";
connectAttr "Corgi_Hips.s" "Corgi_Leg_L.is";
connectAttr "Corgi_Leg_L_rotateX.o" "Corgi_Leg_L.rx";
connectAttr "Corgi_Leg_L_rotateY.o" "Corgi_Leg_L.ry";
connectAttr "Corgi_Leg_L_rotateZ.o" "Corgi_Leg_L.rz";
connectAttr "Corgi_Leg_L.s" "joint1.is";
connectAttr "joint1_rotateX.o" "joint1.rx";
connectAttr "joint1_rotateY.o" "joint1.ry";
connectAttr "joint1_rotateZ.o" "joint1.rz";
connectAttr "joint1.s" "Corgi_Shin_L.is";
connectAttr "Corgi_Shin_L_rotateX.o" "Corgi_Shin_L.rx";
connectAttr "Corgi_Shin_L_rotateY.o" "Corgi_Shin_L.ry";
connectAttr "Corgi_Shin_L_rotateZ.o" "Corgi_Shin_L.rz";
connectAttr "Corgi_Shin_L.s" "Corgi_Foot_L.is";
connectAttr "Corgi_Foot_L_rotateX.o" "Corgi_Foot_L.rx";
connectAttr "Corgi_Foot_L_rotateY.o" "Corgi_Foot_L.ry";
connectAttr "Corgi_Foot_L_rotateZ.o" "Corgi_Foot_L.rz";
connectAttr "Corgi_Foot_L.s" "Corgi_Toes_L.is";
connectAttr "Corgi_center.s" "Corgi_Shoulders.is";
connectAttr "Corgi_Shoulders_rotateX.o" "Corgi_Shoulders.rx";
connectAttr "Corgi_Shoulders_rotateY.o" "Corgi_Shoulders.ry";
connectAttr "Corgi_Shoulders_rotateZ.o" "Corgi_Shoulders.rz";
connectAttr "Corgi_Shoulders.s" "Corgi_Arm_R.is";
connectAttr "Corgi_Arm_R_rotateX.o" "Corgi_Arm_R.rx";
connectAttr "Corgi_Arm_R_rotateY.o" "Corgi_Arm_R.ry";
connectAttr "Corgi_Arm_R_rotateZ.o" "Corgi_Arm_R.rz";
connectAttr "Corgi_Arm_R.s" "Corgi_Forearm_R.is";
connectAttr "Corgi_Forearm_R_rotateX.o" "Corgi_Forearm_R.rx";
connectAttr "Corgi_Forearm_R_rotateY.o" "Corgi_Forearm_R.ry";
connectAttr "Corgi_Forearm_R_rotateZ.o" "Corgi_Forearm_R.rz";
connectAttr "Corgi_Forearm_R.s" "Corgi_Hand_R.is";
connectAttr "Corgi_Hand_R_rotateX.o" "Corgi_Hand_R.rx";
connectAttr "Corgi_Hand_R_rotateY.o" "Corgi_Hand_R.ry";
connectAttr "Corgi_Hand_R_rotateZ.o" "Corgi_Hand_R.rz";
connectAttr "Corgi_Hand_R.s" "Corgi_Fingers_R.is";
connectAttr "Corgi_Fingers_R_rotateX.o" "Corgi_Fingers_R.rx";
connectAttr "Corgi_Fingers_R_rotateY.o" "Corgi_Fingers_R.ry";
connectAttr "Corgi_Fingers_R_rotateZ.o" "Corgi_Fingers_R.rz";
connectAttr "Corgi_Shoulders.s" "Corgi_Arm_L.is";
connectAttr "Corgi_Arm_L_rotateX.o" "Corgi_Arm_L.rx";
connectAttr "Corgi_Arm_L_rotateY.o" "Corgi_Arm_L.ry";
connectAttr "Corgi_Arm_L_rotateZ.o" "Corgi_Arm_L.rz";
connectAttr "Corgi_Arm_L.s" "Corgi_Forearm_L.is";
connectAttr "Corgi_Forearm_L_rotateX.o" "Corgi_Forearm_L.rx";
connectAttr "Corgi_Forearm_L_rotateY.o" "Corgi_Forearm_L.ry";
connectAttr "Corgi_Forearm_L_rotateZ.o" "Corgi_Forearm_L.rz";
connectAttr "Corgi_Forearm_L.s" "Corgi_Hand_L.is";
connectAttr "Corgi_Hand_L_rotateX.o" "Corgi_Hand_L.rx";
connectAttr "Corgi_Hand_L_rotateY.o" "Corgi_Hand_L.ry";
connectAttr "Corgi_Hand_L_rotateZ.o" "Corgi_Hand_L.rz";
connectAttr "Corgi_Hand_L.s" "Corgi_Fingers_L.is";
connectAttr "Corgi_Fingers_L_rotateX.o" "Corgi_Fingers_L.rx";
connectAttr "Corgi_Fingers_L_rotateY.o" "Corgi_Fingers_L.ry";
connectAttr "Corgi_Fingers_L_rotateZ.o" "Corgi_Fingers_L.rz";
connectAttr "Corgi_Shoulders.s" "Corgi_head.is";
connectAttr "Corgi_head_rotateX.o" "Corgi_head.rx";
connectAttr "Corgi_head_rotateY.o" "Corgi_head.ry";
connectAttr "Corgi_head_rotateZ.o" "Corgi_head.rz";
connectAttr "Corgi_head.s" "Corgi_Hat.is";
connectAttr "Corgi_Hat_rotateX.o" "Corgi_Hat.rx";
connectAttr "Corgi_Hat_rotateY.o" "Corgi_Hat.ry";
connectAttr "Corgi_Hat_rotateZ.o" "Corgi_Hat.rz";
connectAttr "Corgi_head.s" "Corgi_Ear_R.is";
connectAttr "Corgi_Ear_R_rotateX.o" "Corgi_Ear_R.rx";
connectAttr "Corgi_Ear_R_rotateY.o" "Corgi_Ear_R.ry";
connectAttr "Corgi_Ear_R_rotateZ.o" "Corgi_Ear_R.rz";
connectAttr "Corgi_Ear_R.s" "Corgi_Eartip_R.is";
connectAttr "Corgi_Eartip_R_rotateX.o" "Corgi_Eartip_R.rx";
connectAttr "Corgi_Eartip_R_rotateY.o" "Corgi_Eartip_R.ry";
connectAttr "Corgi_Eartip_R_rotateZ.o" "Corgi_Eartip_R.rz";
connectAttr "Corgi_head.s" "Corgi_Ear_L.is";
connectAttr "Corgi_Ear_L_rotateX.o" "Corgi_Ear_L.rx";
connectAttr "Corgi_Ear_L_rotateY.o" "Corgi_Ear_L.ry";
connectAttr "Corgi_Ear_L_rotateZ.o" "Corgi_Ear_L.rz";
connectAttr "Corgi_Ear_L.s" "Corgi_Eartip_L.is";
connectAttr "Corgi_Eartip_L_rotateX.o" "Corgi_Eartip_L.rx";
connectAttr "Corgi_Eartip_L_rotateY.o" "Corgi_Eartip_L.ry";
connectAttr "Corgi_Eartip_L_rotateZ.o" "Corgi_Eartip_L.rz";
connectAttr "Corgi_head.s" "Corgi_lowerJaw.is";
connectAttr "Corgi_lowerJaw_rotateX.o" "Corgi_lowerJaw.rx";
connectAttr "Corgi_lowerJaw_rotateY.o" "Corgi_lowerJaw.ry";
connectAttr "Corgi_lowerJaw_rotateZ.o" "Corgi_lowerJaw.rz";
connectAttr "Corgi_lowerJaw.s" "Corgi_Jawtip.is";
connectAttr "Corgi_Jawtip_rotateX.o" "Corgi_Jawtip.rx";
connectAttr "Corgi_Jawtip_rotateY.o" "Corgi_Jawtip.ry";
connectAttr "Corgi_Jawtip_rotateZ.o" "Corgi_Jawtip.rz";
connectAttr "Corgi_head.s" "Corgi_Tongue_root.is";
connectAttr "Corgi_Tongue_root_rotateX.o" "Corgi_Tongue_root.rx";
connectAttr "Corgi_Tongue_root_rotateY.o" "Corgi_Tongue_root.ry";
connectAttr "Corgi_Tongue_root_rotateZ.o" "Corgi_Tongue_root.rz";
connectAttr "Corgi_Tongue_root.s" "Corgi_tongue1.is";
connectAttr "Corgi_tongue1_rotateX.o" "Corgi_tongue1.rx";
connectAttr "Corgi_tongue1_rotateY.o" "Corgi_tongue1.ry";
connectAttr "Corgi_tongue1_rotateZ.o" "Corgi_tongue1.rz";
connectAttr "Corgi_tongue1.s" "Corgi_tongue2.is";
connectAttr "Corgi_tongue2_rotateX.o" "Corgi_tongue2.rx";
connectAttr "Corgi_tongue2_rotateY.o" "Corgi_tongue2.ry";
connectAttr "Corgi_tongue2_rotateZ.o" "Corgi_tongue2.rz";
connectAttr "Corgi_tongue2.s" "Corgi_tongue3.is";
connectAttr "Corgi_tongue3_rotateX.o" "Corgi_tongue3.rx";
connectAttr "Corgi_tongue3_rotateY.o" "Corgi_tongue3.ry";
connectAttr "Corgi_tongue3_rotateZ.o" "Corgi_tongue3.rz";
connectAttr "Corgi_tongue3.s" "Corgi_tongue4.is";
connectAttr "Corgi_tongue4_rotateX.o" "Corgi_tongue4.rx";
connectAttr "Corgi_tongue4_rotateY.o" "Corgi_tongue4.ry";
connectAttr "Corgi_tongue4_rotateZ.o" "Corgi_tongue4.rz";
connectAttr "Corgi_tongue4.s" "Corgi_Tongue5.is";
connectAttr "Corgi_Tongue5_rotateX.o" "Corgi_Tongue5.rx";
connectAttr "Corgi_Tongue5_rotateY.o" "Corgi_Tongue5.ry";
connectAttr "Corgi_Tongue5_rotateZ.o" "Corgi_Tongue5.rz";
connectAttr "skinCluster1.og[0]" "group2Shape.i";
connectAttr "groupId1.id" "group2Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "group2Shape.iog.og[0].gco";
connectAttr "groupId2.id" "group2Shape.iog.og[1].gid";
connectAttr "blinn1SG.mwc" "group2Shape.iog.og[1].gco";
connectAttr "groupId3.id" "group2Shape.iog.og[2].gid";
connectAttr "lambert3SG.mwc" "group2Shape.iog.og[2].gco";
connectAttr "groupId7.id" "group2Shape.iog.og[11].gid";
connectAttr "skinCluster1GroupId.id" "group2Shape.iog.og[15].gid";
connectAttr "skinCluster1Set.mwc" "group2Shape.iog.og[15].gco";
connectAttr "groupId9.id" "group2Shape.iog.og[16].gid";
connectAttr "tweakSet1.mwc" "group2Shape.iog.og[16].gco";
connectAttr "tweak1.vl[0].vt[0]" "group2Shape.twl";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "blinn1.oc" "blinn1SG.ss";
connectAttr "groupId2.msg" "blinn1SG.gn" -na;
connectAttr "group2Shape.iog.og[1]" "blinn1SG.dsm" -na;
connectAttr "blinn1SG.msg" "materialInfo1.sg";
connectAttr "blinn1.msg" "materialInfo1.m";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "lambert2.oc" "lambert2SG.ss";
connectAttr "lambert2SG.msg" "materialInfo2.sg";
connectAttr "lambert2.msg" "materialInfo2.m";
connectAttr "lambert3.oc" "lambert3SG.ss";
connectAttr "group2Shape.iog.og[2]" "lambert3SG.dsm" -na;
connectAttr "groupId3.msg" "lambert3SG.gn" -na;
connectAttr "lambert3SG.msg" "materialInfo3.sg";
connectAttr "lambert3.msg" "materialInfo3.m";
connectAttr "group2ShapeOrig.w" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "groupParts1.og" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "groupParts2.og" "groupParts3.ig";
connectAttr "groupId3.id" "groupParts3.gi";
connectAttr "groupParts3.og" "groupParts7.ig";
connectAttr "groupId7.id" "groupParts7.gi";
connectAttr "skinCluster1GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "bindPose1.msg" "skinCluster1.bp";
connectAttr "Corgi_Root.wm" "skinCluster1.ma[0]";
connectAttr "Corgi_center.wm" "skinCluster1.ma[1]";
connectAttr "Corgi_Hips.wm" "skinCluster1.ma[2]";
connectAttr "Corgi_Leg_R.wm" "skinCluster1.ma[3]";
connectAttr "joint2.wm" "skinCluster1.ma[4]";
connectAttr "Corgi_Shin_R.wm" "skinCluster1.ma[5]";
connectAttr "Corgi_Foot_R.wm" "skinCluster1.ma[6]";
connectAttr "Corgi_Toes_R.wm" "skinCluster1.ma[7]";
connectAttr "Corgi_Leg_L.wm" "skinCluster1.ma[8]";
connectAttr "joint1.wm" "skinCluster1.ma[9]";
connectAttr "Corgi_Shin_L.wm" "skinCluster1.ma[10]";
connectAttr "Corgi_Foot_L.wm" "skinCluster1.ma[11]";
connectAttr "Corgi_Toes_L.wm" "skinCluster1.ma[12]";
connectAttr "Corgi_Shoulders.wm" "skinCluster1.ma[13]";
connectAttr "Corgi_Arm_R.wm" "skinCluster1.ma[14]";
connectAttr "Corgi_Forearm_R.wm" "skinCluster1.ma[15]";
connectAttr "Corgi_Hand_R.wm" "skinCluster1.ma[16]";
connectAttr "Corgi_Fingers_R.wm" "skinCluster1.ma[17]";
connectAttr "Corgi_Arm_L.wm" "skinCluster1.ma[18]";
connectAttr "Corgi_Forearm_L.wm" "skinCluster1.ma[19]";
connectAttr "Corgi_Hand_L.wm" "skinCluster1.ma[20]";
connectAttr "Corgi_Fingers_L.wm" "skinCluster1.ma[21]";
connectAttr "Corgi_head.wm" "skinCluster1.ma[22]";
connectAttr "Corgi_Hat.wm" "skinCluster1.ma[23]";
connectAttr "Corgi_Ear_R.wm" "skinCluster1.ma[24]";
connectAttr "Corgi_Eartip_R.wm" "skinCluster1.ma[25]";
connectAttr "Corgi_Ear_L.wm" "skinCluster1.ma[26]";
connectAttr "Corgi_Eartip_L.wm" "skinCluster1.ma[27]";
connectAttr "Corgi_lowerJaw.wm" "skinCluster1.ma[28]";
connectAttr "Corgi_Jawtip.wm" "skinCluster1.ma[29]";
connectAttr "Corgi_Tongue_root.wm" "skinCluster1.ma[30]";
connectAttr "Corgi_tongue1.wm" "skinCluster1.ma[31]";
connectAttr "Corgi_tongue2.wm" "skinCluster1.ma[32]";
connectAttr "Corgi_tongue3.wm" "skinCluster1.ma[33]";
connectAttr "Corgi_tongue4.wm" "skinCluster1.ma[34]";
connectAttr "Corgi_Tongue5.wm" "skinCluster1.ma[35]";
connectAttr "Corgi_Root.liw" "skinCluster1.lw[0]";
connectAttr "Corgi_center.liw" "skinCluster1.lw[1]";
connectAttr "Corgi_Hips.liw" "skinCluster1.lw[2]";
connectAttr "Corgi_Leg_R.liw" "skinCluster1.lw[3]";
connectAttr "joint2.liw" "skinCluster1.lw[4]";
connectAttr "Corgi_Shin_R.liw" "skinCluster1.lw[5]";
connectAttr "Corgi_Foot_R.liw" "skinCluster1.lw[6]";
connectAttr "Corgi_Toes_R.liw" "skinCluster1.lw[7]";
connectAttr "Corgi_Leg_L.liw" "skinCluster1.lw[8]";
connectAttr "joint1.liw" "skinCluster1.lw[9]";
connectAttr "Corgi_Shin_L.liw" "skinCluster1.lw[10]";
connectAttr "Corgi_Foot_L.liw" "skinCluster1.lw[11]";
connectAttr "Corgi_Toes_L.liw" "skinCluster1.lw[12]";
connectAttr "Corgi_Shoulders.liw" "skinCluster1.lw[13]";
connectAttr "Corgi_Arm_R.liw" "skinCluster1.lw[14]";
connectAttr "Corgi_Forearm_R.liw" "skinCluster1.lw[15]";
connectAttr "Corgi_Hand_R.liw" "skinCluster1.lw[16]";
connectAttr "Corgi_Fingers_R.liw" "skinCluster1.lw[17]";
connectAttr "Corgi_Arm_L.liw" "skinCluster1.lw[18]";
connectAttr "Corgi_Forearm_L.liw" "skinCluster1.lw[19]";
connectAttr "Corgi_Hand_L.liw" "skinCluster1.lw[20]";
connectAttr "Corgi_Fingers_L.liw" "skinCluster1.lw[21]";
connectAttr "Corgi_head.liw" "skinCluster1.lw[22]";
connectAttr "Corgi_Hat.liw" "skinCluster1.lw[23]";
connectAttr "Corgi_Ear_R.liw" "skinCluster1.lw[24]";
connectAttr "Corgi_Eartip_R.liw" "skinCluster1.lw[25]";
connectAttr "Corgi_Ear_L.liw" "skinCluster1.lw[26]";
connectAttr "Corgi_Eartip_L.liw" "skinCluster1.lw[27]";
connectAttr "Corgi_lowerJaw.liw" "skinCluster1.lw[28]";
connectAttr "Corgi_Jawtip.liw" "skinCluster1.lw[29]";
connectAttr "Corgi_Tongue_root.liw" "skinCluster1.lw[30]";
connectAttr "Corgi_tongue1.liw" "skinCluster1.lw[31]";
connectAttr "Corgi_tongue2.liw" "skinCluster1.lw[32]";
connectAttr "Corgi_tongue3.liw" "skinCluster1.lw[33]";
connectAttr "Corgi_tongue4.liw" "skinCluster1.lw[34]";
connectAttr "Corgi_Tongue5.liw" "skinCluster1.lw[35]";
connectAttr "Corgi_Root.obcc" "skinCluster1.ifcl[0]";
connectAttr "Corgi_center.obcc" "skinCluster1.ifcl[1]";
connectAttr "Corgi_Hips.obcc" "skinCluster1.ifcl[2]";
connectAttr "Corgi_Leg_R.obcc" "skinCluster1.ifcl[3]";
connectAttr "joint2.obcc" "skinCluster1.ifcl[4]";
connectAttr "Corgi_Shin_R.obcc" "skinCluster1.ifcl[5]";
connectAttr "Corgi_Foot_R.obcc" "skinCluster1.ifcl[6]";
connectAttr "Corgi_Toes_R.obcc" "skinCluster1.ifcl[7]";
connectAttr "Corgi_Leg_L.obcc" "skinCluster1.ifcl[8]";
connectAttr "joint1.obcc" "skinCluster1.ifcl[9]";
connectAttr "Corgi_Shin_L.obcc" "skinCluster1.ifcl[10]";
connectAttr "Corgi_Foot_L.obcc" "skinCluster1.ifcl[11]";
connectAttr "Corgi_Toes_L.obcc" "skinCluster1.ifcl[12]";
connectAttr "Corgi_Shoulders.obcc" "skinCluster1.ifcl[13]";
connectAttr "Corgi_Arm_R.obcc" "skinCluster1.ifcl[14]";
connectAttr "Corgi_Forearm_R.obcc" "skinCluster1.ifcl[15]";
connectAttr "Corgi_Hand_R.obcc" "skinCluster1.ifcl[16]";
connectAttr "Corgi_Fingers_R.obcc" "skinCluster1.ifcl[17]";
connectAttr "Corgi_Arm_L.obcc" "skinCluster1.ifcl[18]";
connectAttr "Corgi_Forearm_L.obcc" "skinCluster1.ifcl[19]";
connectAttr "Corgi_Hand_L.obcc" "skinCluster1.ifcl[20]";
connectAttr "Corgi_Fingers_L.obcc" "skinCluster1.ifcl[21]";
connectAttr "Corgi_head.obcc" "skinCluster1.ifcl[22]";
connectAttr "Corgi_Hat.obcc" "skinCluster1.ifcl[23]";
connectAttr "Corgi_Ear_R.obcc" "skinCluster1.ifcl[24]";
connectAttr "Corgi_Eartip_R.obcc" "skinCluster1.ifcl[25]";
connectAttr "Corgi_Ear_L.obcc" "skinCluster1.ifcl[26]";
connectAttr "Corgi_Eartip_L.obcc" "skinCluster1.ifcl[27]";
connectAttr "Corgi_lowerJaw.obcc" "skinCluster1.ifcl[28]";
connectAttr "Corgi_Jawtip.obcc" "skinCluster1.ifcl[29]";
connectAttr "Corgi_Tongue_root.obcc" "skinCluster1.ifcl[30]";
connectAttr "Corgi_tongue1.obcc" "skinCluster1.ifcl[31]";
connectAttr "Corgi_tongue2.obcc" "skinCluster1.ifcl[32]";
connectAttr "Corgi_tongue3.obcc" "skinCluster1.ifcl[33]";
connectAttr "Corgi_tongue4.obcc" "skinCluster1.ifcl[34]";
connectAttr "Corgi_Tongue5.obcc" "skinCluster1.ifcl[35]";
connectAttr "Corgi_Hat.msg" "skinCluster1.ptt";
connectAttr "groupParts9.og" "tweak1.ip[0].ig";
connectAttr "groupId9.id" "tweak1.ip[0].gi";
connectAttr "skinCluster1GroupId.msg" "skinCluster1Set.gn" -na;
connectAttr "group2Shape.iog.og[15]" "skinCluster1Set.dsm" -na;
connectAttr "skinCluster1.msg" "skinCluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "groupId9.msg" "tweakSet1.gn" -na;
connectAttr "group2Shape.iog.og[16]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "groupParts7.og" "groupParts9.ig";
connectAttr "groupId9.id" "groupParts9.gi";
connectAttr "Corgi_Root.msg" "bindPose1.m[0]";
connectAttr "Corgi_center.msg" "bindPose1.m[1]";
connectAttr "Corgi_Hips.msg" "bindPose1.m[2]";
connectAttr "Corgi_Leg_R.msg" "bindPose1.m[3]";
connectAttr "joint2.msg" "bindPose1.m[4]";
connectAttr "Corgi_Shin_R.msg" "bindPose1.m[5]";
connectAttr "Corgi_Foot_R.msg" "bindPose1.m[6]";
connectAttr "Corgi_Toes_R.msg" "bindPose1.m[7]";
connectAttr "Corgi_Leg_L.msg" "bindPose1.m[8]";
connectAttr "joint1.msg" "bindPose1.m[9]";
connectAttr "Corgi_Shin_L.msg" "bindPose1.m[10]";
connectAttr "Corgi_Foot_L.msg" "bindPose1.m[11]";
connectAttr "Corgi_Toes_L.msg" "bindPose1.m[12]";
connectAttr "Corgi_Shoulders.msg" "bindPose1.m[13]";
connectAttr "Corgi_Arm_R.msg" "bindPose1.m[14]";
connectAttr "Corgi_Forearm_R.msg" "bindPose1.m[15]";
connectAttr "Corgi_Hand_R.msg" "bindPose1.m[16]";
connectAttr "Corgi_Fingers_R.msg" "bindPose1.m[17]";
connectAttr "Corgi_Arm_L.msg" "bindPose1.m[18]";
connectAttr "Corgi_Forearm_L.msg" "bindPose1.m[19]";
connectAttr "Corgi_Hand_L.msg" "bindPose1.m[20]";
connectAttr "Corgi_Fingers_L.msg" "bindPose1.m[21]";
connectAttr "Corgi_head.msg" "bindPose1.m[22]";
connectAttr "Corgi_Hat.msg" "bindPose1.m[23]";
connectAttr "Corgi_Ear_R.msg" "bindPose1.m[24]";
connectAttr "Corgi_Eartip_R.msg" "bindPose1.m[25]";
connectAttr "Corgi_Ear_L.msg" "bindPose1.m[26]";
connectAttr "Corgi_Eartip_L.msg" "bindPose1.m[27]";
connectAttr "Corgi_lowerJaw.msg" "bindPose1.m[28]";
connectAttr "Corgi_Jawtip.msg" "bindPose1.m[29]";
connectAttr "Corgi_Tongue_root.msg" "bindPose1.m[30]";
connectAttr "Corgi_tongue1.msg" "bindPose1.m[31]";
connectAttr "Corgi_tongue2.msg" "bindPose1.m[32]";
connectAttr "Corgi_tongue3.msg" "bindPose1.m[33]";
connectAttr "Corgi_tongue4.msg" "bindPose1.m[34]";
connectAttr "Corgi_Tongue5.msg" "bindPose1.m[35]";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[2]" "bindPose1.p[3]";
connectAttr "bindPose1.m[3]" "bindPose1.p[4]";
connectAttr "bindPose1.m[4]" "bindPose1.p[5]";
connectAttr "bindPose1.m[5]" "bindPose1.p[6]";
connectAttr "bindPose1.m[6]" "bindPose1.p[7]";
connectAttr "bindPose1.m[2]" "bindPose1.p[8]";
connectAttr "bindPose1.m[8]" "bindPose1.p[9]";
connectAttr "bindPose1.m[9]" "bindPose1.p[10]";
connectAttr "bindPose1.m[10]" "bindPose1.p[11]";
connectAttr "bindPose1.m[11]" "bindPose1.p[12]";
connectAttr "bindPose1.m[1]" "bindPose1.p[13]";
connectAttr "bindPose1.m[13]" "bindPose1.p[14]";
connectAttr "bindPose1.m[14]" "bindPose1.p[15]";
connectAttr "bindPose1.m[15]" "bindPose1.p[16]";
connectAttr "bindPose1.m[16]" "bindPose1.p[17]";
connectAttr "bindPose1.m[13]" "bindPose1.p[18]";
connectAttr "bindPose1.m[18]" "bindPose1.p[19]";
connectAttr "bindPose1.m[19]" "bindPose1.p[20]";
connectAttr "bindPose1.m[20]" "bindPose1.p[21]";
connectAttr "bindPose1.m[13]" "bindPose1.p[22]";
connectAttr "bindPose1.m[22]" "bindPose1.p[23]";
connectAttr "bindPose1.m[22]" "bindPose1.p[24]";
connectAttr "bindPose1.m[24]" "bindPose1.p[25]";
connectAttr "bindPose1.m[22]" "bindPose1.p[26]";
connectAttr "bindPose1.m[26]" "bindPose1.p[27]";
connectAttr "bindPose1.m[22]" "bindPose1.p[28]";
connectAttr "bindPose1.m[28]" "bindPose1.p[29]";
connectAttr "bindPose1.m[22]" "bindPose1.p[30]";
connectAttr "bindPose1.m[30]" "bindPose1.p[31]";
connectAttr "bindPose1.m[31]" "bindPose1.p[32]";
connectAttr "bindPose1.m[32]" "bindPose1.p[33]";
connectAttr "bindPose1.m[33]" "bindPose1.p[34]";
connectAttr "bindPose1.m[34]" "bindPose1.p[35]";
connectAttr "Corgi_Root.bps" "bindPose1.wm[0]";
connectAttr "Corgi_center.bps" "bindPose1.wm[1]";
connectAttr "Corgi_Hips.bps" "bindPose1.wm[2]";
connectAttr "Corgi_Leg_R.bps" "bindPose1.wm[3]";
connectAttr "joint2.bps" "bindPose1.wm[4]";
connectAttr "Corgi_Shin_R.bps" "bindPose1.wm[5]";
connectAttr "Corgi_Foot_R.bps" "bindPose1.wm[6]";
connectAttr "Corgi_Toes_R.bps" "bindPose1.wm[7]";
connectAttr "Corgi_Leg_L.bps" "bindPose1.wm[8]";
connectAttr "joint1.bps" "bindPose1.wm[9]";
connectAttr "Corgi_Shin_L.bps" "bindPose1.wm[10]";
connectAttr "Corgi_Foot_L.bps" "bindPose1.wm[11]";
connectAttr "Corgi_Toes_L.bps" "bindPose1.wm[12]";
connectAttr "Corgi_Shoulders.bps" "bindPose1.wm[13]";
connectAttr "Corgi_Arm_R.bps" "bindPose1.wm[14]";
connectAttr "Corgi_Forearm_R.bps" "bindPose1.wm[15]";
connectAttr "Corgi_Hand_R.bps" "bindPose1.wm[16]";
connectAttr "Corgi_Fingers_R.bps" "bindPose1.wm[17]";
connectAttr "Corgi_Arm_L.bps" "bindPose1.wm[18]";
connectAttr "Corgi_Forearm_L.bps" "bindPose1.wm[19]";
connectAttr "Corgi_Hand_L.bps" "bindPose1.wm[20]";
connectAttr "Corgi_Fingers_L.bps" "bindPose1.wm[21]";
connectAttr "Corgi_head.bps" "bindPose1.wm[22]";
connectAttr "Corgi_Hat.bps" "bindPose1.wm[23]";
connectAttr "Corgi_Ear_R.bps" "bindPose1.wm[24]";
connectAttr "Corgi_Eartip_R.bps" "bindPose1.wm[25]";
connectAttr "Corgi_Ear_L.bps" "bindPose1.wm[26]";
connectAttr "Corgi_Eartip_L.bps" "bindPose1.wm[27]";
connectAttr "Corgi_lowerJaw.bps" "bindPose1.wm[28]";
connectAttr "Corgi_Jawtip.bps" "bindPose1.wm[29]";
connectAttr "Corgi_Tongue_root.bps" "bindPose1.wm[30]";
connectAttr "Corgi_tongue1.bps" "bindPose1.wm[31]";
connectAttr "Corgi_tongue2.bps" "bindPose1.wm[32]";
connectAttr "Corgi_tongue3.bps" "bindPose1.wm[33]";
connectAttr "Corgi_tongue4.bps" "bindPose1.wm[34]";
connectAttr "Corgi_Tongue5.bps" "bindPose1.wm[35]";
connectAttr "blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "blinn1.msg" ":defaultShaderList1.s" -na;
connectAttr "lambert2.msg" ":defaultShaderList1.s" -na;
connectAttr "lambert3.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "group2Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
// End of NewTugAnim.ma
