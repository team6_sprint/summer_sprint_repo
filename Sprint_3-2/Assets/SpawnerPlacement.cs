﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SpawnerPlacement : MonoBehaviour {

    public List<GameObject> leftSpawn;
    //List<GameObject> leftSpawnTemp = leftSpawn;
    public List<GameObject> rightSpawn;
    GameObject leftSpawner;
    GameObject rightSpawner;
    public GameObject MasterObject;

    // Use this for initialization
    void Start () {
        
        leftSpawner = new GameObject("LeftSpawner");
        rightSpawner = new GameObject("rightSpawner");
        float NVZ = 0;
        float NVX = 3;
        float NVY = 3;
        for (int i = 0; i<1; i++)
        {
            NVX = 3;
            for (int i2 = 0; i2 < 10; i2++)
            {
                
                GameObject cloneL;
               cloneL = Instantiate(leftSpawner, new Vector3(NVX, NVY, NVZ), Quaternion.Euler(0,-90,0));
                cloneL.tag = "LeftSpawner";
                leftSpawn.Add(cloneL);
                cloneL.transform.parent = MasterObject.transform;
                GameObject cloneR;
                cloneR = Instantiate(leftSpawner, new Vector3(-NVX, NVY, NVZ), Quaternion.Euler(0,90,0));
                cloneR.tag = "RightSpawner";
                rightSpawn.Add(cloneR);
                cloneR.transform.parent = MasterObject.transform;
                NVX += 1.25f;
            }
            NVZ += 2;
            NVX = 3.1f;
            for (int i2 = 0; i2 < 10; i2++)
            {
                GameObject cloneL;
                cloneL = Instantiate(leftSpawner, new Vector3(NVX, NVY, NVZ), Quaternion.Euler(0, -90, 0));
                cloneL.tag = "LeftSpawner";
                leftSpawn.Add(cloneL);
                cloneL.transform.parent = MasterObject.transform;
                GameObject cloneR;
                cloneR = Instantiate(leftSpawner, new Vector3(-NVX, NVY, NVZ), Quaternion.Euler(0, 90, 0));
                cloneR.tag = "RightSpawner";
                rightSpawn.Add(cloneR);
                cloneR.transform.parent = MasterObject.transform;
                NVX += 1.25f;
            }
            NVZ += 2.25f;
            NVX = 3.2f;
            for (int i2 = 0; i2 < 10; i2++)
            {
                GameObject cloneL;
                cloneL = Instantiate(leftSpawner, new Vector3(NVX, NVY, NVZ), Quaternion.Euler(0, -90, 0));
                cloneL.tag = "LeftSpawner";
                leftSpawn.Add(cloneL);
                cloneL.transform.parent = MasterObject.transform;
                GameObject cloneR;
                cloneR = Instantiate(leftSpawner, new Vector3(-NVX, NVY, NVZ), Quaternion.Euler(0, 90, 0));
                cloneR.tag = "RightSpawner";
                rightSpawn.Add(cloneR);
                cloneR.transform.parent = MasterObject.transform;
                NVX += 1.3f;
            }
            NVZ += 2.5f;
            NVX = 3.3f;
            for (int i2 = 0; i2 < 10; i2++)
            {
                GameObject cloneL;
                cloneL = Instantiate(leftSpawner, new Vector3(NVX, NVY, NVZ), Quaternion.Euler(0, -90, 0));
                cloneL.tag = "LeftSpawner";
                leftSpawn.Add(cloneL);
                cloneL.transform.parent = MasterObject.transform;
                GameObject cloneR;
                cloneR = Instantiate(leftSpawner, new Vector3(-NVX, NVY, NVZ), Quaternion.Euler(0, 90, 0));
                cloneR.tag = "RightSpawner";
                rightSpawn.Add(cloneR);
                cloneR.transform.parent = MasterObject.transform;
                NVX += 1.33f;
            }
            NVZ += 2.75f;
            NVX = 3.4f;
            for (int i2 = 0; i2 < 10; i2++)
            {
                GameObject cloneL;
                cloneL = Instantiate(leftSpawner, new Vector3(NVX, NVY, NVZ), Quaternion.Euler(0, -90, 0));
                cloneL.tag = "LeftSpawner";
                leftSpawn.Add(cloneL);
                cloneL.transform.parent = MasterObject.transform;
                GameObject cloneR;
                cloneR = Instantiate(leftSpawner, new Vector3(-NVX, NVY, NVZ), Quaternion.Euler(0, 90, 0));
                cloneR.tag = "RightSpawner";
                rightSpawn.Add(cloneR);
                cloneR.transform.parent = MasterObject.transform;
                NVX += 1.4f;
            }
            NVZ += 3.5f;
            NVX = 3.5f;
            
            //NVX = 1;
            print("NVZ= "+NVZ + "NVY = "+ NVY +  "NVX= "+NVX);
        }

        // GameObject.FindGameObjectsWithTag("LeftSpawner")
        //BuildPipeline.BuildAssetBundles("C:/Users/willtrav/Documents/summer_sprint_repo/Sprint_3-2/Assets/Resources", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows64);
        //System.IO.File.WriteAllText("C:/Users/willtrav/Documents/summer_sprint_repo/Sprint_3-2/Assets/Resources", .ToString());
        print(leftSpawn.Count);
       // GameObject[] rightSpawn;
    }

	// Update is called once per frame
	void Update () {
		
	}
}
