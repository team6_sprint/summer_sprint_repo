﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyHFTScript : MonoBehaviour {
    bool left = true;
    int leftcount;
    int rightcount;
    public int Lindex;
    public int Rindex;
    public Material corg1;
    public Material corg2;
    CorgiScript corgscrip;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    void PlaceOnTeams(GameObject newPlayer) {
        //print("Managing Player Spawning");
        //print(newPlayer.name);
       
        GameObject[] lefts = GameObject.FindGameObjectsWithTag("Left");
        //print(lefts.Length);

        GameObject[] rights = GameObject.FindGameObjectsWithTag("Right");
        //print(rights.Length);

        if (rights.Length < lefts.Length)
        {
            newPlayer.tag = "Right";
            Renderer newPlayerRend;
            newPlayerRend = newPlayer.GetComponentInChildren<Renderer>();
            newPlayerRend.enabled = true;
            newPlayerRend.material = corg1;

            
            left = !left;
        }else { newPlayer.tag = "Left";
            Renderer newPlayerRend;
            newPlayerRend = newPlayer.GetComponentInChildren<Renderer>();
            newPlayerRend.enabled = true;
            newPlayerRend.material = corg2;

            left = !left;
        }

        
        if (newPlayer.tag == "Left")
        {
            newPlayer.transform.position = SprintLevelSettings.Instance.Spawner.leftSpawn[Lindex].transform.position;
            
            newPlayer.transform.parent = SprintLevelSettings.Instance.Spawner.leftSpawn[Lindex].transform;
            newPlayer.transform.localRotation = new Quaternion(0, 0, 0, 0);
            //CorgiScript.team1 = false;

            Lindex++;
        }

        if (newPlayer.tag == "Right")
        {
            newPlayer.transform.position = SprintLevelSettings.Instance.Spawner.rightSpawn[Rindex].transform.position;
            
            newPlayer.transform.parent = SprintLevelSettings.Instance.Spawner.rightSpawn[Rindex].transform;
            newPlayer.transform.localRotation = new Quaternion(0, 0, 0, 0);
            // CorgiScript.team1 = true;
            Rindex++;
        }

    }

}
