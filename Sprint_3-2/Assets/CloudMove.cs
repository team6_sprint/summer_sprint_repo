﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMove : MonoBehaviour {
    Vector3 speed;


	// Use this for initialization
	void Start () {
		speed= new Vector3(Random.Range(.1f, .75f) * Time.deltaTime, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
        transform.position += speed;

        if (transform.position.x > 25)
        {
            transform.position = new Vector3(-20, Random.Range(10.5f, 15f), transform.position.z);
        }
	}
}
