﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPlayerControlScript : MonoBehaviour {
    public bool redPressed;
    public bool bluePressed;
    private SprintLevelSettings levelSettings;
    public bool deleteWhenDisconnected = true;
    public Transform nameTransform;

    //HFT Stuff
    public Color baseColor;
    private Material m_material;
    private HFTGamepad m_gamepad;
    private HFTInput m_hftInput;
    private GUIStyle m_guiStyle = new GUIStyle();
    private GUIContent m_guiName = new GUIContent("");
    private Rect m_nameRect = new Rect(0, 0, 0, 0);
    private string m_playerName;
    public int playnum;
    [HideInInspector]
    public static int m_playerNumber = 0;



    // Use this for initialization
    void Start () {
        //Copied from HFT BirdScript
        m_material = GetComponent<Renderer>().material;
        m_gamepad = GetComponent<HFTGamepad>();
        m_hftInput = GetComponent<HFTInput>();
        SetColor(m_playerNumber++);
        SetName(m_gamepad.Name);

        // Notify us if the name changes.
        m_gamepad.OnNameChange += ChangeName;

        // Delete ourselves if disconnected
        m_gamepad.OnDisconnect += Remove;

        //Gets Sprint Level Settings script
        levelSettings = GameObject.FindGameObjectWithTag("GameController").GetComponent<SprintLevelSettings>();
    }

    void Remove()
    {
        if (deleteWhenDisconnected)
        {
            Destroy(gameObject);

        }
    }

    // Update is called once per frame
    void Update () {
      redPressed = m_hftInput.GetButtonDown("fire1") || Input.GetKeyDown("space");
      bluePressed = m_hftInput.GetButtonDown("fire2") || Input.GetKeyDown("return");
    }

    void SetName(string name)
    {
        m_playerName = name;
        gameObject.name = "Player-" + m_playerName;
        m_guiName = new GUIContent(m_playerName);
        Vector2 size = m_guiStyle.CalcSize(m_guiName);
        //Sets size of the text box
        m_nameRect.width = size.x + 12;
        m_nameRect.height = size.y + 5;
    }

    void SetColor(int colorNdx)
    {
        // Pick a color
        float hueAdjust = (((colorNdx & 0x01) << 5) |
                           ((colorNdx & 0x02) << 3) |
                           ((colorNdx & 0x04) << 1) |
                           ((colorNdx & 0x08) >> 1) |
                           ((colorNdx & 0x10) >> 3) |
                           ((colorNdx & 0x20) >> 5)) / 64.0f;
        float valueAdjust = (colorNdx & 0x20) != 0 ? -0.5f : 0.0f;
        float satAdjust = (colorNdx & 0x10) != 0 ? -0.5f : 0.0f;

        // get the hsva for the baseColor
        Vector4 hsva = HFTColorUtils.ColorToHSVA(baseColor);

        // adjust that base color by the amount we picked
        hsva.x += hueAdjust;
        hsva.y += satAdjust;
        hsva.z += valueAdjust;

        // now get the adjusted color.
        Color playerColor = HFTColorUtils.HSVAToColor(hsva);

        // Tell the gamepad to change color
        m_gamepad.color = playerColor;

        // Create a 1 pixel texture for the OnGUI code to draw the label behind name
        Color[] pix = new Color[1];
        pix[0] = playerColor;
        Texture2D tex = new Texture2D(1, 1);
        tex.SetPixels(pix);
        tex.Apply();
        m_guiStyle.normal.background = tex;

        // Set the HSVA material of the character to the color adjustments.
        m_material.SetVector("_HSVAAdjust", new Vector4(hueAdjust, satAdjust, valueAdjust, 0.0f));
    }

    void OnGUI()
    {
        Vector2 size = m_guiStyle.CalcSize(m_guiName);
        Vector3 coords = Camera.main.WorldToScreenPoint(nameTransform.position);
        m_nameRect.x = coords.x - size.x * 0.5f - 5f;
        m_nameRect.y = Screen.height - coords.y;
        m_guiStyle.normal.textColor = Color.black;
        m_guiStyle.contentOffset = new Vector2(4, 2);
        GUI.Box(m_nameRect, m_playerName, m_guiStyle);
    }

    // Called when the user changes their name.
    void ChangeName(object sender, System.EventArgs e)
    {
        SetName(m_gamepad.Name);
    }
}
